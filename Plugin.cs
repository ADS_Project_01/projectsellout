using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AutoCount.PlugIn;
using AutoCount.Data;
using AutoCount.Authentication;
using System.Windows.Forms;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
namespace ProjectSellOut
{

    /// <summary>
    /// Summary description for PlugInsInitializer.
    /// </summary>
    public class Plugin : AutoCount.PlugIn.BasePlugIn
    {
        //protected UserAuthentication myUserAuthentication;
        public Plugin() : base(new Guid("12D34B4A-692D-46DE-8D7D-B3520D4CA679"),
        "Export Sell Out for AC 2.0", new AssemblyInfoEntity().AssemblyVersion) //: base(new Guid("11215689-C88A-4E8E-7529-784750166A03"), "Export Sell Out for AC 2.0", new AssemblyInfoEntity().AssemblyVersion)
        {
            SetManufacturer("PT AUTOCOUNT INDONESIA");
            SetManufacturerUrl("www.autocountindo.com");
            SetCopyright("PT AUTOCOUNT INDONESIA");
            SetSalesPhone("081288490055");
            SetSupportPhone("081288490055");
            //Minimum AutoCount Accounting version is required
            SetMinimumAccountingVersionRequired("2.0.0.55");
            SetDevExpressComponentVersionRequired("19.2.7");
            //AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;

            //Set this Plug-In is free
            SetIsFreeLicense(true);
        }

        //private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        //{
        //    using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("DevExpress.Spreadsheet.v19.2.Core.dll"))
        //    {
        //        byte[] assemblyData = new byte[stream.Length];
        //        stream.Read(assemblyData, 0, assemblyData.Length);
        //        return Assembly.Load(assemblyData);
        //    }

        //    //Action<Type> noop = _ => { };
        //    //var lib1 = typeof(DocumentFormat.OpenXml.OpenXmlAttribute);
        //    //noop(lib1);
        //    // noop(lib2);
        //    // noop(lib3);
        //}

        public static bool IsValidDatabase(DBSetting dbSetting)
        {
            object obj = dbSetting.ExecuteScalar("select count(*) from dbo.sysobjects where id = object_id(N'[dbo].[SO_Setting]') and OBJECTPROPERTY(id, N'IsUserTable') = 1");
            if (obj != null && obj != DBNull.Value)
                return System.Convert.ToInt32(obj) > 0;
            else
                return false;
        }
        //public override void AfterUnload(AutoCount.PlugIn.BaseArgs e)
        //    AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.BGT.ToString());
        //{
        //    AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.BGT_GEN.ToString());
        //    //AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.BGT_GEN_MACHINE.ToString());
        //    //AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.BGT_GEN_MACHINE_DELETE.ToString());
        //    //AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.BGT_GEN_MACHINE_EDIT.ToString());
        //    //AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.BGT_GEN_MACHINE_NEW.ToString());
        //   // AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.BGT_GEN_MACHINE_OPEN.ToString());
        //   // AutoCount.Authentication.AccessRightMap.RemoveAccessRightRecord(AccessRightConst.BGT_GEN_MACHINE_SHOW.ToString());

        //}

        public override bool BeforeLoad(AutoCount.PlugIn.BeforeLoadArgs e)
        {
            //e.CompanyName="";
            if (!IsValidDatabase(e.DBSetting))
            {
                if (!AutoCount.AppMessage.ShowConfirmMessage("Do you want to install Budget plug -in to this database ?"))
                    return false;
                if (!AutoCount.AppMessage.ShowConfirmMessage("Do you really want to install Budget plug -in to this database ?\n(Note: Once installed, you cannot remove it.)"))
                    return false;
                AutoCount.Data.DBUtils dbUtils = AutoCount.Data.DBUtils.Create(e.DBSetting);
                dbUtils.ExecuteDDL(Path.Combine(e.PlugInFolder, "ExportSellOut.SQ$"));
            }

            e.MainMenuCaption = "Export Sell Out";

            //e.AccessRightFilename = "Budget.acd";
            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
    new AutoCount.Authentication.AccessRightRecord(AccessRightConst.SO.ToString(),"PlugIns", "Export Sell Out PlugIns"));
            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
    new AutoCount.Authentication.AccessRightRecord(AccessRightConst.SO_GEN.ToString(), AccessRightConst.SO.ToString(), "General Maintenance"));

            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
  new AutoCount.Authentication.AccessRightRecord(AccessRightConst.SO_SETTING.ToString(), AccessRightConst.SO_GEN.ToString(), "General Settings"));
            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new AutoCount.Authentication.AccessRightRecord(AccessRightConst.SO_SETTING_SHOW.ToString(), AccessRightConst.SO_SETTING.ToString(), "Show General Settings"));
            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new AutoCount.Authentication.AccessRightRecord(AccessRightConst.SO_SETTING_OPEN.ToString(), AccessRightConst.SO_SETTING.ToString(), "Open General Settings"));

            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
 new AutoCount.Authentication.AccessRightRecord(AccessRightConst.SO_EXPORT.ToString(), AccessRightConst.SO.ToString(), "Export Sell Out"));
            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new AutoCount.Authentication.AccessRightRecord(AccessRightConst.SO_EXPORT_SHOW.ToString(), AccessRightConst.SO_EXPORT.ToString(), "Show Export Sell Out"));
            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
new AutoCount.Authentication.AccessRightRecord(AccessRightConst.SO_EXPORT_OPEN.ToString(), AccessRightConst.SO_EXPORT.ToString(), "Open Export Sell Out"));


            ////            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            ////   new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_TOOLS.ToString(), AccessRightConst.BGT.ToString(), "Tools", AutoCount.Controller.ModuleOptions.None));
            ////            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            ////new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_TOOLS_RECALCULATE.ToString(), AccessRightConst.BGT_TOOLS.ToString(), "Recalculate Stock Costing", AutoCount.Controller.ModuleOptions.None));
            ////            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            ////new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_TOOLS_RECALCULATE_SHOW.ToString(), AccessRightConst.BGT_TOOLS_RECALCULATE.ToString(), "Show Recalculate Stock Costing", AutoCount.Controller.ModuleOptions.None));
            ////            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            ////new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_TOOLS_RECALCULATE_OPEN.ToString(), AccessRightConst.BGT_TOOLS_RECALCULATE.ToString(), "Open Recalculate Stock Costing", AutoCount.Controller.ModuleOptions.None));

            ////            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            ////new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_REPORTS.ToString(), AccessRightConst.BGT.ToString(), "Reports", AutoCount.Controller.ModuleOptions.None));
            ////            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            ////new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_REPORTS_STOCKCARDWIP.ToString(), AccessRightConst.BGT_REPORTS.ToString(), "", AutoCount.Controller.ModuleOptions.None));
            ////            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            ////new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_REPORTS_STOCKCARDWIP_SHOW.ToString(), AccessRightConst.BGT_REPORTS_STOCKCARDWIP.ToString(), "Show Stock Card WIP Report", AutoCount.Controller.ModuleOptions.None));
            ////            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            ////new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_REPORTS_STOCKCARDWIP_OPEN.ToString(), AccessRightConst.BGT_REPORTS_STOCKCARDWIP.ToString(), "Open Stock Card WIP Report", AutoCount.Controller.ModuleOptions.None));




            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_GEN_ACTIVITY.ToString(), AccessRightConst.BGT_GEN.ToString(), "Activity Maintenance", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_GEN_ACTIVITY_SHOW.ToString(), AccessRightConst.BGT_GEN_ACTIVITY.ToString(), "Show Activity", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_GEN_ACTIVITY_OPEN.ToString(), AccessRightConst.BGT_GEN_ACTIVITY.ToString(), "Open Activity", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_GEN_ACTIVITY_NEW.ToString(), AccessRightConst.BGT_GEN_ACTIVITY.ToString(), "New Activity", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_GEN_ACTIVITY_EDIT.ToString(), AccessRightConst.BGT_GEN_ACTIVITY.ToString(), "Edit Activity", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_GEN_ACTIVITY_VIEW.ToString(), AccessRightConst.BGT_GEN_ACTIVITY.ToString(), "View Activity", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_GEN_ACTIVITY_DELETE.ToString(), AccessRightConst.BGT_GEN_ACTIVITY.ToString(), "Delete Activity", AutoCount.Controller.ModuleOptions.None));


            //            /// Access Right Budget
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET.ToString(), AccessRightConst.BGT.ToString(), "Budget", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_SHOW.ToString(), AccessRightConst.BGT_BUDGET.ToString(), "Show Budget", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_OPEN.ToString(), AccessRightConst.BGT_BUDGET.ToString(), "Open Budget", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_NEW.ToString(), AccessRightConst.BGT_BUDGET.ToString(), "New Budget", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_EDIT.ToString(), AccessRightConst.BGT_BUDGET.ToString(), "Edit Budget", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_VIEW.ToString(), AccessRightConst.BGT_BUDGET.ToString(), "View Budget", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_DELETE.ToString(), AccessRightConst.BGT_BUDGET.ToString(), "Delete Budget", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_CANCEL.ToString(), AccessRightConst.BGT_BUDGET.ToString(), "Cancel/Uncancel Budget", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_DOC_REPORT.ToString(), AccessRightConst.BGT_BUDGET.ToString(), "Budget Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_DOC_REPORT_PREVIEW.ToString(), AccessRightConst.BGT_BUDGET_DOC_REPORT.ToString(), "Preview Budget Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_DOC_REPORT_PRINT.ToString(), AccessRightConst.BGT_BUDGET_DOC_REPORT.ToString(), "Print Budget Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_DOC_REPORT_EXPORT.ToString(), AccessRightConst.BGT_BUDGET_DOC_REPORT.ToString(), "Export Budget Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //          new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_LISTING_REPORT.ToString(), AccessRightConst.BGT_BUDGET.ToString(), "Budget Listing Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_LISTING_REPORT_SHOW.ToString(), AccessRightConst.BGT_BUDGET_LISTING_REPORT.ToString(), "Show Budget Listing Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_LISTING_REPORT_OPEN.ToString(), AccessRightConst.BGT_BUDGET_LISTING_REPORT.ToString(), "Open Budget Listing Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_LISTING_REPORT_PREVIEW.ToString(), AccessRightConst.BGT_BUDGET_LISTING_REPORT.ToString(), "Preview Budget Listing Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //          new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_LISTING_REPORT_PRINT.ToString(), AccessRightConst.BGT_BUDGET_LISTING_REPORT.ToString(), "Print Budget Listing Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_LISTING_REPORT_EXPORT.ToString(), AccessRightConst.BGT_BUDGET_LISTING_REPORT.ToString(), "Export Budget Listing Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //          new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_DTLLIST_REPORT.ToString(), AccessRightConst.BGT_BUDGET.ToString(), "Budget Detail Listing Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_DTLLIST_REPORT_SHOW.ToString(), AccessRightConst.BGT_BUDGET_DTLLIST_REPORT.ToString(), "Show Budget Detail Listing Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_DTLLIST_REPORT_OPEN.ToString(), AccessRightConst.BGT_BUDGET_DTLLIST_REPORT.ToString(), "Open Budget Detail Listing Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_DTLLIST_REPORT_PREVIEW.ToString(), AccessRightConst.BGT_BUDGET_DTLLIST_REPORT.ToString(), "Preview Budget Detail Listing Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //          new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_DTLLIST_REPORT_PRINT.ToString(), AccessRightConst.BGT_BUDGET_DTLLIST_REPORT.ToString(), "Print Budget Detail Listing Report", AutoCount.Controller.ModuleOptions.None));



            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_PRINTED.ToString(), AccessRightConst.BGT_BUDGET.ToString(), "Printed Budget", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_PRINTED_CANCEL.ToString(), AccessRightConst.BGT_BUDGET_PRINTED.ToString(), "Can Cancel/Uncancel Printed Budget", AutoCount.Controller.ModuleOptions.None));

            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_PRINTED_EDIT.ToString(), AccessRightConst.BGT_BUDGET_PRINTED.ToString(), "Can Edit Printed Budget", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_PRINTED_DELETE.ToString(), AccessRightConst.BGT_BUDGET_PRINTED.ToString(), "Can Delete Printed Budget", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_PRINTED_PREVIEW.ToString(), AccessRightConst.BGT_BUDGET_PRINTED.ToString(), "Can Preview Printed Budget", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_PRINTED_PRINT.ToString(), AccessRightConst.BGT_BUDGET_PRINTED.ToString(), "Can Print Printed Budget", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //           new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_PRINTED_EXPORT.ToString(), AccessRightConst.BGT_BUDGET_PRINTED.ToString(), "Can Export Printed Budget", AutoCount.Controller.ModuleOptions.None));

            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //           new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_ACTUALLST_REPORT.ToString(), AccessRightConst.BGT.ToString(), "Budget Actual Listing Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_ACTUALLST_REPORT_SHOW.ToString(), AccessRightConst.BGT_BUDGET_ACTUALLST_REPORT.ToString(), "Show Budget Actual Listing Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_ACTUALLST_REPORT_OPEN.ToString(), AccessRightConst.BGT_BUDGET_ACTUALLST_REPORT.ToString(), "Open Budget Actual Listing Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_ACTUALLST_REPORT_PREVIEW.ToString(), AccessRightConst.BGT_BUDGET_ACTUALLST_REPORT.ToString(), "Preview Budget Actual Listing Report", AutoCount.Controller.ModuleOptions.None));
            //            AutoCount.Authentication.AccessRightMap.AddAccessRightRecord(
            //          new AutoCount.Authentication.AccessRightRecord(AccessRightConst.BGT_BUDGET_ACTUALLST_REPORT_PRINT.ToString(), AccessRightConst.BGT_BUDGET_ACTUALLST_REPORT.ToString(), "Print Budget Actual Listing Report", AutoCount.Controller.ModuleOptions.None));


            //////////////////////



            //////////////////////
            // AutoCount.Report.AutoCountReport.RegisterReportType(
            //FormDealerListing.ReportType, typeof(DealerListingReportTypeHandler));
            base.BeforeLoad(e);
            return true;
        }
        //Guid myGuid = Guid.NewGuid();
        //[AutoCount.PlugIn.BasePlugIn(Guid.NewGuid,"","")]
        //AutoCount.PlugIn.BasePlugInAttribute(myGuid,"BGT System","1.0.0");
        ////[AutoCount.PlugIns.PlugInsInitializer("AutoCount Accounting Sample Plug-Ins", "Sample")]
        //public static bool Initialize(AutoCount.PlugIn.BeforeLoadArgs e)
        //{
        //    //e.DBSetting = AutoCount.Application.DBSetting;
        //    if (!IsValidDatabase(e.DBSetting))
        //    {
        //        if (!BCE.Application.AppMessage.ShowConfirmMessage("Do you want to install BGT System to this database?"))
        //            return false;

        //        if (!BCE.Application.AppMessage.ShowConfirmMessage("Do you really want to install BGT System to this database?\n(Note: Once installed, you cannot remove it.)"))
        //            return false;

        //        BCE.Data.DBUtils dbUtils = BCE.Data.DBUtils.Create(e.DBSetting);

        //        dbUtils.ExecuteDDL(Path.Combine(e.PlugInFolder, "Budget.SQ$"));
        //    }

        //    e.MainMenuCaption = "BGT System";
        //    e.AccessRightFilename = "Budget.acd";

        //    //AutoCount.Report.AutoCountReport.RegisterReportType(FormDealerListing.ReportType, typeof(DealerListingReportTypeHandler));

        //    return true;
        //}
        //public override void AfterLoad(AutoCount.PlugIn.BaseArgs e)
        //{
        //    BCE.Application.AppMessage.ShowMessage(string.Format("Account book {0}\nhas loaded BGT System Plugin", e.CompanyName));
        //    base.AfterLoad(e);
        //}

    }
}
