
ALTER FUNCTION [dbo].[F_BGT_GetAvailableBudget] (@ActivityCode nvarchar(50),@ProjNo nvarchar(20),@CreditorCode nvarchar(12))
RETURNS DECIMAL(19,2)
AS BEGIN
Declare @CurrencyCode nvarchar(20)
Declare @BudgetAmt decimal(19,2)
Declare @APInvoiceAmt decimal(19,2)
Declare @APCreditNoteAmt decimal(19,2)
Declare @BalanceAmt decimal(19,2)
Declare @Rate decimal(19,2)
BEGIN
--(select BankBuyRate from currrate where currencyCode=@CurrencyCode and a.DocDate between FromDate and ToDate)
select @BudgetAmt=sum(Amount),@CurrencyCode=CurrencyCode from BGT_Budget a  with(nolock) inner join BGT_BudgetDtl b  with(nolock) on a.DocKey=b.DocKey
where a.Cancelled='F' and a.CreditorCode=@CreditorCode and b.ProjNo=@ProjNo and ActivityCode=@ActivityCode
Group By a.CreditorCode,b.ProjNo,ActivityCode,a.CurrencyCode
--select @APInvoiceAmt=SUM(b.LocalSubTotal/isnull(cr.BankBuyRate,1)) REMARKS: LANGSUNG AMBIL AMOUNT SAJA 05/05/2020 BY DWI
select @APInvoiceAmt=SUM(b.Amount)
from APInvoice a with(nolock)
inner join
APInvoiceDTL b  with(nolock)
on a.DocKey=b.DocKey
left outer join
(select * from currrate where currencyCode=@CurrencyCode) cr
on a.DocDate between FromDate and ToDate
where Cancelled='F'
and b.ProjNo=@ProjNo
and b.UDF_ActCode=@ActivityCode
and a.CreditorCode=@CreditorCode
GROUP BY a.CreditorCode,b.UDF_ActCode,b.ProjNo
--select @APCreditNoteAmt=SUM(b.LocalSubTotal/isnull(cr.BankBuyRate,1))REMARKS: LANGSUNG AMBIL AMOUNT SAJA 05/05/2020 BY DWI
select @APCreditNoteAmt=SUM(b.Amount)
from APCN a with(nolock)
inner join
APCNDTL b  with(nolock)
on a.DocKey=b.DocKey
left outer join
(select * from currrate where currencyCode=@CurrencyCode) cr
on a.DocDate between FromDate and ToDate
where Cancelled='F'
and b.ProjNo=@ProjNo
and b.UDF_ActCode=@ActivityCode
and a.CreditorCode=@CreditorCode
GROUP BY a.CreditorCode,b.UDF_ActCode,b.ProjNo
END
set @BalanceAmt=COALESCE(@BudgetAmt,0)-(COALESCE(@APInvoiceAmt,0)-COALESCE(@APCreditNoteAmt,0))
RETURN @BalanceAmt
END
