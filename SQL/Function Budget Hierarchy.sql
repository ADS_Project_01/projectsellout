
CREATE Function[dbo].[F_BGT_Project_Hierarchy_Up](@SubProjNo nvarchar(30),@ProjType  nvarchar(20))
RETURNS nvarchar(50)
AS BEGIN
Declare @ProjNo nvarchar(30)
		BEGIN
		WITH Project AS
		(
			SELECT c1.ProjNo, c1.ParentProjNo, c1.Description, [level] = 1,c1.UDF_ProjType AS ProjType
			FROM dbo.[Project] c1
			WHERE c1.ProjNo=@SubProjNo
			UNION ALL
			SELECT c2.ProjNo, c2.ParentProjNo, c2.Description, [level] = Project.[level] + 1,c2.UDF_ProjType AS ProjType
			FROM dbo.[Project] c2 INNER JOIN Project ON Project.ParentProjNo = c2.ProjNo  -- where Project.ProjNo='E04'
		)
		SELECT TOP 1 @ProjNo=Project.ProjNo
		FROM Project where ProjType=@ProjType
		OPTION (MAXRECURSION 0)
		end
		return @ProjNo;
END
GO


