


ALTER view [dbo].[vBGT_BudgetActualReport1]
as
select
ROW_NUMBER() OVER(ORDER BY ab.ActivityCode DESC) as RowNo,
[dbo].[F_BGT_Project_Hierarchy_Up](ab.ProjNo,'Name Of Product') AS NameOfProduct,
[dbo].[F_BGT_Project_Hierarchy_Up](ab.ProjNo,'Name of Indication') AS NameofIndication, 
[dbo].[F_BGT_Project_Hierarchy_Up](ab.ProjNo,'Vendor Name') AS VendorName, 
[dbo].[F_BGT_Project_Hierarchy_Up](ab.ProjNo,'Country Name') AS CountryName, 
[dbo].[F_BGT_Project_Hierarchy_Up](ab.ProjNo,'Hospital Name') AS HospitalName,   
[dbo].[F_BGT_Project_Hierarchy_Up](ab.ProjNo,'PI Name') AS PIName, 
ab.*,
c.PINo,c.PIDate,c.PIAmount,
d.CNAmount,d.CNNo,d.CNDate
 from 
(select a1.CreditorCode,a2.ActivityCode,a2.ActivityGroup,a1.ParentProjNo,
a2.ProjNo,a2.AccNo,a2.AccDescription,a2.ActivityGroupDesc,a2.Description,
SUM(a2.Qty) as Qty,SUM(a2.UnitCost) as UnitCost,SUM(a2.Amount) as Amount
 from BGT_Budget a1 WITH(NOLOCK)  
inner join
BGT_BudgetDtl a2  WITH(NOLOCK)  
on a1.DocKey=a2.DocKey 
WHERE
a1.Cancelled='F'
Group By a1.CreditorCode,a2.ActivityCode,a2.ActivityGroup,a1.ParentProjNo,
a2.ProjNo,a2.AccNo,a2.AccDescription,a2.ActivityGroupDesc,a2.Description
) ab
inner join
(SELECT AIP.DocNo AS PINo,aip.DocDate as PIDate,aip.CreditorCode as CreditorCode2,
AIPDTL.UDF_ActCode as ActivityCode2,ProjNo as ProjNo2,UDF_ActGroup as ActivityGroup2,
AIPDTL.UDF_Qty AS PIQty, AIPDTL.UDF_UnitCost as PIUnitCost,
AIPDTL.Amount as PIAmount FROM APInvoice AIP  WITH(NOLOCK)  inner join
APInvoiceDTL AIPDTL   WITH(NOLOCK)  
on AIP.DocKey=AIPDTL.DocKey where Cancelled='F') c
on ab.ActivityCode=c.ActivityCode2 
and ab.ProjNo=c.ProjNo2 
and c.ActivityGroup2=ab.ActivityGroup
and c.CreditorCode2=ab.CreditorCode
left outer join
(SELECT APCN.DocNo as CNNo,APCN.DocDate as CNDate,APCN.CreditorCode as CreditorCode3,
UDF_ActCode as ActivityCode3,UDF_ActGroup as ActivityGroup3,ProjNo as ProjNo3,APCNDTL.Amount as CNAmount FROM APCN  WITH(NOLOCK)  inner join
APCNDTL WITH(NOLOCK)  
on APCN.DocKey=APCNDTL.DocKey where Cancelled='F') d
on ab.ActivityCode=d.ActivityCode3 
and ab.ProjNo=d.ProjNo3 
and d.ActivityGroup3=ab.ActivityGroup
and d.CreditorCode3=ab.CreditorCode

GO

alter view [dbo].[vBGT_BudgetActualReport2]
as
select
ROW_NUMBER() OVER(ORDER BY ab.ActivityCode DESC) as RowNo,
[dbo].[F_BGT_Project_Hierarchy_Up](ab.ProjNo,'Name Of Product') AS NameOfProduct,
[dbo].[F_BGT_Project_Hierarchy_Up](ab.ProjNo,'Name of Indication') AS NameofIndication, 
[dbo].[F_BGT_Project_Hierarchy_Up](ab.ProjNo,'Vendor Name') AS VendorName, 
[dbo].[F_BGT_Project_Hierarchy_Up](ab.ProjNo,'Country Name') AS CountryName, 
[dbo].[F_BGT_Project_Hierarchy_Up](ab.ProjNo,'Hospital Name') AS HospitalName,   
[dbo].[F_BGT_Project_Hierarchy_Up](ab.ProjNo,'PI Name') AS PIName, 
ab.*,
c.*,
d.*,null as CNNo, null as CNDate
 from 
(select a1.CreditorCode,a2.ActivityCode,a2.ActivityGroup,a1.ParentProjNo,
a2.ProjNo,a2.AccNo,a2.AccDescription,a2.ActivityGroupDesc,a2.Description,
SUM(a2.Qty) as Qty,SUM(a2.UnitCost) as UnitCost,SUM(a2.Amount) as Amount
 from BGT_Budget a1 WITH(NOLOCK)  
inner join
BGT_BudgetDtl a2  WITH(NOLOCK)  
on a1.DocKey=a2.DocKey 
WHERE
a1.Cancelled='F'
Group By a1.CreditorCode,a2.ActivityCode,a2.ActivityGroup,a1.ParentProjNo,
a2.ProjNo,a2.AccNo,a2.AccDescription,a2.ActivityGroupDesc,a2.Description
) ab
inner join
(SELECT AIP.DocNo AS PINo,aip.DocDate as PIDate,aip.CreditorCode as CreditorCode2,
AIPDTL.UDF_ActCode as ActivityCode2,ProjNo as ProjNo2,UDF_ActGroup as ActivityGroup2,
AIPDTL.UDF_Qty AS PIQty, AIPDTL.UDF_UnitCost as PIUnitCost,
AIPDTL.Amount as PIAmount FROM APInvoice AIP  WITH(NOLOCK)  inner join
APInvoiceDTL AIPDTL   WITH(NOLOCK)  
on AIP.DocKey=AIPDTL.DocKey where Cancelled='F') c
on ab.ActivityCode=c.ActivityCode2 
and ab.ProjNo=c.ProjNo2 
and c.ActivityGroup2=ab.ActivityGroup
and c.CreditorCode2=ab.CreditorCode
left outer join
(SELECT APCN.CreditorCode as CreditorCode3,
UDF_ActCode as ActivityCode3,UDF_ActGroup as ActivityGroup3,ProjNo as ProjNo3
,SUM(APCNDTL.Amount) as CNAmount FROM APCN  WITH(NOLOCK)  inner join
APCNDTL WITH(NOLOCK)  
on APCN.DocKey=APCNDTL.DocKey where Cancelled='F'
GROUP BY APCN.CreditorCode,
UDF_ActCode,UDF_ActGroup,ProjNo
) d
on ab.ActivityCode=d.ActivityCode3 
and ab.ProjNo=d.ProjNo3 
and d.ActivityGroup3=ab.ActivityGroup
and d.CreditorCode3=ab.CreditorCode



