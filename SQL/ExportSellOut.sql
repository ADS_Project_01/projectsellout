
CREATE TABLE [dbo].[SO_Setting](
	[rk] [decimal](27, 0) NULL,
	[rkd] [decimal](27, 0) NULL,
	[Location_Type] [nvarchar](10) NULL,
	[Sales_Person_Code] [nvarchar](10) NULL,
	[Active] [nvarchar](10) NULL,
	[Region_Code] [nvarchar](30) NULL,
	[SubRegion_Code] [nvarchar](30) NULL,
	[State_Province] [nvarchar](30) NULL,
	[Country] [nvarchar](9) NULL,
	[Creation_Date] [datetime] NULL,
	[Modification_Date] [datetime] NULL,
	[PathString] [nvarchar](max) NULL,
	[UpdateCount] [decimal](18, 0) NULL,
	[UDF_HasExport] [dbo].[d_Boolean] NULL,
	[UDF_ExDate] [datetime] NULL,
	[UDF_ExportBy] [nvarchar](100) NULL
) ON [PRIMARY]


GO
CREATE Function[dbo].[SO_StatusIV_F](@FromDocType nvarchar(5),@FromDocNo nvarchar(30),@ItemCode nvarchar(50),@Seq bigint)
RETURNS nvarchar(30)
AS BEGIN
Declare @CountIncomplete bigint
Declare @Status nvarchar(30)
set @CountIncomplete=0
    BEGIN
   if @FromDocType='DO'
		BEGIN
		select @CountIncomplete=COUNT(*) from DR with(NOLOCK) 
		INNER JOIN 
		DRDTL with(NOLOCK)
		 ON DR.DocKey=DRDTL.DocKey
		  WHERE Cancelled='F' AND FromDocNo=@FromDocNo and ItemCode=@ItemCode;
		END
	ELSE IF @FromDocType='SO'
		BEGIN
		select @CountIncomplete=COUNT(*) from SODTL with(NOLOCK) 		
		  WHERE FromDocNo=@FromDocNo and Qty-TransferedQty>0 and ItemCode=@ItemCode;
		END	 
		
		IF @CountIncomplete=0
		begin
			SET @Status='Complete';
		end
		else
		begin
			SET @Status='Not Complete';
		end
		
    end
    return @Status;
   END;
   GO
/****** Object:  Table [dbo].[UDFList]    Script Date: 06/06/2020 16:07:21 ******/
/****** Object:  Table [dbo].[UDFLayout]    Script Date: 06/06/2020 16:07:21 ******/
/****** Object:  Table [dbo].[UDF]    Script Date: 06/06/2020 16:07:21 ******/
SET IDENTITY_INSERT [dbo].[UDF] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[UDF]([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid])
SELECT 1, N'Debtor', N'City', 1, N'0', N'City', N'Size=30
Required=False
Unique=False
ListName=
', N'8bb285d6-234e-4c20-874f-1bb23ab62065' UNION ALL
SELECT 2, N'Debtor', N'Provinsi', 2, N'0', N'Province', N'Size=30
Required=False
Unique=False
ListName=
', N'50f5be19-7367-48e9-8a7c-d8fb16a8269c' UNION ALL
SELECT 3, N'Debtor', N'Country', 3, N'0', N'Country', N'Size=9
Required=False
Unique=False
ListName=
', N'7aa13c54-446e-4af6-b9f2-9a67c3a71f57' UNION ALL
SELECT 4, N'Debtor', N'Region', 4, N'0', N'Region', N'Size=30
Required=False
Unique=False
ListName=
', N'698dd165-3879-4640-8f1f-e21baeb9f560' UNION ALL
SELECT 5, N'Debtor', N'Subregion', 5, N'0', N'Subregion', N'Size=30
Required=False
Unique=False
ListName=
', N'84a77256-77a2-43c3-92ba-840d40a42787' UNION ALL
SELECT 6, N'IV', N'HasExport', 1, N'4', N'Has Export', N'Unique=False
Required=False
', N'1d72509c-ba4e-464f-9057-7587bdfd35ec' UNION ALL
SELECT 7, N'IV', N'ExDate', 2, N'3', N'ExDate', N'DateType=DateTime
Unique=False
Required=False
', N'137923cf-d277-4a6c-89e5-23c9cbe710fb' UNION ALL
SELECT 8, N'IV', N'ExportBy', 3, N'0', N'ExportBy', N'ListName=
Required=False
Unique=False
Size=100
', N'de82d5c3-aade-4543-a15c-ba560e740368' UNION ALL
SELECT 9, N'IVDTL', N'SONO', 11, N'0', N'No. SO', N'ListName=
Required=False
Unique=False
Size=30
', N'a32bec96-101a-4ab3-a13d-c334963b7dd9' UNION ALL
SELECT 10, N'IVDTL', N'DtlKey', 12, N'2', N'SO Line Number', N'Required=False
Unique=False
', N'e9ffaa5f-fc14-44fe-9953-fe501561934b' UNION ALL
SELECT 11, N'PO', N'Name1', 1, N'0', N'Name1', N'ListName=
Required=False
Unique=False
Size=50
', N'7b34d47b-c25d-4c5e-a687-98531de72cc4' UNION ALL
SELECT 12, N'PODTL', N'Name1', 1, N'0', N'Name1', N'ListName=
Required=False
Unique=False
Size=50
', N'a6470d93-e9d9-471b-8496-f403b25caeb3' UNION ALL
SELECT 13, N'SalesAgent', N'JobTitle', 1, N'0', N'JobTitle', N'ListName=
Required=False
Unique=False
Size=30
', N'c72b6a92-6ab4-4320-bcd2-ce0c05555016' UNION ALL
SELECT 14, N'SalesAgent', N'Role', 2, N'0', N'Role', N'ListName=
Required=False
Unique=False
Size=30
', N'cf83d346-9b90-49fb-be7f-ecbb7daee176' UNION ALL
SELECT 15, N'SalesAgent', N'Manager', 3, N'0', N'Manager', N'ListName=
Required=False
Unique=False
Size=30
', N'40df3704-8eea-48bd-8c64-c668b86b9b13' UNION ALL
SELECT 16, N'SalesAgent', N'Team', 4, N'0', N'Team', N'ListName=
Required=False
Unique=False
Size=30
', N'e958a1e9-57b8-4b06-a814-0ff4f5ba774b' UNION ALL
SELECT 17, N'SalesAgent', N'CrDate', 5, N'3', N'Creation Date', N'DateType=DateTime
Unique=False
Required=False
', N'10bb5271-4535-45f7-b641-9c373c8db817' UNION ALL
SELECT 18, N'SalesAgent', N'ModDate', 6, N'3', N'ModDate', N'DateType=DateTime
Unique=False
Required=False
', N'f968d6f9-a77f-4d7a-a707-2a238230958d' UNION ALL
SELECT 19, N'Debtor', N'CrDate', 6, N'3', N'Create Date', N'DateType=Date
Unique=False
Required=False
', N'500b64b5-ce8e-4fbc-a2a1-8346e3277ace' UNION ALL
SELECT 20, N'Debtor', N'ModDate', 6, N'3', N'Modification Date', N'DateType=Date
Unique=False
Required=False
', N'a3672ba7-5862-4558-b287-f102a7f6bcd4' UNION ALL
SELECT 21, N'SODTL', N'Disc1P', 1, N'1', N'Disc. 1 (%)', N'Precision=18
Required=False
Unique=False
Scale=2
', N'333fec51-a29e-4be9-aa9f-79b22b266e77' UNION ALL
SELECT 22, N'SODTL', N'Disc1Amt', 2, N'1', N'Disc.1 Amount', N'Precision=18
Required=False
Unique=False
Scale=2
', N'9aceb840-ee92-429c-87ac-b3ae26dab002' UNION ALL
SELECT 23, N'SODTL', N'Disc2P', 3, N'1', N'Disc. 2 (%)', N'Precision=18
Required=False
Unique=False
Scale=2
', N'31c31f8d-5661-4cff-96b6-4503c23f8676' UNION ALL
SELECT 24, N'SODTL', N'Disc2Amt', 4, N'1', N'Disc. 2 Amount', N'Precision=18
Required=False
Unique=False
Scale=2
', N'37b20507-f050-4d45-b854-d2be2b9a4b69' UNION ALL
SELECT 25, N'SODTL', N'Disc3P', 5, N'1', N'Disc. 3 (%)', N'Precision=18
Required=False
Unique=False
Scale=2
', N'23432ba1-8146-4c52-8e22-4e674e7e9f1c' UNION ALL
SELECT 26, N'SODTL', N'Disc3Amt', 6, N'1', N'Disc. 3 Amount', N'Precision=18
Required=False
Unique=False
Scale=2
', N'e9ab3f42-9409-4d2c-b8b8-1010795cd50f' UNION ALL
SELECT 27, N'SODTL', N'Disc4P', 7, N'1', N'Disc. 4 (%)', N'Precision=18
Required=False
Unique=False
Scale=2
', N'453392d2-2bc2-4b68-b23f-3a6edcb22afe' UNION ALL
SELECT 28, N'SODTL', N'Disc4Amt', 8, N'1', N'Disc. 4 Amount', N'Precision=18
Required=False
Unique=False
Scale=2
', N'6165ef9b-b288-48df-8c16-ad42d82f0ce6' UNION ALL
SELECT 29, N'SODTL', N'Disc5P', 9, N'1', N'Disc. 5 (%)', N'Precision=18
Required=False
Unique=False
Scale=2
', N'e11103f6-692b-452b-bc57-6e143a3bf920' UNION ALL
SELECT 30, N'SODTL', N'Disc5Amt', 10, N'1', N'Disc. 5 Amount', N'Precision=18
Required=False
Unique=False
Scale=2
', N'9ad9ed24-d96d-4f96-8ef5-ac73fa4d5d34' UNION ALL
SELECT 31, N'DODTL', N'Disc1P', 1, N'1', N'Disc. 1 (%)', N'Precision=18
Required=False
Unique=False
Scale=2
', N'9bb3a344-8480-416d-919d-b47b3702c6ca' UNION ALL
SELECT 32, N'DODTL', N'Disc1Amt', 2, N'1', N'Disc.1 Amount', N'Precision=18
Required=False
Unique=False
Scale=2
', N'175bfc8b-851f-41b2-bc38-f440818ffd85' UNION ALL
SELECT 33, N'DODTL', N'Disc2P', 3, N'1', N'Disc. 2 (%)', N'Precision=18
Required=False
Unique=False
Scale=2
', N'a2f0b8e0-fe69-4b81-8321-856763f5637b' UNION ALL
SELECT 34, N'DODTL', N'Disc2Amt', 4, N'1', N'Disc. 2 Amount', N'Precision=18
Required=False
Unique=False
Scale=2
', N'617dce38-221e-4406-8be8-20424119831e' UNION ALL
SELECT 35, N'DODTL', N'Disc3P', 5, N'1', N'Disc. 3 (%)', N'Precision=18
Required=False
Unique=False
Scale=2
', N'd0a40084-06db-4d6f-9389-6fd2520ac662' UNION ALL
SELECT 36, N'DODTL', N'Disc3Amt', 6, N'1', N'Disc. 3 Amount', N'Precision=18
Required=False
Unique=False
Scale=2
', N'23ac8c3f-c3b8-4355-b793-0be59532f388' UNION ALL
SELECT 37, N'DODTL', N'Disc4P', 7, N'1', N'Disc. 4 (%)', N'Precision=18
Required=False
Unique=False
Scale=2
', N'4866582c-337f-44a2-acf9-18fa6b386362' UNION ALL
SELECT 38, N'DODTL', N'Disc4Amt', 8, N'1', N'Disc. 4 Amount', N'Precision=18
Required=False
Unique=False
Scale=2
', N'e5a49245-9a83-45b8-9a80-97ccc0416d95' UNION ALL
SELECT 39, N'DODTL', N'Disc5P', 9, N'1', N'Disc. 5 (%)', N'Precision=18
Required=False
Unique=False
Scale=2
', N'fa8c5c57-b17e-4d4e-8727-07fc33a9ffd7' UNION ALL
SELECT 40, N'DODTL', N'Disc5Amt', 10, N'1', N'Disc. 5 Amount', N'Precision=18
Required=False
Unique=False
Scale=2
', N'd5f1907e-4611-483c-98d1-0ec7c66e68cf' UNION ALL
SELECT 41, N'IVDTL', N'Disc1P', 1, N'1', N'Disc. 1 (%)', N'Precision=18
Required=False
Unique=False
Scale=2
', N'07cbedd6-3dee-4fbc-afab-5e04fbff4784' UNION ALL
SELECT 42, N'IVDTL', N'Disc1Amt', 2, N'1', N'Disc.1 Amount', N'Precision=18
Required=False
Unique=False
Scale=2
', N'2eb40631-a56f-49e8-80c4-4f2c33fea016' UNION ALL
SELECT 43, N'IVDTL', N'Disc2P', 3, N'1', N'Disc. 2 (%)', N'Precision=18
Required=False
Unique=False
Scale=2
', N'5db64863-24f3-4b26-ad14-086d2d8cabf0' UNION ALL
SELECT 44, N'IVDTL', N'Disc2Amt', 4, N'1', N'Disc. 2 Amount', N'Precision=18
Required=False
Unique=False
Scale=2
', N'152204a5-91df-4712-8ad5-aac80c2a7d04' UNION ALL
SELECT 45, N'IVDTL', N'Disc3P', 5, N'1', N'Disc. 3 (%)', N'Precision=18
Required=False
Unique=False
Scale=2
', N'73e28788-cebc-4a2a-be63-f57a856e6fac' UNION ALL
SELECT 46, N'IVDTL', N'Disc3Amt', 6, N'1', N'Disc. 3 Amount', N'Precision=18
Required=False
Unique=False
Scale=2
', N'bb57645a-1e06-4046-ab79-79dca69de95e' UNION ALL
SELECT 47, N'IVDTL', N'Disc4P', 7, N'1', N'Disc. 4 (%)', N'Precision=18
Required=False
Unique=False
Scale=2
', N'3682204d-fecd-4f1b-be75-69c0cc9fee24' UNION ALL
SELECT 48, N'IVDTL', N'Disc4Amt', 8, N'1', N'Disc. 4 Amount', N'Precision=18
Required=False
Unique=False
Scale=2
', N'a747716b-d208-4a25-83ca-b777a9cd25bb' UNION ALL
SELECT 49, N'IVDTL', N'Disc5P', 9, N'1', N'Disc. 5 (%)', N'Precision=18
Required=False
Unique=False
Scale=2
', N'd36be64c-d9e9-4b60-b086-6e8a69e36fd4' UNION ALL
SELECT 50, N'IVDTL', N'Disc5Amt', 10, N'1', N'Disc. 5 Amount', N'Precision=18
Required=False
Unique=False
Scale=2
', N'9adb35b6-8d00-47a5-8ec8-0fcf5fee3c44'
COMMIT;
RAISERROR (N'[dbo].[UDF]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[UDF]([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid])
SELECT 51, N'DODTL', N'SONO', 10, N'0', N'SONO', N'ListName=
Required=False
Unique=False
Size=50
', N'e1762a5c-21b7-4e2d-b9b5-023c3139e888' UNION ALL
SELECT 52, N'Item', N'PBPCode', 0, N'0', N'PBPCode', N'ListName=
Required=False
Unique=False
Size=50
', N'c78b88f9-64b0-467b-b9d0-e8afc9fc7a62' UNION ALL
SELECT 53, N'Debtor', N'HasExport', 8, N'4', N'HasExport', N'Unique=False
Required=False
DefaultValue=F
', N'8175017a-f8d0-4a4a-94e6-00e90e2c7505' UNION ALL
SELECT 54, N'Debtor', N'ExDate', 9, N'3', N'ExDate', N'DateType=Date
Unique=False
Required=False
', N'325ea7c1-8d1c-4ed5-8f90-a028eceba16b' UNION ALL
SELECT 55, N'Debtor', N'ExportBy', 10, N'0', N'ExportBy', N'ListName=
Required=False
Unique=False
Size=100
', N'472d9985-bfcb-4f5f-986b-110c8eb8e323' UNION ALL
SELECT 56, N'Item', N'HasExport', 2, N'4', N'HasExport', N'Unique=False
Required=False
DefaultValue=F
', N'e3b80354-90e8-4275-8700-ffbb10a518bf' UNION ALL
SELECT 57, N'Item', N'ExDate', 3, N'3', N'ExDate', N'DateType=Date
Unique=False
Required=False
', N'9ddcff49-eeb1-4ba8-a10c-c8ab67de038f' UNION ALL
SELECT 58, N'Item', N'ExportBy', 4, N'0', N'ExportBy', N'ListName=
Required=False
Unique=False
Size=100
', N'a347445a-a7af-4c3b-9ef7-3750f9929fe2' UNION ALL
SELECT 59, N'SalesAgent', N'HasExport', 7, N'4', N'HasExport', N'Unique=False
Required=False
DefaultValue=F
', N'47b472f1-9eb3-4577-b316-cab81f3f57b7' UNION ALL
SELECT 60, N'SalesAgent', N'ExDate', 8, N'3', N'ExDate', N'DateType=Date
Unique=False
Required=False
', N'a767796b-69f6-4e28-adcc-a0bdc1b58c08' UNION ALL
SELECT 61, N'SalesAgent', N'ExportBy', 9, N'0', N'ExportBy', N'Size=100
Required=False
Unique=False
ListName=
', N'2085d7c8-c2af-4276-8d60-1db8706a6d25'
COMMIT;
RAISERROR (N'[dbo].[UDF]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[UDF] OFF;
GO
alter table Item
ADD [UDF_PBPCode] [nvarchar](50) NULL
alter table Item
ADD [UDF_HasExport] [nvarchar](1) NULL
GO
alter table Item
ADD	[UDF_ExDate] [datetime] NULL
GO
alter table Item
ADD	[UDF_ExportBy] [nvarchar](100) NULL
GO
alter table Debtor
add [UDF_City] [nvarchar](30) NULL
GO
alter table Debtor
add	[UDF_Provinsi] [nvarchar](30) NULL
GO
alter table Debtor
add	[UDF_Country] [nvarchar](9) NULL
GO
alter table Debtor
add	[UDF_Region] [nvarchar](30) NULL
GO
alter table Debtor
add	[UDF_Subregion] [nvarchar](30) NULL
GO
alter table Debtor
add	[UDF_CrDate] [datetime] NULL
GO
alter table Debtor
add	[UDF_ModDate] [datetime] NULL
GO
alter table Debtor
ADD [UDF_HasExport] [nvarchar](1) NULL
GO
alter table Debtor
ADD	[UDF_ExDate] [datetime] NULL
GO
alter table Debtor
ADD	[UDF_ExportBy] [nvarchar](100) NULL
GO
alter table SalesAgent
ADD	[UDF_JobTitle] [nvarchar](30) NULL
GO
alter table SalesAgent
ADD	[UDF_Role] [nvarchar](30) NULL
GO
alter table SalesAgent
ADD	[UDF_Manager] [nvarchar](30) NULL
GO
alter table SalesAgent
ADD	[UDF_Team] [nvarchar](30) NULL
GO
alter table SalesAgent
ADD	[UDF_CrDate] [datetime] NULL
GO
alter table SalesAgent
ADD	[UDF_ModDate] [datetime] NULL
GO
alter table SalesAgent
ADD [UDF_HasExport] [nvarchar](1) NULL
GO
alter table SalesAgent
ADD	[UDF_ExDate] [datetime] NULL
GO
alter table SalesAgent
ADD	[UDF_ExportBy] [nvarchar](100) NULL
GO
ALTER TABLE IV
ADD	[UDF_HasExport] [nvarchar](1) NULL
GO
ALTER TABLE IV
ADD	[UDF_ExDate] [datetime] NULL
GO
ALTER TABLE IV
ADD	[UDF_ExportBy] [nvarchar](100) NULL
GO
ALTER TABLE IVDTL
ADD	[UDF_SONO] [nvarchar](30) NULL
GO
ALTER TABLE IVDTL
ADD	[UDF_DtlKey] [int] NULL
GO
alter TABLE [dbo].[PO]
ADD	[UDF_Name1] [nvarchar](50) NULL
GO
alter TABLE [dbo].[PODTL]
ADD	[UDF_Name1] [nvarchar](50) NULL


GO
alter TABLE [dbo].[DODTL]
ADD	[UDF_Name1] [nvarchar](50) NULL
GO
alter TABLE [dbo].[DODTL]
ADD [UDF_Disc1P] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[DODTL]
ADD	[UDF_Disc5Amt] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[DODTL]
ADD	[UDF_SONO] [nvarchar](50) NULL
GO
alter TABLE [dbo].[DODTL]
ADD	[UDF_Disc1Amt] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[DODTL]
ADD	[UDF_Disc2P] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[DODTL]
ADD	[UDF_Disc2Amt] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[DODTL]
ADD	[UDF_Disc3P] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[DODTL]
ADD	[UDF_Disc3Amt] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[DODTL]
ADD	[UDF_Disc4P] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[DODTL]
ADD	[UDF_Disc4Amt] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[DODTL]
ADD	[UDF_Disc5P] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[SODTL]
ADD	[UDF_Disc1P] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[SODTL]
ADD		[UDF_Disc5Amt] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[SODTL]
ADD		[UDF_Disc1Amt] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[SODTL]
ADD		[UDF_Disc2P] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[SODTL]
ADD		[UDF_Disc2Amt] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[SODTL]
ADD		[UDF_Disc3P] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[SODTL]
ADD		[UDF_Disc3Amt] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[SODTL]
ADD		[UDF_Disc4P] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[SODTL]
ADD		[UDF_Disc4Amt] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[SODTL]
ADD		[UDF_Disc5P] [decimal](18, 2) NULL
GO
alter TABLE [dbo].[IVDTL]
ADD			[UDF_Disc1P] [decimal](18, 2) NULL
alter TABLE [dbo].[IVDTL]
ADD		[UDF_Disc5Amt] [decimal](18, 2) NULL
alter TABLE [dbo].[IVDTL]
ADD		[UDF_Disc1Amt] [decimal](18, 2) NULL
alter TABLE [dbo].[IVDTL]
ADD		[UDF_Disc2P] [decimal](18, 2) NULL
alter TABLE [dbo].[IVDTL]
ADD		[UDF_Disc2Amt] [decimal](18, 2) NULL
alter TABLE [dbo].[IVDTL]
ADD		[UDF_Disc3P] [decimal](18, 2) NULL
alter TABLE [dbo].[IVDTL]
ADD		[UDF_Disc3Amt] [decimal](18, 2) NULL
alter TABLE [dbo].[IVDTL]
ADD		[UDF_Disc4P] [decimal](18, 2) NULL
alter TABLE [dbo].[IVDTL]
ADD		[UDF_Disc4Amt] [decimal](18, 2) NULL
alter TABLE [dbo].[IVDTL]
ADD		[UDF_Disc5P] [decimal](18, 2) NULL
	GO
INSERT INTO [dbo].[SO_Setting]
           ([rk]
           ,[rkd]
           ,[Location_Type]
           ,[Sales_Person_Code]
           ,[Active]
           ,[Region_Code]
           ,[SubRegion_Code]
           ,[State_Province]
           ,[Country]
           ,[Creation_Date]
           ,[Modification_Date]
            ,[PathString]
             ,[UpdateCount],	[UDF_HasExport],	[UDF_ExDate] ,
	[UDF_ExportBy])
     VALUES
           (0
           ,0
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,'D:\\'
           ,0
           ,'F',NULL,NULL)
GO
CREATE VIEW [dbo].[SO_Product_View]
as
SELECT
case when LEN( ItemCode)>30 then SUBSTRING(ItemCode,1,30) else ItemCode end AS Local_Product_Code,
case when LEN( Description)>50 then SUBSTRING(Description,1,50) else Description end AS Local_Product_Desc,
case when LEN( BaseUOM)>8 then SUBSTRING(BaseUOM,1,8) else BaseUOM end AS UOM,
CreatedTimeStamp AS Creation_Date,
LastModified AS Modification_Date,
--CR 2020-11-04
isnull(UDF_HasExport,'F') as UDF_HasExport,
UDF_ExDate as Export_Date,
UDF_ExportBy as Export_By
FROM ITEM
GO
/****** Object:  View [dbo].[SO_Debtor_View]    Script Date: 11/07/2020 09:34:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SO_Debtor_View]
as
SELECT
case when LEN( AccNo)>10 then SUBSTRING(AccNo,1,10) else AccNo end AS End_Customer_Code,
case when LEN(CompanyName)>100 then SUBSTRING(CompanyName,1,100) else CompanyName end AS End_Customer_Name,
case when LEN(DebtorType)>10 then SUBSTRING(DebtorType,1,10) else DebtorType end AS Customer_Type,
case when LEN(Address1)>150 then SUBSTRING(Address1,1,150) else Address1 end AS Address1,
case when LEN(Address2)>150 then SUBSTRING(Address2,1,150) else Address2 end AS Address2,
case when LEN(Address3)>150 then SUBSTRING(Address3,1,150) else Address3 end AS Address3,
UDF_City AS City,
UDF_Provinsi AS State_Province,
UDF_Country AS Country,
case when LEN(PostCode)>5 then SUBSTRING(PostCode,1,5) else PostCode end AS Zip_Code,
case when LEN(Debtor.Attention)>30 then SUBSTRING(Attention,1,30) else Attention end AS Contact_Person,
case when LEN(Phone1)>30 then SUBSTRING(Phone1,1,30) else Phone1 end AS Telephone_Number,
case when LEN(EmailAddress)>50 then SUBSTRING(EmailAddress,1,50) else EmailAddress end AS Email,
case when IsActive='T' then 'ACTIVE' else 'NOT ACTIVE' end AS Active,
case when LEN(UDF_Region)>30 then SUBSTRING((UDF_Region),1,30) else UDF_Region end AS Region_Code,
case when LEN(UDF_Subregion)>30 then SUBSTRING((UDF_Subregion),1,30) else UDF_Subregion end AS SubRegion_Code,
UDF_CrDate as Creation_Date,
UDF_ModDate as Modification_Date,
--CR 2020-11-04
isnull(UDF_HasExport,'F') as UDF_HasExport,
UDF_ExDate as Export_Date,
UDF_ExportBy as Export_By
FROM Debtor
GO
/****** Object:  View [dbo].[SO_StockCard_View]    Script Date: 11/07/2020 09:34:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SO_StockCard_View]
as
select Distributor_Code,Local_Product_Code,UOM,Stock_Date,Stock_position,Creation_Date, Modification_Date,UDF_HasExport,Export_Date,Export_By from(
SELECT case when LEN((select RegisterNo from Profile))>10 then SUBSTRING((select RegisterNo from Profile),1,10) else (select RegisterNo from Profile) end AS Distributor_Code,
case when LEN( MStock.ItemCode)>30 then SUBSTRING(MStock.ItemCode,1,30) else MStock.ItemCode end AS Local_Product_Code,
--diubah sesuai dokumen excel
--case when LEN(UOM)>8 then SUBSTRING(UOM,1,8)  else UOM end AS UOM,
case when LEN(MStock.BaseUOM)>8 then SUBSTRING(MStock.BaseUOM,1,8)  else MStock.BaseUOM end AS UOM,
DocDate AS Stock_Date,
isnull(Qty,0) AS Stock_position,
DocDate AS Creation_Date,
SCard.LastModified AS Modification_Date,
--CR 2020-11-04
isnull(Mstock.UDF_HasExport,'F') as UDF_HasExport,
MStock.UDF_ExDate as Export_Date,
MStock.UDF_ExportBy as Export_By
FROM StockDTL SCard with(NOLOCK) 
--Request Pak Agus 2020-08-11
INNER JOIN Item MStock WITH(nolock)  ON SCard.ItemCode=MStock.ItemCode WHERE ISNULL(MStock.UDF_PBPCode,'')<>''
) result
GO
/****** Object:  View [dbo].[SO_ItemNonStock_View]    Script Date: 11/07/2020 09:34:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SO_ItemNonStock_View]
as
select Distributor_Code,Local_Product_Code,UOM,Stock_Date,Stock_position,Creation_Date, Modification_Date,UDF_HasExport,Export_Date,Export_By from(
SELECT case when LEN((select RegisterNo from Profile))>10 then SUBSTRING((select RegisterNo from Profile),1,10) else (select RegisterNo from Profile) end AS Distributor_Code,
case when LEN( MStock.ItemCode)>30 then SUBSTRING(MStock.ItemCode,1,30) else MStock.ItemCode end AS Local_Product_Code,
case when LEN(MStock.BaseUOM)>8 then SUBSTRING(MStock.BaseUOM,1,8)  else MStock.BaseUOM end AS UOM,
MStock.CreatedTimeStamp AS Stock_Date,
0 AS Stock_position,
 MStock.CreatedTimeStamp  AS Creation_Date,
 MStock.LastModified  AS Modification_Date,
--CR 2020-11-04
isnull(UDF_HasExport,'F') as UDF_HasExport,
 UDF_ExDate as Export_Date,
 UDF_ExportBy as Export_By
FROM Item MStock WITH(nolock) WHERE ISNULL(MStock.UDF_PBPCode,'')<>'') result
GO
/****** Object:  View [dbo].[SO_Invoice_View]    Script Date: 11/07/2020 09:34:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SO_Invoice_View]
as
SELECT 
 case when LEN((select RegisterNo from Profile))>10 then SUBSTRING((select RegisterNo from Profile),1,10) else (select RegisterNo from Profile) end AS Distributor_Code,
 case when LEN( DebtorCode)>30 then SUBSTRING(DebtorCode,1,30) else DebtorCode end AS Bill_To_Code,
 case when LEN( SalesAgent)>10 then SUBSTRING(SalesAgent,1,10) else SalesAgent end AS Employee_Num,
 case when LEN( ShipVia)>10 then SUBSTRING(ShipVia,1,10) else ShipVia end AS Ship_To_Code,
 case when LEN( UDF_SONO)>30 then SUBSTRING(UDF_SONO,1,30) else UDF_SONO end AS Sales_Order_Number,
UDF_DtlKey  AS Sales_Order_Line_Number,
 case when LEN( DocNo)>30 then SUBSTRING(DocNo,1,30) else DocNo end AS Invoice_Number,
 --Seq/16  AS Invoice_Line_Number, Remarks by Dwi 2020-06-05
(Seq/16)*1000  AS Invoice_Line_Number,
DocDate AS Invoice_Date,
 case when LEN( DisplayTerm)>10 then SUBSTRING(DisplayTerm,1,10) else DisplayTerm end AS Payment_Term,
DocDate AS Sale_Date,
DeliveryDate AS Shipment_Date,
 case when LEN( ItemCode)>30 then SUBSTRING(ItemCode,1,30) else ItemCode end AS Local_Product_Code,
UnitPrice AS Unit_Price,
UDF_Disc1Amt as Discount1,
UDF_Disc2Amt as Discount2,
UDF_Disc3Amt as Discount3,
UDF_Disc4Amt as Discount4,
UDF_Disc5Amt as Discount5,
((Qty*UnitPrice)-isnull(UDF_Disc1Amt,0)-isnull(UDF_Disc2Amt,0)-isnull(UDF_Disc3Amt,0)-isnull(UDF_Disc4Amt,0)-isnull(UDF_Disc5Amt,0)) as Net_Sales,
(Qty*UnitPrice) as Gross_Sales,
 case when LEN( UOM)>8 then SUBSTRING(UOM,1,8) else UOM end AS Local_UOM,
Qty as Local_Quantity,
'Sales' as Transaction_Type,
case 
Cancelled
when 
'F' THEN
case TransferedQty when 0 then [dbo].[SO_StatusIV_F](IVDTL.FromDocType,IVDTL.FromDocNo,IVDTL.ItemCode,(UDF_DtlKey/1000)*16)
else 'Not Complete' 
 end
  ELSE 'Not Complete' END as Status,
CreatedTimeStamp as Creation_Date,
LastModified as Modification_Date,
null as CRM_REF_NUM,
--CR 2020-11-04
isnull(IV.UDF_HasExport,'F') as UDF_HasExport,
IV.UDF_ExDate as Export_Date,
IV.UDF_ExportBy as Export_By
from IV WITH (NOLOCK)
inner join IVDTL WITH (NOLOCK) on iv.DocKey=IVDTL.DocKey
   WHERE Cancelled='F'
GO
/****** Object:  View [dbo].[SO_SalesAgent_View]    Script Date: 11/07/2020 09:34:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SO_SalesAgent_View]
as
SELECT 
 case when LEN( SalesAgent)>10 then SUBSTRING(SalesAgent,1,10) else SalesAgent end AS Employee_Num,
 case when LEN(Description)>30 then SUBSTRING(Description,1,30) else Description end AS Full_Name,
UDF_JobTitle  AS Job_Title,
UDF_Role AS Role,
 UDF_Manager AS Manager_Code,
UDF_Team AS Team,
 case IsActive when 'F' THEN 'Not Active' ELSE 'Active  ' END AS Active,
 UDF_CrDate AS Creation_Date,
 UDF_ModDate AS Modification_Date,
 --CR 2020-11-04
isnull(UDF_HasExport,'F') as UDF_HasExport,
 UDF_ExDate as Export_Date,
 UDF_ExportBy as Export_By
 FROM SalesAgent
GO
/****** Object:  View [dbo].[SO_CompanyProfile_View]    Script Date: 11/07/2020 09:34:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SO_CompanyProfile_View]
as
SELECT
case when LEN( RegisterNo)>10 then SUBSTRING(RegisterNo,1,10) else RegisterNo end AS Distributor_Code,
case when LEN( CompanyName)>20 then SUBSTRING(CompanyName,1,20) else CompanyName end AS Distributor_Name,
case when LEN(Address1)>150 then SUBSTRING(Address1,1,150) else Address1 end AS Address1,
case when LEN(Address2)>150 then SUBSTRING(Address2,1,150) else Address2 end AS Address2,
case when LEN(Address3)>150 then SUBSTRING(Address3,1,150) else Address3 end AS Address3,
case when LEN(Address4)>30 then SUBSTRING(Address4,1,30) else Address4 end AS City,
case when LEN((select State_Province from SO_Setting))>30 then SUBSTRING((select State_Province from SO_Setting),1,30) else (select State_Province from SO_Setting) end AS State_Province,
case when LEN((select Country from SO_Setting))>9 then SUBSTRING((select Country from SO_Setting),1,9) else (select Country from SO_Setting) end AS Country,
case when LEN(Profile.PostCode)>5 then SUBSTRING(Profile.PostCode,1,5) else Profile.PostCode end AS Zip_Code,
case when LEN(Profile.Contact)>30 then SUBSTRING(Profile.Contact,1,30) else Profile.Contact end AS Contact_Person,
case when LEN(Profile.Phone1)>30 then SUBSTRING(Profile.Phone1,1,30) else Profile.Phone1 end AS Telephone_Number,
case when LEN(Profile.EmailAddress)>50 then SUBSTRING(Profile.EmailAddress,1,50) else Profile.EmailAddress end AS Email,
case when LEN((select Location_Type from SO_Setting))>10 then SUBSTRING((select Location_Type from SO_Setting),1,10) else (select Location_Type from SO_Setting) end AS Location_Type,
case when LEN((select Sales_Person_Code from SO_Setting))>10 then SUBSTRING((select Sales_Person_Code from SO_Setting),1,10) else (select Sales_Person_Code from SO_Setting) end AS Sales_Person_Code,
case when LEN((select Active from SO_Setting))>10 then SUBSTRING((select Active from SO_Setting),1,10) else (select Active from SO_Setting) end AS Active,
case when LEN((select Region_Code from SO_Setting))>10 then SUBSTRING((select Region_Code from SO_Setting),1,10) else (select Region_Code from SO_Setting) end AS Region_Code,
case when LEN((select SubRegion_Code from SO_Setting))>10 then SUBSTRING((select SubRegion_Code from SO_Setting),1,10) else (select SubRegion_Code from SO_Setting) end AS SubRegion_Code,
(select Creation_Date from SO_Setting) as Creation_Date,
(select Modification_Date from SO_Setting) as Modification_Date,
(select ISNULL(UDF_HasExport,'F') from SO_Setting) as UDF_HasExport
FROM Profile
GO
CREATE TRIGGER [dbo].[UpdateLastModified]
   ON [dbo].[Profile]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
UPDATE SO_Setting SET Modification_Date=GETDATE()


END
GO

CREATE PROCEDURE [dbo].[SO_StockQuery_P](@ToDate nvarchar(100))
AS
-- atas query non stock card
select Distributor_Code,Local_Product_Code,UOM,convert(varchar(10),cast(@ToDate as DATE),103) as Stock_Date,0 as Stock_position,MAX(Creation_Date) AS Creation_Date, MAX(Modification_Date) AS Modification_Date,UDF_HasExport from SO_ItemNonStock_View where Local_Product_Code Not in(select Local_Product_Code from SO_StockCard_View WHERE Stock_Date<=@ToDate) GROUP By Distributor_Code,Local_Product_Code,UOM,UDF_HasExport
 union all  -- bawah query stock card
select * from(select Distributor_Code,Local_Product_Code,UOM,convert(varchar(10),cast(@ToDate as DATE),103)as Stock_Date,case when SUM(Stock_position) <0 then 0 else SUM(Stock_position) end as Stock_position,MAX(Creation_Date) AS Creation_Date, MAX(Modification_Date) AS Modification_Date,UDF_HasExport from SO_StockCard_View WHERE Stock_Date<=@ToDate GROUP By Distributor_Code,Local_Product_Code,UOM,UDF_HasExport) result ORDER BY Local_Product_Code
GO
BEGIN TRANSACTION;
INSERT INTO [dbo].[UserScript]([ScriptName], [Language], [Script], [LastUpdate], [Guid])
SELECT N'Debtor', N'CS', N'using AutoCount.ARAP;
using AutoCount.ARAP.Debtor;
using AutoCount.Data;
using AutoCount.Authentication;
using AutoCount;
using AutoCount.Localization;
using System;
using System.Data;
using System.Windows.Forms;

        /// <summary>
        /// Use this event to validate the data before save, if the data is invalid, you can assign your error message to e.ErrorMessage.
        /// </summary>
        /// <param name="e">The event argument</param>
        public void BeforeSave(AutoCount.ARAP.Debtor.DebtorBeforeSaveEventArgs e)
		{
            if (e.MasterRecord.UDF["City"].ToString() == "" || e.MasterRecord.UDF["Provinsi"].ToString() == "" || e.MasterRecord.UDF["Country"].ToString() == "" || e.MasterRecord.UDF["Region"].ToString() == "" || e.MasterRecord.UDF["Subregion"].ToString() == "")
            {
                AutoCount.AppMessage.ShowErrorMessage("City, Province, Country, Region dan Subregion wajib dilengkapi !!!");
                e.AbortSave();
                return;
            }
            string sAccNo = System.Convert.ToString(e.MasterRecord.AccNo);
            string sSql = "SELECT Isnull(A.AccNo,'''') FROM Debtor A WHERE A.AccNo=''" + sAccNo + "''";
            object obj = e.DBSetting.ExecuteScalar(sSql);
            string sDebtor = System.Convert.ToString(obj);
            object oCdate = e.DBSetting.ExecuteScalar("SELECT convert(Datetime,Isnull(UDF_CrDate,''1900-01-01''),102) FROM Debtor A Where A.AccNo=''" + sAccNo + "''");
            DateTime tCdate = System.Convert.ToDateTime(oCdate);
            object oExby = e.DBSetting.ExecuteScalar("SELECT UDF_ExportBy FROM Debtor A Where A.AccNo=''" + sAccNo + "''");
            string sExby=System.Convert.ToString(oExby);
            object oExDate = e.DBSetting.ExecuteScalar("SELECT convert(Datetime,Isnull(UDF_ExDate,''1900-01-01''),102) FROM Debtor A Where A.AccNo=''" + sAccNo + "''");
            DateTime tExDate = System.Convert.ToDateTime(oExDate);            
            if (sDebtor != "")
            {
                object oDate = e.DBSetting.ExecuteScalar("SELECT GETDATE()");
                DateTime dtime = AutoCount.Converter.ToDateTime(oDate);
                e.MasterRecord.UDF["ModDate"] = dtime;
                if (e.MasterRecord.UDF["CrDate"].ToString() == "")
                {
                    e.MasterRecord.UDF["CrDate"] = tCdate;
                }
                else if (e.MasterRecord.UDF["CrDate"].ToString() != "" && System.Convert.ToDateTime(e.MasterRecord.UDF["CrDate"]) != tCdate)
                {
                    e.MasterRecord.UDF["CrDate"] = tCdate;
                }
                e.MasterRecord.UDF["ExportBy"] = sExby;
                e.MasterRecord.UDF["ExDate"] = tExDate;
            }
            else 
            {
                object oDate = e.DBSetting.ExecuteScalar("SELECT GETDATE()");
                DateTime dtime = AutoCount.Converter.ToDateTime(oDate);
                e.MasterRecord.UDF["CrDate"] = dtime;
                e.MasterRecord.UDF["ModDate"] = dtime;
                    e.MasterRecord.UDF["ExportBy"] = "";
                    e.MasterRecord.UDF["ExDate"] = DBNull.Value;
            }
            e.MasterRecord.UDF["HasExport"] = "F";
        }', 4, N'008227bd-ed93-47d5-b758-6e7791b1261c' UNION ALL
SELECT N'DO', N'CS', N'using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
        /// <summary>
        /// Use this event to update data to database through the e.DBSetting, which will make your update in transaction control.
        /// </summary>
        /// <param name="e">The event argument</param>
        public void OnSave(AutoCount.Invoicing.Sales.DeliveryOrder.DeliveryOrderOnSaveEventArgs e)
        {
            for (int i = 0; i < e.MasterRecord.DetailCount; i++)
            {
                AutoCount.Invoicing.Sales.DeliveryOrder.DeliveryOrderDetailRecord dodtl = e.MasterRecord.GetDetailRecord(i);
                dodtl.UDF["SONO"] = dodtl.FromDocNo.ToString();
            }
        }
', 1, N'c83c1404-3e86-4bef-b0a3-f35bf2f68a42' UNION ALL
SELECT N'Item', N'CS', N'using AutoCount.Document;
using System;
using System.Data;
		/// <summary>
		/// Use this event to validate the data before save, if the data is invalid, you can assign your error message to e.ErrorMessage.
		/// </summary>
		/// <param name="e">The event argument</param>
		public void BeforeSave(AutoCount.Stock.Item.StockItemBeforeSaveEventArgs e)
		{
            string sSql = "SELECT Isnull(A.ItemCode,'''') FROM item A WHERE A.ItemCode=''" + e.MasterRecord.ItemCode + "''";
            object obj = e.DBSetting.ExecuteScalar(sSql);
            string sItem = System.Convert.ToString(obj);
            object oExby = e.DBSetting.ExecuteScalar("SELECT UDF_ExportBy FROM item A WHERE A.ItemCode=''" + e.MasterRecord.ItemCode + "''");
            string sExby = System.Convert.ToString(oExby);
            object oExDate = e.DBSetting.ExecuteScalar("SELECT convert(Datetime,Isnull(UDF_ExDate,''1900-01-01''),102) FROM item A WHERE A.ItemCode=''" + e.MasterRecord.ItemCode + "''");
            DateTime tExDate = System.Convert.ToDateTime(oExDate);
            if (sItem != "")
            {
                e.MasterRecord.UDF["ExportBy"] = sExby;
                e.MasterRecord.UDF["ExDate"] = tExDate;
            }
else
{
                    e.MasterRecord.UDF["ExportBy"] = "";
                    e.MasterRecord.UDF["ExDate"] = DBNull.Value;
}
            e.MasterRecord.UDF["HasExport"] = "F";
        }', 2, N'47ce0167-8a9f-4727-8f71-09727cc4e86d' UNION ALL
SELECT N'IV', N'CS', N'
using System;
using System.Data;
using AutoCount;
using AutoCount.Data;
using AutoCount.Authentication;
using AutoCount.Application;
using AutoCount.Invoicing.Sales.Invoice;

           /// <summary>
            /// Use this event to update data to database through the e.DBSetting, which will make your update in transaction control.
            /// </summary>
            /// <param name="e">The event argument</param>
            public void OnSave(AutoCount.Invoicing.Sales.Invoice.InvoiceOnSaveEventArgs e)
            {

                
                 for (int i = 0; i < e.MasterRecord.DetailCount; i++)
                {
                    AutoCount.Invoicing.Sales.Invoice.InvoiceDetailRecord ivdtl = e.MasterRecord.GetDetailRecord(i);
                    if (ivdtl.UDF["SONO"] != null && ivdtl.UDF["SONO"] != DBNull.Value)
                    {
                          object obj = e.DBSetting.ExecuteScalar("SELECT seq FROM SODtl A with(nolock) INNER JOIN SO B with(nolock) ON A.DocKey=B.DocKey WHERE B.DocNo=''" + ivdtl.UDF["SONO"] + "'' and A.ItemCode=''" + ivdtl.ItemCode + "''");
                          decimal dobj = AutoCount.Converter.ToDecimal(obj);
                          decimal dLine = (dobj / 16) * 1000;
                          ivdtl.DataRow["UDF_DtlKey"] = dLine;
                          //AutoCount.AppMessage.ShowErrorMessage(dLine.ToString());
                         // ivdtl.UDF["dtlkey"] = dLine;
                    }
                 }
            
            }
            /// <summary>
            /// Use this event to validate the data before save, if the data is invalid, you can assign your error message to e.ErrorMessage.
            /// </summary>
            /// <param name="e">The event argument</param>
            public void BeforeSave(AutoCount.Invoicing.Sales.Invoice.InvoiceBeforeSaveEventArgs e)
            {
                string sSql = "SELECT Isnull(A.DocNo,'''') FROM IV A WHERE A.DocNo=''" + e.MasterRecord.DocNo + "''";
                object obj = e.DBSetting.ExecuteScalar(sSql);
                string sItem = System.Convert.ToString(obj);
                object oExby = e.DBSetting.ExecuteScalar("SELECT UDF_ExportBy FROM IV A WHERE A.DocNo=''" + e.MasterRecord.DocNo + "''");
                string sExby = System.Convert.ToString(oExby);
                object oExDate = e.DBSetting.ExecuteScalar("SELECT convert(Datetime,Isnull(UDF_ExDate,''1900-01-01''),102) FROM IV A WHERE A.DocNo=''" + e.MasterRecord.DocNo + "''");
                DateTime tExDate = System.Convert.ToDateTime(oExDate);
                if (sItem != "")
                {
                    e.MasterRecord.UDF["ExportBy"] = sExby;
                    e.MasterRecord.UDF["ExDate"] = tExDate;
                }
                else
                {
                    e.MasterRecord.UDF["ExportBy"] = "";
                    e.MasterRecord.UDF["ExDate"] = DBNull.Value;
                }
                e.MasterRecord.UDF["HasExport"] = "F";
            }', 13, N'043f0637-65d3-41e5-b56b-14bb3eae14b0' UNION ALL
SELECT N'SalesAgent', N'CS', N'using System;
using System.Data;
using AutoCount;
using AutoCount.Data;
using AutoCount.Authentication;

       /// <summary>
        /// Use this event to validate the data before save, if the data is invalid, you can assign your error message to e.ErrorMessage.
        /// </summary>
        /// <param name="e">The event argument</param>
public void BeforeSave(AutoCount.GeneralMaint.SalesAgent.SalesAgentBeforeSaveEventArgs e)
        {
            //if (myUserSession != null)
            //{
            if (e.MasterRecord.UDF["JobTitle"].ToString() == "" || e.MasterRecord.UDF["Role"].ToString() == "" || e.MasterRecord.UDF["Manager"].ToString() == "" || e.MasterRecord.UDF["Team"].ToString() == "")
            {
                AutoCount.AppMessage.ShowErrorMessage("Job Title, Role, Manager dan Team wajib dilengkapi !!!");
                    e.AbortSave();
                    return;
            }
                string sAgent = System.Convert.ToString(e.MasterRecord.SalesAgent);
                string sSql = "SELECT Isnull(A.SalesAgent,'''') FROM SalesAgent A WHERE A.SalesAgent=''" + sAgent + "''";
                object obj = e.DBSetting.ExecuteScalar(sSql);
                string sSales = System.Convert.ToString(obj);
                object oCdate = e.DBSetting.ExecuteScalar("SELECT convert(Datetime,Isnull(UDF_CrDate,''1900-01-01''),102) FROM SalesAgent A Where SalesAgent=''"+ sAgent +"''");
                DateTime tCdate = System.Convert.ToDateTime(oCdate);
                object oExby = e.DBSetting.ExecuteScalar("SELECT UDF_ExportBy FROM SalesAgent A Where SalesAgent=''" + sAgent + "''");
                string sExby = System.Convert.ToString(oExby);
                object oExDate = e.DBSetting.ExecuteScalar("SELECT convert(Datetime,Isnull(UDF_ExDate,''1900-01-01''),102) FROM SalesAgent A Where SalesAgent=''" + sAgent + "''");
                DateTime tExDate = System.Convert.ToDateTime(oExDate);  
                //if (obj != null || obj != DBNull.Value || obj.ToString() != "")
                if (sSales != "" )
                {
                    object oDate = e.DBSetting.ExecuteScalar("SELECT GETDATE()");
                    DateTime dtime = AutoCount.Converter.ToDateTime(oDate);
                    e.MasterRecord.UDF["ModDate"] = dtime;
                    if (System.Convert.ToDateTime(e.MasterRecord.UDF["CrDate"]) != tCdate)
                    {
                        e.MasterRecord.UDF["CrDate"] = tCdate;
                    }
                    e.MasterRecord.UDF["ExportBy"] = sExby;
                    e.MasterRecord.UDF["ExDate"] = tExDate;
                }
                else //if (obj==null)
                {
                    object oDate = e.DBSetting.ExecuteScalar("SELECT GETDATE()");
                    DateTime dtime = AutoCount.Converter.ToDateTime(oDate);
                    e.MasterRecord.UDF["CrDate"] = dtime;
                    e.MasterRecord.UDF["ModDate"] = dtime;
                    e.MasterRecord.UDF["ExportBy"] = "";
                    e.MasterRecord.UDF["ExDate"] = DBNull.Value;
                }
                e.MasterRecord.UDF["HasExport"] = "F";
            //}
        }
', 4, N'41fea913-2a1a-4ed6-b736-18183d2eb66a'
COMMIT;
RAISERROR (N'[dbo].[UserScript]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO





