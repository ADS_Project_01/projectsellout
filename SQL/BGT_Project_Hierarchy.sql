
CREATE PROCEDURE [dbo].[BGT_Project_Hierarchy](@ParentProjNo nvarchar(50))
as
begin
WITH Project AS
(
    SELECT c1.ProjNo, c1.ParentProjNo, c1.Description, [level] = 1
    FROM dbo.[Project] c1
    WHERE c1.ParentProjNo=@ParentProjNo
    UNION ALL
    SELECT c2.ProjNo, c2.ParentProjNo, c2.Description, [level] = Project.[level] + 1
    FROM dbo.[Project] c2 INNER JOIN Project ON Project.ProjNo = c2.ParentProjNo-- where Project.ProjNo='E04'
)
SELECT Project.level, Project.ProjNo, ParentProjNo, REPLICATE('  ', Project.level - 1) + Project.Description as Description
FROM Project
OPTION (MAXRECURSION 0)
end

GO


