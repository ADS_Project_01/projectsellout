

CREATE view [dbo].[vBGT_BudgetActualReport1]
as
select
ROW_NUMBER() OVER(ORDER BY a.DocKey DESC) as RowNo,
[dbo].[F_BGT_Project_Hierarchy_Up](b.ProjNo,'Name Of Product') AS NameOfProduct,
[dbo].[F_BGT_Project_Hierarchy_Up](b.ProjNo,'Name of Indication') AS NameofIndication, 
[dbo].[F_BGT_Project_Hierarchy_Up](b.ProjNo,'Vendor Name') AS VendorName, 
[dbo].[F_BGT_Project_Hierarchy_Up](b.ProjNo,'Country Name') AS CountryName, 
[dbo].[F_BGT_Project_Hierarchy_Up](b.ProjNo,'Hospital Name') AS HospitalName,   
[dbo].[F_BGT_Project_Hierarchy_Up](b.ProjNo,'PI Name') AS PIName, 
a.ContractNo,a.ContractDate,a.DocNo as BudgetNo,a.DocDate as BudgetDate,a.ParentProjNo,a.RefDocNo,a.BudgetType,
b.*,
c.*,
d.*
 from 
BGT_Budget a
inner join
BGT_BudgetDtl b 
on a.DocKey=b.DocKey 
inner join
(SELECT AIP.DocNo AS PINo,aip.DocDate as PIDate,aip.CreditorCode as CreditorCode2,
AIPDTL.UDF_ActCode as ActivityCode2,ProjNo as ProjNo2,UDF_ActGroup as ActivityGroup2,
AIPDTL.UDF_Qty AS PIQty, AIPDTL.UDF_UnitCost as PIUnitCost,
AIPDTL.Amount as PIAmount FROM APInvoice AIP  WITH(NOLOCK)  inner join
APInvoiceDTL AIPDTL   WITH(NOLOCK)  
on AIP.DocKey=AIPDTL.DocKey where Cancelled='F') c
on b.ActivityCode=c.ActivityCode2 
and b.ProjNo=c.ProjNo2 
and c.ActivityGroup2=b.ActivityGroup
and c.CreditorCode2=a.CreditorCode
left outer join
(SELECT APCN.DocNo as CNNo,APCN.DocDate as CNDate,APCN.CreditorCode as CreditorCode3,
UDF_ActCode as ActivityCode3,UDF_ActGroup as ActivityGroup3,ProjNo as ProjNo3,APCNDTL.Amount as CNAmount FROM APCN  WITH(NOLOCK)  inner join
APCNDTL WITH(NOLOCK)  
on APCN.DocKey=APCNDTL.DocKey where Cancelled='F') d
on b.ActivityCode=d.ActivityCode3 
and b.ProjNo=d.ProjNo3 
and d.ActivityGroup3=b.ActivityGroup
and d.CreditorCode3=a.CreditorCode
WHERE
a.Cancelled='F'
GO
CREATE view [dbo].[vBGT_BudgetActualReport2]
as
select
ROW_NUMBER() OVER(ORDER BY a.DocKey DESC) as RowNo,
[dbo].[F_BGT_Project_Hierarchy_Up](b.ProjNo,'Name Of Product') AS NameOfProduct,
[dbo].[F_BGT_Project_Hierarchy_Up](b.ProjNo,'Name of Indication') AS NameofIndication, 
[dbo].[F_BGT_Project_Hierarchy_Up](b.ProjNo,'Vendor Name') AS VendorName, 
[dbo].[F_BGT_Project_Hierarchy_Up](b.ProjNo,'Country Name') AS CountryName, 
[dbo].[F_BGT_Project_Hierarchy_Up](b.ProjNo,'Hospital Name') AS HospitalName,   
[dbo].[F_BGT_Project_Hierarchy_Up](b.ProjNo,'PI Name') AS PIName, 
a.ContractNo,a.ContractDate,a.DocNo as BudgetNo,a.DocDate as BudgetDate,a.ParentProjNo,a.RefDocNo,a.BudgetType,
b.*,
c.*,
d.*,null as CNNo, null as CNDate
 from 
BGT_Budget a
inner join
BGT_BudgetDtl b 
on a.DocKey=b.DocKey 
inner join
(SELECT AIP.DocNo AS PINo,aip.DocDate as PIDate,aip.CreditorCode as CreditorCode2,
AIPDTL.UDF_ActCode as ActivityCode2,ProjNo as ProjNo2,UDF_ActGroup as ActivityGroup2,
AIPDTL.UDF_Qty AS PIQty, AIPDTL.UDF_UnitCost as PIUnitCost,
AIPDTL.Amount as PIAmount FROM APInvoice AIP  WITH(NOLOCK)  inner join
APInvoiceDTL AIPDTL   WITH(NOLOCK)  
on AIP.DocKey=AIPDTL.DocKey where Cancelled='F') c
on b.ActivityCode=c.ActivityCode2 
and b.ProjNo=c.ProjNo2 
and c.ActivityGroup2=b.ActivityGroup
and c.CreditorCode2=a.CreditorCode
left outer join
(SELECT APCN.CreditorCode as CreditorCode3,
UDF_ActCode as ActivityCode3,UDF_ActGroup as ActivityGroup3,ProjNo as ProjNo3
,SUM(APCNDTL.Amount) as CNAmount FROM APCN  WITH(NOLOCK)  inner join
APCNDTL WITH(NOLOCK)  
on APCN.DocKey=APCNDTL.DocKey where Cancelled='F'
GROUP BY APCN.CreditorCode,
UDF_ActCode,UDF_ActGroup,ProjNo
) d
on b.ActivityCode=d.ActivityCode3 
and b.ProjNo=d.ProjNo3 
and d.ActivityGroup3=b.ActivityGroup
and d.CreditorCode3=a.CreditorCode
WHERE
a.Cancelled='F'
GO
Create view [dbo].[v_BGT_BudgetDetail]
as
SELECT BGT_Budget.[DocKey]
      ,[DocNo]
      ,[DocDate]
      ,[ContractNo]
      ,[ContractDate]
      ,BGT_Budget.[Description]
      ,[CreditorCode]
      ,[CreditorName]
      ,BGT_Budget.[ParentProjNo]
      ,[BudgetType]
      ,BGT_Budget.[CurrencyCode]
      ,[Remark1]
      ,[Remark2]
      ,[Remark3]
      ,[Remark4]
      ,[Total]
      ,[PrintCount]
      ,[Cancelled]
      ,BGT_Budget.[LastModified]
      ,BGT_Budget.[LastModifiedUserID]
      ,BGT_Budget.[CreatedTimeStamp]
      ,BGT_Budget.[CreatedUserID]
      ,BGT_Budget.[ExternalLink]
      ,[RefDocNo]
      ,BGT_Budget.[LastUpdate]
      ,[CanSync]
      ,BGT_Budget.[Guid]
      ,[DtlKey]
      ,[Seq]
      ,[Numbering]
      ,BGT_BudgetDtl.[ActivityGroup]
      ,BGT_BudgetDtl.[ActivityCode]
      ,BGT_BudgetDtl.[Description] as DtlDescription
      ,[FurtherDescription]
      ,BGT_BudgetDtl.[ProjNo]
      ,BGT_BudgetDtl.[AccNo]
      ,[Status]
      ,[StatusDesc]
      ,BGT_BudgetDtl.[AccDescription]
      ,[Qty]
      ,BGT_BudgetDtl.[UOM]
      ,[UnitCost]
      ,[Amount]
      ,[PrintOut]
      ,Project.Description as ProjDesc
       FROM BGT_Budget
INNER JOIN BGT_BudgetDtl
ON BGT_Budget.DocKey = BGT_BudgetDtl.DocKey 
 LEFT OUTER JOIN BGT_Activity D ON (BGT_BudgetDtl.ActivityCode = D.ActivityCode)  
 LEFT OUTER JOIN 
 Project On (BGT_BudgetDtl.ProjNo = Project.ProjNo)  
 LEFT OUTER JOIN Creditor On (BGT_Budget.CreditorCode = Creditor.AccNo)
