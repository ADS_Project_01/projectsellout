ALTER TABLE SO_Setting
ADD	[UDF_HasExport] [nvarchar](1) NULL
GO
ALTER TABLE SO_Setting
ADD	[UDF_ExDate] [datetime] NULL
GO
ALTER TABLE SO_Setting
ADD	[UDF_ExportBy] [nvarchar](100) NULL