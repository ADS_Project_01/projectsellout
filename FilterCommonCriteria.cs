﻿using AutoCount.SearchFilter;
using AutoCount.Stock;
using AutoCount.Localization;
using System;
using System.Collections;
namespace ProjectSellOut
{
    [Serializable]
    public class FilterCommonCriteria : ReportCriteria
    {
        //protected AutoCount.SearchFilter.Filter myDateFilter = new AutoCount.SearchFilter.Filter("BG_BP", "DocDate", Localizer.GetString((Enum)StockStringId.DocDate, new object[0]), FilterControlType.Date);
        //protected AutoCount.SearchFilter.Filter myDocumentNoFilter = new AutoCount.SearchFilter.Filter("BG_BP", "DocNo", Localizer.GetString((Enum)StockStringId.DocNo, new object[0]), FilterControlType.None);
        protected AutoCount.SearchFilter.Filter myDealerCodeFilter = new AutoCount.SearchFilter.Filter("SampleDealer", "DealerCode", "DealerCode", FilterControlType.None);

        protected CancelledDocumentOption myIsPrintCancelled = CancelledDocumentOption.UnCancelled;
        protected int myReportTypeOption;

        public AutoCount.SearchFilter.Filter DealerFilter
        {
            get
            {
                return this.myDealerCodeFilter;
            }
        }

      
        public CancelledDocumentOption IsPrintCancelled
        {
            get
            {
                return this.myIsPrintCancelled;
            }
            set
            {
                this.myIsPrintCancelled = value;
            }
        }

        public int ReportTypeOption
        {
            get
            {
                return this.myReportTypeOption;
            }
            set
            {
                this.myReportTypeOption = value;
            }
        }

        protected override ArrayList GetFilterOptionsText()
        {
            ArrayList filterOptionsText = base.GetFilterOptionsText();
            string str1 = this.myDealerCodeFilter.BuildReadableText(false);
            if (str1.Length > 0)
                filterOptionsText.Add((object)Localizer.GetString((Enum)DealerStringId.DealerCode, new object[1]
        {
          (object) str1
        }));
            //string str2 = this.myDocumentNoFilter.BuildReadableText(false);
            //if (str2.Length > 0)
            //    filterOptionsText.Add((object)str2);
            return filterOptionsText;
        }
    }


}
