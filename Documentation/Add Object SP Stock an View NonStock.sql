
create PROCEDURE [dbo].[SO_StockQuery_P](@ToDate nvarchar(100))
AS
-- atas query non stock card
select Distributor_Code,Local_Product_Code,UOM,@ToDate as Stock_Date,0 as Stock_position,MAX(Creation_Date) AS Creation_Date, MAX(Modification_Date) AS Modification_Date from SO_ItemNonStock_View where Local_Product_Code Not in(select Local_Product_Code from SO_StockCard_View WHERE Stock_Date<=@ToDate) GROUP By Distributor_Code,Local_Product_Code,UOM
 union all  -- bawah query stock card
select * from(select Distributor_Code,Local_Product_Code,UOM,@ToDate as Stock_Date,case when SUM(Stock_position) <0 then 0 else SUM(Stock_position) end as Stock_position,MAX(Creation_Date) AS Creation_Date, MAX(Modification_Date) AS Modification_Date from SO_StockCard_View WHERE Stock_Date<=@ToDate GROUP By Distributor_Code,Local_Product_Code,UOM) result ORDER BY Local_Product_Code
GO
CREATE VIEW [dbo].[SO_ItemNonStock_View]
as
select Distributor_Code,Local_Product_Code,UOM,Stock_Date,Stock_position,Creation_Date, Modification_Date from(
SELECT case when LEN((select RegisterNo from Profile))>10 then SUBSTRING((select RegisterNo from Profile),1,10) else (select RegisterNo from Profile) end AS Distributor_Code,
case when LEN( MStock.ItemCode)>30 then SUBSTRING(MStock.ItemCode,1,30) else MStock.ItemCode end AS Local_Product_Code,
case when LEN(MStock.BaseUOM)>8 then SUBSTRING(MStock.BaseUOM,1,8)  else MStock.BaseUOM end AS UOM,
 MStock.CreatedTimeStamp AS Stock_Date,
0 AS Stock_position,
 MStock.CreatedTimeStamp  AS Creation_Date,
 MStock.LastModified  AS Modification_Date
FROM Item MStock WITH(nolock) WHERE ISNULL(MStock.UDF_PBPCode,'')<>'') result