
CREATE TRIGGER [dbo].[UpdateLastModified]
   ON [dbo].[Profile]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
UPDATE SO_Setting SET Modification_Date=GETDATE()


END

