USE [AED_AEDAUTOCOUNT2]
GO
/****** Object:  Table [dbo].[UserScript]    Script Date: 06/06/2020 16:11:54 ******/
INSERT [dbo].[UserScript] ([ScriptName], [Language], [Script], [LastUpdate], [Guid]) VALUES (N'Debtor', N'CS', N'using AutoCount.ARAP;
using AutoCount.ARAP.Debtor;
using AutoCount.Data;
using AutoCount.Authentication;
using AutoCount;
using AutoCount.Localization;
using System;
using System.Data;
using System.Windows.Forms;

        /// <summary>
        /// Use this event to validate the data before save, if the data is invalid, you can assign your error message to e.ErrorMessage.
        /// </summary>
        /// <param name="e">The event argument</param>
        public void BeforeSave(AutoCount.ARAP.Debtor.DebtorBeforeSaveEventArgs e)
		{
            if (e.MasterRecord.UDF["City"].ToString() == "" || e.MasterRecord.UDF["Provinsi"].ToString() == "" || e.MasterRecord.UDF["Country"].ToString() == "" || e.MasterRecord.UDF["Region"].ToString() == "" || e.MasterRecord.UDF["Subregion"].ToString() == "")
            {
                AutoCount.AppMessage.ShowErrorMessage("City, Province, Country, Region dan Subregion wajib dilengkapi !!!");
                e.AbortSave();
                return;
            }
            string sAccNo = System.Convert.ToString(e.MasterRecord.AccNo);
            string sSql = "SELECT Isnull(A.AccNo,'''') FROM Debtor A WHERE A.AccNo=''" + sAccNo + "''";
            object obj = e.DBSetting.ExecuteScalar(sSql);
            string sDebtor = System.Convert.ToString(obj);
            object oCdate = e.DBSetting.ExecuteScalar("SELECT convert(Datetime,Isnull(UDF_CrDate,''1900-01-01''),102) FROM Debtor A Where A.AccNo=''" + sAccNo + "''");
            DateTime tCdate = System.Convert.ToDateTime(oCdate);
            if (sDebtor != "")
            {
                object oDate = e.DBSetting.ExecuteScalar("SELECT GETDATE()");
                DateTime dtime = AutoCount.Converter.ToDateTime(oDate);
                e.MasterRecord.UDF["ModDate"] = dtime;
                if (e.MasterRecord.UDF["CrDate"].ToString() == "")
                {
                    e.MasterRecord.UDF["CrDate"] = tCdate;
                }
                else if (e.MasterRecord.UDF["CrDate"].ToString() != "" && System.Convert.ToDateTime(e.MasterRecord.UDF["CrDate"]) != tCdate)
                {
                    e.MasterRecord.UDF["CrDate"] = tCdate;
                }
            }
            else 
            {
                object oDate = e.DBSetting.ExecuteScalar("SELECT GETDATE()");
                DateTime dtime = AutoCount.Converter.ToDateTime(oDate);
                e.MasterRecord.UDF["CrDate"] = dtime;
	e.MasterRecord.UDF["ModDate"] = dtime;
            }
        }', 1, N'36c56879-ada7-408d-8183-ba8eeee3ac89')
INSERT [dbo].[UserScript] ([ScriptName], [Language], [Script], [LastUpdate], [Guid]) VALUES (N'IV', N'CS', N'using System;
using System.Data;
using AutoCount;
using AutoCount.Data;
using AutoCount.Authentication;
using AutoCount.Application;
using AutoCount.Invoicing.Sales.Invoice;

private AutoCount.Authentication.UserSession myUserSession;
private AutoCount.Data.DBSetting mydbsetting;

            public void OnFormInitialize(AutoCount.Invoicing.Sales.Invoice.FormInvoiceEntry.FormInitializeEventArgs e)
            {
                //AutoCount.Data.DBSetting dbSetting = new AutoCount.Data.DBSetting(DBServerType.SQL2000, local, dbName);
//              string userid = AutoCount.Authentication.UserSession.CurrentUserSession.LoginUserID;
                this.myUserSession = UserSession.CurrentUserSession;
                this.mydbsetting = this.myUserSession.DBSetting;
            }

           /// <summary>
            /// Occurs whenever partial transfer occur
            /// </summary>
            /// <param name="e">The event argument</param>
            public void OnPartialTransfer(AutoCount.Invoicing.Sales.Invoice.InvoicePartialTransferEventArgs e)
            {


                string sDO = e.CurrentDetailRecord.FromDocNo;
                string sItem = e.CurrentDetailRecord.ItemCode;
                object obj = myUserSession.DBSetting.ExecuteScalar("SELECT FromDocNo FROM DODtl A with(nolock) INNER JOIN DO B with(nolock) ON A.DocKey=B.DocKey WHERE B.DocNo=''" + sDO + "'' and A.ItemCode =''"+ sItem +"''");
                if (obj != null || obj != DBNull.Value)
                {
                    e.CurrentDetailRecord.UDF["SONO"] = obj.ToString();
                }

            }

            /// <summary>
            /// Occurs whenever a document is full transfer using option 1
            /// </summary>
            /// <param name="e">The event argument</param>
            public void OnFullTransferOption1(AutoCount.Invoicing.Sales.Invoice.InvoiceFullTransferOption1EventArgs e)
            {
                string sDO = e.CurrentDetailRecord.FromDocNo;
                string sItem = e.CurrentDetailRecord.ItemCode;
                object obj = myUserSession.DBSetting.ExecuteScalar("SELECT FromDocNo FROM DODtl A with(nolock) INNER JOIN DO B with(nolock) ON A.DocKey=B.DocKey WHERE B.DocNo=''" + sDO + "'' and A.ItemCode =''" + sItem + "''");
                if (obj != null || obj != DBNull.Value)
                {
                    e.CurrentDetailRecord.UDF["SONO"] = obj.ToString();
                }
            }

            /// <summary>
            /// Use this event to validate the data before save, if the data is invalid, you can assign your error message to e.ErrorMessage.
            /// </summary>
            /// <param name="e">The event argument</param>
            public void BeforeSave(AutoCount.Invoicing.Sales.Invoice.InvoiceBeforeSaveEventArgs e)
            {
                for (int i = 0; i < e.MasterRecord.DetailCount; i++)
                {
                    AutoCount.Invoicing.Sales.Invoice.InvoiceDetailRecord ivdtl = e.MasterRecord.GetDetailRecord(i);
                    if (ivdtl.UDF["SONO"] != null || ivdtl.UDF["SONO"] != DBNull.Value)
                    {
                        object obj = myUserSession.DBSetting.ExecuteScalar("SELECT seq FROM SODtl A with(nolock) INNER JOIN SO B with(nolock) ON A.DocKey=B.DocKey WHERE B.DocNo=''" + ivdtl.UDF["SONO"] + "'' and A.ItemCode=''" + ivdtl.ItemCode + "''");
                        decimal dobj=AutoCount.Converter.ToDecimal(obj);
                        decimal dLine=(dobj/16)*1000;
                        ivdtl.UDF["dtlkey"] = dLine;
                    }
                }
            }
', 1, N'4fa3d1d1-404a-443b-8e3d-c210f5168e49')
INSERT [dbo].[UserScript] ([ScriptName], [Language], [Script], [LastUpdate], [Guid]) VALUES (N'SalesAgent', N'CS', N'using System;
using System.Data;
using AutoCount;
using AutoCount.Data;
using AutoCount.Authentication;

       /// <summary>
        /// Use this event to validate the data before save, if the data is invalid, you can assign your error message to e.ErrorMessage.
        /// </summary>
        /// <param name="e">The event argument</param>
        public void BeforeSave(AutoCount.GeneralMaint.SalesAgent.SalesAgentBeforeSaveEventArgs e)
        {
            //if (myUserSession != null)
            //{
            if (e.MasterRecord.UDF["JobTitle"].ToString() == "" || e.MasterRecord.UDF["Role"].ToString() == "" || e.MasterRecord.UDF["Manager"].ToString() == "" || e.MasterRecord.UDF["Team"].ToString() == "")
            {
                AutoCount.AppMessage.ShowErrorMessage("Job Title, Role, Manager dan Team wajib dilengkapi !!!");
                    e.AbortSave();
                    return;
            }
                string sAgent = System.Convert.ToString(e.MasterRecord.SalesAgent);
                string sSql = "SELECT Isnull(A.SalesAgent,'''') FROM SalesAgent A WHERE A.SalesAgent=''" + sAgent + "''";
                object obj = e.DBSetting.ExecuteScalar(sSql);
                string sSales = System.Convert.ToString(obj);
                object oCdate = e.DBSetting.ExecuteScalar("SELECT convert(Datetime,Isnull(UDF_CrDate,''1900-01-01''),102) FROM SalesAgent A Where SalesAgent=''"+ sAgent +"''");
                DateTime tCdate = System.Convert.ToDateTime(oCdate);
                if (sSales != "" )
                {
                    object oDate = e.DBSetting.ExecuteScalar("SELECT GETDATE()");
                    DateTime dtime = AutoCount.Converter.ToDateTime(oDate);
                    e.MasterRecord.UDF["ModDate"] = dtime;
                    if (System.Convert.ToDateTime(e.MasterRecord.UDF["CrDate"]) != tCdate)
                    {
                        e.MasterRecord.UDF["CrDate"] = tCdate;
                    }
                }
                else                 {
                    object oDate = e.DBSetting.ExecuteScalar("SELECT GETDATE()");
                    DateTime dtime = AutoCount.Converter.ToDateTime(oDate);
                    e.MasterRecord.UDF["CrDate"] = dtime;
	e.MasterRecord.UDF["ModDate"] = dtime;
                }
        }
', 1, N'93211d28-5db7-48c0-841e-a4e1e1bb2fed')
