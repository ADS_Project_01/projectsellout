create Function[dbo].[SO_StatusIV_F](@FromDocType nvarchar(5),@FromDocNo nvarchar(30),@ItemCode nvarchar(50),@Seq bigint)
RETURNS nvarchar(30)
AS BEGIN
Declare @CountIncomplete bigint
Declare @Status nvarchar(30)
set @CountIncomplete=0
    BEGIN
   if @FromDocType='DO'
		BEGIN
		select @CountIncomplete=COUNT(*) from DR with(NOLOCK) 
		INNER JOIN 
		DRDTL with(NOLOCK)
		 ON DR.DocKey=DRDTL.DocKey
		  WHERE Cancelled='F' AND FromDocNo=@FromDocNo and ItemCode=@ItemCode;
		END
	ELSE IF @FromDocType='SO'
		BEGIN
		select @CountIncomplete=COUNT(*) from SODTL with(NOLOCK) 		
		  WHERE FromDocNo=@FromDocNo and Qty-TransferedQty>0 and ItemCode=@ItemCode;
		END	 
		
		IF @CountIncomplete=0
		begin
			SET @Status='Complete';
		end
		else
		begin
			SET @Status='Not Complete';
		end
		
    end
    return @Status;
   END;
   GO
ALTER VIEW [dbo].[SO_Invoice_View]
as
SELECT 
 case when LEN((select RegisterNo from Profile))>10 then SUBSTRING((select RegisterNo from Profile),1,10) else (select RegisterNo from Profile) end AS Distributor_Code,
 case when LEN( DebtorCode)>30 then SUBSTRING(DebtorCode,1,30) else DebtorCode end AS Bill_To_Code,
 case when LEN( SalesAgent)>10 then SUBSTRING(SalesAgent,1,10) else SalesAgent end AS Employee_Num,
 case when LEN( ShipVia)>10 then SUBSTRING(ShipVia,1,10) else ShipVia end AS Ship_To_Code,
 case when LEN( UDF_SONO)>30 then SUBSTRING(UDF_SONO,1,30) else UDF_SONO end AS Sales_Order_Number,
UDF_DtlKey  AS Sales_Order_Line_Number,
 case when LEN( DocNo)>30 then SUBSTRING(DocNo,1,30) else DocNo end AS Invoice_Number,
 --Seq/16  AS Invoice_Line_Number, Remarks by Dwi 2020-06-05
(Seq/16)*1000  AS Invoice_Line_Number,
DocDate AS Invoice_Date,
 case when LEN( DisplayTerm)>10 then SUBSTRING(DisplayTerm,1,10) else DisplayTerm end AS Payment_Term,
DocDate AS Sale_Date,
DeliveryDate AS Shipment_Date,
 case when LEN( ItemCode)>30 then SUBSTRING(ItemCode,1,30) else ItemCode end AS Local_Product_Code,
UnitPrice AS Unit_Price,
null as Discount1,
null as Discount2,
null as Discount3,
null as Discount4,
null as Discount5,
NetTotal as Net_Sales,
Total as Gross_Sales,
 case when LEN( UOM)>8 then SUBSTRING(UOM,1,8) else UOM end AS Local_UOM,
Qty as Local_Quantity,
'Sales' as Transaction_Type,
case 
Cancelled
when 
'F' THEN
case TransferedQty when 0 then [dbo].[SO_StatusIV_F](IVDTL.FromDocType,IVDTL.FromDocNo,IVDTL.ItemCode,(UDF_DtlKey/1000)*16)
else 'Not Complete' 
 end
  ELSE 'Not Complete' END as Status,
CreatedTimeStamp as Creation_Date,
LastModified as Modification_Date,
null as CRM_REF_NUM
from IV WITH (NOLOCK)
inner join IVDTL WITH (NOLOCK) on iv.DocKey=IVDTL.DocKey
  -- WHERE Cancelled='F'

UPDATE RPA_WO
SET
Status='Planned'


UPDATE RPA_WOProduct
SET
Status='Planned'
select * from ItemUOM

select * from RPA_TransRawMaterial

select * from RPA_WOProduct

SELECT d.*,case when c.ProductRatio>1 then (25*b.Rate)/ProductRatio else ((25*b.Rate)/h.Qty)*c.Rate end as QtyRM FROM RPA_WO a with(nolock) inner join RPA_WOProduct b with(nolock) on a.DocKey=b.DocKey inner join RPA_WODtl  c with(nolock) on a.DocKey=c.DocKey and c.FromDocDtlKey=b.DtlKey inner join RPA_ItemBOM  h with(nolock) on h.BOMCode=c.BOMCode inner join (select rm.* from RPA_RMDTL rm with(nolock) inner join RPA_RM rmd with(nolock) on rm.DocKey=rmd.DocKey where Cancelled='F') d   on  d.FromDocDtlKey=c.DtlKey and d.FromDocNo=a.DocNo where a.DocNo='WO-00004' and b.DtlKey='7706' and ISNULL(c.Level,0)=0 and b.ItemCode='RW00003'
