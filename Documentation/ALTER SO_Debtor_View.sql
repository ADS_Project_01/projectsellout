
ALTER VIEW [dbo].[SO_Debtor_View]
as
SELECT
case when LEN( AccNo)>10 then SUBSTRING(AccNo,1,10) else AccNo end AS End_Customer_Code,
case when LEN(CompanyName)>20 then SUBSTRING(CompanyName,1,20) else CompanyName end AS End_Customer_Name,
case when LEN(DebtorType)>10 then SUBSTRING(DebtorType,1,10) else DebtorType end AS Customer_Type,
case when LEN(Address1)>150 then SUBSTRING(Address1,1,150) else Address1 end AS Address1,
case when LEN(Address2)>150 then SUBSTRING(Address2,1,150) else Address2 end AS Address2,
case when LEN(Address3)>150 then SUBSTRING(Address3,1,150) else Address3 end AS Address3,
UDF_City AS City,
UDF_Provinsi AS State_Province,
UDF_Country AS Country,
case when LEN(PostCode)>5 then SUBSTRING(PostCode,1,5) else PostCode end AS Zip_Code,
case when LEN(Debtor.Attention)>30 then SUBSTRING(Attention,1,30) else Attention end AS Contact_Person,
case when LEN(Phone1)>30 then SUBSTRING(Phone1,1,30) else Phone1 end AS Telephone_Number,
case when LEN(EmailAddress)>50 then SUBSTRING(EmailAddress,1,50) else EmailAddress end AS Email,
case when LEN((select Active from SO_Setting))>10 then SUBSTRING((select Active from SO_Setting),1,10) else (select Active from SO_Setting) end AS Active,
case when LEN((select Region_Code from SO_Setting))>10 then SUBSTRING((select Region_Code from SO_Setting),1,10) else (select Region_Code from SO_Setting) end AS Region_Code,
case when LEN((select SubRegion_Code from SO_Setting))>10 then SUBSTRING((select SubRegion_Code from SO_Setting),1,10) else (select SubRegion_Code from SO_Setting) end AS SubRegion_Code,
UDF_CrDate as Creation_Date,
UDF_ModDate as Modification_Date
FROM Debtor

GO


