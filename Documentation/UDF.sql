USE [AED_AEDAUTOCOUNT2]
GO
/****** Object:  Table [dbo].[UDFList]    Script Date: 06/06/2020 16:07:21 ******/
/****** Object:  Table [dbo].[UDFLayout]    Script Date: 06/06/2020 16:07:21 ******/
/****** Object:  Table [dbo].[UDF]    Script Date: 06/06/2020 16:07:21 ******/
SET IDENTITY_INSERT [dbo].[UDF] ON
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (1, N'PO', N'Name1', 0, N'0', N'Name1', N'ListName=
Required=False
Unique=False
Size=50
', N'fefe6d81-7efc-46eb-a7c9-0f1079e47c36')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (2, N'PODTL', N'Name1', 0, N'0', N'Name1', N'ListName=
Required=False
Unique=False
Size=50
', N'5e1f1e7f-6211-45e4-b97c-abeb61eccf57')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (3, N'IVDTL', N'SONO', 0, N'0', N'No. SO', N'Size=30
Required=False
Unique=False
ListName=
', N'bda0eae5-0e43-4ae0-9e03-7cf20204a13a')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (4, N'IVDTL', N'DtlKey', 1, N'2', N'SO Line Number', N'Required=False
Unique=False
', N'3e046688-d775-4ef0-9e1f-c93d9934505f')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (5, N'IV', N'HasExport', 0, N'4', N'Has Export', N'Unique=False
Required=False
', N'd1c53f1c-c1f9-4642-96fe-c9af6dfc9c4d')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (6, N'IV', N'ExDate', 2, N'3', N'ExDate', N'DateType=DateTime
Unique=False
Required=False
', N'548992c0-8143-4efe-82df-7ec652d15dbd')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (7, N'IV', N'ExportBy', 2, N'0', N'ExportBy', N'ListName=
Required=False
Unique=False
Size=100
', N'3fbf62d0-5499-4ec0-95f0-e52328815576')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (8, N'Debtor', N'City', 0, N'0', N'City', N'Size=30
Required=False
Unique=False
ListName=
', N'c120c82c-0937-43d4-8fdb-cfe810e271d6')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (9, N'Debtor', N'Provinsi', 1, N'0', N'Province', N'Size=30
Required=False
Unique=False
ListName=
', N'83357816-a2ad-424d-803b-34787207e940')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (10, N'Debtor', N'Country', 2, N'0', N'Country', N'Size=9
Required=False
Unique=False
ListName=
', N'483a1d5e-876a-4e43-aa73-ea6c61876659')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (11, N'Debtor', N'Region', 3, N'0', N'Region', N'Size=30
Required=False
Unique=False
ListName=
', N'ccbe069d-0f46-4a94-bd88-1fc86f74ab57')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (12, N'Debtor', N'Subregion', 4, N'0', N'Subregion', N'Size=30
Required=False
Unique=False
ListName=
', N'71ae6db7-787c-4e55-ac27-f07e3d8c68a8')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (13, N'SalesAgent', N'JobTitle', 0, N'0', N'JobTitle', N'ListName=
Required=False
Unique=False
Size=30
', N'fa5b0c1e-a6b9-4368-959b-4bfafa10c71b')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (14, N'SalesAgent', N'Role', 1, N'0', N'Role', N'ListName=
Required=False
Unique=False
Size=30
', N'535aa0f4-5533-4bf9-9555-2c46c9d2ad1e')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (15, N'SalesAgent', N'Manager', 2, N'0', N'Manager', N'ListName=
Required=False
Unique=False
Size=30
', N'ccbd5bb4-f96f-4664-82ff-2e38dba84958')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (16, N'SalesAgent', N'Team', 3, N'0', N'Team', N'ListName=
Required=False
Unique=False
Size=30
', N'70361ac0-00a1-4b7d-b8b0-686768f151e1')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (17, N'SalesAgent', N'CrDate', 5, N'3', N'Creation Date', N'DateType=DateTime
Unique=False
Required=False
', N'cf63e5b4-155a-4db4-a63c-9337068f6207')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (18, N'SalesAgent', N'ModDate', 5, N'3', N'ModDate', N'DateType=DateTime
Unique=False
Required=False
', N'af3dae91-c90f-4859-90fd-c1a11f683eeb')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (19, N'Debtor', N'CrDate', 6, N'3', N'Create Date', N'DateType=Date
Unique=False
Required=False
', N'd5084e3e-0c51-4914-9663-c8a63c01db77')
INSERT [dbo].[UDF] ([AutoKey], [TableName], [FieldName], [Seq], [FieldType], [Caption], [Properties], [Guid]) VALUES (20, N'Debtor', N'ModDate', 7, N'3', N'Modification Date', N'DateType=Date
Unique=False
Required=False
', N'c6ae985b-9f1e-46ae-ab2f-52e029f8f4e3')
SET IDENTITY_INSERT [dbo].[UDF] OFF
