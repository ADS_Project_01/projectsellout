using System;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using AutoCount.Data;
using AutoCount.Authentication;
namespace ProjectSellOut
{
	/// <summary>
	/// Summary description for MainEntry.
	/// </summary>
	public class MainEntry
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
            // Here you must provide a dbSetting which points to your AutoCount 2006 database.
            //DBSetting dbSetting = new DBSetting(DBServerType.SQL2000, @"(local)\A2006", "AED_NEW_BG");
            //AutoCount.MainEntry.MainEntry.SubProjectStartup(dbSetting);
            
            // untuk memunculkan form login
            AutoCount.MainEntry.Startup.Default.SubProjectStartupWithLogin("", "");


            DBSetting dbSetting = UserSession.CurrentUserSession.DBSetting;
            AutoCount.CompanyProfile profile = AutoCount.CompanyProfile.Create(dbSetting);

            AutoCount.PlugIn.BeforeLoadArgs e = new AutoCount.PlugIn.BeforeLoadArgs(UserSession.CurrentUserSession, Guid.NewGuid(), System.Windows.Forms.Application.StartupPath, profile);

            //if (Plugin.BeforeLoad(e))
			{
                FormMain form = new FormMain(dbSetting);
				System.Windows.Forms.Application.Run(form);
			}
		}
	}
}
