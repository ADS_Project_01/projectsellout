using System;

namespace ProjectSellOut
{
	/// <summary>
	/// AccessRightConst class defines a lsit of command constant value for use in BCE.DBUtils.AccessRight.IsAccessible method.
	/// </summary>
	public class AccessRightConst
	{
        public const string SO = "SO";
        public const string SO_GEN = "SO_GEN";
        /// <summary>
       
      
        /// <summary>
        /// 
        /// General Setting
        /// </summary>
        public const string SO_SETTING = "SO_SETTING";
        /// <summary>
        /// 
        /// General Setting Show
        /// </summary>
        public const string SO_SETTING_SHOW = "SO_SETTING_SHOW";
        /// <summary>
        /// 
        /// General Setting Open
        /// </summary>
        public const string SO_SETTING_OPEN = "SO_SETTING_OPEN";
       
      

        /// <summary>
        /// 
        ///Stock Receive Maintenance
        /// </summary>
        public const string SO_EXPORT = "SO_EXPORT";
      

        /// <summary>
        /// OpenStock Receive Maintenance
        /// </summary>
        public const string SO_EXPORT_OPEN = "SO_EXPORT_OPEN";

        /// <summary>
        /// ShowStock Receive Maintenance
        /// </summary>
        public const string SO_EXPORT_SHOW = "SO_EXPORT_SHOW";

      

    }
}
