﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
namespace ProjectSellOut.GeneralMaintenance
{
    public partial class Register : Form
    {
        public bool IsRegistered = false;
        GLib.GExecuteScalar es = new GLib.GExecuteScalar();

        public Register()
        {
            InitializeComponent();
            tbProductID.Text = GLib.G.GetProductID(new AssemblyInfoEntity().Product);
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            decimal dCode;
            dCode = GLib.G.Encrypt(GLib.G.GetProductID(new AssemblyInfoEntity().Product));
            if (dCode == ntbProductKey.DecimalValue)
            {
                es.Query("UPDATE SO_Setting SET rk=" + ntbProductKey.DecimalValue.ToString());
                GLib.G.MsgBox("SUCCESSFULLY REGISTERED!", "Registration");
                IsRegistered = true;
                Close();
            }
            else
            {
                GLib.G.MsgBox("Invalid Key!", "Registration");
            }
        }
    }
}
