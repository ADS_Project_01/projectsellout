﻿using AutoCount.ActivityStream;
using AutoCount.Application;
using AutoCount.Authentication;
using AutoCount.Controller;
using AutoCount.Controls;
using AutoCount.Controls.FilterUI;
using AutoCount.Data;
using AutoCount.Forms;
using AutoCount.LicenseControl;
using AutoCount.Localization;
using AutoCount.Options;
using AutoCount.Report;
using AutoCount.Scripting;
using AutoCount.SearchFilter;
using AutoCount.Serialization;
using AutoCount.Settings;
using AutoCount.UDF;
using AutoCount.Utils;
using AutoCount.XtraUtils;
using AutoCount.Invoicing;
using AutoCount.Invoicing.Sales;
using AutoCount.Invoicing.Sales.Invoice;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.Customization;
using DevExpress.XtraTab;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using DocumentFormat.OpenXml;
using System.IO;
using System.Net;
using System.Xml.Serialization;
using GLib;
namespace ProjectSellOut.GeneralMaintenance
{
    [AutoCount.PlugIn.MenuItem("General Settings", 2, false, "SO_SETTING_SHOW", "SO_SETTING_OPEN")]
    [AutoCount.Application.SingleInstanceThreadForm]
    //[AutoCount.PlugIn.MenuItem("General Settings", BeginNewGroup = false, MenuOrder = 1, OpenAccessRight = "SO_SETTING_OPEN", ParentBeginNewGroup = false, ParentMenuCaption = "General Maintenance", ParentMenuOrder = 1, ShowAsDialog = false, VisibleAccessRight = "SO_SETTING_SHOW")]
    // [AutoCount.AutoCount.PlugIn.MenuItem("General Settings", 1, false, "RPA_SETTINGS_SHOW", "RPA_SETTINGS_OPEN")]


    //[AutoCount.AutoCount.PlugIns.PlugInsEntryPoint("General Settings", 1, false, "BG_GENERAL_SETTINGS", "BG_GENERAL_SETTINGS", "Maintenance", 1, false)]
    public partial class GeneralSettings : DevExpress.XtraEditors.XtraForm
    {
        bool bFirstTime;
        //private GLib.GExecuteScalar es = new GLib.GExecuteScalar();
        //private GLib.GDataTable gdt = new GLib.GDataTable();
        DataTable dtSettings = new DataTable();
        protected DBSetting dbSetting;
        protected UserSession myUserSession;
        private string sLanguage;
        private AssemblyInfoEntity info;
        public GeneralSettings(UserSession userSession)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Location = new Point((Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                          (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
            //this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            info = new AssemblyInfoEntity();
            bFirstTime = true;
            myUserSession = userSession;
            dbSetting = userSession.DBSetting;
            dtSettings=dbSetting.GetDataTable("SELECT * FROM SO_Setting",false,"");
            dtSettings.TableName = "SO_Setting";
            sLanguage = "en-US";
            //if (AutoCount.Application.ThreadForm.UILanguage.ToString() == "Indonesian")
            //{
            //    sLanguage = "id";
            //   // Thread.CurrentThread.CurrentUICulture = new CultureInfo("id");
            //}
            InitializeComponent();



            //txtLocation_Type.EditValue = dtSettings.Rows[0]["Location_Type"]!=null ? dtSettings.Rows[0]["Location_Type"].ToString():null;
            //txtRegion_Code.EditValue = dtSettings.Rows[0]["Region_Code"] != null ? dtSettings.Rows[0]["Region_Code"].ToString() : null;
            //txtActive.EditValue = dtSettings.Rows[0]["Active"] != null ? dtSettings.Rows[0]["Active"].ToString() : null;
            //txtSales_Person_Code.EditValue = dtSettings.Rows[0]["Sales_Person_Code"] != null ? dtSettings.Rows[0]["Sales_Person_Code"].ToString() : null;
            //txtSubRegion_Code.EditValue = dtSettings.Rows[0]["SubRegion_Code"] != null ? dtSettings.Rows[0]["SubRegion_Code"].ToString() : null;
            
            //txtCountry.EditValue = dtSettings.Rows[0]["Country"] != null ? dtSettings.Rows[0]["Country"].ToString() : null;
            //txtState_Province.EditValue = dtSettings.Rows[0]["State_Province"] != null ? dtSettings.Rows[0]["State_Province"].ToString() : null;
            ////  deCreation_Date.DateTime = dtSettings.Rows[0]["Creation_Date"] != DBNull.Value ? AutoCount.Converter.ToDateTime(dtSettings.Rows[0]["Creation_Date"].ToString()) : DateTime.MinValue;

            txtLocation_Type.DataBindings.Clear();
            txtLocation_Type.DataBindings.Add("EditValue", dtSettings, "Location_Type");
            txtRegion_Code.DataBindings.Clear();
            txtRegion_Code.DataBindings.Add("EditValue", dtSettings, "Region_Code");
            txtSubRegion_Code.DataBindings.Clear();
            txtSubRegion_Code.DataBindings.Add("EditValue", dtSettings, "SubRegion_Code");
            txtSales_Person_Code.DataBindings.Clear();
            txtSales_Person_Code.DataBindings.Add("EditValue", dtSettings, "Sales_Person_Code");
            txtActive.DataBindings.Clear();
            txtActive.DataBindings.Add("EditValue", dtSettings, "Active");
            txtCountry.DataBindings.Clear();
            txtCountry.DataBindings.Add("EditValue", dtSettings, "Country");
            txtState_Province.DataBindings.Clear();
            txtState_Province.DataBindings.Add("EditValue", dtSettings, "State_Province");
            deModification_Date.DataBindings.Clear();
            deModification_Date.DataBindings.Add("EditValue", dtSettings, "Modification_Date");// ".EditValue = dtSettings.Rows[0]["Modification_Date"] != DBNull.Value ? dtSettings.Rows[0]["Modification_Date"].ToString() : null;
            deCreation_Date.DataBindings.Clear();
            deCreation_Date.DataBindings.Add("EditValue", dtSettings, "Creation_Date");
            txtPath.DataBindings.Clear();
            txtPath.DataBindings.Add("EditValue", dtSettings, "PathString");
            

            bool bCanEditIsJournal = true;

            if (GLib.G.IsRegistered("SO_Setting", info.Product))
                btnActivate.Enabled = false;
            else
                btnActivate.Enabled = true;

            if (GLib.G.IsRegistered("SO_Setting", info.Product))
            {
                btnTry.Enabled = false;
                btnActivate.Enabled = false;
                lblExpiry.Text = "";
            }
            else
            {
                btnTry.Enabled = true;
                btnActivate.Enabled = true;
                // Trial
                decimal d = System.Convert.ToDecimal(dtSettings.Rows[0]["rkd"]);
                string sExpiry = "This plug-ins will be expired on " + GLib.G.DecryptDateTime(d).ToLongDateString();
                lblExpiry.Text = sExpiry;
            }

        }

        private void GeneralSettings_Activated(object sender, EventArgs e)
        {
            if (bFirstTime)
            {
                bFirstTime = false;
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            dtSettings.Rows[0]["PathString"] = txtPath.Text;
            if (AutoCount.Converter.ToInt32(dtSettings.Rows[0]["UpdateCount"]) < 2)
            {
                dtSettings.Rows[0]["Creation_Date"] = myUserSession.DBSetting.GetServerTime();
            }
            dtSettings.Rows[0]["UpdateCount"] = AutoCount.Converter.ToInt32(dtSettings.Rows[0]["UpdateCount"]) + 1;
            dtSettings.Rows[0]["Modification_Date"] = myUserSession.DBSetting.GetServerTime();

            // dbSetting.ExecuteNonQuery("UPDATE docnoformat SET NextNumber=? where DocType='BG' AND Name='BG_DO'", (object)AutoCount.Data.Convert.ToInt16(txtNextNumber.EditValue));
            DataRow drsetting = dbSetting.GetFirstDataRow("select * from so_setting", "");
            if (drsetting != null)
            {
                dtSettings.Rows[0]["rk"] = drsetting["rk"];
                dtSettings.Rows[0]["rkd"] = drsetting["rkd"];
            }

            dbSetting.ExecuteNonQuery(GLib.G.SQLUPDATECommand(dtSettings.Rows[0], false, ""), "");
            //dbSetting.ExecuteNonQuery("UPDATE SO_Setting SET Location_Type='" + txtLocation_Type.EditValue.ToString() + "'," +
            //    "Region_Code='" + txtRegion_Code.EditValue.ToString()+ "',"+
            //    "Active = '" + txtActive.EditValue.ToString() + "', " +
            //    "Sales_Person_Code='" + txtSales_Person_Code.EditValue.ToString() + "'," +
            //     "SubRegion_Code = '" + txtSubRegion_Code.EditValue.ToString() + "'") ;
            // AutoCount.AutoCount.GeneralMaint.DocumentNoFormat.DocumentNoMaintenanceSQL DocNoFormat = AutoCount.AutoCount.GeneralMaint.DocumentNoFormat.DocumentNoMaintenanceSQL.CreateDocumentNoMaint(dbSetting);
            // AutoCount.AutoCount.GeneralMaint.DocumentNoFormat.DialogDocumentNoMaintenance form =new AutoCount.AutoCount.GeneralMaint.DocumentNoFormat.DialogDocumentNoMaintenance(dbSetting);
            // form.Show();
            //DataTable myDocNoFormatTable = DocNoFormat.LoadDocNoFormatList("BG");
            //myDocNoFormatTable.Rows[0]["Format"]=txtEditDocNoFormat.EditValue;
            //myDocNoFormatTable.Rows[0]["NextNumber"] =txtNextNumber.EditValue;
            //DocNoFormat.Save(myDocNoFormatTable);




            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnActivate_Click(object sender, EventArgs e)
        {
            Register reg = new Register();
            reg.ShowDialog();
            if (reg.IsRegistered)
            {
                btnActivate.Enabled = false;
                btnTry.Enabled = false;
                lblExpiry.Text = "";
            }
        }

        private void btnTry_Click(object sender, EventArgs e)
        {
            openFileDialog1.CheckFileExists = false;
            openFileDialog1.DefaultExt = "GDF";
            openFileDialog1.Title = "Load Registration File";
            openFileDialog1.FileName = "PlugInsReg.GDF";

            openFileDialog1.Filter = "Registration file (*.GDF)|*.GDF|All files (*.*)|*.*";

            string s;
            decimal d;
            DateTime dt;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(openFileDialog1.FileName.ToString());
                sr.ReadLine(); sr.ReadLine();
                s = sr.ReadLine();
                d = System.Convert.ToDecimal(s);
                dt = GLib.G.DecryptDateTime(d);
                GLib.GExecuteScalar es = new GLib.GExecuteScalar();
                es.Query("UPDATE SO_Setting SET rkd=" + d.ToString());
                lblExpiry.Text = "This plug-ins will be expired on " + GLib.G.DecryptDateTime(d).ToLongDateString();

                System.Windows.Forms.MessageBox.Show("This plug-ins will be expired on " + dt.ToLongDateString(), "Register Trial", MessageBoxButtons.OK);
            }
        }

        private void GeneralSettings_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " +info.AssemblyVersion;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textEdit1_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void xtraTabControl1_Click(object sender, EventArgs e)
        {

        }

        private void btnBrowseFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowNewFolderButton = true;
            // Show the FolderBrowserDialog.  
            DialogResult result = folderBrowserDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                txtPath.Text = folderBrowserDialog1.SelectedPath;
                // Environment.SpecialFolder root = folderBrowserDialog1.RootFolder;
            }
        }
    }
}
