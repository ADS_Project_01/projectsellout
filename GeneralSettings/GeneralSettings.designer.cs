﻿namespace ProjectSellOut.GeneralMaintenance
{
    partial class GeneralSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GeneralSettings));
            this.lblExpiry = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnTry = new DevExpress.XtraEditors.SimpleButton();
            this.btnActivate = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.deModification_Date = new DevExpress.XtraEditors.DateEdit();
            this.deCreation_Date = new DevExpress.XtraEditors.DateEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCountry = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.txtState_Province = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSubRegion_Code = new DevExpress.XtraEditors.TextEdit();
            this.txtRegion_Code = new DevExpress.XtraEditors.TextEdit();
            this.txtSales_Person_Code = new DevExpress.XtraEditors.TextEdit();
            this.txtActive = new DevExpress.XtraEditors.TextEdit();
            this.txtLocation_Type = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnBrowseFolder = new DevExpress.XtraEditors.SimpleButton();
            this.txtPath = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deModification_Date.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deModification_Date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deCreation_Date.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deCreation_Date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState_Province.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubRegion_Code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegion_Code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSales_Person_Code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation_Type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPath.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblExpiry
            // 
            this.lblExpiry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblExpiry.AutoSize = true;
            this.lblExpiry.Font = new System.Drawing.Font("Arial Narrow", 8.25F);
            this.lblExpiry.ForeColor = System.Drawing.Color.Blue;
            this.lblExpiry.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblExpiry.Location = new System.Drawing.Point(324, 235);
            this.lblExpiry.Name = "lblExpiry";
            this.lblExpiry.Size = new System.Drawing.Size(140, 15);
            this.lblExpiry.TabIndex = 41;
            this.lblExpiry.Text = "This plug-ins will be expired on ";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnCancel);
            this.panelControl1.Controls.Add(this.btnSave);
            this.panelControl1.Controls.Add(this.btnTry);
            this.panelControl1.Controls.Add(this.deModification_Date);
            this.panelControl1.Controls.Add(this.btnActivate);
            this.panelControl1.Controls.Add(this.deCreation_Date);
            this.panelControl1.Controls.Add(this.label9);
            this.panelControl1.Controls.Add(this.groupControl3);
            this.panelControl1.Controls.Add(this.label8);
            this.panelControl1.Controls.Add(this.lblExpiry);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(788, 296);
            this.panelControl1.TabIndex = 57;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(703, 227);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(73, 23);
            this.btnCancel.TabIndex = 65;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(613, 227);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(73, 23);
            this.btnSave.TabIndex = 64;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnTry
            // 
            this.btnTry.Location = new System.Drawing.Point(673, 198);
            this.btnTry.Name = "btnTry";
            this.btnTry.Size = new System.Drawing.Size(103, 23);
            this.btnTry.TabIndex = 63;
            this.btnTry.Text = "&Trial Lisence";
            this.btnTry.Click += new System.EventHandler(this.btnTry_Click);
            // 
            // btnActivate
            // 
            this.btnActivate.Location = new System.Drawing.Point(673, 169);
            this.btnActivate.Name = "btnActivate";
            this.btnActivate.Size = new System.Drawing.Size(103, 23);
            this.btnActivate.TabIndex = 62;
            this.btnActivate.Text = "&Activate Lisence";
            this.btnActivate.Click += new System.EventHandler(this.btnActivate_Click);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.btnBrowseFolder);
            this.groupControl3.Controls.Add(this.txtPath);
            this.groupControl3.Controls.Add(this.label10);
            this.groupControl3.Controls.Add(this.txtCountry);
            this.groupControl3.Controls.Add(this.label7);
            this.groupControl3.Controls.Add(this.txtState_Province);
            this.groupControl3.Controls.Add(this.label6);
            this.groupControl3.Controls.Add(this.txtSubRegion_Code);
            this.groupControl3.Controls.Add(this.txtRegion_Code);
            this.groupControl3.Controls.Add(this.txtSales_Person_Code);
            this.groupControl3.Controls.Add(this.txtActive);
            this.groupControl3.Controls.Add(this.txtLocation_Type);
            this.groupControl3.Controls.Add(this.label5);
            this.groupControl3.Controls.Add(this.label3);
            this.groupControl3.Controls.Add(this.label2);
            this.groupControl3.Controls.Add(this.label1);
            this.groupControl3.Controls.Add(this.label4);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl3.Location = new System.Drawing.Point(2, 2);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(784, 159);
            this.groupControl3.TabIndex = 61;
            this.groupControl3.Text = "Company Profile";
            // 
            // deModification_Date
            // 
            this.deModification_Date.EditValue = null;
            this.deModification_Date.Location = new System.Drawing.Point(136, 216);
            this.deModification_Date.Name = "deModification_Date";
            this.deModification_Date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deModification_Date.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deModification_Date.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deModification_Date.Size = new System.Drawing.Size(134, 20);
            this.deModification_Date.TabIndex = 7;
            this.deModification_Date.Visible = false;
            // 
            // deCreation_Date
            // 
            this.deCreation_Date.EditValue = null;
            this.deCreation_Date.Location = new System.Drawing.Point(136, 195);
            this.deCreation_Date.Name = "deCreation_Date";
            this.deCreation_Date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deCreation_Date.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deCreation_Date.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.deCreation_Date.Size = new System.Drawing.Size(134, 20);
            this.deCreation_Date.TabIndex = 6;
            this.deCreation_Date.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(16, 219);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 72;
            this.label9.Text = "Modification Date";
            this.label9.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(16, 198);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 13);
            this.label8.TabIndex = 70;
            this.label8.Text = "Creation Date";
            this.label8.Visible = false;
            // 
            // txtCountry
            // 
            this.txtCountry.Location = new System.Drawing.Point(511, 71);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Properties.MaxLength = 9;
            this.txtCountry.Size = new System.Drawing.Size(233, 20);
            this.txtCountry.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(391, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 68;
            this.label7.Text = "Country";
            // 
            // txtState_Province
            // 
            this.txtState_Province.Location = new System.Drawing.Point(511, 50);
            this.txtState_Province.Name = "txtState_Province";
            this.txtState_Province.Properties.MaxLength = 30;
            this.txtState_Province.Size = new System.Drawing.Size(233, 20);
            this.txtState_Province.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(391, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 66;
            this.label6.Text = "State Province";
            // 
            // txtSubRegion_Code
            // 
            this.txtSubRegion_Code.Location = new System.Drawing.Point(511, 29);
            this.txtSubRegion_Code.Name = "txtSubRegion_Code";
            this.txtSubRegion_Code.Properties.MaxLength = 30;
            this.txtSubRegion_Code.Size = new System.Drawing.Size(233, 20);
            this.txtSubRegion_Code.TabIndex = 4;
            // 
            // txtRegion_Code
            // 
            this.txtRegion_Code.Location = new System.Drawing.Point(134, 92);
            this.txtRegion_Code.Name = "txtRegion_Code";
            this.txtRegion_Code.Properties.MaxLength = 30;
            this.txtRegion_Code.Size = new System.Drawing.Size(233, 20);
            this.txtRegion_Code.TabIndex = 3;
            // 
            // txtSales_Person_Code
            // 
            this.txtSales_Person_Code.Location = new System.Drawing.Point(134, 50);
            this.txtSales_Person_Code.Name = "txtSales_Person_Code";
            this.txtSales_Person_Code.Properties.MaxLength = 10;
            this.txtSales_Person_Code.Size = new System.Drawing.Size(233, 20);
            this.txtSales_Person_Code.TabIndex = 1;
            // 
            // txtActive
            // 
            this.txtActive.Location = new System.Drawing.Point(134, 71);
            this.txtActive.Name = "txtActive";
            this.txtActive.Properties.MaxLength = 10;
            this.txtActive.Size = new System.Drawing.Size(233, 20);
            this.txtActive.TabIndex = 2;
            // 
            // txtLocation_Type
            // 
            this.txtLocation_Type.Location = new System.Drawing.Point(134, 29);
            this.txtLocation_Type.Name = "txtLocation_Type";
            this.txtLocation_Type.Properties.MaxLength = 10;
            this.txtLocation_Type.Size = new System.Drawing.Size(233, 20);
            this.txtLocation_Type.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(391, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 59;
            this.label5.Text = "Subregion Code";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(14, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 57;
            this.label3.Text = "Region Code";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(14, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 55;
            this.label2.Text = "Active";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(14, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 53;
            this.label1.Text = "Sales Person Code";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(14, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 51;
            this.label4.Text = "Location Type";
            // 
            // btnBrowseFolder
            // 
            this.btnBrowseFolder.Location = new System.Drawing.Point(435, 112);
            this.btnBrowseFolder.Name = "btnBrowseFolder";
            this.btnBrowseFolder.Size = new System.Drawing.Size(86, 23);
            this.btnBrowseFolder.TabIndex = 75;
            this.btnBrowseFolder.Text = "Browse Folder";
            this.btnBrowseFolder.Click += new System.EventHandler(this.btnBrowseFolder_Click);
            // 
            // txtPath
            // 
            this.txtPath.EditValue = "";
            this.txtPath.Location = new System.Drawing.Point(134, 113);
            this.txtPath.Name = "txtPath";
            this.txtPath.Properties.MaxLength = 100000;
            this.txtPath.Size = new System.Drawing.Size(295, 20);
            this.txtPath.TabIndex = 73;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(14, 119);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 15);
            this.label10.TabIndex = 74;
            this.label10.Text = "Path";
            // 
            // GeneralSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 296);
            this.Controls.Add(this.panelControl1);
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("GeneralSettings.IconOptions.Icon")));
            this.Name = "GeneralSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "General Setting";
            this.Activated += new System.EventHandler(this.GeneralSettings_Activated);
            this.Load += new System.EventHandler(this.GeneralSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deModification_Date.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deModification_Date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deCreation_Date.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deCreation_Date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState_Province.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubRegion_Code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegion_Code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSales_Person_Code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation_Type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPath.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblExpiry;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnTry;
        private DevExpress.XtraEditors.SimpleButton btnActivate;
        private DevExpress.XtraEditors.TextEdit txtSubRegion_Code;
        private DevExpress.XtraEditors.TextEdit txtRegion_Code;
        private DevExpress.XtraEditors.TextEdit txtSales_Person_Code;
        private DevExpress.XtraEditors.TextEdit txtActive;
        private DevExpress.XtraEditors.TextEdit txtLocation_Type;
        private DevExpress.XtraEditors.DateEdit deModification_Date;
        private DevExpress.XtraEditors.DateEdit deCreation_Date;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.TextEdit txtCountry;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.TextEdit txtState_Province;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.SimpleButton btnBrowseFolder;
        private DevExpress.XtraEditors.TextEdit txtPath;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}