ALTER VIEW [dbo].[SO_StockCard_View]
as
select Distributor_Code,Local_Product_Code,UOM,MAX(Stock_Date) as Stock_Date,SUM(Stock_position) as Stock_position,MAX(Creation_Date) as Creation_Date,MAX(Modification_Date) as Modification_Date from(
SELECT 
  case when LEN((select RegisterNo from Profile))>10 then SUBSTRING((select RegisterNo from Profile),1,10) else (select RegisterNo from Profile) end AS Distributor_Code,
  case when LEN( ItemCode)>30 then SUBSTRING(ItemCode,1,30) else ItemCode end AS Local_Product_Code,
 case when LEN( UOM)>8 then SUBSTRING(UOM,1,8) else UOM end AS UOM,
DocDate AS Stock_Date,
Qty AS Stock_position,
DocDate AS Creation_Date,
LastModified AS Modification_Date
FROM StockDTL) result
group by Distributor_Code,Local_Product_Code,UOM
GO

ALTER VIEW [dbo].[SO_Invoice_View]
as
SELECT 
 case when LEN((select RegisterNo from Profile))>10 then SUBSTRING((select RegisterNo from Profile),1,10) else (select RegisterNo from Profile) end AS Distributor_Code,
 case when LEN( DebtorCode)>30 then SUBSTRING(DebtorCode,1,30) else DebtorCode end AS Bill_To_Code,
 case when LEN( SalesAgent)>10 then SUBSTRING(SalesAgent,1,10) else SalesAgent end AS Employee_Num,
 case when LEN( ShipVia)>10 then SUBSTRING(ShipVia,1,10) else ShipVia end AS Ship_To_Code,
 case when LEN( UDF_SONO)>30 then SUBSTRING(UDF_SONO,1,30) else UDF_SONO end AS Sales_Order_Number,
UDF_DtlKey  AS Sales_Order_Line_Number,
 case when LEN( DocNo)>30 then SUBSTRING(DocNo,1,30) else DocNo end AS Invoice_Number,
Seq/16  AS Invoice_Line_Number,
DocDate AS Invoice_Date,
 case when LEN( DisplayTerm)>10 then SUBSTRING(DisplayTerm,1,10) else DisplayTerm end AS Payment_Term,
DocDate AS Sale_Date,
DeliveryDate AS Shipment_Date,
 case when LEN( ItemCode)>30 then SUBSTRING(ItemCode,1,30) else ItemCode end AS Local_Product_Code,
UnitPrice AS Unit_Price,
null as Discount1,
null as Discount2,
null as Discount3,
null as Discount4,
null as Discount5,
NetTotal as Net_Sales,
Total as Gross_Sales,
 case when LEN( UOM)>8 then SUBSTRING(UOM,1,8) else UOM end AS Local_UOM,
Qty as Local_Quantity,
'Sales' as Transaction_Type,
'Complete' as Status,
CreatedTimeStamp as Creation_Date,
LastModified as Modification_Date,
null as CRM_REF_NUM
from IV WITH (NOLOCK)
inner join IVDTL WITH (NOLOCK) on iv.DocKey=IVDTL.DocKey
   WHERE Cancelled='F'
