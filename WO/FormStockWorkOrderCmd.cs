﻿// Type: BCE.AutoCount.Manufacturing.StockWorkOrder.FormStockWorkOrderCmd
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.CommonForms;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controller.FunctionVisibility;
using BCE.AutoCount.Controls;
using BCE.AutoCount.Data;
using BCE.AutoCount.Help;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.RegistryID.CommandFormStartupID;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.UDF;
using BCE.AutoCount.XtraUtils;
using BCE.Controls;
using BCE.Data;
using BCE.Localization;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;

namespace RPASystem.WorkOrder
{
  [SingleInstanceThreadForm]
  public class FormStockWorkOrderCmd : XtraForm
  {
    private string myGroupSummaryId = "";
    private StockWorkOrderCommand myStockWorkOrderCmd;
    private DBSetting myDBSetting;
    private DocumentCommandFormControllerWithOutstandingReport myCommandFormController;
    private AsyncDataSetUpdateDelegate myUpdateDataSetDelegate;
    private ScriptObject myScriptObject;
    private IContainer components;
    private PanelHeader panelHeader1;
    private PreviewButton previewButton1;
    private PrintButton printButton1;
    private Bar bar1;
    private BarDockControl barDockControlBottom;
    private BarDockControl barDockControlLeft;
    private BarDockControl barDockControlRight;
    private BarDockControl barDockControlTop;
    private BarManager barManager1;
    private CheckEdit chkEdtShowAtStartup;
    private HyperLinkEdit lblFind;
    private HyperLinkEdit lblNew;
    private HyperLinkEdit lblPrint;
    private PanelControl panelCmd;
    private PanelControl panelControl1;
    private PanelControl panelGrid;
    private RepositoryItemCheckEdit repChkedtIsMultilevel;
    private RepositoryItemTextEdit repositoryItemTextEdit1;
    private SimpleButton sbtnAOProcessing;
    private SimpleButton sbtnDel;
    private SimpleButton sbtnEdit;
    private SimpleButton sbtnRefresh;
    private SimpleButton sbtnView;
    private GridColumn colAssemblyCost;
    private GridColumn colBatchNo;
    private GridColumn colCancelled;
    private GridColumn colCreatedTimeStamp;
    private GridColumn colCreatedUserID;
    private GridColumn colDeptNo;
    private GridColumn colDescription;
    private GridColumn colDocDate;
    private GridColumn colDocNo;
    private GridColumn colIsMultilevel;
    private GridColumn colItemCode;
    private GridColumn colLastModified;
    private GridColumn colLastModifiedUserID;
    private GridColumn colLocation;
    private GridColumn colNetTotal;
    private GridColumn colPrintCount;
    private GridColumn colProjNo;
    private GridColumn colQty;
    private GridColumn colRefDocNo;
    private GridColumn colRemark1;
    private GridColumn colRemark2;
    private GridColumn colRemark3;
    private GridColumn colRemark4;
    private GridColumn colTotal;
    private GridControl gridControlAll;
    private GridView gridViewStockMain;
    private Label label1;
    private Label lblShowAll;
    private GridColumn colTransferFrom;
    private GridColumn colTransferTo;
    private GridColumn colExpCompletedDate;

    public FormStockWorkOrderCmd(DBSetting dbSetting)
    {
      this.InitializeComponent();
      this.myDBSetting = dbSetting;
      this.myScriptObject = ScriptManager.CreateObject(dbSetting, "StockWorkOrderCommandForm");
      this.myStockWorkOrderCmd = StockWorkOrderCommand.Create(this.myDBSetting);
      StockWorkOrderCommand.DataSetUpdate.Add(this.myDBSetting, this.myUpdateDataSetDelegate = new AsyncDataSetUpdateDelegate(this.UpdateDataSet));
      this.previewButton1.ReportType = "Stock Work Order Document";
      this.previewButton1.SetDBSetting(this.myDBSetting);
      this.printButton1.ReportType = "Stock Work Order Document";
      this.printButton1.SetDBSetting(this.myDBSetting);
      this.InitCommandFormController(this.barManager1);
      BCE.AutoCount.Help.HelpProvider.SetHelpTopic(this.panelHeader1, "Stock_Assembly_Order.htm");
      DBSetting dbSetting1 = this.myDBSetting;
      string docType = "AO";
      long docKey = 0L;
      long eventKey = 0L;
      // ISSUE: variable of a boxed type
      StockWorkOrderString local = StockWorkOrderString.OpenStockWorkOrder;
      object[] objArray = new object[1];
      int index = 0;
      string loginUserId = this.myStockWorkOrderCmd.UserAuthentication.LoginUserID;
      objArray[index] = (object) loginUserId;
      string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
      string detail = "";
      Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
    }

    private void UpdateDataSet(DataSet ds, AsyncDataSetUpdateAction action)
    {
      DataTable dataTable1 = (DataTable) this.gridControlAll.DataSource;
      if (dataTable1.PrimaryKey.Length != 0)
      {
        DataTable dataTable2 = ds.Tables["Master"];
        long num = BCE.Data.Convert.ToInt64(dataTable2.Rows[0]["DocKey"]);
        DataRow row = dataTable1.Rows.Find((object) num);
        if (action == AsyncDataSetUpdateAction.Update)
        {
          if (row == null)
            row = dataTable1.NewRow();
          foreach (DataColumn index1 in (InternalDataCollectionBase) row.Table.Columns)
          {
            int index2 = dataTable2.Columns.IndexOf(index1.ColumnName);
            if (index2 >= 0)
              row[index1] = dataTable2.Rows[0][index2];
          }
          row.EndEdit();
          if (row.RowState == DataRowState.Detached)
            dataTable1.Rows.Add(row);
          for (int rowHandle = 0; rowHandle < this.gridViewStockMain.RowCount; ++rowHandle)
          {
            DataRow dataRow = this.gridViewStockMain.GetDataRow(rowHandle);
            if (dataRow != null && BCE.Data.Convert.ToInt64(dataRow["DocKey"]) == num)
            {
              this.gridViewStockMain.FocusedRowHandle = rowHandle;
              break;
            }
          }
        }
        else
        {
          if (row != null)
            row.Delete();
          dataTable1.AcceptChanges();
        }
      }
    }

    private void InitCommandFormController(BarManager barManager)
    {
      this.myCommandFormController = new DocumentCommandFormControllerWithOutstandingReport(this.myDBSetting, (Form) this, barManager);
      this.myCommandFormController.NewDocumentEvent = new MethodInvoker(this.NewDocument);
      this.myCommandFormController.EditDocumentEvent = new MethodInvoker(this.EditDocument);
      this.myCommandFormController.ViewDocumentEvent = new MethodInvoker(this.ViewDocument);
      this.myCommandFormController.DeleteDocumentEvent = new MethodInvoker(this.DeleteDocument);
      this.myCommandFormController.PreviewWithDefaultReportEvent = new PrintWithDefaultReportMethodInvoker(this.PreviewDocument);
      this.myCommandFormController.PrintWithDefaultReportEvent = new PrintWithDefaultReportMethodInvoker(this.PrintDocument);
      this.myCommandFormController.FindEvent = new MethodInvoker(this.Find);
      this.myCommandFormController.GeneralPrintEvent = new MethodInvoker(this.GeneralPrint);
      this.myCommandFormController.RefreshGridEvent = new MethodInvoker(this.RefreshGrid);
      this.myCommandFormController.ShowAllRecordsEvent = new ShowAllRecordsDelegate(this.ShowAllRecords);
      this.myCommandFormController.DesignDocumentStyleReportEvent = new MethodInvoker(this.DesignDocumentStyleReport);
      this.myCommandFormController.DesignListingStyleReportEvent = new MethodInvoker(this.DesignDocumentListingStyleReport);
      this.myCommandFormController.DesignDetailListingReportEvent = new MethodInvoker(this.DesignDetailListingReport);
      this.myCommandFormController.DesignOutstandingListingReportEvent = new MethodInvoker(this.DesignOutStandingDocumentListingStyleReport);
      this.myCommandFormController.ReportOptionEvent = new MethodInvoker(this.ReportOption);
      this.myCommandFormController.SetNewControl((Control) this.lblNew);
      this.myCommandFormController.SetFindControl((Control) this.lblFind);
      this.myCommandFormController.SetPrintControl((Control) this.lblPrint);
      this.myCommandFormController.SetShowAllControl((Control) this.lblShowAll);
      this.myCommandFormController.SetMainGridView(this.gridViewStockMain);
      this.myCommandFormController.SetEditDocumentControl((Control) this.sbtnEdit);
      this.myCommandFormController.SetViewDocumentControl((Control) this.sbtnView);
      this.myCommandFormController.SetDeleteDocumentControl((Control) this.sbtnDel);
      this.myCommandFormController.SetPreviewWithDefaultReportControl(this.previewButton1);
      this.myCommandFormController.SetPrintWithDefaultReportControl(this.printButton1);
      this.myCommandFormController.SetRefreshGridControl((Control) this.sbtnRefresh);
      Control[] arrControl = new Control[4];
      int index1 = 0;
      HyperLinkEdit hyperLinkEdit1 = this.lblNew;
      arrControl[index1] = (Control) hyperLinkEdit1;
      int index2 = 1;
      HyperLinkEdit hyperLinkEdit2 = this.lblFind;
      arrControl[index2] = (Control) hyperLinkEdit2;
      int index3 = 2;
      HyperLinkEdit hyperLinkEdit3 = this.lblPrint;
      arrControl[index3] = (Control) hyperLinkEdit3;
      int index4 = 3;
      Label label = this.lblShowAll;
      arrControl[index4] = (Control) label;
      Utils.SetHottrackControls(arrControl);
    }

    public static void StartEntryForm(StockWorkOrder stockAssemblyOrder)
    {
      string assemblyQualifiedName = typeof (FormStockWorkOrderEntry).AssemblyQualifiedName;
      System.Type[] types = new System.Type[1];
      int index1 = 0;
      System.Type type = stockAssemblyOrder.GetType();
      types[index1] = type;
      object[] parameters = new object[1];
      int index2 = 0;
      StockWorkOrder stockAssemblyOrder1 = stockAssemblyOrder;
      parameters[index2] = (object) stockAssemblyOrder1;
      new ThreadForm(assemblyQualifiedName, types, parameters).Show();
    }

    public static void ViewDocument(DBSetting dbSetting, long docKey)
    {
      if (UserAuthentication.GetOrCreate(dbSetting).AccessRight.IsAccessible("MF_ASMORDER_VIEW", true))
      {
        try
        {
          StockWorkOrder stockAssemblyOrder = StockWorkOrderCommand.Create(dbSetting).View(docKey);
          if (stockAssemblyOrder == null)
            AppMessage.ShowMessage((IWin32Window) null, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_RecordsNotExist, new object[0]));
          else
            FormStockWorkOrderCmd.StartEntryForm(stockAssemblyOrder);
        }
        catch (AppException ex)
        {
          AppMessage.ShowErrorMessage((IWin32Window) null, ex.Message);
        }
      }
    }

    public static void NewDocument(DBSetting dbSetting)
    {
      if (UserAuthentication.GetOrCreate(dbSetting).AccessRight.IsAccessible("MF_ASMORDER_NEW", true))
      {
        try
        {
          StockWorkOrder stockAssemblyOrder = StockWorkOrderCommand.Create(dbSetting).AddNew();
          if (stockAssemblyOrder != null)
            FormStockWorkOrderCmd.StartEntryForm(stockAssemblyOrder);
        }
        catch (AppException ex)
        {
          AppMessage.ShowErrorMessage((IWin32Window) null, ex.Message);
        }
      }
    }

    public static void EditTempDocument(DBSetting dbSetting, long docKey)
    {
      try
      {
        StockWorkOrder stockAssemblyOrder = StockWorkOrderCommand.Create(dbSetting).LoadFromTempDocument(docKey);
        if (stockAssemblyOrder == null)
          AppMessage.ShowMessage((IWin32Window) null, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_RecordsNotExist, new object[0]));
        else
          FormStockWorkOrderCmd.StartEntryForm(stockAssemblyOrder);
      }
      catch (DataAccessException ex)
      {
        AppMessage.ShowErrorMessage((IWin32Window) null, ex.Message);
      }
    }

    private void InitFormControls()
    {
      FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting);
      string fieldname1 = "DocDate";
      string fieldtype1 = "Date";
      formControlUtil.AddField(fieldname1, fieldtype1);
      string fieldname2 = "Qty";
      string fieldtype2 = "Quantity";
      formControlUtil.AddField(fieldname2, fieldtype2);
      string fieldname3 = "Total";
      string fieldtype3 = "Currency";
      formControlUtil.AddField(fieldname3, fieldtype3);
      string fieldname4 = "AssemblyCost";
      string fieldtype4 = "Currency";
      formControlUtil.AddField(fieldname4, fieldtype4);
      string fieldname5 = "NetTotal";
      string fieldtype5 = "Currency";
      formControlUtil.AddField(fieldname5, fieldtype5);
      string fieldname6 = "LastModified";
      string fieldtype6 = "DateTime";
      formControlUtil.AddField(fieldname6, fieldtype6);
      string fieldname7 = "CreatedTimeStamp";
      string fieldtype7 = "DateTime";
      formControlUtil.AddField(fieldname7, fieldtype7);
      FormStockWorkOrderCmd assemblyOrderCmd = this;
      formControlUtil.InitControls((Control) assemblyOrderCmd);
    }

    private void InitGroupSummary()
    {
      StringBuilder stringBuilder = new StringBuilder(40);
      stringBuilder.Append("DocKey");
      if (this.colNetTotal.Visible)
        stringBuilder.Append("NetTotal");
      if (this.colTotal.Visible)
        stringBuilder.Append("Total");
      if (this.colAssemblyCost.Visible)
        stringBuilder.Append("AssemblyCost");
      string str1 = ((object) stringBuilder).ToString();
      if (str1 != this.myGroupSummaryId)
      {
        this.myGroupSummaryId = str1;
        this.gridViewStockMain.GroupSummary.Clear();
        GridGroupSummaryItemCollection groupSummary1 = this.gridViewStockMain.GroupSummary;
        int num1 = 3;
        string fieldName1 = "DocNo";
                // ISSUE: variable of the null type
                GridColumn local1 = null;
        // ISSUE: variable of a boxed type
        GridGroupSummaryItemStringId local2 =  GridGroupSummaryItemStringId.Count;
        object[] objArray1 = new object[1];
        int index1 = 0;
        string str2 = "{0}";
        objArray1[index1] = (object) str2;
        string string1 = BCE.Localization.Localizer.GetString((Enum) local2, objArray1);
        groupSummary1.Add((SummaryItemType) num1, fieldName1, (GridColumn) local1, string1);
        if (this.colNetTotal.Visible)
        {
          GridGroupSummaryItemCollection groupSummary2 = this.gridViewStockMain.GroupSummary;
          int num2 = 0;
          string fieldName2 = "NetTotal";
                    // ISSUE: variable of the null type
                    GridColumn local3 = null;
          // ISSUE: variable of a boxed type
          GridGroupSummaryItemStringId local4 = GridGroupSummaryItemStringId.NetTotal;
          object[] objArray2 = new object[1];
          int index2 = 0;
          string currencyFormatString = this.myStockWorkOrderCmd.DecimalSetting.GetCurrencyFormatString(0);
          objArray2[index2] = (object) currencyFormatString;
          string string2 = BCE.Localization.Localizer.GetString((Enum) local4, objArray2);
          groupSummary2.Add((SummaryItemType) num2, fieldName2, (GridColumn) local3, string2);
        }
        if (this.colTotal.Visible)
        {
          GridGroupSummaryItemCollection groupSummary2 = this.gridViewStockMain.GroupSummary;
          int num2 = 0;
          string fieldName2 = "Total";
                    // ISSUE: variable of the null type
                    GridColumn local3 = null;
          // ISSUE: variable of a boxed type
          GridGroupSummaryItemStringId local4 =  GridGroupSummaryItemStringId.Total;
          object[] objArray2 = new object[1];
          int index2 = 0;
          string currencyFormatString = this.myStockWorkOrderCmd.DecimalSetting.GetCurrencyFormatString(0);
          objArray2[index2] = (object) currencyFormatString;
          string string2 = BCE.Localization.Localizer.GetString((Enum) local4, objArray2);
          groupSummary2.Add((SummaryItemType) num2, fieldName2, (GridColumn) local3, string2);
        }
        if (this.colAssemblyCost.Visible)
        {
          GridGroupSummaryItemCollection groupSummary2 = this.gridViewStockMain.GroupSummary;
          int num2 = 0;
          string fieldName2 = "AssemblyCost";
                    // ISSUE: variable of the null type
                    GridColumn local3 = null;
          // ISSUE: variable of a boxed type
          GridGroupSummaryItemStringId local4 = GridGroupSummaryItemStringId.AssemblyCost;
          object[] objArray2 = new object[1];
          int index2 = 0;
          string costFormatString = this.myStockWorkOrderCmd.DecimalSetting.GetCostFormatString(0);
          objArray2[index2] = (object) costFormatString;
          string string2 = BCE.Localization.Localizer.GetString((Enum) local4, objArray2);
          groupSummary2.Add((SummaryItemType) num2, fieldName2, (GridColumn) local3, string2);
        }
      }
    }

    private void LoadCommandFormStartupState()
    {
      bool boolean = DBRegistry.Create(this.myDBSetting).GetBoolean((IRegistryID) new StockAssemblyOrderCommandFormStartupID());
      this.chkEdtShowAtStartup.Checked = boolean;
      this.chkEdtShowAtStartup.CheckedChanged += new EventHandler(this.chkEdtShowAtStartup_CheckedChanged);
      this.FormInitialize();
      this.myCommandFormController.ShowAll(boolean);
    }

    private void SaveCommandFormStartupState()
    {
      DBRegistry dbRegistry = DBRegistry.Create(this.myDBSetting);
            StockAssemblyOrderCommandFormStartupID commandFormStartupId1 = new StockAssemblyOrderCommandFormStartupID();
      commandFormStartupId1.NewValue = (object)  (this.chkEdtShowAtStartup.Checked ? true : false);
            StockAssemblyOrderCommandFormStartupID commandFormStartupId2 = commandFormStartupId1;
      dbRegistry.SetValue((IRegistryID) commandFormStartupId2);
    }

    private long GetSelectedDocKey(ref string docNo)
    {
      if (this.gridViewStockMain.FocusedRowHandle >= 0)
      {
        DataRow dataRow = this.gridViewStockMain.GetDataRow(this.gridViewStockMain.FocusedRowHandle);
        if (dataRow != null)
        {
          long num = BCE.Data.Convert.ToInt64(dataRow["DocKey"]);
          docNo = dataRow.Table.Columns.IndexOf("DocNo") < 0 ? "(Internal DocKey=" + num.ToString() + ")" : dataRow["DocNo"].ToString();
          return num;
        }
        else
          return -1L;
      }
      else
      {
        AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_RecordNotSelected, new object[0]));
        return -1L;
      }
    }

    private void View(long docKey)
    {
      try
      {
        StockWorkOrder stockAssemblyOrder = this.myStockWorkOrderCmd.View(docKey);
        if (stockAssemblyOrder == null)
          AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_RecordsNotExist, new object[0]));
        else
          FormStockWorkOrderCmd.StartEntryForm(stockAssemblyOrder);
      }
      catch (AppException ex)
      {
        AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
      }
    }

    private void Edit(long docKey)
    {
      try
      {
        StockWorkOrder stockAssemblyOrder = this.myStockWorkOrderCmd.Edit(docKey);
        if (stockAssemblyOrder == null)
          AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_RecordsNotExist, new object[0]));
        else
          FormStockWorkOrderCmd.StartEntryForm(stockAssemblyOrder);
      }
      catch (AppException ex)
      {
        AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
      }
    }

    private void SearchAll(bool allColumns)
    {
      ColumnView view = this.gridViewStockMain.Columns.View;
      string prefix = "ASMORDER";
      int num = allColumns ? 1 : 0;
      string[] mustIncludeColNames = new string[1];
      int index1 = 0;
      string str1 = "DocKey";
      mustIncludeColNames[index1] = str1;
      string[] excludeColNames = new string[3];
      int index2 = 0;
      string str2 = "Delete";
      excludeColNames[index2] = str2;
      int index3 = 1;
      string str3 = "TransferFrom";
      excludeColNames[index3] = str3;
      int index4 = 2;
      string str4 = "TransferTo";
      excludeColNames[index4] = str4;
      string columnSQL = ColumnViewUtils.BuildSQLColumnListFromColumnView(view, prefix, num != 0, mustIncludeColNames, excludeColNames);
      try
      {
        this.myStockWorkOrderCmd.InquireAllMaster(columnSQL);
      }
      catch (AppException ex)
      {
        AppMessage.ShowErrorMessage(ex.Message);
      }
      FormStockWorkOrderCmd.FormLoadDataEventArgs loadDataEventArgs1 = new FormStockWorkOrderCmd.FormLoadDataEventArgs(this, this.myStockWorkOrderCmd.DataTableAllMaster);
      ScriptObject scriptObject = this.myScriptObject;
      string name = "OnFormLoadData";
      System.Type[] types = new System.Type[1];
      int index5 = 0;
      System.Type type = loadDataEventArgs1.GetType();
      types[index5] = type;
      object[] objArray = new object[1];
      int index6 = 0;
      FormStockWorkOrderCmd.FormLoadDataEventArgs loadDataEventArgs2 = loadDataEventArgs1;
      objArray[index6] = (object) loadDataEventArgs2;
      scriptObject.RunMethod(name, types, objArray);
    }

    private void ReloadAllColumns(object sender, EventArgs e)
    {
      this.SearchAll(true);
    }

    private void NewDocument()
    {
      if (this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_NEW", (XtraForm) this))
      {
        try
        {
          StockWorkOrder stockAssemblyOrder = this.myStockWorkOrderCmd.AddNew();
          if (stockAssemblyOrder != null)
            FormStockWorkOrderCmd.StartEntryForm(stockAssemblyOrder);
        }
        catch (AppException ex)
        {
          AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
        }
      }
    }

    private void Find()
    {
      using (FormStockWorkFind stockAssemblyFind = new FormStockWorkFind(this.myStockWorkOrderCmd))
      {
        int num = (int) stockAssemblyFind.ShowDialog();
      }
    }

    private void EditDocument()
    {
      if (this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_EDIT", (XtraForm) this))
      {
        string docNo = "";
        long selectedDocKey = this.GetSelectedDocKey(ref docNo);
        if (selectedDocKey > -1L && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "ASMORDER", selectedDocKey) <= 0 || this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_PRINTED_EDIT", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedEditPrintedDocument, new object[0]))))
          this.Edit(selectedDocKey);
      }
    }

    private void ViewDocument()
    {
      if (this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_VIEW", (XtraForm) this))
      {
        string docNo = "";
        long selectedDocKey = this.GetSelectedDocKey(ref docNo);
        if (selectedDocKey > -1L)
          this.View(selectedDocKey);
      }
    }

    private void PreviewDocument(bool useDefaultReport)
    {
      string docNo = "";
      long selectedDocKey = this.GetSelectedDocKey(ref docNo);
      if (selectedDocKey > -1L && this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_DOC_REPORT_PREVIEW", (XtraForm) this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "ASMORDER", selectedDocKey) <= 0 || this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_PRINTED_PREVIEW", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedPreviewPrintedDocument, new object[0]))))
      {
        // ISSUE: variable of a boxed type
        StockWorkOrderString local =  StockWorkOrderString.StockWorkOrder;
        object[] objArray1 = new object[1];
        int index1 = 0;
        string str = docNo;
        objArray1[index1] = (object) str;
        ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString((Enum) local, objArray1), "MF_ASMORDER_DOC_REPORT_PRINT", "MF_ASMORDER_DOC_REPORT_EXPORT", "");
        reportInfo.DocType = "AO";
        reportInfo.DocKey = selectedDocKey;
        reportInfo.UpdatePrintCountTableName = "ASMORDER";
        reportInfo.CheckBeforePrintEvent += new CheckBeforeEventHandler(this.CheckBeforePrint);
        reportInfo.CheckBeforeExportEvent += new CheckBeforeEventHandler(this.CheckBeforeExport);
        reportInfo.EmailAndFaxInfo = StockWorkOrderCommand.GetEmailAndFaxInfo(selectedDocKey, this.myDBSetting);
        BeforePreviewDocumentEventArgs documentEventArgs1 = new BeforePreviewDocumentEventArgs(reportInfo.EmailAndFaxInfo, reportInfo.DocKey, this.myDBSetting);
        ScriptObject scriptObject = this.myScriptObject;
        string name = "BeforePreviewDocument";
        System.Type[] types = new System.Type[1];
        int index2 = 0;
        System.Type type = documentEventArgs1.GetType();
        types[index2] = type;
        object[] objArray2 = new object[1];
        int index3 = 0;
        BeforePreviewDocumentEventArgs documentEventArgs2 = documentEventArgs1;
        objArray2[index3] = (object) documentEventArgs2;
        scriptObject.RunMethod(name, types, objArray2);
        reportInfo.Tag = (object) documentEventArgs1;
        if (documentEventArgs1.AllowPreview)
          ReportTool.PreviewReport("Stock Work Order Document", this.myStockWorkOrderCmd.GetReportDataSource(selectedDocKey), this.myDBSetting, useDefaultReport, false, this.myStockWorkOrderCmd.ReportOption, reportInfo);
      }
    }

    private bool CheckBeforePrint(ReportInfo reportInfo)
    {
      if (!((BeforePreviewDocumentEventArgs) reportInfo.Tag).AllowPrint || SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight && DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "ASMORDER", reportInfo.DocKey) > 0 && !this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_PRINTED_PRINT", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedPrintPrintedDocument, new object[0])))
        return false;
      else
        return true;
    }

    private bool CheckBeforeExport(ReportInfo reportInfo)
    {
      if (!((BeforePreviewDocumentEventArgs) reportInfo.Tag).AllowExport || SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight && DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "ASMORDER", reportInfo.DocKey) > 0 && !this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_PRINTED_EXPORT", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedExportPrintedDocument, new object[0])))
        return false;
      else
        return true;
    }

    private void PrintDocument(bool useDefaultReport)
    {
      string docNo = "";
      long selectedDocKey = this.GetSelectedDocKey(ref docNo);
      if (selectedDocKey > -1L && this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_DOC_REPORT_PRINT", (XtraForm) this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "ASMORDER", selectedDocKey) <= 0 || this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_PRINTED_PRINT", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedPrintPrintedDocument, new object[0]))))
      {
        // ISSUE: variable of a boxed type
        StockWorkOrderString local = StockWorkOrderString.StockWorkOrder;
        object[] objArray = new object[1];
        int index = 0;
        string str = docNo;
        objArray[index] = (object) str;
        ReportTool.PrintReport("Stock Work Order Document", this.myStockWorkOrderCmd.GetReportDataSource(selectedDocKey), this.myDBSetting, useDefaultReport, this.myStockWorkOrderCmd.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString((Enum) local, objArray), "MF_ASMORDER_DOC_REPORT_PRINT", "MF_ASMORDER_DOC_REPORT_EXPORT", "")
        {
          DocType = "AO",
          DocKey = selectedDocKey,
          UpdatePrintCountTableName = "ASMORDER",
          EmailAndFaxInfo = StockWorkOrderCommand.GetEmailAndFaxInfo(selectedDocKey, this.myDBSetting)
        });
      }
    }

    private void DeleteDocument()
    {
      if (this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_DELETE", (XtraForm) this))
      {
        string docNo = "";
        long selectedDocKey = this.GetSelectedDocKey(ref docNo);
        if (selectedDocKey > -1L && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "ASMORDER", selectedDocKey) <= 0 || this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_PRINTED_DELETE", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedDeletePrintedDocument, new object[0]))))
        {
          // ISSUE: variable of a boxed type
          StockAssemblyOrderStringId local = StockAssemblyOrderStringId.ConfirmMessage_DeleteStockAssemblyOrder;
          object[] objArray = new object[1];
          int index = 0;
          string str = docNo;
          objArray[index] = (object) str;
          if (AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString((Enum) local, objArray)))
          {
            try
            {
              this.myStockWorkOrderCmd.Delete(selectedDocKey);
              AppMessage.ShowMessage(BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_DeleteSuccessfully, new object[0]));
            }
            catch (DBConcurrencyException ex)
            {
              AppMessage.ShowErrorMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ErrorMessage_UnableFindCauseDeletedByOthers, new object[0]));
            }
            catch (AppException ex)
            {
              AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
            }
          }
        }
      }
    }

    private void ShowAllRecords(bool show)
    {
      this.panelGrid.Visible = show;
      if (this.panelGrid.Visible && this.gridControlAll.DataSource == null)
      {
        this.SearchAll(false);
        this.gridControlAll.DataSource = (object) this.myStockWorkOrderCmd.DataTableAllMaster;
      }
    }

    private void RefreshGrid()
    {
      this.SearchAll(false);
    }

    private void GeneralPrint()
    {
      string[] rptTypes = new string[3];
      int index1 = 0;
      string string1 = BCE.Localization.Localizer.GetString((Enum) StockWorkOrderString.BatchPrintStockWorkOrder_PrintStockWorkOrderListing, new object[0]);
      rptTypes[index1] = string1;
      int index2 = 1;
      string string2 = BCE.Localization.Localizer.GetString((Enum) StockWorkOrderString.PrintStockWorkOrderDetailListing, new object[0]);
      rptTypes[index2] = string2;
      int index3 = 2;
      string string3 = BCE.Localization.Localizer.GetString((Enum) StockWorkOrderString.PrintOutstandingStockWorkOrderListing, new object[0]);
      rptTypes[index3] = string3;
      switch (FormSelectListingReportType.SelectListingReportType(rptTypes))
      {
        case 0:
          CommandCenter.PrintStockAssemblyOrderListing();
          break;
        case 1:
          CommandCenter.PrintStockAssemblyOrderDetailListing();
          break;
        case 2:
          CommandCenter.PrintOutstandingStockAssemblyOrderListing();
          break;
      }
    }

    private void DesignDocumentStyleReport()
    {
      ReportTool.DesignReport("Stock Work Order Document", this.myDBSetting);
    }

    private void DesignDocumentListingStyleReport()
    {
      ReportTool.DesignReport("Stock Work Order Listing", this.myDBSetting);
    }

    private void DesignDetailListingReport()
    {
      ReportTool.DesignReport("Stock Work Order Detail Listing", this.myDBSetting);
    }

    private void DesignOutStandingDocumentListingStyleReport()
    {
      ReportTool.DesignReport("Outstanding Stock Work Order", this.myDBSetting);
    }

    private void ReportOption()
    {
      FormBasicReportOption.ShowReportOption(this.myStockWorkOrderCmd.ReportOption);
      this.myStockWorkOrderCmd.SaveReportOption();
    }

    private void repositoryItemTextEdit1_FormatEditValue(object sender, ConvertEditValueEventArgs e)
    {
      if (e.Value != null)
      {
        if (e.Value.ToString() == "T")
          e.Value = (object) BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Cancelled, new object[0]);
        else
          e.Value = (object) "";
      }
    }

    private void chkEdtShowAtStartup_CheckedChanged(object sender, EventArgs e)
    {
      this.SaveCommandFormStartupState();
    }

    private void FormStockAssemblyCmd_Load(object sender, EventArgs e)
    {
      new UDFUtil(this.myDBSetting).SetupGrid(this.gridViewStockMain, "ASMORDER", false);
      new CustomizeGridLayout(this.myDBSetting, this.Name, this.gridViewStockMain, new EventHandler(this.ReloadAllColumns)).PageHeader = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_StockAssemblyOrderListing, new object[0]);
      foreach (GridColumn gridColumn in (CollectionBase) this.gridViewStockMain.Columns)
        gridColumn.OptionsColumn.AllowEdit = false;
      this.InitFormControls();
      this.LoadCommandFormStartupState();
      this.sbtnAOProcessing.Visible = new BusinessFlow(this.myDBSetting).IsAssemblyOrderProcessingVisible();
    }

    private void gridViewStockMain_Layout(object sender, EventArgs e)
    {
      if (this.gridControlAll.DataSource != null)
        this.InitGroupSummary();
    }

    private void sbtnAOProcessing_Click(object sender, EventArgs e)
    {
      if (this.myStockWorkOrderCmd.UserAuthentication.AccessRight.IsAccessible("BFLOW_AOP_OPEN", (XtraForm) this))
      {
        string docNo = "";
        long selectedDocKey = this.GetSelectedDocKey(ref docNo);
        if (selectedDocKey > -1L)
          this.myStockWorkOrderCmd.StartBusinessFlowForm(selectedDocKey);
      }
    }

    private void FormStockWorkOrderCmd_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
    {
      this.gridControlAll.DataSource = (object) null;
      this.myStockWorkOrderCmd = (StockWorkOrderCommand) null;
      if (this.myUpdateDataSetDelegate != null)
      {
        StockWorkOrderCommand.DataSetUpdate.Remove(this.myDBSetting, this.myUpdateDataSetDelegate);
        this.myUpdateDataSetDelegate = (AsyncDataSetUpdateDelegate) null;
      }
      if (this.myCommandFormController != null)
      {
        this.myCommandFormController.Cleanup();
        this.myCommandFormController = (DocumentCommandFormControllerWithOutstandingReport) null;
      }
      FormStockWorkOrderCmd.FormClosedEventArgs formClosedEventArgs1 = new FormStockWorkOrderCmd.FormClosedEventArgs(this);
      ScriptObject scriptObject = this.myScriptObject;
      string name = "OnFormClosed";
      System.Type[] types = new System.Type[1];
      int index1 = 0;
      System.Type type = formClosedEventArgs1.GetType();
      types[index1] = type;
      object[] objArray = new object[1];
      int index2 = 0;
      FormStockWorkOrderCmd.FormClosedEventArgs formClosedEventArgs2 = formClosedEventArgs1;
      objArray[index2] = (object) formClosedEventArgs2;
      scriptObject.RunMethod(name, types, objArray);
    }

    private void FormInitialize()
    {
      FormStockWorkOrderCmd.FormInitializeEventArgs initializeEventArgs1 = new FormStockWorkOrderCmd.FormInitializeEventArgs(this);
      ScriptObject scriptObject = this.myScriptObject;
      string name = "OnFormInitialize";
      System.Type[] types = new System.Type[1];
      int index1 = 0;
      System.Type type = initializeEventArgs1.GetType();
      types[index1] = type;
      object[] objArray = new object[1];
      int index2 = 0;
      FormStockWorkOrderCmd.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
      objArray[index2] = (object) initializeEventArgs2;
      scriptObject.RunMethod(name, types, objArray);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormStockWorkOrderCmd));
      this.panelGrid = new PanelControl();
      this.sbtnAOProcessing = new SimpleButton();
      this.printButton1 = new PrintButton();
      this.previewButton1 = new PreviewButton();
      this.gridControlAll = new GridControl();
      this.gridViewStockMain = new GridView();
      this.colDocNo = new GridColumn();
      this.colDocDate = new GridColumn();
      this.colDescription = new GridColumn();
      this.colItemCode = new GridColumn();
      this.colLocation = new GridColumn();
      this.colBatchNo = new GridColumn();
      this.colProjNo = new GridColumn();
      this.colDeptNo = new GridColumn();
      this.colQty = new GridColumn();
      this.colTotal = new GridColumn();
      this.colAssemblyCost = new GridColumn();
      this.colNetTotal = new GridColumn();
      this.colRemark1 = new GridColumn();
      this.colRemark2 = new GridColumn();
      this.colRemark3 = new GridColumn();
      this.colRemark4 = new GridColumn();
      this.colPrintCount = new GridColumn();
      this.colCancelled = new GridColumn();
      this.repositoryItemTextEdit1 = new RepositoryItemTextEdit();
      this.colLastModified = new GridColumn();
      this.colLastModifiedUserID = new GridColumn();
      this.colCreatedTimeStamp = new GridColumn();
      this.colCreatedUserID = new GridColumn();
      this.colRefDocNo = new GridColumn();
      this.colIsMultilevel = new GridColumn();
      this.repChkedtIsMultilevel = new RepositoryItemCheckEdit();
      this.colTransferFrom = new GridColumn();
      this.colTransferTo = new GridColumn();
      this.colExpCompletedDate = new GridColumn();
      this.chkEdtShowAtStartup = new CheckEdit();
      this.sbtnRefresh = new SimpleButton();
      this.sbtnView = new SimpleButton();
      this.sbtnDel = new SimpleButton();
      this.sbtnEdit = new SimpleButton();
      this.barManager1 = new BarManager(this.components);
      this.bar1 = new Bar();
      this.barDockControlTop = new BarDockControl();
      this.barDockControlBottom = new BarDockControl();
      this.barDockControlLeft = new BarDockControl();
      this.barDockControlRight = new BarDockControl();
      this.panelCmd = new PanelControl();
      this.lblPrint = new HyperLinkEdit();
      this.lblFind = new HyperLinkEdit();
      this.lblNew = new HyperLinkEdit();
      this.lblShowAll = new Label();
      this.label1 = new Label();
      this.panelControl1 = new PanelControl();
      this.panelHeader1 = new PanelHeader();
      this.panelGrid.BeginInit();
      this.panelGrid.SuspendLayout();
      this.gridControlAll.BeginInit();
      this.gridViewStockMain.BeginInit();
      this.repositoryItemTextEdit1.BeginInit();
      this.repChkedtIsMultilevel.BeginInit();
      this.chkEdtShowAtStartup.Properties.BeginInit();
      this.barManager1.BeginInit();
      this.panelCmd.BeginInit();
      this.panelCmd.SuspendLayout();
      this.lblPrint.Properties.BeginInit();
      this.lblFind.Properties.BeginInit();
      this.lblNew.Properties.BeginInit();
      this.panelControl1.BeginInit();
      this.panelControl1.SuspendLayout();
      this.SuspendLayout();
      componentResourceManager.ApplyResources((object) this.panelGrid, "panelGrid");
      this.panelGrid.BorderStyle = BorderStyles.NoBorder;
      this.panelGrid.Controls.Add((Control) this.sbtnAOProcessing);
      this.panelGrid.Controls.Add((Control) this.printButton1);
      this.panelGrid.Controls.Add((Control) this.previewButton1);
      this.panelGrid.Controls.Add((Control) this.gridControlAll);
      this.panelGrid.Controls.Add((Control) this.chkEdtShowAtStartup);
      this.panelGrid.Controls.Add((Control) this.sbtnRefresh);
      this.panelGrid.Controls.Add((Control) this.sbtnView);
      this.panelGrid.Controls.Add((Control) this.sbtnDel);
      this.panelGrid.Controls.Add((Control) this.sbtnEdit);
      this.panelGrid.Name = "panelGrid";
      componentResourceManager.ApplyResources((object) this.sbtnAOProcessing, "sbtnAOProcessing");
      this.sbtnAOProcessing.Name = "sbtnAOProcessing";
      this.sbtnAOProcessing.Click += new EventHandler(this.sbtnAOProcessing_Click);
      componentResourceManager.ApplyResources((object) this.printButton1, "printButton1");
      this.printButton1.Name = "printButton1";
      this.printButton1.ReportType = "";
      componentResourceManager.ApplyResources((object) this.previewButton1, "previewButton1");
      this.previewButton1.Name = "previewButton1";
      this.previewButton1.ReportType = "";
      componentResourceManager.ApplyResources((object) this.gridControlAll, "gridControlAll");
      this.gridControlAll.EmbeddedNavigator.AccessibleDescription = componentResourceManager.GetString("gridControlAll.EmbeddedNavigator.AccessibleDescription");
      this.gridControlAll.EmbeddedNavigator.AccessibleName = componentResourceManager.GetString("gridControlAll.EmbeddedNavigator.AccessibleName");
      this.gridControlAll.EmbeddedNavigator.AllowHtmlTextInToolTip = (DefaultBoolean) componentResourceManager.GetObject("gridControlAll.EmbeddedNavigator.AllowHtmlTextInToolTip");
      this.gridControlAll.EmbeddedNavigator.Anchor = (AnchorStyles) componentResourceManager.GetObject("gridControlAll.EmbeddedNavigator.Anchor");
      this.gridControlAll.EmbeddedNavigator.BackgroundImage = (Image) componentResourceManager.GetObject("gridControlAll.EmbeddedNavigator.BackgroundImage");
      this.gridControlAll.EmbeddedNavigator.BackgroundImageLayout = (ImageLayout) componentResourceManager.GetObject("gridControlAll.EmbeddedNavigator.BackgroundImageLayout");
      this.gridControlAll.EmbeddedNavigator.Buttons.Append.Enabled = false;
      this.gridControlAll.EmbeddedNavigator.Buttons.Append.Hint = componentResourceManager.GetString("gridControlAll.EmbeddedNavigator.Buttons.Append.Hint");
      this.gridControlAll.EmbeddedNavigator.Buttons.Append.Visible = false;
      this.gridControlAll.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
      this.gridControlAll.EmbeddedNavigator.Buttons.CancelEdit.Hint = componentResourceManager.GetString("gridControlAll.EmbeddedNavigator.Buttons.CancelEdit.Hint");
      this.gridControlAll.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
      this.gridControlAll.EmbeddedNavigator.Buttons.Edit.Enabled = false;
      this.gridControlAll.EmbeddedNavigator.Buttons.Edit.Hint = componentResourceManager.GetString("gridControlAll.EmbeddedNavigator.Buttons.Edit.Hint");
      this.gridControlAll.EmbeddedNavigator.Buttons.Edit.Visible = false;
      this.gridControlAll.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
      this.gridControlAll.EmbeddedNavigator.Buttons.EndEdit.Hint = componentResourceManager.GetString("gridControlAll.EmbeddedNavigator.Buttons.EndEdit.Hint");
      this.gridControlAll.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
      this.gridControlAll.EmbeddedNavigator.Buttons.Remove.Enabled = false;
      this.gridControlAll.EmbeddedNavigator.Buttons.Remove.Hint = componentResourceManager.GetString("gridControlAll.EmbeddedNavigator.Buttons.Remove.Hint");
      this.gridControlAll.EmbeddedNavigator.Buttons.Remove.Visible = false;
      this.gridControlAll.EmbeddedNavigator.ImeMode = (ImeMode) componentResourceManager.GetObject("gridControlAll.EmbeddedNavigator.ImeMode");
      this.gridControlAll.EmbeddedNavigator.MaximumSize = (Size) componentResourceManager.GetObject("gridControlAll.EmbeddedNavigator.MaximumSize");
      this.gridControlAll.EmbeddedNavigator.TextLocation = (NavigatorButtonsTextLocation) componentResourceManager.GetObject("gridControlAll.EmbeddedNavigator.TextLocation");
      this.gridControlAll.EmbeddedNavigator.ToolTip = componentResourceManager.GetString("gridControlAll.EmbeddedNavigator.ToolTip");
      this.gridControlAll.EmbeddedNavigator.ToolTipIconType = (ToolTipIconType) componentResourceManager.GetObject("gridControlAll.EmbeddedNavigator.ToolTipIconType");
      this.gridControlAll.EmbeddedNavigator.ToolTipTitle = componentResourceManager.GetString("gridControlAll.EmbeddedNavigator.ToolTipTitle");
      this.gridControlAll.MainView = (BaseView) this.gridViewStockMain;
      this.gridControlAll.Name = "gridControlAll";
      RepositoryItemCollection repositoryItems = this.gridControlAll.RepositoryItems;
      RepositoryItem[] items1 = new RepositoryItem[2];
      int index1 = 0;
      RepositoryItemTextEdit repositoryItemTextEdit = this.repositoryItemTextEdit1;
      items1[index1] = (RepositoryItem) repositoryItemTextEdit;
      int index2 = 1;
      RepositoryItemCheckEdit repositoryItemCheckEdit = this.repChkedtIsMultilevel;
      items1[index2] = (RepositoryItem) repositoryItemCheckEdit;
      repositoryItems.AddRange(items1);
      this.gridControlAll.UseEmbeddedNavigator = true;
      ViewRepositoryCollection viewCollection = this.gridControlAll.ViewCollection;
      BaseView[] views = new BaseView[1];
      int index3 = 0;
      GridView gridView = this.gridViewStockMain;
      views[index3] = (BaseView) gridView;
      viewCollection.AddRange(views);
      componentResourceManager.ApplyResources((object) this.gridViewStockMain, "gridViewStockMain");
      GridColumnCollection columns1 = this.gridViewStockMain.Columns;
      GridColumn[] columns2 = new GridColumn[27];
      int index4 = 0;
      GridColumn gridColumn1 = this.colDocNo;
      columns2[index4] = gridColumn1;
      int index5 = 1;
      GridColumn gridColumn2 = this.colDocDate;
      columns2[index5] = gridColumn2;
      int index6 = 2;
      GridColumn gridColumn3 = this.colDescription;
      columns2[index6] = gridColumn3;
      int index7 = 3;
      GridColumn gridColumn4 = this.colItemCode;
      columns2[index7] = gridColumn4;
      int index8 = 4;
      GridColumn gridColumn5 = this.colLocation;
      columns2[index8] = gridColumn5;
      int index9 = 5;
      GridColumn gridColumn6 = this.colBatchNo;
      columns2[index9] = gridColumn6;
      int index10 = 6;
      GridColumn gridColumn7 = this.colProjNo;
      columns2[index10] = gridColumn7;
      int index11 = 7;
      GridColumn gridColumn8 = this.colDeptNo;
      columns2[index11] = gridColumn8;
      int index12 = 8;
      GridColumn gridColumn9 = this.colQty;
      columns2[index12] = gridColumn9;
      int index13 = 9;
      GridColumn gridColumn10 = this.colTotal;
      columns2[index13] = gridColumn10;
      int index14 = 10;
      GridColumn gridColumn11 = this.colAssemblyCost;
      columns2[index14] = gridColumn11;
      int index15 = 11;
      GridColumn gridColumn12 = this.colNetTotal;
      columns2[index15] = gridColumn12;
      int index16 = 12;
      GridColumn gridColumn13 = this.colRemark1;
      columns2[index16] = gridColumn13;
      int index17 = 13;
      GridColumn gridColumn14 = this.colRemark2;
      columns2[index17] = gridColumn14;
      int index18 = 14;
      GridColumn gridColumn15 = this.colRemark3;
      columns2[index18] = gridColumn15;
      int index19 = 15;
      GridColumn gridColumn16 = this.colRemark4;
      columns2[index19] = gridColumn16;
      int index20 = 16;
      GridColumn gridColumn17 = this.colPrintCount;
      columns2[index20] = gridColumn17;
      int index21 = 17;
      GridColumn gridColumn18 = this.colCancelled;
      columns2[index21] = gridColumn18;
      int index22 = 18;
      GridColumn gridColumn19 = this.colLastModified;
      columns2[index22] = gridColumn19;
      int index23 = 19;
      GridColumn gridColumn20 = this.colLastModifiedUserID;
      columns2[index23] = gridColumn20;
      int index24 = 20;
      GridColumn gridColumn21 = this.colCreatedTimeStamp;
      columns2[index24] = gridColumn21;
      int index25 = 21;
      GridColumn gridColumn22 = this.colCreatedUserID;
      columns2[index25] = gridColumn22;
      int index26 = 22;
      GridColumn gridColumn23 = this.colRefDocNo;
      columns2[index26] = gridColumn23;
      int index27 = 23;
      GridColumn gridColumn24 = this.colIsMultilevel;
      columns2[index27] = gridColumn24;
      int index28 = 24;
      GridColumn gridColumn25 = this.colTransferFrom;
      columns2[index28] = gridColumn25;
      int index29 = 25;
      GridColumn gridColumn26 = this.colTransferTo;
      columns2[index29] = gridColumn26;
      int index30 = 26;
      GridColumn gridColumn27 = this.colExpCompletedDate;
      columns2[index30] = gridColumn27;
      columns1.AddRange(columns2);
      this.gridViewStockMain.GridControl = this.gridControlAll;
      this.gridViewStockMain.Name = "gridViewStockMain";
      this.gridViewStockMain.OptionsBehavior.AllowIncrementalSearch = true;
      this.gridViewStockMain.OptionsView.ShowFooter = true;
      GridColumnSortInfoCollection sortInfo = this.gridViewStockMain.SortInfo;
      GridColumnSortInfo[] sortInfos = new GridColumnSortInfo[1];
      int index31 = 0;
      GridColumnSortInfo gridColumnSortInfo = new GridColumnSortInfo(this.colDocNo, ColumnSortOrder.Descending);
      sortInfos[index31] = gridColumnSortInfo;
      sortInfo.AddRange(sortInfos);
      this.gridViewStockMain.Layout += new EventHandler(this.gridViewStockMain_Layout);
      componentResourceManager.ApplyResources((object) this.colDocNo, "colDocNo");
      this.colDocNo.FieldName = "DocNo";
      this.colDocNo.Name = "colDocNo";
      this.colDocNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDocDate, "colDocDate");
      this.colDocDate.FieldName = "DocDate";
      this.colDocDate.Name = "colDocDate";
      this.colDocDate.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDescription, "colDescription");
      this.colDescription.FieldName = "Description";
      this.colDescription.Name = "colDescription";
      this.colDescription.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colItemCode, "colItemCode");
      this.colItemCode.FieldName = "ItemCode";
      this.colItemCode.Name = "colItemCode";
      this.colItemCode.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colLocation, "colLocation");
      this.colLocation.FieldName = "Location";
      this.colLocation.Name = "colLocation";
      this.colLocation.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colBatchNo, "colBatchNo");
      this.colBatchNo.FieldName = "BatchNo";
      this.colBatchNo.Name = "colBatchNo";
      this.colBatchNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colProjNo, "colProjNo");
      this.colProjNo.FieldName = "ProjNo";
      this.colProjNo.Name = "colProjNo";
      this.colProjNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDeptNo, "colDeptNo");
      this.colDeptNo.FieldName = "DeptNo";
      this.colDeptNo.Name = "colDeptNo";
      this.colDeptNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colQty, "colQty");
      this.colQty.FieldName = "Qty";
      this.colQty.Name = "colQty";
      this.colQty.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colTotal, "colTotal");
      this.colTotal.FieldName = "Total";
      this.colTotal.Name = "colTotal";
      this.colTotal.OptionsColumn.AllowEdit = false;
      GridColumnSummaryItemCollection summary1 = this.colTotal.Summary;
      GridSummaryItem[] items2 = new GridSummaryItem[1];
      int index32 = 0;
      GridColumnSummaryItem columnSummaryItem1 = new GridColumnSummaryItem((SummaryItemType) componentResourceManager.GetObject("colTotal.Summary"));
      items2[index32] = (GridSummaryItem) columnSummaryItem1;
      summary1.AddRange(items2);
      componentResourceManager.ApplyResources((object) this.colAssemblyCost, "colAssemblyCost");
      this.colAssemblyCost.FieldName = "AssemblyCost";
      this.colAssemblyCost.Name = "colAssemblyCost";
      this.colAssemblyCost.OptionsColumn.AllowEdit = false;
      GridColumnSummaryItemCollection summary2 = this.colAssemblyCost.Summary;
      GridSummaryItem[] items3 = new GridSummaryItem[1];
      int index33 = 0;
      GridColumnSummaryItem columnSummaryItem2 = new GridColumnSummaryItem((SummaryItemType) componentResourceManager.GetObject("colAssemblyCost.Summary"));
      items3[index33] = (GridSummaryItem) columnSummaryItem2;
      summary2.AddRange(items3);
      componentResourceManager.ApplyResources((object) this.colNetTotal, "colNetTotal");
      this.colNetTotal.FieldName = "NetTotal";
      this.colNetTotal.Name = "colNetTotal";
      this.colNetTotal.OptionsColumn.AllowEdit = false;
      GridColumnSummaryItemCollection summary3 = this.colNetTotal.Summary;
      GridSummaryItem[] items4 = new GridSummaryItem[1];
      int index34 = 0;
      GridColumnSummaryItem columnSummaryItem3 = new GridColumnSummaryItem((SummaryItemType) componentResourceManager.GetObject("colNetTotal.Summary"));
      items4[index34] = (GridSummaryItem) columnSummaryItem3;
      summary3.AddRange(items4);
      componentResourceManager.ApplyResources((object) this.colRemark1, "colRemark1");
      this.colRemark1.FieldName = "Remark1";
      this.colRemark1.Name = "colRemark1";
      this.colRemark1.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark2, "colRemark2");
      this.colRemark2.FieldName = "Remark2";
      this.colRemark2.Name = "colRemark2";
      this.colRemark2.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark3, "colRemark3");
      this.colRemark3.FieldName = "Remark3";
      this.colRemark3.Name = "colRemark3";
      this.colRemark3.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark4, "colRemark4");
      this.colRemark4.FieldName = "Remark4";
      this.colRemark4.Name = "colRemark4";
      this.colRemark4.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colPrintCount, "colPrintCount");
      this.colPrintCount.FieldName = "PrintCount";
      this.colPrintCount.Name = "colPrintCount";
      this.colPrintCount.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colCancelled, "colCancelled");
      this.colCancelled.ColumnEdit = (RepositoryItem) this.repositoryItemTextEdit1;
      this.colCancelled.FieldName = "Cancelled";
      this.colCancelled.Name = "colCancelled";
      this.colCancelled.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.repositoryItemTextEdit1, "repositoryItemTextEdit1");
      this.repositoryItemTextEdit1.Mask.AutoComplete = (AutoCompleteType) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.AutoComplete");
      this.repositoryItemTextEdit1.Mask.BeepOnError = (bool) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.BeepOnError");
      this.repositoryItemTextEdit1.Mask.EditMask = componentResourceManager.GetString("repositoryItemTextEdit1.Mask.EditMask");
      this.repositoryItemTextEdit1.Mask.IgnoreMaskBlank = (bool) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.IgnoreMaskBlank");
      this.repositoryItemTextEdit1.Mask.MaskType = (DevExpress.XtraEditors.Mask.MaskType) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.MaskType");
      this.repositoryItemTextEdit1.Mask.PlaceHolder = (char) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.PlaceHolder");
      this.repositoryItemTextEdit1.Mask.SaveLiteral = (bool) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.SaveLiteral");
      this.repositoryItemTextEdit1.Mask.ShowPlaceHolders = (bool) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.ShowPlaceHolders");
      this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = (bool) componentResourceManager.GetObject("repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat");
      this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
      this.repositoryItemTextEdit1.FormatEditValue += new ConvertEditValueEventHandler(this.repositoryItemTextEdit1_FormatEditValue);
      componentResourceManager.ApplyResources((object) this.colLastModified, "colLastModified");
      this.colLastModified.FieldName = "LastModified";
      this.colLastModified.Name = "colLastModified";
      this.colLastModified.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colLastModifiedUserID, "colLastModifiedUserID");
      this.colLastModifiedUserID.FieldName = "LastModifiedUserID";
      this.colLastModifiedUserID.Name = "colLastModifiedUserID";
      this.colLastModifiedUserID.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colCreatedTimeStamp, "colCreatedTimeStamp");
      this.colCreatedTimeStamp.FieldName = "CreatedTimeStamp";
      this.colCreatedTimeStamp.Name = "colCreatedTimeStamp";
      this.colCreatedTimeStamp.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colCreatedUserID, "colCreatedUserID");
      this.colCreatedUserID.FieldName = "CreatedUserID";
      this.colCreatedUserID.Name = "colCreatedUserID";
      this.colCreatedUserID.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRefDocNo, "colRefDocNo");
      this.colRefDocNo.FieldName = "RefDocNo";
      this.colRefDocNo.Name = "colRefDocNo";
      this.colRefDocNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colIsMultilevel, "colIsMultilevel");
      this.colIsMultilevel.ColumnEdit = (RepositoryItem) this.repChkedtIsMultilevel;
      this.colIsMultilevel.FieldName = "IsMultilevel";
      this.colIsMultilevel.Name = "colIsMultilevel";
      componentResourceManager.ApplyResources((object) this.repChkedtIsMultilevel, "repChkedtIsMultilevel");
      this.repChkedtIsMultilevel.Name = "repChkedtIsMultilevel";
      this.repChkedtIsMultilevel.ValueChecked = (object) "T";
      this.repChkedtIsMultilevel.ValueUnchecked = (object) "F";
      componentResourceManager.ApplyResources((object) this.colTransferFrom, "colTransferFrom");
      this.colTransferFrom.FieldName = "TransferFrom";
      this.colTransferFrom.Name = "colTransferFrom";
      this.colTransferFrom.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colTransferTo, "colTransferTo");
      this.colTransferTo.FieldName = "TransferTo";
      this.colTransferTo.Name = "colTransferTo";
      this.colTransferTo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colExpCompletedDate, "colExpCompletedDate");
      this.colExpCompletedDate.FieldName = "ExpectedCompletedDate";
      this.colExpCompletedDate.Name = "colExpCompletedDate";
      this.colExpCompletedDate.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.chkEdtShowAtStartup, "chkEdtShowAtStartup");
      this.chkEdtShowAtStartup.Name = "chkEdtShowAtStartup";
      this.chkEdtShowAtStartup.Properties.AccessibleDescription = componentResourceManager.GetString("chkEdtShowAtStartup.Properties.AccessibleDescription");
      this.chkEdtShowAtStartup.Properties.AccessibleName = componentResourceManager.GetString("chkEdtShowAtStartup.Properties.AccessibleName");
      this.chkEdtShowAtStartup.Properties.Appearance.BackColor = (Color) componentResourceManager.GetObject("chkEdtShowAtStartup.Properties.Appearance.BackColor");
      this.chkEdtShowAtStartup.Properties.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("chkEdtShowAtStartup.Properties.Appearance.GradientMode");
      this.chkEdtShowAtStartup.Properties.Appearance.Image = (Image) componentResourceManager.GetObject("chkEdtShowAtStartup.Properties.Appearance.Image");
      this.chkEdtShowAtStartup.Properties.Appearance.Options.UseBackColor = true;
      this.chkEdtShowAtStartup.Properties.AutoHeight = (bool) componentResourceManager.GetObject("chkEdtShowAtStartup.Properties.AutoHeight");
      this.chkEdtShowAtStartup.Properties.Caption = componentResourceManager.GetString("chkEdtShowAtStartup.Properties.Caption");
      this.chkEdtShowAtStartup.Properties.DisplayValueChecked = componentResourceManager.GetString("chkEdtShowAtStartup.Properties.DisplayValueChecked");
      this.chkEdtShowAtStartup.Properties.DisplayValueGrayed = componentResourceManager.GetString("chkEdtShowAtStartup.Properties.DisplayValueGrayed");
      this.chkEdtShowAtStartup.Properties.DisplayValueUnchecked = componentResourceManager.GetString("chkEdtShowAtStartup.Properties.DisplayValueUnchecked");
      this.chkEdtShowAtStartup.CheckedChanged += new EventHandler(this.chkEdtShowAtStartup_CheckedChanged);
      componentResourceManager.ApplyResources((object) this.sbtnRefresh, "sbtnRefresh");
      this.sbtnRefresh.Name = "sbtnRefresh";
      componentResourceManager.ApplyResources((object) this.sbtnView, "sbtnView");
      this.sbtnView.Name = "sbtnView";
      componentResourceManager.ApplyResources((object) this.sbtnDel, "sbtnDel");
      this.sbtnDel.Appearance.ForeColor = (Color) componentResourceManager.GetObject("sbtnDel.Appearance.ForeColor");
      this.sbtnDel.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("sbtnDel.Appearance.GradientMode");
      this.sbtnDel.Appearance.Image = (Image) componentResourceManager.GetObject("sbtnDel.Appearance.Image");
      this.sbtnDel.Appearance.Options.UseForeColor = true;
      this.sbtnDel.Name = "sbtnDel";
      componentResourceManager.ApplyResources((object) this.sbtnEdit, "sbtnEdit");
      this.sbtnEdit.Name = "sbtnEdit";
      Bars bars1 = this.barManager1.Bars;
      Bar[] bars2 = new Bar[1];
      int index35 = 0;
      Bar bar = this.bar1;
      bars2[index35] = bar;
      bars1.AddRange(bars2);
      this.barManager1.DockControls.Add(this.barDockControlTop);
      this.barManager1.DockControls.Add(this.barDockControlBottom);
      this.barManager1.DockControls.Add(this.barDockControlLeft);
      this.barManager1.DockControls.Add(this.barDockControlRight);
      this.barManager1.Form = (Control) this;
      this.barManager1.MainMenu = this.bar1;
      this.barManager1.MaxItemId = 0;
      this.bar1.BarName = "Custom 1";
      this.bar1.DockCol = 0;
      this.bar1.DockRow = 0;
      this.bar1.DockStyle = BarDockStyle.Top;
      this.bar1.OptionsBar.AllowQuickCustomization = false;
      this.bar1.OptionsBar.DrawDragBorder = false;
      this.bar1.OptionsBar.MultiLine = true;
      this.bar1.OptionsBar.UseWholeRow = true;
      componentResourceManager.ApplyResources((object) this.bar1, "bar1");
      componentResourceManager.ApplyResources((object) this.barDockControlTop, "barDockControlTop");
      this.barDockControlTop.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("barDockControlTop.Appearance.GradientMode");
      this.barDockControlTop.Appearance.Image = (Image) componentResourceManager.GetObject("barDockControlTop.Appearance.Image");
      this.barDockControlTop.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlBottom, "barDockControlBottom");
      this.barDockControlBottom.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("barDockControlBottom.Appearance.GradientMode");
      this.barDockControlBottom.Appearance.Image = (Image) componentResourceManager.GetObject("barDockControlBottom.Appearance.Image");
      this.barDockControlBottom.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlLeft, "barDockControlLeft");
      this.barDockControlLeft.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("barDockControlLeft.Appearance.GradientMode");
      this.barDockControlLeft.Appearance.Image = (Image) componentResourceManager.GetObject("barDockControlLeft.Appearance.Image");
      this.barDockControlLeft.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlRight, "barDockControlRight");
      this.barDockControlRight.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("barDockControlRight.Appearance.GradientMode");
      this.barDockControlRight.Appearance.Image = (Image) componentResourceManager.GetObject("barDockControlRight.Appearance.Image");
      this.barDockControlRight.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.panelCmd, "panelCmd");
      this.panelCmd.BorderStyle = BorderStyles.NoBorder;
      this.panelCmd.Controls.Add((Control) this.lblPrint);
      this.panelCmd.Controls.Add((Control) this.lblFind);
      this.panelCmd.Controls.Add((Control) this.lblNew);
      this.panelCmd.Controls.Add((Control) this.lblShowAll);
      this.panelCmd.Controls.Add((Control) this.label1);
      this.panelCmd.Name = "panelCmd";
      componentResourceManager.ApplyResources((object) this.lblPrint, "lblPrint");
      this.lblPrint.Name = "lblPrint";
      this.lblPrint.Properties.AccessibleDescription = componentResourceManager.GetString("lblPrint.Properties.AccessibleDescription");
      this.lblPrint.Properties.AccessibleName = componentResourceManager.GetString("lblPrint.Properties.AccessibleName");
      this.lblPrint.Properties.Appearance.BackColor = (Color) componentResourceManager.GetObject("lblPrint.Properties.Appearance.BackColor");
      this.lblPrint.Properties.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("lblPrint.Properties.Appearance.GradientMode");
      this.lblPrint.Properties.Appearance.Image = (Image) componentResourceManager.GetObject("lblPrint.Properties.Appearance.Image");
      this.lblPrint.Properties.Appearance.Options.UseBackColor = true;
      this.lblPrint.Properties.AutoHeight = (bool) componentResourceManager.GetObject("lblPrint.Properties.AutoHeight");
      this.lblPrint.Properties.BorderStyle = BorderStyles.NoBorder;
      this.lblPrint.Properties.Caption = componentResourceManager.GetString("lblPrint.Properties.Caption");
      this.lblPrint.Properties.LinkColor = Color.Black;
      this.lblPrint.Properties.Mask.AutoComplete = (AutoCompleteType) componentResourceManager.GetObject("lblPrint.Properties.Mask.AutoComplete");
      this.lblPrint.Properties.Mask.BeepOnError = (bool) componentResourceManager.GetObject("lblPrint.Properties.Mask.BeepOnError");
      this.lblPrint.Properties.Mask.EditMask = componentResourceManager.GetString("lblPrint.Properties.Mask.EditMask");
      this.lblPrint.Properties.Mask.IgnoreMaskBlank = (bool) componentResourceManager.GetObject("lblPrint.Properties.Mask.IgnoreMaskBlank");
      this.lblPrint.Properties.Mask.MaskType = (DevExpress.XtraEditors.Mask.MaskType) componentResourceManager.GetObject("lblPrint.Properties.Mask.MaskType");
      this.lblPrint.Properties.Mask.PlaceHolder = (char) componentResourceManager.GetObject("lblPrint.Properties.Mask.PlaceHolder");
      this.lblPrint.Properties.Mask.SaveLiteral = (bool) componentResourceManager.GetObject("lblPrint.Properties.Mask.SaveLiteral");
      this.lblPrint.Properties.Mask.ShowPlaceHolders = (bool) componentResourceManager.GetObject("lblPrint.Properties.Mask.ShowPlaceHolders");
      this.lblPrint.Properties.Mask.UseMaskAsDisplayFormat = (bool) componentResourceManager.GetObject("lblPrint.Properties.Mask.UseMaskAsDisplayFormat");
      this.lblPrint.Properties.NullValuePrompt = componentResourceManager.GetString("lblPrint.Properties.NullValuePrompt");
      this.lblPrint.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("lblPrint.Properties.NullValuePromptShowForEmptyValue");
      componentResourceManager.ApplyResources((object) this.lblFind, "lblFind");
      this.lblFind.Name = "lblFind";
      this.lblFind.Properties.AccessibleDescription = componentResourceManager.GetString("lblFind.Properties.AccessibleDescription");
      this.lblFind.Properties.AccessibleName = componentResourceManager.GetString("lblFind.Properties.AccessibleName");
      this.lblFind.Properties.Appearance.BackColor = (Color) componentResourceManager.GetObject("lblFind.Properties.Appearance.BackColor");
      this.lblFind.Properties.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("lblFind.Properties.Appearance.GradientMode");
      this.lblFind.Properties.Appearance.Image = (Image) componentResourceManager.GetObject("lblFind.Properties.Appearance.Image");
      this.lblFind.Properties.Appearance.Options.UseBackColor = true;
      this.lblFind.Properties.AutoHeight = (bool) componentResourceManager.GetObject("lblFind.Properties.AutoHeight");
      this.lblFind.Properties.BorderStyle = BorderStyles.NoBorder;
      this.lblFind.Properties.Caption = componentResourceManager.GetString("lblFind.Properties.Caption");
      this.lblFind.Properties.LinkColor = Color.Black;
      this.lblFind.Properties.Mask.AutoComplete = (AutoCompleteType) componentResourceManager.GetObject("lblFind.Properties.Mask.AutoComplete");
      this.lblFind.Properties.Mask.BeepOnError = (bool) componentResourceManager.GetObject("lblFind.Properties.Mask.BeepOnError");
      this.lblFind.Properties.Mask.EditMask = componentResourceManager.GetString("lblFind.Properties.Mask.EditMask");
      this.lblFind.Properties.Mask.IgnoreMaskBlank = (bool) componentResourceManager.GetObject("lblFind.Properties.Mask.IgnoreMaskBlank");
      this.lblFind.Properties.Mask.MaskType = (DevExpress.XtraEditors.Mask.MaskType) componentResourceManager.GetObject("lblFind.Properties.Mask.MaskType");
      this.lblFind.Properties.Mask.PlaceHolder = (char) componentResourceManager.GetObject("lblFind.Properties.Mask.PlaceHolder");
      this.lblFind.Properties.Mask.SaveLiteral = (bool) componentResourceManager.GetObject("lblFind.Properties.Mask.SaveLiteral");
      this.lblFind.Properties.Mask.ShowPlaceHolders = (bool) componentResourceManager.GetObject("lblFind.Properties.Mask.ShowPlaceHolders");
      this.lblFind.Properties.Mask.UseMaskAsDisplayFormat = (bool) componentResourceManager.GetObject("lblFind.Properties.Mask.UseMaskAsDisplayFormat");
      this.lblFind.Properties.NullValuePrompt = componentResourceManager.GetString("lblFind.Properties.NullValuePrompt");
      this.lblFind.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("lblFind.Properties.NullValuePromptShowForEmptyValue");
      componentResourceManager.ApplyResources((object) this.lblNew, "lblNew");
      this.lblNew.Name = "lblNew";
      this.lblNew.Properties.AccessibleDescription = componentResourceManager.GetString("lblNew.Properties.AccessibleDescription");
      this.lblNew.Properties.AccessibleName = componentResourceManager.GetString("lblNew.Properties.AccessibleName");
      this.lblNew.Properties.Appearance.BackColor = (Color) componentResourceManager.GetObject("lblNew.Properties.Appearance.BackColor");
      this.lblNew.Properties.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("lblNew.Properties.Appearance.GradientMode");
      this.lblNew.Properties.Appearance.Image = (Image) componentResourceManager.GetObject("lblNew.Properties.Appearance.Image");
      this.lblNew.Properties.Appearance.Options.UseBackColor = true;
      this.lblNew.Properties.AutoHeight = (bool) componentResourceManager.GetObject("lblNew.Properties.AutoHeight");
      this.lblNew.Properties.BorderStyle = BorderStyles.NoBorder;
      this.lblNew.Properties.Caption = componentResourceManager.GetString("lblNew.Properties.Caption");
      this.lblNew.Properties.LinkColor = Color.DarkGoldenrod;
      this.lblNew.Properties.Mask.AutoComplete = (AutoCompleteType) componentResourceManager.GetObject("lblNew.Properties.Mask.AutoComplete");
      this.lblNew.Properties.Mask.BeepOnError = (bool) componentResourceManager.GetObject("lblNew.Properties.Mask.BeepOnError");
      this.lblNew.Properties.Mask.EditMask = componentResourceManager.GetString("lblNew.Properties.Mask.EditMask");
      this.lblNew.Properties.Mask.IgnoreMaskBlank = (bool) componentResourceManager.GetObject("lblNew.Properties.Mask.IgnoreMaskBlank");
      this.lblNew.Properties.Mask.MaskType = (DevExpress.XtraEditors.Mask.MaskType) componentResourceManager.GetObject("lblNew.Properties.Mask.MaskType");
      this.lblNew.Properties.Mask.PlaceHolder = (char) componentResourceManager.GetObject("lblNew.Properties.Mask.PlaceHolder");
      this.lblNew.Properties.Mask.SaveLiteral = (bool) componentResourceManager.GetObject("lblNew.Properties.Mask.SaveLiteral");
      this.lblNew.Properties.Mask.ShowPlaceHolders = (bool) componentResourceManager.GetObject("lblNew.Properties.Mask.ShowPlaceHolders");
      this.lblNew.Properties.Mask.UseMaskAsDisplayFormat = (bool) componentResourceManager.GetObject("lblNew.Properties.Mask.UseMaskAsDisplayFormat");
      this.lblNew.Properties.NullValuePrompt = componentResourceManager.GetString("lblNew.Properties.NullValuePrompt");
      this.lblNew.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("lblNew.Properties.NullValuePromptShowForEmptyValue");
      componentResourceManager.ApplyResources((object) this.lblShowAll, "lblShowAll");
      this.lblShowAll.Cursor = Cursors.Hand;
      this.lblShowAll.ForeColor = Color.Black;
      this.lblShowAll.Name = "lblShowAll";
      componentResourceManager.ApplyResources((object) this.label1, "label1");
      this.label1.ForeColor = Color.FromArgb(56, 115, 250);
      this.label1.Name = "label1";
      componentResourceManager.ApplyResources((object) this.panelControl1, "panelControl1");
      this.panelControl1.BorderStyle = BorderStyles.NoBorder;
      this.panelControl1.Controls.Add((Control) this.panelGrid);
      this.panelControl1.Name = "panelControl1";
      componentResourceManager.ApplyResources((object) this.panelHeader1, "panelHeader1");
      this.panelHeader1.HelpTopicId = "Stock_Assembly_Order.htm";
      this.panelHeader1.Name = "panelHeader1";
      componentResourceManager.ApplyResources((object) this, "$this");
      this.AutoScaleMode = AutoScaleMode.Dpi;
      this.Controls.Add((Control) this.panelControl1);
      this.Controls.Add((Control) this.panelCmd);
      this.Controls.Add((Control) this.panelHeader1);
      this.Controls.Add((Control) this.barDockControlLeft);
      this.Controls.Add((Control) this.barDockControlRight);
      this.Controls.Add((Control) this.barDockControlBottom);
      this.Controls.Add((Control) this.barDockControlTop);
      this.KeyPreview = true;
      this.Name = "FormStockWorkOrderCmd";
      this.WindowState = FormWindowState.Maximized;
      this.FormClosed += new FormClosedEventHandler(this.FormStockWorkOrderCmd_FormClosed);
      this.Load += new EventHandler(this.FormStockAssemblyCmd_Load);
      this.panelGrid.EndInit();
      this.panelGrid.ResumeLayout(false);
      this.gridControlAll.EndInit();
      this.gridViewStockMain.EndInit();
      this.repositoryItemTextEdit1.EndInit();
      this.repChkedtIsMultilevel.EndInit();
      this.chkEdtShowAtStartup.Properties.EndInit();
      this.barManager1.EndInit();
      this.panelCmd.EndInit();
      this.panelCmd.ResumeLayout(false);
      this.panelCmd.PerformLayout();
      this.lblPrint.Properties.EndInit();
      this.lblFind.Properties.EndInit();
      this.lblNew.Properties.EndInit();
      this.panelControl1.EndInit();
      this.panelControl1.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    public class FormEventArgs
    {
      private FormStockWorkOrderCmd myForm;

      public StockWorkOrderCommand Command
      {
        get
        {
          return this.myForm.myStockWorkOrderCmd;
        }
      }

      public PanelControl PanelAction
      {
        get
        {
          return this.myForm.panelCmd;
        }
      }

      public PanelControl PanelGrid
      {
        get
        {
          return this.myForm.panelGrid;
        }
      }

      public GridControl GridControl
      {
        get
        {
          return this.myForm.gridControlAll;
        }
      }

      public FormStockWorkOrderCmd Form
      {
        get
        {
          return this.myForm;
        }
      }

      public DBSetting DBSetting
      {
        get
        {
          return this.myForm.myDBSetting;
        }
      }

      public FormEventArgs(FormStockWorkOrderCmd form)
      {
        this.myForm = form;
      }
    }

    public class FormInitializeEventArgs : FormStockWorkOrderCmd.FormEventArgs
    {
      public FormInitializeEventArgs(FormStockWorkOrderCmd form)
        : base(form)
      {
      }
    }

    public class FormClosedEventArgs : FormStockWorkOrderCmd.FormEventArgs
    {
      public FormClosedEventArgs(FormStockWorkOrderCmd form)
        : base(form)
      {
      }
    }

    public class FormLoadDataEventArgs : FormStockWorkOrderCmd.FormEventArgs
    {
      private DataTable myGridDataTable;

      public DataTable GridDataTable
      {
        get
        {
          return this.myGridDataTable;
        }
      }

      public FormLoadDataEventArgs(FormStockWorkOrderCmd form, DataTable gridDataTable)
        : base(form)
      {
        this.myGridDataTable = gridDataTable;
      }
    }
  }
}
