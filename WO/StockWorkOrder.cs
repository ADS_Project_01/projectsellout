﻿// Type: BCE.AutoCount.Manufacturing.StockWorkOrder.StockWorkOrder
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount.Common;
using BCE.AutoCount.ContextException;
using BCE.AutoCount.Document;
using BCE.AutoCount.Invoicing;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.RegistryID.PrimaryKeyID;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.Stock;
using BCE.AutoCount.UDF;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using DevExpress.XtraTreeList.Nodes;
using System;
using System.Collections;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;

namespace RPASystem.WorkOrder
{
  public class StockWorkOrder : IImportExport
  {
    private string myDocNoFormatName = "";
    private const string MasterTableName = "Master";
    private const string DetailTableName = "Detail";
    private const string ASMBOMOptionalTableName = "ASMBOMOptional";
    internal DataSet myDataSet;
    private StockWorkOrderAction myAction;
    private DataRow myRow;
    private StockWorkOrderCommand myCommand;
    private DataTable myMasterTable;
    private DataTable myDetailTable;
    private DataTable myASMBOMOptionalTable;
    private bool myEnableAutoLoadItemDetail;
    private int myColumnChangedCounter;
    private int myColumnChangingCounter;
    private int myUpdateCostCounter;
    private bool myKeepLastModifiedInfo;
    private UpdateBusinessFlowDelegate myUpdateBusinessFlowEvent;
    internal ScriptObject myScriptObject;
    private SetDocNoFormatEventHandler SetDocNoFormatEvent2;
        private ResumeUndoDelegate ResumeUndoEvent2;
        private PauseUndoDelegate PauseUndoEvent2;
        private ConfirmChangingItemCodeEventHandler DetailItemCodeChangedEvent2;
        private ConfirmChangingItemCodeEventHandler MasterItemCodeChangedEvent2;
        public ScriptObject ScriptObject
    {
      get
      {
        return this.myScriptObject;
      }
    }

    public string DocNoFormatName
    {
      get
      {
        return this.myDocNoFormatName;
      }
      set
      {
        if (this.SetDocNoFormatEvent2 != null)
          this.SetDocNoFormatEvent2(value);
        this.myDocNoFormatName = value;
      }
    }

    public bool EnableAutoLoadItemDetail
    {
      get
      {
        return this.myEnableAutoLoadItemDetail;
      }
      set
      {
        this.myEnableAutoLoadItemDetail = value;
      }
    }

    public DataTable DataTableMaster
    {
      get
      {
        return this.myMasterTable;
      }
    }

    public DataTable DataTableDetail
    {
      get
      {
        return this.myDetailTable;
      }
    }

    public DataTable DataTableASMBOMOptional
    {
      get
      {
        return this.myASMBOMOptionalTable;
      }
    }

    public DataSet StockWorkOrderDataSet
    {
      get
      {
        return this.myDataSet;
      }
    }

    public int DetailCount
    {
      get
      {
        return this.GetValidDetailRows().Length;
      }
    }

    public StockWorkOrderAction Action
    {
      get
      {
        return this.myAction;
      }
    }

    public StockWorkOrderCommand Command
    {
      get
      {
        return this.myCommand;
      }
    }

    public UpdateBusinessFlowDelegate UpdateBusinessFlowEvent
    {
      get
      {
        return this.myUpdateBusinessFlowEvent;
      }
      set
      {
        this.myUpdateBusinessFlowEvent = value;
      }
    }

    public DataRow MasterRow
    {
      get
      {
        return this.myRow;
      }
    }

    public bool KeepLastModifiedInfo
    {
      get
      {
        return this.myKeepLastModifiedInfo;
      }
      set
      {
        this.myKeepLastModifiedInfo = value;
      }
    }

    public long DocKey
    {
      get
      {
        return BCE.Data.Convert.ToInt64(this.myRow["DocKey"]);
      }
    }

    public DBString FromDocType
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["FromDocType"]);
      }
    }

    public long FromDocDtlKey
    {
      get
      {
        return BCE.Data.Convert.ToInt64(this.myRow["FromDocDtlKey"]);
      }
    }

    public DBString DocNo
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["DocNo"]);
      }
      set
      {
        this.myRow["DocNo"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public bool IsMultilevel
    {
      get
      {
        return BCE.Data.Convert.TextToBoolean(this.myRow["IsMultilevel"]);
      }
      set
      {
        this.myRow["IsMultilevel"] = (object) BCE.Data.Convert.BooleanToText(value);
      }
    }

    public DBDateTime DocDate
    {
      get
      {
        return BCE.Data.Convert.ToDBDateTime(this.myRow["DocDate"]);
      }
      set
      {
        if (value.HasValue)
          this.myRow["DocDate"] = BCE.Data.Convert.ToDBObject((DBDateTime)value);
        else
          this.myRow["DocDate"] = (object) DBNull.Value;
      }
    }

    public DBString Description
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Description"]);
      }
      set
      {
        this.myRow["Description"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString ItemCode
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["ItemCode"]);
      }
      set
      {
        this.myRow["ItemCode"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString Location
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Location"]);
      }
      set
      {
        this.myRow["Location"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString BatchNo
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["BatchNo"]);
      }
      set
      {
        this.myRow["BatchNo"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString ProjNo
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["ProjNo"]);
      }
      set
      {
        this.myRow["ProjNo"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString DeptNo
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["DeptNo"]);
      }
      set
      {
        this.myRow["DeptNo"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBDecimal Qty
    {
      get
      {
        return BCE.Data.Convert.ToDBDecimal(this.myRow["Qty"]);
      }
      set
      {
        this.myRow["Qty"] = this.myCommand.DecimalSetting.RoundToQuantityDBObject(value);
      }
    }

    public DBDecimal Total
    {
      get
      {
        return BCE.Data.Convert.ToDBDecimal(this.myRow["Total"]);
      }
    }

    public DBDecimal AssemblyCost
    {
      get
      {
        return BCE.Data.Convert.ToDBDecimal(this.myRow["AssemblyCost"]);
      }
      set
      {
        this.myRow["AssemblyCost"] = this.myCommand.DecimalSetting.RoundToCostDBObject(value);
      }
    }

    public DBDecimal NetTotal
    {
      get
      {
        return BCE.Data.Convert.ToDBDecimal(this.myRow["NetTotal"]);
      }
    }

    public DBString Note
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Note"]);
      }
      set
      {
        if (value.HasValue)
          this.myRow["Note"] = (object) Rtf.ToArialRichText((string) value);
        else
          this.myRow["Note"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString Remark1
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Remark1"]);
      }
      set
      {
        this.myRow["Remark1"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString Remark2
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Remark2"]);
      }
      set
      {
        this.myRow["Remark2"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString Remark3
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Remark3"]);
      }
      set
      {
        this.myRow["Remark3"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString Remark4
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["Remark4"]);
      }
      set
      {
        this.myRow["Remark4"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public short PrintCount
    {
      get
      {
        return BCE.Data.Convert.ToInt16(this.myRow["PrintCount"]);
      }
    }

    public bool Cancelled
    {
      get
      {
        return BCE.Data.Convert.TextToBoolean(this.myRow["Cancelled"]);
      }
    }

    public DBDateTime LastModified
    {
      get
      {
        return BCE.Data.Convert.ToDBDateTime(this.myRow["LastModified"]);
      }
    }

    public DBString LastModifiedUserID
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["LastModifiedUserID"]);
      }
    }

    public DBDateTime CreatedTimeStamp
    {
      get
      {
        return BCE.Data.Convert.ToDBDateTime(this.myRow["CreatedTimeStamp"]);
      }
    }

    public DBString CreatedUserID
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["CreatedUserID"]);
      }
    }

    public DBString RefDocNo
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["RefDocNo"]);
      }
      set
      {
        this.myRow["RefDocNo"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public DBString ExpectedCompletedDate
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["ExpectedCompletedDate"]);
      }
      set
      {
        this.myRow["ExpectedCompletedDate"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public bool CanSync
    {
      get
      {
        return BCE.Data.Convert.TextToBoolean(this.myRow["CanSync"]);
      }
      set
      {
        this.myRow["CanSync"] = (object) BCE.Data.Convert.BooleanToText(value);
      }
    }

    public DBString ExternalLinkText
    {
      get
      {
        return BCE.Data.Convert.ToDBString(this.myRow["ExternalLink"]);
      }
      set
      {
        this.myRow["ExternalLink"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public ExternalLink ExternalLink
    {
      get
      {
        return new ExternalLink(this.myRow, "ExternalLink");
      }
    }

    public DBString SerialNoList
    {
      get
      {
        return (DBString) this.myRow["SerialNoList"].ToString();
      }
      set
      {
        this.myRow["SerialNoList"] = BCE.Data.Convert.ToDBObject(value);
      }
    }

    public event ConfirmChangingItemCodeEventHandler MasterItemCodeChangedEvent
    {
      add
      {
        ConfirmChangingItemCodeEventHandler codeEventHandler = this.MasterItemCodeChangedEvent2;
        ConfirmChangingItemCodeEventHandler comparand;
        do
        {
          comparand = codeEventHandler;
          codeEventHandler = Interlocked.CompareExchange<ConfirmChangingItemCodeEventHandler>(ref this.MasterItemCodeChangedEvent2, comparand + value, comparand);
        }
        while (codeEventHandler != comparand);
      }
      remove
      {
        ConfirmChangingItemCodeEventHandler codeEventHandler = this.MasterItemCodeChangedEvent2;
        ConfirmChangingItemCodeEventHandler comparand;
        do
        {
          comparand = codeEventHandler;
          codeEventHandler = Interlocked.CompareExchange<ConfirmChangingItemCodeEventHandler>(ref this.MasterItemCodeChangedEvent2, comparand - value, comparand);
        }
        while (codeEventHandler != comparand);
      }
    }

    public event ConfirmChangingItemCodeEventHandler DetailItemCodeChangedEvent
    {
      add
      {
        ConfirmChangingItemCodeEventHandler codeEventHandler = this.DetailItemCodeChangedEvent2;
        ConfirmChangingItemCodeEventHandler comparand;
        do
        {
          comparand = codeEventHandler;
          codeEventHandler = Interlocked.CompareExchange<ConfirmChangingItemCodeEventHandler>(ref this.DetailItemCodeChangedEvent2, comparand + value, comparand);
        }
        while (codeEventHandler != comparand);
      }
      remove
      {
        ConfirmChangingItemCodeEventHandler codeEventHandler = this.DetailItemCodeChangedEvent2;
        ConfirmChangingItemCodeEventHandler comparand;
        do
        {
          comparand = codeEventHandler;
          codeEventHandler = Interlocked.CompareExchange<ConfirmChangingItemCodeEventHandler>(ref this.DetailItemCodeChangedEvent2, comparand - value, comparand);
        }
        while (codeEventHandler != comparand);
      }
    }

    public event PauseUndoDelegate PauseUndoEvent
    {
      add
      {
        PauseUndoDelegate pauseUndoDelegate = this.PauseUndoEvent2;
        PauseUndoDelegate comparand;
        do
        {
          comparand = pauseUndoDelegate;
          pauseUndoDelegate = Interlocked.CompareExchange<PauseUndoDelegate>(ref this.PauseUndoEvent2, comparand + value, comparand);
        }
        while (pauseUndoDelegate != comparand);
      }
      remove
      {
        PauseUndoDelegate pauseUndoDelegate = this.PauseUndoEvent2;
        PauseUndoDelegate comparand;
        do
        {
          comparand = pauseUndoDelegate;
          pauseUndoDelegate = Interlocked.CompareExchange<PauseUndoDelegate>(ref this.PauseUndoEvent2, comparand - value, comparand);
        }
        while (pauseUndoDelegate != comparand);
      }
    }

    public event ResumeUndoDelegate ResumeUndoEvent
    {
      add
      {
        ResumeUndoDelegate resumeUndoDelegate = this.ResumeUndoEvent2;
        ResumeUndoDelegate comparand;
        do
        {
          comparand = resumeUndoDelegate;
          resumeUndoDelegate = Interlocked.CompareExchange<ResumeUndoDelegate>(ref this.ResumeUndoEvent2, comparand + value, comparand);
        }
        while (resumeUndoDelegate != comparand);
      }
      remove
      {
        ResumeUndoDelegate resumeUndoDelegate = this.ResumeUndoEvent2;
        ResumeUndoDelegate comparand;
        do
        {
          comparand = resumeUndoDelegate;
          resumeUndoDelegate = Interlocked.CompareExchange<ResumeUndoDelegate>(ref this.ResumeUndoEvent2, comparand - value, comparand);
        }
        while (resumeUndoDelegate != comparand);
      }
    }

    public event SetDocNoFormatEventHandler SetDocNoFormatEvent
    {
      add
      {
        SetDocNoFormatEventHandler formatEventHandler = this.SetDocNoFormatEvent2;
        SetDocNoFormatEventHandler comparand;
        do
        {
          comparand = formatEventHandler;
          formatEventHandler = Interlocked.CompareExchange<SetDocNoFormatEventHandler>(ref this.SetDocNoFormatEvent2, comparand + value, comparand);
        }
        while (formatEventHandler != comparand);
      }
      remove
      {
        SetDocNoFormatEventHandler formatEventHandler = this.SetDocNoFormatEvent2;
        SetDocNoFormatEventHandler comparand;
        do
        {
          comparand = formatEventHandler;
          formatEventHandler = Interlocked.CompareExchange<SetDocNoFormatEventHandler>(ref this.SetDocNoFormatEvent2, comparand - value, comparand);
        }
        while (formatEventHandler != comparand);
      }
    }

    internal StockWorkOrder(StockWorkOrderCommand command, DataSet aDataSet, StockWorkOrderAction action)
    {
      this.myEnableAutoLoadItemDetail = false;
      this.myCommand = command;
      this.myDataSet = aDataSet;
      this.myAction = action;
      this.myScriptObject = ScriptManager.CreateObject(command.DBSetting, "ASMORDER");
      this.myMasterTable = this.myDataSet.Tables["Master"];
      this.myDetailTable = this.myDataSet.Tables["Detail"];
      this.myASMBOMOptionalTable = this.myDataSet.Tables["ASMBOMOptional"];
      this.myRow = this.myMasterTable.Rows[0];
      this.myMasterTable.ColumnChanged += new DataColumnChangeEventHandler(this.MasterDataColumnChangeEventHandler);
      this.myDetailTable.RowChanged += new DataRowChangeEventHandler(this.myDetailTable_RowChanged);
      this.myDetailTable.ColumnChanged += new DataColumnChangeEventHandler(this.DetailDataColumnChangedEventHandler);
      this.myDetailTable.ColumnChanging += new DataColumnChangeEventHandler(this.DetailDataColumnChangingEventHandler);
      this.myDetailTable.RowDeleted += new DataRowChangeEventHandler(this.DetailDataRowDeletedEventHandler);
      this.myDetailTable.RowDeleting += new DataRowChangeEventHandler(this.myDetailTable_RowDeleting);
    }

    public static bool CanCopyMasterField(string columnName)
    {
      string[] strArray = new string[14];
      int index1 = 0;
      string str1 = "DocKey";
      strArray[index1] = str1;
      int index2 = 1;
      string str2 = "DocNo";
      strArray[index2] = str2;
      int index3 = 2;
      string str3 = "DocDate";
      strArray[index3] = str3;
      int index4 = 3;
      string str4 = "Total";
      strArray[index4] = str4;
      int index5 = 4;
      string str5 = "PrintCount";
      strArray[index5] = str5;
      int index6 = 5;
      string str6 = "Cancelled";
      strArray[index6] = str6;
      int index7 = 6;
      string str7 = "LastModified";
      strArray[index7] = str7;
      int index8 = 7;
      string str8 = "LastModifiedUserID";
      strArray[index8] = str8;
      int index9 = 8;
      string str9 = "CreatedTimeStamp";
      strArray[index9] = str9;
      int index10 = 9;
      string str10 = "CreatedUserID";
      strArray[index10] = str10;
      int index11 = 10;
      string str11 = "LastUpdate";
      strArray[index11] = str11;
      int index12 = 11;
      string str12 = "CanSync";
      strArray[index12] = str12;
      int index13 = 12;
      string str13 = "Guid";
      strArray[index13] = str13;
      int index14 = 13;
      string str14 = "TransferedQty";
      strArray[index14] = str14;
      foreach (string strA in strArray)
      {
        if (string.Compare(strA, columnName, true) == 0)
          return false;
      }
      return true;
    }

    public static bool CanCopyDetailField(string columnName)
    {
      string[] strArray = new string[7];
      int index1 = 0;
      string str1 = "DtlKey";
      strArray[index1] = str1;
      int index2 = 1;
      string str2 = "DocKey";
      strArray[index2] = str2;
      int index3 = 2;
      string str3 = "Seq";
      strArray[index3] = str3;
      int index4 = 3;
      string str4 = "OldDtlKey";
      strArray[index4] = str4;
      int index5 = 4;
      string str5 = "Guid";
      strArray[index5] = str5;
      int index6 = 5;
      string str6 = "TransferedQty";
      strArray[index6] = str6;
      int index7 = 6;
      string str7 = "TransferPOQty";
      strArray[index7] = str7;
      foreach (string strA in strArray)
      {
        if (string.Compare(strA, columnName, true) == 0)
          return false;
      }
      return true;
    }

    private void DisableColumnChangedEvent()
    {
      this.myColumnChangedCounter = this.myColumnChangedCounter + 1;
    }

    private void DisableColumnChangingEvent()
    {
      this.myColumnChangingCounter = this.myColumnChangingCounter + 1;
    }

    private void EnableColumnChangedEvent()
    {
      if (this.myColumnChangedCounter > 0)
        this.myColumnChangedCounter = this.myColumnChangedCounter - 1;
    }

    private void EnableColumnChangingEvent()
    {
      if (this.myColumnChangingCounter > 0)
        this.myColumnChangingCounter = this.myColumnChangingCounter - 1;
    }

    private bool IsColumnChangedEventDisabled()
    {
      return this.myColumnChangedCounter > 0;
    }

    private bool IsColumnChangingEventDisabled()
    {
      return this.myColumnChangingCounter > 0;
    }

    private void DisableUpdateCostEvent()
    {
      this.myUpdateCostCounter = this.myUpdateCostCounter + 1;
    }

    private void EnableUpdateCostEvent()
    {
      if (this.myUpdateCostCounter > 0)
        this.myUpdateCostCounter = this.myUpdateCostCounter - 1;
    }

    private bool IsUpdateCostEventDisabled()
    {
      return this.myUpdateCostCounter > 0;
    }

    public void PauseUndo()
    {
      // ISSUE: reference to a compiler-generated field
      if (this.PauseUndoEvent2 != null)
      {
        // ISSUE: reference to a compiler-generated field
        this.PauseUndoEvent2();
      }
    }

    public void ResumeUndo(bool captureChanges)
    {
      // ISSUE: reference to a compiler-generated field
      if (this.ResumeUndoEvent2 != null)
      {
        // ISSUE: reference to a compiler-generated field
        this.ResumeUndoEvent2(captureChanges);
      }
    }

    public void TransferFromSO(long dtlKey, string itemCode, Decimal qty)
    {
      this.myRow.BeginEdit();
      this.myRow["ItemCode"] = (object) itemCode;
      this.myRow["Qty"] = (object) qty;
      this.myRow["FromDocType"] = (object) "SO";
      this.myRow["FromDocDtlKey"] = (object) dtlKey;
      this.myRow.EndEdit();
    }

    public void SaveToTempDocument(string saveReason)
    {
      this.myCommand.SaveToTempDocument(this, saveReason);
    }

    [Obsolete("The userID parameter will be ignored.")]
    public void Save(string userID)
    {
      this.Save();
    }

    public void Save()
    {
      if (this.myAction == StockWorkOrderAction.View)
      {
        throw new Exception(Localizer.GetString((Enum) StockAssemblyOrderStringId.ErrorMessage_CannotSave, new object[0]));
      }
      else
      {
        TransactionControl.CheckTransactionCount(this.myCommand.DBSetting);
        this.myCommand.myFiscalYear.CheckTransactionDate((DateTime) this.DocDate, "StockWorkOrder", this.myCommand.DBSetting);
        if (this.DetailCount == 0)
        {
          throw new NoDetailRecordFoundException();
        }
        else
        {
          bool flag = this.myRow.RowState != DataRowState.Unchanged;
          foreach (DataRow dataRow in this.GetValidDetailRows())
          {
            if (!flag && dataRow.RowState != DataRowState.Unchanged)
              flag = true;
            if (dataRow["ItemCode"].ToString().Length > 0)
            {
              if (dataRow["Location"] == DBNull.Value)
                throw new NullLocationException();
              else if (BCE.Data.Convert.ToDecimal(dataRow["Qty"]) < Decimal.Zero)
                throw new NotAllowNegativeQuantityException();
            }
          }
          if (BCE.Data.Convert.ToDecimal(this.myRow["TransferedQty"]) > BCE.Data.Convert.ToDecimal(this.myRow["Qty"]))
          {
            throw new PartialTransferQtyLessThanTransferedQtyException(this.myRow["ItemCode"].ToString());
          }
          else
          {
            foreach (DataRow dataRow in (InternalDataCollectionBase) this.myDetailTable.Rows)
            {
              if (dataRow.RowState != DataRowState.Deleted && BCE.Data.Convert.ToDecimal(dataRow["TransferedQty"]) > BCE.Data.Convert.ToDecimal(dataRow["Qty"]))
                throw new PartialTransferQtyLessThanTransferedQtyException(dataRow["ItemCode"].ToString());
            }
            if (!flag && this.myDetailTable.Select("", "Seq", DataViewRowState.Deleted).Length != 0)
              flag = true;
            if (flag)
            {
              if (!this.myKeepLastModifiedInfo || this.myAction == StockWorkOrderAction.New)
              {
                this.myRow["LastModifiedUserID"] = (object) this.myCommand.UserAuthentication.LoginUserID;
                this.myRow["LastModified"] = (object) this.myCommand.DBSetting.GetServerTime();
              }
              if (BCE.Data.Convert.ToDateTime(this.myRow["CreatedTimeStamp"]) == DateTime.MinValue)
                this.myRow["CreatedTimeStamp"] = this.myRow["LastModified"];
              if (this.myRow["CreatedUserID"].ToString().Length == 0)
                this.myRow["CreatedUserID"] = this.myRow["LastModifiedUserID"];
              this.myRow["LastUpdate"] = (object) (BCE.Data.Convert.ToInt32(this.myRow["LastUpdate"]) + 1);
              this.myRow.EndEdit();
              this.myCommand.SaveData(this);
            }
            this.myAction = StockWorkOrderAction.View;
          }
        }
      }
    }

    public void Cancel()
    {
      if (this.myAction == StockWorkOrderAction.View)
      {
        throw new Exception(Localizer.GetString((Enum) StockAssemblyOrderStringId.ErrorMessage_CannotCancel, new object[0]));
      }
      else
      {
        this.myDataSet.RejectChanges();
        this.myAction = StockWorkOrderAction.View;
      }
    }

    public void Edit()
    {
      if (this.myAction == StockWorkOrderAction.View)
      {
        this.myCommand.myFiscalYear.CheckTransactionDate((DateTime) this.DocDate, "StockWorkOrder", this.myCommand.DBSetting);
        this.myAction = StockWorkOrderAction.Edit;
      }
      else
        throw new InvalidOperationException("Cannot edit the document which is in Edit/New mode.");
    }

    public void Delete()
    {
      if (this.myAction == StockWorkOrderAction.View)
        this.myCommand.Delete(this.DocKey);
      else
        throw new InvalidOperationException("Cannot delete the document which is in Edit/New mode.");
    }

    public void CancelDocument(string userID)
    {
      if (this.myAction == StockWorkOrderAction.View)
      {
        long num = BCE.Data.Convert.ToInt64(this.MasterRow["DocKey"]);
        DBSetting dbSetting = this.myCommand.DBSetting;
        string cmdText = "SELECT FromASMOrderDocKey FROM ASM WHERE FromASMOrderDocKey = ?";
        object[] objArray = new object[1];
        int index = 0;
        // ISSUE: variable of a boxed type
        long local =num;
        objArray[index] = (object) local;
        if (dbSetting.ExecuteScalar(cmdText, objArray) != null)
        {
          throw new TransferedDocNotAllowToCancelException();
        }
        else
        {
          this.Edit();
          this.myRow["Cancelled"] = (object) BCE.Data.Convert.BooleanToText(true);
          this.Save(userID);
        }
      }
      else
        throw new InvalidOperationException("Cannot void the document which is in Edit/New mode.");
    }

    public void UncancelDocument(string userID)
    {
      if (this.myAction == StockWorkOrderAction.View)
      {
        this.Edit();
        this.myRow["Cancelled"] = (object) BCE.Data.Convert.BooleanToText(false);
        this.Save(userID);
      }
      else
        throw new InvalidOperationException("Cannot un-void the document which is in Edit/New mode.");
    }

    public string ExportAsXml()
    {
      StringWriter stringWriter = new StringWriter();
      this.myDataSet.WriteXml((TextWriter) stringWriter, XmlWriteMode.WriteSchema);
      return stringWriter.ToString();
    }

    public void ImportFromXml(string xmlString, bool wholeDocument)
    {
      DataSet newDataSet = new DataSet();
      StringReader stringReader = new StringReader(xmlString);
      int num = (int) newDataSet.ReadXml((TextReader) stringReader, XmlReadMode.Auto);
      this.ImportFromDataSet(newDataSet, wholeDocument);
    }

    public void ImportFromDataSet(DataSet newDataSet, bool wholeDocument)
    {
      if (this.myAction == StockWorkOrderAction.View)
        throw new Exception(Localizer.GetString((Enum)StockAssemblyOrderStringId.ErrorMessage_CannotImport, new object[0]));
      else if (newDataSet.Tables.Count > 0 && newDataSet.Tables[0].TableName == "Master")
      {
        if (wholeDocument)
        {
          this.BeginLoadDetailData();
          try
          {
            DataTable dataTable = newDataSet.Tables[0];
            DataRow dataRow = dataTable.Rows[0];
            foreach (DataColumn index1 in (InternalDataCollectionBase) dataTable.Columns)
            {
              if (StockWorkOrder.CanCopyMasterField(index1.ColumnName))
              {
                int index2 = this.myMasterTable.Columns.IndexOf(index1.ColumnName);
                if (index2 >= 0)
                  this.myRow[index2] = dataRow[index1];
              }
            }
          }
          finally
          {
            this.EndLoadDetailData();
          }
        }
        if (newDataSet.Tables.Count > 1)
        {
          DataTable table = newDataSet.Tables[1];
          string typeName = this.myDetailTable.Columns["DtlKey"].DataType.ToString();
          table.Columns.Add("OldDtlKey", Type.GetType(typeName));
          this.myDetailTable.Columns.Add("OldDtlKey", Type.GetType(typeName));
          DataView dataView = new DataView(table);
          if (table.Columns.IndexOf("Seq") >= 0)
            dataView.Sort = "Seq";
          this.BeginLoadDetailData();
          try
          {
            for (int index1 = 0; index1 < dataView.Count; ++index1)
            {
              DataRowView dataRowView = dataView[index1];
              DataRow dataRow = (DataRow) null;
              foreach (DataColumn dataColumn in (InternalDataCollectionBase) table.Columns)
              {
                if (StockWorkOrder.CanCopyDetailField(dataColumn.ColumnName))
                {
                  int index2 = this.myDetailTable.Columns.IndexOf(dataColumn.ColumnName);
                  if (index2 >= 0)
                  {
                    if (dataRow == null)
                    {
                      dataRow = this.AddDetail().Row;
                      dataRow["OldDtlKey"] = dataRowView["DtlKey"];
                    }
                    dataRow[index2] = dataRowView[dataColumn.ColumnName];
                  }
                }
              }
            }
            foreach (DataRow dataRow1 in this.myDetailTable.Select("OldDtlKey is not null AND IsBOMItem='T'"))
            {
              foreach (DataRow dataRow2 in this.myDetailTable.Select("OldDtlKey is not null AND ParentDtlKey=" + (object) BCE.Data.Convert.ToInt64(dataRow1["OldDtlKey"])))
                dataRow2["ParentDtlKey"] = dataRow1["DtlKey"];
            }
          }
          finally
          {
            if (this.myDetailTable.Columns.Contains("OldDtlKey"))
              this.myDetailTable.Columns.Remove("OldDtlKey");
            if (table.Columns.Contains("OldDtlKey"))
              table.Columns.Remove("OldDtlKey");
            this.EndLoadDetailData();
          }
        }
      }
    }

    public bool ImportFromTabDelimitedText(string tabText, bool wholeDocument)
    {
      DataTable tempTable = StringHelper.TabDelimittedTextToDataTable(tabText);
      if (tempTable.Columns.Count < 2)
      {
        return false;
      }
      else
      {
        bool hadApply = false;
        DataTable dataTable1 = this.myDataSet.Tables["Master"];
        DataTable dataTable2 = this.myDataSet.Tables["Detail"];
        DataRow masterRow = dataTable1.Rows[0];
        for (int currentRowIndex1 = 0; currentRowIndex1 < tempTable.Rows.Count; ++currentRowIndex1)
        {
          DataRow row = tempTable.Rows[currentRowIndex1];
          if (wholeDocument)
            hadApply = this.ImportToMasterTable(hadApply, masterRow, row, currentRowIndex1);
          if (string.Compare(row[0].ToString(), "ItemCode", true, CultureInfo.InvariantCulture) == 0)
          {
            DataRow columnRow = row;
            for (int currentRowIndex2 = currentRowIndex1 + 1; currentRowIndex2 < tempTable.Rows.Count; ++currentRowIndex2)
              hadApply = this.ImportToDetailTable(tempTable, hadApply, currentRowIndex2, columnRow);
            break;
          }
        }
        return hadApply;
      }
    }

    private bool ImportToMasterTable(bool hadApply, DataRow masterRow, DataRow row, int currentRowIndex)
    {
      if (row[1].ToString().Length > 0)
      {
        Hashtable hashtable1 = new Hashtable();
        hashtable1.Add((object) "docno", (object) null);
        hashtable1.Add((object) "description", (object) null);
        hashtable1.Add((object) "refdocno", (object) null);
        hashtable1.Add((object) "expectedcompleteddate", (object) null);
        hashtable1.Add((object) "bomitemcode", (object) null);
        hashtable1.Add((object) "location", (object) null);
        hashtable1.Add((object) "projno", (object) null);
        hashtable1.Add((object) "batchno", (object) null);
        hashtable1.Add((object) "deptno", (object) null);
        Hashtable hashtable2 = new Hashtable();
        Hashtable hashtable3 = new Hashtable();
        Hashtable hashtable4 = new Hashtable();
        Hashtable hashtable5 = new Hashtable();
        foreach (UDFColumn udfColumn in new UDFUtil(this.myCommand.myDBSetting).GetUDF("ASMORDER"))
        {
          if (udfColumn.Type == UDFType.Boolean)
            hashtable4.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
          else if (udfColumn.Type == UDFType.Date)
            hashtable5.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
          else if (udfColumn.Type == UDFType.Text || udfColumn.Type == UDFType.System || udfColumn.Type == UDFType.ImageLink)
            hashtable1.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
          else if (udfColumn.Type == UDFType.Decimal)
            hashtable2.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
          else if (udfColumn.Type == UDFType.Integer)
            hashtable3.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
        }
        bool flag = false;
        string name = row[0].ToString().ToLowerInvariant();
        if (name == "date")
          name = "docdate";
        if (masterRow.Table.Columns.Contains(name) || name == "bomitemcode")
        {
          if (hashtable1.ContainsKey((object) name))
          {
            if (name == "bomitemcode")
              name = "itemcode";
            int maxLength = masterRow.Table.Columns[name].MaxLength;
            masterRow[name] = maxLength <= 0 || row[1].ToString().Length <= maxLength ? (object) row[1].ToString() : (object) row[1].ToString().Substring(0, maxLength);
            flag = true;
          }
          else if (hashtable5.ContainsKey((object) name))
          {
            try
            {
              masterRow[name] = this.ConvertToDateTimeFormat(row[1].ToString());
            }
            catch
            {
                            // ISSUE: variable of a boxed type
                            StockAssemblyOrderStringId local1 = StockAssemblyOrderStringId.ErrorMessage_InvalidDateFormat;
              object[] objArray = new object[2];
              int index1 = 0;
              string str = name;
              objArray[index1] = (object) str;
              int index2 = 1;
              // ISSUE: variable of a boxed type
              int local2 =  currentRowIndex;
              objArray[index2] = (object) local2;
              throw new AppException(Localizer.GetString((Enum) local1, objArray));
            }
            flag = true;
          }
          else if (hashtable2.ContainsKey((object) name))
          {
            try
            {
              masterRow[name] = (object) BCE.Data.Convert.ToDecimal(row[1]);
            }
            catch
            {
                            // ISSUE: variable of a boxed type
                            StockAssemblyOrderStringId local1 = StockAssemblyOrderStringId.ErrorMessage_InvalidNumberFormat;
              object[] objArray = new object[2];
              int index1 = 0;
              string str = name;
              objArray[index1] = (object) str;
              int index2 = 1;
              // ISSUE: variable of a boxed type
              int local2 =  currentRowIndex;
              objArray[index2] = (object) local2;
              throw new AppException(Localizer.GetString((Enum) local1, objArray));
            }
            flag = true;
          }
          else if (hashtable3.ContainsKey((object) name))
          {
            try
            {
              masterRow[name] = (object) BCE.Data.Convert.ToInt32(row[1]);
            }
            catch
            {
                            // ISSUE: variable of a boxed type
                            StockAssemblyOrderStringId local1 = StockAssemblyOrderStringId.ErrorMessage_InvalidNumberFormat;
              object[] objArray = new object[2];
              int index1 = 0;
              string str = name;
              objArray[index1] = (object) str;
              int index2 = 1;
              // ISSUE: variable of a boxed type
              int local2 = currentRowIndex;
              objArray[index2] = (object) local2;
              throw new AppException(Localizer.GetString((Enum) local1, objArray));
            }
            flag = true;
          }
          else if (hashtable4.ContainsKey((object) name))
          {
            masterRow[name] = (object) BCE.Data.Convert.BooleanToText(BCE.Data.Convert.TextToBoolean(row[1]));
            flag = true;
          }
          else if (name == "qty")
          {
            try
            {
              masterRow[name] = (object) this.myCommand.DecimalSetting.RoundQuantity(BCE.Data.Convert.ToDecimal(row[1]));
              flag = true;
            }
            catch
            {
                            // ISSUE: variable of a boxed type
                            StockAssemblyOrderStringId local1 = StockAssemblyOrderStringId.ErrorMessage_InvalidNumberFormat;
              object[] objArray = new object[2];
              int index1 = 0;
              string str = name;
              objArray[index1] = (object) str;
              int index2 = 1;
              // ISSUE: variable of a boxed type
              int local2 = currentRowIndex;
              objArray[index2] = (object) local2;
              throw new AppException(Localizer.GetString((Enum) local1, objArray));
            }
          }
          else if (name == "docdate")
          {
            try
            {
              object obj = this.ConvertToDateTimeFormat(row[1].ToString());
              if (obj != DBNull.Value)
                this.DocDate = (DBDateTime) ((DateTime) obj);
            }
            catch
            {
                            // ISSUE: variable of a boxed type
                            StockAssemblyOrderStringId local1 = StockAssemblyOrderStringId.ErrorMessage_InvalidDateFormat;
              object[] objArray = new object[2];
              int index1 = 0;
              string str = "Date";
              objArray[index1] = (object) str;
              int index2 = 1;
              // ISSUE: variable of a boxed type
              int local2 =  currentRowIndex;
              objArray[index2] = (object) local2;
              throw new AppException(Localizer.GetString((Enum) local1, objArray));
            }
            flag = true;
          }
        }
        if (flag)
          hadApply = true;
      }
      return hadApply;
    }

    private bool ImportToDetailTable(DataTable tempTable, bool hadApply, int currentRowIndex, DataRow columnRow)
    {
      StockWorkOrderDetail assemblyOrderDetail = this.AddDetail();
      DataRow row = assemblyOrderDetail.Row;
      if (assemblyOrderDetail != null)
      {
        DataRow dataRow = tempTable.Rows[currentRowIndex];
        Hashtable hashtable1 = new Hashtable();
        hashtable1.Add((object) "itemcode", (object) null);
        hashtable1.Add((object) "description", (object) null);
        hashtable1.Add((object) "batchno", (object) null);
        hashtable1.Add((object) "location", (object) null);
        hashtable1.Add((object) "projno", (object) null);
        hashtable1.Add((object) "deptno", (object) null);
        hashtable1.Add((object) "remark", (object) null);
        Hashtable hashtable2 = new Hashtable();
        hashtable2.Add((object) "furtherdescription", (object) null);
        Hashtable hashtable3 = new Hashtable();
        hashtable3.Add((object) "printout", (object) null);
        Hashtable hashtable4 = new Hashtable();
        Hashtable hashtable5 = new Hashtable();
        Hashtable hashtable6 = new Hashtable();
        hashtable6.Add((object) "uomrate", (object) null);
        Hashtable hashtable7 = new Hashtable();
        foreach (UDFColumn udfColumn in new UDFUtil(this.myCommand.myDBSetting).GetUDF("ASMORDERDTL"))
        {
          if (udfColumn.Type == UDFType.Boolean)
            hashtable3.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
          else if (udfColumn.Type == UDFType.Date)
            hashtable4.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
          else if (udfColumn.Type == UDFType.Memo)
            hashtable5.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
          else if (udfColumn.Type == UDFType.RichText)
            hashtable2.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
          else if (udfColumn.Type == UDFType.Text || udfColumn.Type == UDFType.System || udfColumn.Type == UDFType.ImageLink)
            hashtable1.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
          else if (udfColumn.Type == UDFType.Decimal)
            hashtable6.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
          else if (udfColumn.Type == UDFType.Integer)
            hashtable7.Add((object) udfColumn.FieldName.ToLowerInvariant(), (object) null);
        }
        foreach (DataColumn index1 in (InternalDataCollectionBase) tempTable.Columns)
        {
          if (dataRow[index1].ToString().Length > 0)
          {
            bool flag = false;
            string name = columnRow[index1].ToString().ToLowerInvariant();
            if (row.Table.Columns.Contains(name))
            {
              if (hashtable1.ContainsKey((object) name))
              {
                int maxLength = row.Table.Columns[name].MaxLength;
                row[name] = maxLength <= 0 || dataRow[index1].ToString().Length <= maxLength ? (object) dataRow[index1].ToString() : (object) dataRow[index1].ToString().Substring(0, maxLength);
                flag = true;
              }
              else if (hashtable5.ContainsKey((object) name))
              {
                row[name] = (object) dataRow[index1].ToString();
                flag = true;
              }
              else if (hashtable2.ContainsKey((object) name))
              {
                row[name] = (object) Rtf.ToArialRichText(dataRow[index1].ToString());
                flag = true;
              }
              else if (hashtable4.ContainsKey((object) name))
              {
                try
                {
                  row[name] = this.ConvertToDateTimeFormat(dataRow[index1].ToString());
                }
                catch
                {
                                    // ISSUE: variable of a boxed type
                                    StockAssemblyOrderStringId local1 = StockAssemblyOrderStringId.ErrorMessage_InvalidDateFormat;
                  object[] objArray = new object[2];
                  int index2 = 0;
                  string str = name;
                  objArray[index2] = (object) str;
                  int index3 = 1;
                  // ISSUE: variable of a boxed type
                  int local2 =currentRowIndex;
                  objArray[index3] = (object) local2;
                  throw new AppException(Localizer.GetString((Enum) local1, objArray));
                }
                flag = true;
              }
              else if (hashtable3.ContainsKey((object) name))
              {
                row[name] = (object) BCE.Data.Convert.BooleanToText(BCE.Data.Convert.TextToBoolean(dataRow[index1]));
                flag = true;
              }
              else if (hashtable6.ContainsKey((object) name))
              {
                try
                {
                  row[name] = (object) BCE.Data.Convert.ToDecimal(dataRow[index1]);
                  flag = true;
                }
                catch
                {
                                    // ISSUE: variable of a boxed type
                                    StockAssemblyOrderStringId local1 = StockAssemblyOrderStringId.ErrorMessage_InvalidNumberFormat;
                  object[] objArray = new object[2];
                  int index2 = 0;
                  string str = name;
                  objArray[index2] = (object) str;
                  int index3 = 1;
                  // ISSUE: variable of a boxed type
                  int local2 = currentRowIndex;
                  objArray[index3] = (object) local2;
                  throw new AppException(Localizer.GetString((Enum) local1, objArray));
                }
              }
              else if (hashtable7.ContainsKey((object) name))
              {
                try
                {
                  row[name] = (object) BCE.Data.Convert.ToInt32(dataRow[index1]);
                  flag = true;
                }
                catch
                {
                                    // ISSUE: variable of a boxed type
                                    StockAssemblyOrderStringId local1 = StockAssemblyOrderStringId.ErrorMessage_InvalidNumberFormat;
                  object[] objArray = new object[2];
                  int index2 = 0;
                  string str = name;
                  objArray[index2] = (object) str;
                  int index3 = 1;
                  // ISSUE: variable of a boxed type
                  int local2 =  currentRowIndex;
                  objArray[index3] = (object) local2;
                  throw new AppException(Localizer.GetString((Enum) local1, objArray));
                }
              }
              else if (name == "qty")
              {
                try
                {
                  row[name] = (object) this.myCommand.DecimalSetting.RoundQuantity(BCE.Data.Convert.ToDecimal(dataRow[index1]));
                  flag = true;
                }
                catch
                {
                                    // ISSUE: variable of a boxed type
                                    StockAssemblyOrderStringId local1 = StockAssemblyOrderStringId.ErrorMessage_InvalidNumberFormat;
                  object[] objArray = new object[2];
                  int index2 = 0;
                  string str = name;
                  objArray[index2] = (object) str;
                  int index3 = 1;
                  // ISSUE: variable of a boxed type
                  int local2 = currentRowIndex;
                  objArray[index3] = (object) local2;
                  throw new AppException(Localizer.GetString((Enum) local1, objArray));
                }
              }
              else if (name == "itemcost" || name == "overheadcost")
              {
                try
                {
                  row[name] = (object) this.myCommand.DecimalSetting.RoundCost(BCE.Data.Convert.ToDecimal(dataRow[index1]));
                  flag = true;
                }
                catch
                {
                                    // ISSUE: variable of a boxed type
                                    StockAssemblyOrderStringId local1 = StockAssemblyOrderStringId.ErrorMessage_InvalidNumberFormat;
                  object[] objArray = new object[2];
                  int index2 = 0;
                  string str = name;
                  objArray[index2] = (object) str;
                  int index3 = 1;
                  // ISSUE: variable of a boxed type
                  int local2 =  currentRowIndex;
                  objArray[index3] = (object) local2;
                  throw new AppException(Localizer.GetString((Enum) local1, objArray));
                }
              }
              else if (name == "subtotal")
              {
                try
                {
                  row["Subtotal"] = (object) this.myCommand.DecimalSetting.RoundCurrency(BCE.Data.Convert.ToDecimal(dataRow[index1]));
                  flag = true;
                }
                catch
                {
                                    // ISSUE: variable of a boxed type
                                    StockAssemblyOrderStringId local1 = StockAssemblyOrderStringId.ErrorMessage_InvalidNumberFormat;
                  object[] objArray = new object[2];
                  int index2 = 0;
                  string str = "SubTotal";
                  objArray[index2] = (object) str;
                  int index3 = 1;
                  // ISSUE: variable of a boxed type
                  int local2 =  currentRowIndex;
                  objArray[index3] = (object) local2;
                  throw new AppException(Localizer.GetString((Enum) local1, objArray));
                }
              }
            }
            if (flag)
              hadApply = true;
          }
        }
      }
      return hadApply;
    }

    private object ConvertToDateTimeFormat(string dateTimeString)
    {
      DateTimeFormatInfo dateTimeFormatInfo = new DateTimeFormatInfo();
      dateTimeFormatInfo.ShortDatePattern = this.myCommand.GeneralSetting.ShortDateFormat;
      try
      {
        return (object) System.Convert.ToDateTime(dateTimeString, (IFormatProvider) dateTimeFormatInfo);
      }
      catch
      {
        try
        {
          dateTimeFormatInfo.ShortDatePattern = "MM/dd/yyyy";
          return (object) System.Convert.ToDateTime(dateTimeString, (IFormatProvider) dateTimeFormatInfo);
        }
        catch
        {
          return (object) DBNull.Value;
        }
      }
    }

    public string ExportAsTabDelimiterText()
    {
      string str1 = "Stock Assembly Order";
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.AppendLine(string.Format("{0} {1}", (object) OEM.GetCurrentOEM().ProductName, (object) str1));
      stringBuilder1.AppendLine();
      DataTable dataTable1 = this.myDataSet.Tables["Master"];
      DataTable dataTable2 = this.myDataSet.Tables["Detail"];
      DataRow dataRow1 = dataTable1.Rows[0];
      stringBuilder1.AppendLine("DocNo\t" + this.DocNo.ToString());
      DateTime date = (DateTime) this.DocDate;
      stringBuilder1.AppendLine("Date\t" + this.myCommand.GeneralSetting.FormatDate(date));
      stringBuilder1.AppendLine("Description\t" + this.Description.ToString());
      stringBuilder1.AppendLine("RefDocNo\t" + this.RefDocNo.ToString());
      stringBuilder1.AppendLine("ExpectedCompletedDate\t" + this.ExpectedCompletedDate.ToString());
      stringBuilder1.AppendLine("BOMItemCode\t" + this.ItemCode.ToString());
      stringBuilder1.AppendLine("Qty\t" + this.myCommand.DecimalSetting.FormatQuantity((Decimal) this.Qty));
      stringBuilder1.AppendLine("Location\t" + this.Location.ToString());
      stringBuilder1.AppendLine("ProjNo\t" + this.ProjNo.ToString());
      stringBuilder1.AppendLine("BatchNo\t" + this.BatchNo.ToString());
      stringBuilder1.AppendLine("DeptNo\t" + this.DeptNo.ToString());
      foreach (UDFColumn udfColumn in new UDFUtil(this.myCommand.myDBSetting).GetUDF("ASMORDER"))
      {
        if (dataTable1.Columns.Contains(udfColumn.FieldName))
        {
          if (udfColumn.Type == UDFType.Text || udfColumn.Type == UDFType.System || (udfColumn.Type == UDFType.Memo || udfColumn.Type == UDFType.ImageLink))
            stringBuilder1.AppendLine(udfColumn.FieldName + "\t" + StringHelper.AddDoubleQuoteString(dataRow1[udfColumn.FieldName].ToString()));
          else if (udfColumn.Type == UDFType.Boolean)
            stringBuilder1.AppendLine(udfColumn.FieldName + "\t" + BCE.Data.Convert.TextToBoolean(dataRow1[udfColumn.FieldName]).ToString());
          else if (udfColumn.Type == UDFType.Date)
          {
            string str2 = "";
            if (dataRow1[udfColumn.FieldName] != DBNull.Value)
              str2 = this.myCommand.GeneralSetting.FormatDate(BCE.Data.Convert.ToDateTime(dataRow1[udfColumn.FieldName]));
            stringBuilder1.AppendLine(udfColumn.FieldName + "\t" + str2);
          }
          else if (udfColumn.Type == UDFType.Decimal)
            stringBuilder1.AppendLine(udfColumn.FieldName + (object) "\t" + (string) (object) BCE.Data.Convert.ToDecimal(dataRow1[udfColumn.FieldName]));
          else if (udfColumn.Type == UDFType.Integer)
            stringBuilder1.AppendLine(udfColumn.FieldName + (object) "\t" + (string) (object) BCE.Data.Convert.ToInt32(dataRow1[udfColumn.FieldName]));
        }
      }
      UDFColumn[] udf = new UDFUtil(this.myCommand.myDBSetting).GetUDF("ASMORDERDTL");
      StringBuilder stringBuilder2 = new StringBuilder();
      if (dataTable2.Columns.Contains("ItemCode"))
        stringBuilder2.Append("ItemCode\t");
      if (dataTable2.Columns.Contains("Description"))
        stringBuilder2.Append("Description\t");
      if (dataTable2.Columns.Contains("BatchNo"))
        stringBuilder2.Append("BatchNo\t");
      if (dataTable2.Columns.Contains("ProjNo"))
        stringBuilder2.Append("ProjNo\t");
      if (dataTable2.Columns.Contains("DeptNo"))
        stringBuilder2.Append("DeptNo\t");
      if (dataTable2.Columns.Contains("Rate"))
        stringBuilder2.Append("Rate\t");
      if (dataTable2.Columns.Contains("Qty"))
        stringBuilder2.Append("Qty\t");
      if (dataTable2.Columns.Contains("OverheadCost"))
        stringBuilder2.Append("OverheadCost\t");
      if (dataTable2.Columns.Contains("SubTotalCost"))
        stringBuilder2.Append("SubTotalCost\t");
      if (dataTable2.Columns.Contains("Location"))
        stringBuilder2.Append("Location\t");
      if (dataTable2.Columns.Contains("ItemCost"))
        stringBuilder2.Append("ItemCost\t");
      if (dataTable2.Columns.Contains("Remark"))
        stringBuilder2.Append("Remark\t");
      if (dataTable2.Columns.Contains("FurtherDescription"))
        stringBuilder2.Append("FurtherDescription");
      foreach (UDFColumn udfColumn in udf)
      {
        if (dataTable2.Columns.Contains(udfColumn.FieldName))
        {
          stringBuilder2.Append('\t');
          stringBuilder2.Append(udfColumn.FieldName);
        }
      }
      stringBuilder1.AppendLine(((object) stringBuilder2).ToString());
      foreach (DataRow dataRow2 in dataTable2.Select("", "Seq"))
      {
        string str2 = "";
        if (dataRow2.Table.Columns.Contains("ItemCode"))
          str2 = str2 + StringHelper.AddDoubleQuoteString(dataRow2["ItemCode"].ToString()) + "\t";
        if (dataRow2.Table.Columns.Contains("Description"))
          str2 = str2 + StringHelper.AddDoubleQuoteString(dataRow2["Description"].ToString()) + "\t";
        if (dataRow2.Table.Columns.Contains("BatchNo"))
          str2 = str2 + StringHelper.AddDoubleQuoteString(dataRow2["BatchNo"].ToString()) + "\t";
        if (dataRow2.Table.Columns.Contains("ProjNo"))
          str2 = str2 + StringHelper.AddDoubleQuoteString(dataRow2["ProjNo"].ToString()) + "\t";
        if (dataRow2.Table.Columns.Contains("DeptNo"))
          str2 = str2 + StringHelper.AddDoubleQuoteString(dataRow2["DeptNo"].ToString()) + "\t";
        if (dataRow2.Table.Columns.Contains("Rate"))
          str2 = str2 + StringHelper.AddDoubleQuoteString(dataRow2["Rate"].ToString()) + "\t";
        if (dataRow2.Table.Columns.Contains("Qty"))
        {
          Decimal number = BCE.Data.Convert.ToDecimal(dataRow2["Qty"]);
          str2 = str2 + this.myCommand.DecimalSetting.FormatQuantity(number) + "\t";
        }
        if (dataRow2.Table.Columns.Contains("OverheadCost"))
        {
          Decimal number = BCE.Data.Convert.ToDecimal(dataRow2["OverheadCost"]);
          str2 = str2 + this.myCommand.DecimalSetting.FormatCost(number) + "\t";
        }
        if (dataRow2.Table.Columns.Contains("SubTotalCost"))
        {
          Decimal number = BCE.Data.Convert.ToDecimal(dataRow2["SubTotalCost"]);
          str2 = str2 + this.myCommand.DecimalSetting.FormatCurrency(number) + "\t";
        }
        if (dataRow2.Table.Columns.Contains("Location"))
          str2 = str2 + StringHelper.AddDoubleQuoteString(dataRow2["Location"].ToString()) + "\t";
        if (dataRow2.Table.Columns.Contains("ItemCost"))
        {
          Decimal number = BCE.Data.Convert.ToDecimal(dataRow2["ItemCost"]);
          str2 = str2 + this.myCommand.DecimalSetting.FormatCost(number) + "\t";
        }
        if (dataRow2.Table.Columns.Contains("Remark"))
          str2 = str2 + StringHelper.AddDoubleQuoteString(dataRow2["Remark"].ToString()) + "\t";
        if (dataRow2.Table.Columns.Contains("FurtherDescription"))
        {
          string str3 = dataRow2["FurtherDescription"].ToString();
          int length = str3.Length;
          while (length > 0 && (int) str3[length - 1] == 0)
            --length;
          string aStr = str3.Substring(0, length);
          str2 = str2 + StringHelper.AddDoubleQuoteString(aStr);
        }
        foreach (UDFColumn udfColumn in udf)
        {
          if (dataRow2.Table.Columns.Contains(udfColumn.FieldName))
          {
            if (udfColumn.Type == UDFType.Text || udfColumn.Type == UDFType.System || (udfColumn.Type == UDFType.Memo || udfColumn.Type == UDFType.ImageLink))
              str2 = str2 + "\t" + StringHelper.AddDoubleQuoteString(dataRow2[udfColumn.FieldName].ToString());
            else if (udfColumn.Type == UDFType.RichText)
            {
              string str3 = dataRow2[udfColumn.FieldName].ToString();
              int length = str3.Length;
              while (length > 0 && (int) str3[length - 1] == 0)
                --length;
              string aStr = str3.Substring(0, length);
              str2 = str2 + "\t" + StringHelper.AddDoubleQuoteString(aStr);
            }
            else if (udfColumn.Type == UDFType.Boolean)
              str2 = str2 + "\t" + BCE.Data.Convert.TextToBoolean(dataRow2[udfColumn.FieldName]).ToString();
            else if (udfColumn.Type == UDFType.Date)
            {
              string str3 = "";
              if (dataRow2[udfColumn.FieldName] != DBNull.Value)
                str3 = this.myCommand.GeneralSetting.FormatDate(BCE.Data.Convert.ToDateTime(dataRow2[udfColumn.FieldName]));
              str2 = str2 + "\t" + str3;
            }
            else if (udfColumn.Type == UDFType.Decimal)
              str2 = str2 + (object) "\t" + (string) (object) BCE.Data.Convert.ToDecimal(dataRow2[udfColumn.FieldName]);
            else if (udfColumn.Type == UDFType.Integer)
              str2 = str2 + (object) "\t" + (string) (object) BCE.Data.Convert.ToInt32(dataRow2[udfColumn.FieldName]);
          }
        }
        stringBuilder1.AppendLine(str2);
      }
      return ((object) stringBuilder1).ToString();
    }

    public void BeginLoadDetailData()
    {
      this.myMasterTable.ColumnChanged -= new DataColumnChangeEventHandler(this.MasterDataColumnChangeEventHandler);
      this.myMasterTable.BeginLoadData();
      this.myDetailTable.ColumnChanged -= new DataColumnChangeEventHandler(this.DetailDataColumnChangedEventHandler);
      this.myDetailTable.BeginLoadData();
    }

    public void EndLoadDetailData()
    {
      this.myMasterTable.EndLoadData();
      this.myMasterTable.ColumnChanged += new DataColumnChangeEventHandler(this.MasterDataColumnChangeEventHandler);
      this.myDetailTable.EndLoadData();
      this.myDetailTable.ColumnChanged += new DataColumnChangeEventHandler(this.DetailDataColumnChangedEventHandler);
      this.UpdateSubTotal();
    }

    public DataRow[] GetValidDetailRows()
    {
      return this.myDetailTable.Select("", "Seq", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
    }

    public void PrepareForDispose()
    {
      this.myCommand = (StockWorkOrderCommand) null;
      this.myMasterTable.ColumnChanged -= new DataColumnChangeEventHandler(this.MasterDataColumnChangeEventHandler);
      this.myDetailTable.RowChanged -= new DataRowChangeEventHandler(this.myDetailTable_RowChanged);
      this.myDetailTable.ColumnChanged -= new DataColumnChangeEventHandler(this.DetailDataColumnChangedEventHandler);
      this.myDetailTable.RowDeleted -= new DataRowChangeEventHandler(this.DetailDataRowDeletedEventHandler);
      this.myDetailTable.RowDeleting -= new DataRowChangeEventHandler(this.myDetailTable_RowDeleting);
      this.myMasterTable.Clear();
      this.myDetailTable.Clear();
      this.myMasterTable.PrimaryKey = (DataColumn[]) null;
      this.myMasterTable.Columns.Clear();
      this.myDetailTable.PrimaryKey = (DataColumn[]) null;
      this.myDetailTable.Columns.Clear();
    }

    public StockWorkOrderDetail AddDetail()
    {
      if (this.myAction == StockWorkOrderAction.View)
        throw new Exception(Localizer.GetString((Enum)StockAssemblyOrderStringId.ErrorMessage_CannotEdit, new object[0]));
      else
        return this.InternalAddDetail(SeqUtils.GetLastSeq(this.GetValidDetailRows()));
    }

    public StockWorkOrderDetail InsertDetailBefore(TreeListNode focusNode)
    {
      if (this.myAction == StockWorkOrderAction.View)
      {
        throw new Exception(Localizer.GetString((Enum)StockAssemblyOrderStringId.ErrorMessage_CannotEdit, new object[0]));
      }
      else
      {
        DataRow[] validDetailRows = this.GetValidDetailRows();
        int index1 = -1;
        for (int index2 = 0; index2 < validDetailRows.Length; ++index2)
        {
          if (BCE.Data.Convert.ToInt64(validDetailRows[index2]["DtlKey"]) == BCE.Data.Convert.ToInt64(focusNode[(object) "DtlKey"]))
          {
            index1 = index2;
            break;
          }
        }
        int newSeqAtThisIndex = SeqUtils.GetNewSeqAtThisIndex(index1, this.GetValidDetailRows());
        object parentDtlKey = (object) null;
        if (focusNode.ParentNode != null)
          parentDtlKey = focusNode.ParentNode[(object) "DtlKey"];
        return this.InternalAddDetail(newSeqAtThisIndex, parentDtlKey);
      }
    }

    public StockWorkOrderDetail EditDetail(int index)
    {
      if (this.myAction == StockWorkOrderAction.View)
      {
        throw new InvalidOperationException("Cannot edit detail in the document which is in View mode.");
      }
      else
      {
        DataRow[] validDetailRows = this.GetValidDetailRows();
        if (index >= 0 && index < validDetailRows.Length)
          return new StockWorkOrderDetail(this.myCommand.DBSetting, validDetailRows[index]);
        else
          return (StockWorkOrderDetail) null;
      }
    }

    public StockWorkOrderDetail EditDetail(long dtlKey)
    {
      if (this.myAction == StockWorkOrderAction.View)
      {
        throw new InvalidOperationException("Cannot edit detail in the document which is in View mode.");
      }
      else
      {
        DataRow row = this.myDetailTable.Rows.Find((object) dtlKey);
        if (row == null)
          return (StockWorkOrderDetail) null;
        else if (row.RowState == DataRowState.Deleted)
          throw new InvalidOperationException("Cannot edit a deleted detail record.");
        else
          return new StockWorkOrderDetail(this.myCommand.DBSetting, row);
      }
    }

    public bool DeleteDetail(int index)
    {
      if (this.myAction == StockWorkOrderAction.View)
      {
        throw new InvalidOperationException("Cannot delete detail in the document which is in View mode.");
      }
      else
      {
        DataRow[] validDetailRows = this.GetValidDetailRows();
        if (index >= 0 && index < validDetailRows.Length)
        {
          validDetailRows[index].Delete();
          return true;
        }
        else
          return false;
      }
    }

    public bool DeleteDetail(long dtlKey)
    {
      if (this.myAction == StockWorkOrderAction.View)
      {
        throw new InvalidOperationException("Cannot delete detail in the document which is in View mode.");
      }
      else
      {
        DataRow dataRow = this.myDetailTable.Rows.Find((object) dtlKey);
        if (dataRow == null || dataRow.RowState == DataRowState.Deleted)
        {
          return false;
        }
        else
        {
          dataRow.Delete();
          return true;
        }
      }
    }

    public void ClearDetails()
    {
      if (this.myAction == StockWorkOrderAction.View)
      {
        throw new InvalidOperationException("Cannot clear all details in the document which is in View mode.");
      }
      else
      {
        foreach (DataRow dataRow in this.GetValidDetailRows())
          dataRow.Delete();
      }
    }

    private StockWorkOrderDetail InternalAddDetail(int seq)
    {
      DataRow row = this.myDetailTable.NewRow();
      row.BeginEdit();
      DBRegistry dbRegistry = DBRegistry.Create(this.myCommand.myDBSetting);
      row["DtlKey"] = (object) dbRegistry.IncOne((IRegistryID) new GlobalUniqueKey());
      if (row.Table.Columns.Contains("Guid"))
        row["Guid"] = (object) Guid.NewGuid();
      row["DocKey"] = this.myRow["DocKey"];
      row["Seq"] = (object) seq;
      row["Rate"] = (object) 1;
      row["Qty"] = this.myMasterTable.Rows[0]["Qty"];
      row["PrintOut"] = (object) BCE.Data.Convert.BooleanToText(true);
      row["ProjNo"] = this.myCommand.UserAuthentication.DefaultProject;
      row["DeptNo"] = this.myCommand.UserAuthentication.DefaultDepartment;
      row["StockReceived"] = (object) BCE.Data.Convert.BooleanToText(false);
      row.EndEdit();
      this.myDetailTable.Rows.Add(row);
      return new StockWorkOrderDetail(this.myCommand.DBSetting, row);
    }

    private StockWorkOrderDetail InternalAddDetail(int seq, object parentDtlKey)
    {
      DataRow row = this.myDetailTable.NewRow();
      row.BeginEdit();
      DBRegistry dbRegistry = DBRegistry.Create(this.myCommand.myDBSetting);
      row["DtlKey"] = (object) dbRegistry.IncOne((IRegistryID) new GlobalUniqueKey());
      if (row.Table.Columns.Contains("Guid"))
        row["Guid"] = (object) Guid.NewGuid();
      row["DocKey"] = this.myRow["DocKey"];
      row["Seq"] = (object) seq;
      row["PrintOut"] = (object) BCE.Data.Convert.BooleanToText(true);
      row["ProjNo"] = this.myCommand.UserAuthentication.DefaultProject;
      row["DeptNo"] = this.myCommand.UserAuthentication.DefaultDepartment;
      row["StockReceived"] = (object) BCE.Data.Convert.BooleanToText(false);
      if (parentDtlKey != null)
        row["ParentDtlKey"] = (object) BCE.Data.Convert.ToInt64(parentDtlKey);
      row.EndEdit();
      this.myDetailTable.Rows.Add(row);
      return new StockWorkOrderDetail(this.myCommand.DBSetting, row);
    }

    private string CheckBOMItemDetail(object itemCode, object parentDtlKey, object qty)
    {
      this.PauseUndo();
      try
      {
        this.DeleteAllSubItem(parentDtlKey);
        DataTable dataTable = this.myCommand.LoadItemBOMData(itemCode.ToString());
        string str = "F";
        if (dataTable != null)
        {
          int count = dataTable.Rows.Count;
          this.myCommand.DecimalSetting.RoundQuantity(this.myRow["Qty"]);
          foreach (DataRow dataRow in (InternalDataCollectionBase) dataTable.Rows)
          {
            StockWorkOrderDetail assemblyOrderDetail = this.AddDetail();
            assemblyOrderDetail.ParentDtlKey = BCE.Data.Convert.ToInt64(parentDtlKey);
            assemblyOrderDetail.OverHeadCost = (DBDecimal) BCE.Data.Convert.ToDecimal(dataRow["OverHeadCost"]);
            assemblyOrderDetail.ItemCode = BCE.Data.Convert.ToDBString(dataRow["SubItemCode"]);
            assemblyOrderDetail.Rate = BCE.Data.Convert.ToDBDecimal(dataRow["Qty"]);
            assemblyOrderDetail.Qty = (DBDecimal) ((Decimal) assemblyOrderDetail.Rate * BCE.Data.Convert.ToDecimal(qty));
            if (!BCE.Data.Convert.TextToBoolean(assemblyOrderDetail.Row["IsBomItem"]))
              this.UpdateUnitCost(assemblyOrderDetail.Row);
          }
          this.AddAssemblyCost(itemCode, parentDtlKey, qty);
          this.UpdateSubTotal();
          str = "T";
        }
        return str;
      }
      finally
      {
        this.ResumeUndo(true);
      }
    }

    private void AddAssemblyCost(object itemCode, object parentDtlKey, object qty)
    {
      Decimal assemblyCost = this.myCommand.myHelper.GetAssemblyCost(itemCode.ToString());
      if (assemblyCost != Decimal.Zero)
      {
        StockWorkOrderDetail assemblyOrderDetail = this.AddDetail();
        this.DisableUpdateCostEvent();
        try
        {
          assemblyOrderDetail.OverHeadCost = (DBDecimal) Decimal.Zero;
          assemblyOrderDetail.Description = (DBString) "Assembly Cost";
          assemblyOrderDetail.Rate = (DBDecimal) Decimal.One;
          assemblyOrderDetail.Qty = (DBDecimal) BCE.Data.Convert.ToDecimal(qty);
          assemblyOrderDetail.ParentDtlKey = BCE.Data.Convert.ToInt64(parentDtlKey);
        }
        finally
        {
          this.EnableUpdateCostEvent();
        }
        assemblyOrderDetail.ItemCost = (DBDecimal) assemblyCost;
      }
    }

    private void DeleteAllSubItem(object dtlKey)
    {
      DataRow[] dataRowArray = this.myDetailTable.Select("ParentDtlKey=" + dtlKey);
      if (dataRowArray != null)
      {
        foreach (DataRow dataRow in dataRowArray)
          dataRow.Delete();
      }
    }

    public void DeleteAllSubItems()
    {
      this.DisableUpdateCostEvent();
      try
      {
        DataRow[] dataRowArray1 = this.myDetailTable.Select("ParentDtlKey > 0");
        if (dataRowArray1 != null)
        {
          foreach (DataRow dataRow in dataRowArray1)
            dataRow.Delete();
        }
        DataRow[] dataRowArray2 = this.myDetailTable.Select("IsBOMItem = 'T'", "", DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.ModifiedCurrent);
        if (dataRowArray2 != null)
        {
          foreach (DataRow dataRow in dataRowArray2)
            dataRow["IsBOMItem"] = (object) "F";
        }
      }
      finally
      {
        this.EnableUpdateCostEvent();
      }
      this.UpdateSubTotal();
    }

    private void MasterDataColumnChangeEventHandler(object sender, DataColumnChangeEventArgs e)
    {
      if (string.Compare(e.Column.ColumnName, "ItemCode", true) == 0)
      {
        this.PauseUndo();
        try
        {
          if (e.Row["Qty"] == DBNull.Value)
            e.Row["Qty"] = (object) 1;
          Decimal num = this.myCommand.DecimalSetting.RoundQuantity(e.Row["Qty"]);
          e.Row["AssemblyCost"] = (object) this.myCommand.DecimalSetting.RoundCost(num * this.myCommand.myHelper.GetAssemblyCost(e.Row["ItemCode"].ToString()));
          this.myDetailTable.RowChanged -= new DataRowChangeEventHandler(this.myDetailTable_RowChanged);
          this.myDetailTable.ColumnChanged -= new DataColumnChangeEventHandler(this.DetailDataColumnChangedEventHandler);
          this.myDetailTable.RowDeleted -= new DataRowChangeEventHandler(this.DetailDataRowDeletedEventHandler);
          this.myDetailTable.RowDeleting -= new DataRowChangeEventHandler(this.myDetailTable_RowDeleting);
          this.myDetailTable.BeginLoadData();
          DataRow[] validDetailRows = this.GetValidDetailRows();
          for (int index = validDetailRows.Length - 1; index >= 0; --index)
            validDetailRows[index].Delete();
          this.myDetailTable.EndLoadData();
          this.myDetailTable.RowChanged += new DataRowChangeEventHandler(this.myDetailTable_RowChanged);
          this.myDetailTable.ColumnChanged += new DataColumnChangeEventHandler(this.DetailDataColumnChangedEventHandler);
          this.myDetailTable.RowDeleted += new DataRowChangeEventHandler(this.DetailDataRowDeletedEventHandler);
          this.myDetailTable.RowDeleting += new DataRowChangeEventHandler(this.myDetailTable_RowDeleting);
          DataTable dataTable = this.myCommand.LoadItemBOMData(e.Row["ItemCode"].ToString());
          if (dataTable != null)
          {
            int count = dataTable.Rows.Count;
            foreach (DataRow bomRow in (InternalDataCollectionBase) dataTable.Rows)
            {
              StockWorkOrderDetail assemblyOrderDetail = this.AddDetail();
              assemblyOrderDetail.OverHeadCost = (DBDecimal) BCE.Data.Convert.ToDecimal(bomRow["OverHeadCost"]);
              assemblyOrderDetail.ItemCode = BCE.Data.Convert.ToDBString(bomRow["SubItemCode"]);
              assemblyOrderDetail.Rate = BCE.Data.Convert.ToDBDecimal(bomRow["Qty"]);
              assemblyOrderDetail.Qty = (DBDecimal) ((Decimal) assemblyOrderDetail.Rate * num);
              StockWorkOrderAssignNewDetailFromRawMaterialRecordEventArgs materialRecordEventArgs1 = new StockWorkOrderAssignNewDetailFromRawMaterialRecordEventArgs(this, assemblyOrderDetail.Row, bomRow);
              ScriptObject scriptObject = this.ScriptObject;
              string name = "OnAssignNewDetailFromRawMaterialRecord";
              Type[] types = new Type[1];
              int index1 = 0;
              Type type = materialRecordEventArgs1.GetType();
              types[index1] = type;
              object[] objArray = new object[1];
              int index2 = 0;
              StockWorkOrderAssignNewDetailFromRawMaterialRecordEventArgs materialRecordEventArgs2 = materialRecordEventArgs1;
              objArray[index2] = (object) materialRecordEventArgs2;
              scriptObject.RunMethod(name, types, objArray);
            }
          }
        }
        finally
        {
          this.ResumeUndo(true);
        }
      }
      else if (string.Compare(e.Column.ColumnName, "Qty", true) == 0)
      {
        this.PauseUndo();
        try
        {
          Decimal num1 = this.myCommand.DecimalSetting.RoundQuantity(e.Row["Qty"]);
          foreach (DataRow dataRow in this.GetValidDetailRows())
          {
            if (BCE.Data.Convert.ToInt64(dataRow["ParentDtlKey"]) <= 0L)
            {
              Decimal num2 = this.myCommand.DecimalSetting.RoundQuantity(dataRow["Rate"]);
              dataRow["Qty"] = (object) this.myCommand.DecimalSetting.RoundQuantity(num1 * num2);
            }
          }
          e.Row["AssemblyCost"] = (object) this.myCommand.DecimalSetting.RoundCost(num1 * this.myCommand.myHelper.GetAssemblyCost(e.Row["ItemCode"].ToString()));
        }
        finally
        {
          this.ResumeUndo(true);
        }
      }
      else if (string.Compare(e.Column.ColumnName, "Total", true) == 0 || string.Compare(e.Column.ColumnName, "AssemblyCost", true) == 0)
      {
        Decimal num1 = this.myCommand.DecimalSetting.RoundCost(this.myRow["Total"]);
        Decimal num2 = this.myCommand.DecimalSetting.RoundCost(this.myRow["AssemblyCost"]);
        this.myRow.BeginEdit();
        this.myRow["NetTotal"] = (object) (num1 + num2);
        this.myRow.EndEdit();
      }
      else if (string.Compare(e.Column.ColumnName, "IsMultilevel", true) == 0)
      {
        if (e.Row["ItemCode"].ToString().Length == 0)
          return;
        else if (BCE.Data.Convert.TextToBoolean(e.Row["IsMultilevel"]))
        {
          DataTable dataTable = this.Command.LoadAllBOMItems();
          DataRow[] validDetailRows = this.GetValidDetailRows();
          for (int index = validDetailRows.Length - 1; index >= 0; --index)
          {
            string str = validDetailRows[index]["ItemCode"].ToString();
            if (dataTable.Rows.Find((object) str) != null)
              this.LoadBOMSubItems(validDetailRows[index]);
          }
        }
        else
          this.DeleteAllSubItems();
      }
      StockWorkOrderMasterColumnChangedEventArgs changedEventArgs1 = new StockWorkOrderMasterColumnChangedEventArgs(e.Column.ColumnName, this);
      ScriptObject scriptObject1 = this.myScriptObject;
      string name1 = "OnMasterColumnChanged";
      Type[] types1 = new Type[1];
      int index3 = 0;
      Type type1 = changedEventArgs1.GetType();
      types1[index3] = type1;
      object[] objArray1 = new object[1];
      int index4 = 0;
      StockWorkOrderMasterColumnChangedEventArgs changedEventArgs2 = changedEventArgs1;
      objArray1[index4] = (object) changedEventArgs2;
      scriptObject1.RunMethod(name1, types1, objArray1);
    }

    private void myDetailTable_RowDeleting(object sender, DataRowChangeEventArgs e)
    {
      if (e.Row["IsBOMItem"].ToString() == "T")
        this.DeleteAllSubItem(e.Row["DtlKey"]);
    }

    public void LoadBOMSubItems(DataRow r)
    {
      r["IsBOMItem"] = (object) this.CheckBOMItemDetail(r["ItemCode"], r["DtlKey"], r["Qty"]);
      this.UpdateSubTotal();
    }

    private void DetailDataColumnChangingEventHandler(object sender, DataColumnChangeEventArgs e)
    {
      if (!this.IsColumnChangingEventDisabled() && BCE.Data.Convert.TextToBoolean(e.Row["IsBomItem"]) && (string.Compare(e.Column.ColumnName, "ItemCost", true) == 0 || string.Compare(e.Column.ColumnName, "SubTotalCost", true) == 0))
        e.ProposedValue = e.Row[e.Column.ColumnName];
    }

    private void DetailDataColumnChangedEventHandler(object sender, DataColumnChangeEventArgs e)
    {
      if (!this.IsColumnChangedEventDisabled())
      {
        if (string.Compare(e.Column.ColumnName, "ItemCode", true) == 0)
        {
          if (this.myEnableAutoLoadItemDetail)
          {
            StockDocumentItem stockDocumentItem = this.myCommand.myHelper.LoadStockDocumentItem(e.Row[e.Column].ToString());
            e.Row.BeginEdit();
            if (BCE.Data.Convert.TextToBoolean(this.myRow["IsMultilevel"]))
            {
              e.Row.EndEdit();
              string str = this.CheckBOMItemDetail(e.Row["ItemCode"], e.Row["DtlKey"], e.Row["Qty"]);
              e.Row.BeginEdit();
              e.Row["IsBOMItem"] = (object) str;
            }
            else
              e.Row["IsBOMItem"] = (object) "F";
            if (stockDocumentItem != null)
            {
              this.DisableColumnChangedEvent();
              try
              {
                if (!e.Row["ItemCode"].ToString().Equals(stockDocumentItem.ItemCode))
                  e.Row["ItemCode"] = (object) stockDocumentItem.ItemCode;
                e.Row["Description"] = stockDocumentItem.Description;
                e.Row["FurtherDescription"] = stockDocumentItem.FurtherDescription == DBNull.Value ? (object) DBNull.Value : (object) Rtf.ToArialRichText(stockDocumentItem.FurtherDescription.ToString());
                e.Row["ItemCost"] = stockDocumentItem.Cost;
              }
              finally
              {
                this.EnableColumnChangedEvent();
              }
            }
          }
          this.DisableColumnChangedEvent();
          try
          {
            e.Row["BatchNo"] = (object) DBNull.Value;
            if (e.Row["ItemCode"] == DBNull.Value)
              e.Row["Location"] = (object) DBNull.Value;
            else if (e.Row["Location"] == DBNull.Value)
              e.Row["Location"] = (object) this.myCommand.UserAuthentication.MainLocation;
          }
          finally
          {
            this.EnableColumnChangedEvent();
          }
          this.UpdateUnitCost(e.Row);
        }
        else if (string.Compare(e.Column.ColumnName, "Location", true) == 0 || string.Compare(e.Column.ColumnName, "BatchNo", true) == 0)
          this.UpdateUnitCost(e.Row);
        else if (string.Compare(e.Column.ColumnName, "Qty", true) == 0 || string.Compare(e.Column.ColumnName, "ItemCost", true) == 0 || string.Compare(e.Column.ColumnName, "OverHeadCost", true) == 0)
        {
          if (string.Compare(e.Column.ColumnName, "Qty", true) == 0)
          {
            if (BCE.Data.Convert.TextToBoolean(e.Row["IsBomItem"]))
            {
              foreach (DataRow dataRow in this.myDetailTable.Select("ParentDtlKey=" + e.Row["DtlKey"]))
              {
                Decimal num = this.myCommand.DecimalSetting.RoundQuantity(this.myCommand.DecimalSetting.RoundQuantity(dataRow["Rate"]) * this.myCommand.DecimalSetting.RoundQuantity(e.Row["Qty"]));
                if (!dataRow["Qty"].Equals((object) num))
                  dataRow["Qty"] = (object) num;
              }
            }
            else
              this.UpdateUnitCost(e.Row);
          }
          if (string.Compare(e.Column.ColumnName, "OverHeadCost", true) == 0 && BCE.Data.Convert.TextToBoolean(e.Row["IsBomItem"]))
            this.myDetailTable.ColumnChanging -= new DataColumnChangeEventHandler(this.DetailDataColumnChangingEventHandler);
          this.CalcSubTotal(e.Row);
          if (string.Compare(e.Column.ColumnName, "OverHeadCost", true) == 0 && BCE.Data.Convert.TextToBoolean(e.Row["IsBomItem"]))
            this.myDetailTable.ColumnChanging += new DataColumnChangeEventHandler(this.DetailDataColumnChangingEventHandler);
          e.Row.EndEdit();
        }
        else if (string.Compare(e.Column.ColumnName, "SubTotalCost", true) == 0)
        {
          if (e.Row["ParentDtlKey"].ToString().Length > 0)
          {
            DataRow[] dataRowArray = this.myDetailTable.Select("DtlKey=" + e.Row["ParentDtlKey"]);
            object number1 = this.myDetailTable.Compute("SUM(SubTotalCost)", "ParentDtlKey=" + e.Row["ParentDtlKey"].ToString());
            if (dataRowArray.Length != 0 && BCE.Data.Convert.ToDecimal(dataRowArray[0]["Qty"]) != Decimal.Zero)
            {
              if (dataRowArray[0]["ParentDtlKey"].ToString().Length < 1)
                this.DisableColumnChangedEvent();
              this.DisableColumnChangingEvent();
              dataRowArray[0].BeginEdit();
              dataRowArray[0]["SubTotalCost"] = (object) this.myCommand.DecimalSetting.RoundCost(number1);
              Decimal number2 = (BCE.Data.Convert.ToDecimal(number1) - BCE.Data.Convert.ToDecimal(dataRowArray[0]["OverHeadCost"])) / BCE.Data.Convert.ToDecimal(dataRowArray[0]["Qty"]);
              dataRowArray[0]["ItemCost"] = (object) this.myCommand.DecimalSetting.RoundCost(number2);
              dataRowArray[0].EndEdit();
              this.EnableColumnChangingEvent();
              if (dataRowArray[0]["ParentDtlKey"].ToString().Length < 1)
                this.EnableColumnChangedEvent();
            }
          }
          this.UpdateSubTotal();
        }
        StockWorkOrderDetailColumnChangedEventArgs changedEventArgs1 = new StockWorkOrderDetailColumnChangedEventArgs(e.Column.ColumnName, this, e.Row);
        ScriptObject scriptObject = this.myScriptObject;
        string name = "OnDetailColumnChanged";
        Type[] types = new Type[1];
        int index1 = 0;
        Type type = changedEventArgs1.GetType();
        types[index1] = type;
        object[] objArray = new object[1];
        int index2 = 0;
        StockWorkOrderDetailColumnChangedEventArgs changedEventArgs2 = changedEventArgs1;
        objArray[index2] = (object) changedEventArgs2;
        scriptObject.RunMethod(name, types, objArray);
      }
    }

    private void DetailDataRowDeletedEventHandler(object sender, DataRowChangeEventArgs e)
    {
      this.UpdateSubTotal();
    }

    private void myDetailTable_RowChanged(object sender, DataRowChangeEventArgs e)
    {
      if (e.Action == DataRowAction.Add)
      {
        StockWorkOrderNewDetailEventArgs newDetailEventArgs1 = new StockWorkOrderNewDetailEventArgs(this, e.Row);
        ScriptObject scriptObject = this.myScriptObject;
        string name = "OnNewDetail";
        Type[] types = new Type[1];
        int index1 = 0;
        Type type = newDetailEventArgs1.GetType();
        types[index1] = type;
        object[] objArray = new object[1];
        int index2 = 0;
        StockWorkOrderNewDetailEventArgs newDetailEventArgs2 = newDetailEventArgs1;
        objArray[index2] = (object) newDetailEventArgs2;
        scriptObject.RunMethod(name, types, objArray);
      }
    }

    public void CalcSubTotal(DataRow row)
    {
      if (row["Qty"] == DBNull.Value && row["ItemCost"] == DBNull.Value && row["OverHeadCost"] == DBNull.Value)
      {
        row["SubTotalCost"] = (object) DBNull.Value;
      }
      else
      {
        Decimal num = this.myCommand.DecimalSetting.RoundCost(this.myCommand.DecimalSetting.RoundQuantity(row["Qty"]) * this.myCommand.DecimalSetting.RoundCost(row["ItemCost"]) + this.myCommand.DecimalSetting.RoundCost(row["OverHeadCost"]));
        row["SubTotalCost"] = (object) num;
      }
    }

    public void UpdateSubTotal()
    {
      if (!this.IsUpdateCostEventDisabled())
      {
        Decimal num = new Decimal();
        this.myDetailTable.BeginLoadData();
        foreach (DataRow dataRow in this.GetValidDetailRows())
        {
          if (dataRow["ParentDtlKey"].ToString().Trim().Length == 0)
            num += BCE.Data.Convert.ToDecimal(dataRow["SubTotalCost"]);
        }
        this.myDetailTable.EndLoadData();
        this.myRow.BeginEdit();
        this.myRow["Total"] = (object) num;
        this.myRow.EndEdit();
      }
    }

    public void RecalcRows(DataRow[] rowList)
    {
      this.BeginLoadDetailData();
      foreach (DataRow row in rowList)
        this.CalcSubTotal(row);
      this.EndLoadDetailData();
    }

    public void RecalcAll()
    {
      this.RecalcRows(this.GetValidDetailRows());
    }

    private void UpdateUnitCost(DataRow r)
    {
      if (!this.IsUpdateCostEventDisabled() && r["ItemCode"] != DBNull.Value && r["Location"] != DBNull.Value)
      {
        Decimal num1 = BCE.Data.Convert.ToDecimal(r["Qty"]);
        if (num1 > Decimal.Zero)
        {
          UTDCosting utdCosting = UTDCosting.Create(this.myCommand.myDBSetting);
          ComputedCost computedCost = (ComputedCost) null;
          string baseUom = this.myCommand.myHelper.GetBaseUOM(r["ItemCode"].ToString());
          UTDCostHelper utdCostHelper = UTDCostHelper.Create(this.myCommand.DBSetting, r["ItemCode"].ToString(), baseUom, r["Location"].ToString(), r["BatchNo"], (DateTime) this.DocDate);
          if (r.RowState == DataRowState.Added)
            computedCost = utdCosting.GetNewUTDCost(utdCostHelper, num1, false);
          else if (r.RowState == DataRowState.Modified || r.RowState == DataRowState.Unchanged)
          {
            FIFOCost fifoCostForDtlKey = StockCosting.Create(this.myCommand.myDBSetting).GetFIFOCostForDtlKey(BCE.Data.Convert.ToInt64(r["DtlKey"]));
            Decimal oldQty = BCE.Data.Convert.ToDecimal(r["Qty", DataRowVersion.Original]);
            Decimal num2 = BCE.Data.Convert.ToDecimal(r["ItemCost", DataRowVersion.Original]);
            fifoCostForDtlKey.AppendCost(-oldQty, num2);
            computedCost = utdCosting.GetEditUTDCost(utdCostHelper, oldQty, num1, num2, fifoCostForDtlKey, false);
          }
          if (computedCost != null)
          {
            r["ItemCost"] = (object) (-computedCost.TotalCost / num1);
            r.EndEdit();
          }
        }
      }
    }

    protected int GetLastDetailSeq()
    {
      DataRow[] validDetailRows = this.GetValidDetailRows();
      if (validDetailRows.Length == 0)
        return 0;
      else
        return BCE.Data.Convert.ToInt32(validDetailRows[validDetailRows.Length - 1]["Seq"]);
    }
  }
}
