﻿// Type: BCE.AutoCount.Manufacturing.StockWorkOrder.FormStockWorkOrderPrintListing
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Common;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controls;
using BCE.AutoCount.Data;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.Help;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.SearchFilter;
using BCE.AutoCount.Stock;
using BCE.AutoCount.UDF;
using BCE.AutoCount.XtraUtils;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using BCE.XtraUtils;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace RPASystem.WorkOrder
{
  [SingleInstanceThreadForm]
  public class FormStockWorkOrderPrintListing : XtraForm
  {
    public static string myColumnName;
    private DBSetting myDBSetting;
    private DataTable myDataTable;
    private StockWorkOrderCommand myCommand;
    private AdvancedStockWorkOrderCriteria myCriteria;
    private bool myInSearch;
    private long[] mySelectedDocKeylist;
    private StockWorkOrderReportingCriteria myReportingCriteria;
    private string[] myGroupBy;
    private string[] mySortBy;
    private string[] myDocumentStyleOptions;
    private ScriptObject myScriptObject;
    private MouseDownHelper myMouseDownHelper;
    private IContainer components;
    private PanelHeader panelHeader1;
    private PreviewButton previewButton1;
    private PrintButton printButton1;
    private UCDateSelector ucDateSelector1;
    private UCSearchResult ucSearchResult1;
    private UCStockAssemblyOrderSelector ucStockWorkOrderSelector1;
    private Bar bar2;
    private BarButtonItem barBtnDesignDocumentStyleReport;
    private BarButtonItem barBtnDesignListingStyleReport;
    private BarDockControl barDockControlBottom;
    private BarDockControl barDockControlLeft;
    private BarDockControl barDockControlRight;
    private BarDockControl barDockControlTop;
    private BarManager barManager1;
    private BarSubItem barSubItem1;
    private CheckEdit chkEditShowCriteria;
    private ComboBoxEdit cbCancelledOption;
    private ComboBoxEdit cbEditGroupBy;
    private ComboBoxEdit cbEditReportType;
    private ComboBoxEdit cbEditSortBy;
    private GroupControl gbReportOption;
    private GroupControl groupBox_SearchCriteria;
    private MemoEdit memoEdit_Criteria;
    private PanelControl panel_Center;
    private PanelControl panelControl1;
    private PanelControl panelCriteria;
    private RepositoryItemCheckEdit repositoryItemCheckEdit1;
    private RepositoryItemTextEdit repositoryItemTextEdit_Cancelled;
    private SimpleButton sbtnClose;
    private SimpleButton sbtnInquiry;
    private SimpleButton sbtnToggleOptions;
    private SimpleButton simpleButtonAdvanceSearch;
    private GridColumn colAssemblyCost;
    private GridColumn colBatchNo;
    private GridColumn colCancelled;
    private GridColumn colCheck;
    private GridColumn colCreatedTimeStamp;
    private GridColumn colCreatedUserID;
    private GridColumn colDeptNo;
    private GridColumn colDescription;
    private GridColumn colDocDate;
    private GridColumn colDocNo;
    private GridColumn colItemCode;
    private GridColumn colLastModified;
    private GridColumn colLastModifiedUserID;
    private GridColumn colLocation;
    private GridColumn colNetTotal;
    private GridColumn colPrintCount;
    private GridColumn colProjNo;
    private GridColumn colQty;
    private GridColumn colRefDocNo;
    private GridColumn colRemark1;
    private GridColumn colRemark2;
    private GridColumn colRemark3;
    private GridColumn colRemark4;
    private GridColumn colTotal;
    private GridControl gridControl1;
    private GridView gridView1;
    private XtraTabControl tabControl1;
    private XtraTabPage tabPage1;
    private XtraTabPage tabPage2;
    private Label label3;
    private Label label4;
    private Label label6;
    private Label label7;
    private Label label8;
    private Label label9;
    private GridColumn colExpCompletedDate;

    public DataTable MasterDataTable
    {
      get
      {
        return this.myDataTable;
      }
    }

    public long[] SelectedDocKeys
    {
      get
      {
        return this.mySelectedDocKeylist;
      }
    }

    public string SelectedDocNosInString
    {
      get
      {
        return this.GenerateDocNosToString();
      }
    }

    public string SelectedDocKeysInString
    {
      get
      {
        return StringHelper.ArrayListToCommaString(new ArrayList((ICollection) this.mySelectedDocKeylist));
      }
    }

    public string ColumnName
    {
      get
      {
        return FormStockWorkOrderPrintListing.myColumnName;
      }
    }

    public StockWorkOrderReportingCriteria StockWorkOrderReportingCriteria
    {
      get
      {
        return this.myReportingCriteria;
      }
    }

    public FormStockWorkOrderPrintListing(DBSetting dbSetting)
    {
      FormStockWorkOrderPrintListing orderPrintListing1 = this;
      string[] strArray1 = new string[4];
      int index1 = 0;
      string str1 = "None";
      strArray1[index1] = str1;
      int index2 = 1;
      string str2 = "Date";
      strArray1[index2] = str2;
      int index3 = 2;
      string str3 = "Month";
      strArray1[index3] = str3;
      int index4 = 3;
      string str4 = "Year";
      strArray1[index4] = str4;
      orderPrintListing1.myGroupBy = strArray1;
      FormStockWorkOrderPrintListing orderPrintListing2 = this;
      string[] strArray2 = new string[2];
      int index5 = 0;
      string str5 = "Document No";
      strArray2[index5] = str5;
      int index6 = 1;
      string str6 = "Date";
      strArray2[index6] = str6;
      orderPrintListing2.mySortBy = strArray2;
      FormStockWorkOrderPrintListing orderPrintListing3 = this;
      string[] strArray3 = new string[2];
      int index7 = 0;
      string str7 = "Batch Print Stock Assembly Order";
      strArray3[index7] = str7;
      int index8 = 1;
      string str8 = "Print Stock Assembly Order Listing";
      strArray3[index8] = str8;
      orderPrintListing3.myDocumentStyleOptions = strArray3;
      // ISSUE: explicit constructor call
      //base.\u002Ector();
      this.InitializeComponent();
      this.myScriptObject = ScriptManager.CreateObject(dbSetting, "StockWorkOrderListing");
      this.myDBSetting = dbSetting;
      this.myCommand = StockWorkOrderCommand.Create(dbSetting);
      this.previewButton1.ReportType = "Stock Assembly Order Document";
      this.previewButton1.SetDBSetting(this.myDBSetting);
      this.printButton1.ReportType = "Stock Assembly Order Document";
      this.printButton1.SetDBSetting(this.myDBSetting);
      this.myDataTable = new DataTable();
      this.gridControl1.DataSource = (object) this.myDataTable;
      this.LoadCriteria();
      this.ucSearchResult1.Initialize(this.gridView1, "ToBeUpdate");
      CustomizeGridLayout customizeGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name, this.gridView1, new EventHandler(this.ReloadAllColumns));
      this.InitUserControls();
      this.InitFormControls();
      this.cbEditGroupBy.Properties.Items.AddRange((object[]) this.myGroupBy);
      this.cbEditSortBy.Properties.Items.AddRange((object[]) this.mySortBy);
      this.cbEditReportType.Properties.Items.AddRange((object[]) this.myDocumentStyleOptions);
      this.cbEditReportType.SelectedIndex = 1;
      new UDFUtil(this.myDBSetting).SetupListingReportGrid(this.gridView1, "ASMORDER");
      this.RefreshDesignReport();
      this.myMouseDownHelper = new MouseDownHelper();
      this.myMouseDownHelper.Init(this.gridView1);
      this.myCommand.UserAuthentication.AccessRight.AddListener(new AccessRightListenerDelegate(this.RefreshDesignReport), (Component) this);
      BCE.AutoCount.Help.HelpProvider.SetHelpTopic(this.panelHeader1, "Stock_Assembly_Order.htm");
      DBSetting dbSetting1 = dbSetting;
      string docType = "";
      long docKey = 0L;
      long eventKey = 0L;
      // ISSUE: variable of a boxed type
      StockWorkOrderString local =  StockWorkOrderString.OpenedPrintStockWorkOrderListing;
      object[] objArray = new object[1];
      int index9 = 0;
      string loginUserId = this.myCommand.UserAuthentication.LoginUserID;
      objArray[index9] = (object) loginUserId;
      string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
      string detail = "";
      Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
    }

    private void InitFormControls()
    {
      FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting);
      string fieldname1 = "DocDate";
      string fieldtype1 = "Date";
      formControlUtil.AddField(fieldname1, fieldtype1);
      string fieldname2 = "Qty";
      string fieldtype2 = "Quantity";
      formControlUtil.AddField(fieldname2, fieldtype2);
      string fieldname3 = "Total";
      string fieldtype3 = "Currency";
      formControlUtil.AddField(fieldname3, fieldtype3);
      string fieldname4 = "AssemblyCost";
      string fieldtype4 = "Currency";
      formControlUtil.AddField(fieldname4, fieldtype4);
      string fieldname5 = "NetTotal";
      string fieldtype5 = "Currency";
      formControlUtil.AddField(fieldname5, fieldtype5);
      string fieldname6 = "LastModified";
      string fieldtype6 = "DateTime";
      formControlUtil.AddField(fieldname6, fieldtype6);
      string fieldname7 = "CreatedTimeStamp";
      string fieldtype7 = "DateTime";
      formControlUtil.AddField(fieldname7, fieldtype7);
      FormStockWorkOrderPrintListing orderPrintListing = this;
      formControlUtil.InitControls((Control) orderPrintListing);
      this.InitGroupSummary();
    }

    private void InitGroupSummary()
    {
      this.gridView1.GroupSummary.Clear();
      GridGroupSummaryItemCollection groupSummary1 = this.gridView1.GroupSummary;
      int num1 = 3;
      string fieldName1 = "DocNo";
      // ISSUE: variable of the null type
      GridColumn local1 = null;
      // ISSUE: variable of a boxed type
      GridGroupSummaryItemStringId local2 = GridGroupSummaryItemStringId.Count;
      object[] objArray1 = new object[1];
      int index1 = 0;
      string str = "{0}";
      objArray1[index1] = (object) str;
      string string1 = BCE.Localization.Localizer.GetString((Enum) local2, objArray1);
      groupSummary1.Add((SummaryItemType) num1, fieldName1, (GridColumn) local1, string1);
      if (this.colNetTotal.Visible)
      {
        GridGroupSummaryItemCollection groupSummary2 = this.gridView1.GroupSummary;
        int num2 = 0;
        string fieldName2 = "NetTotal";
        // ISSUE: variable of the null type
        GridColumn local3 = null;
        // ISSUE: variable of a boxed type
        GridGroupSummaryItemStringId local4 =  GridGroupSummaryItemStringId.NetTotal;
        object[] objArray2 = new object[1];
        int index2 = 0;
        string currencyFormatString = this.myCommand.DecimalSetting.GetCurrencyFormatString(0);
        objArray2[index2] = (object) currencyFormatString;
        string string2 = BCE.Localization.Localizer.GetString((Enum) local4, objArray2);
        groupSummary2.Add((SummaryItemType) num2, fieldName2, (GridColumn) local3, string2);
      }
      if (this.colTotal.Visible)
      {
        GridGroupSummaryItemCollection groupSummary2 = this.gridView1.GroupSummary;
        int num2 = 0;
        string fieldName2 = "Total";
        // ISSUE: variable of the null type
        GridColumn local3 = null;
        // ISSUE: variable of a boxed type
        GridGroupSummaryItemStringId local4 =  GridGroupSummaryItemStringId.Total;
        object[] objArray2 = new object[1];
        int index2 = 0;
        string currencyFormatString = this.myCommand.DecimalSetting.GetCurrencyFormatString(0);
        objArray2[index2] = (object) currencyFormatString;
        string string2 = BCE.Localization.Localizer.GetString((Enum) local4, objArray2);
        groupSummary2.Add((SummaryItemType) num2, fieldName2, (GridColumn) local3, string2);
      }
      if (this.colAssemblyCost.Visible)
      {
        GridGroupSummaryItemCollection groupSummary2 = this.gridView1.GroupSummary;
        int num2 = 0;
        string fieldName2 = "AssemblyCost";
        // ISSUE: variable of the null type
        GridColumn local3 = null;
        // ISSUE: variable of a boxed type
        GridGroupSummaryItemStringId local4 =  GridGroupSummaryItemStringId.AssemblyCost;
        object[] objArray2 = new object[1];
        int index2 = 0;
        string costFormatString = this.myCommand.DecimalSetting.GetCostFormatString(0);
        objArray2[index2] = (object) costFormatString;
        string string2 = BCE.Localization.Localizer.GetString((Enum) local4, objArray2);
        groupSummary2.Add((SummaryItemType) num2, fieldName2, (GridColumn) local3, string2);
      }
    }

    private void SaveCriteria()
    {
      PersistenceUtil.SaveCriteriaData(this.myDBSetting, (CustomSetting) this.myReportingCriteria, "StockWorkOrderDocumentListingReport.setting");
    }

    private void LoadCriteria()
    {
      try
      {
        this.myReportingCriteria = (StockWorkOrderReportingCriteria) PersistenceUtil.LoadCriteriaData(this.myDBSetting, "StockWorkOrderDocumentListingReport.setting");
      }
      catch
      {
      }
      if (this.myReportingCriteria == null)
        this.myReportingCriteria = new StockWorkOrderReportingCriteria();
      this.cbEditGroupBy.EditValue = (object) this.myReportingCriteria.GroupBy;
      this.cbEditSortBy.EditValue = (object) this.myReportingCriteria.SortBy;
      this.ucSearchResult1.KeepSearchResult = this.myReportingCriteria.KeepSearchResult;
      this.cbCancelledOption.SelectedIndex = (int) this.myReportingCriteria.IsPrintCancelled;
      this.chkEditShowCriteria.Checked = this.myReportingCriteria.IsShowCriteria;
    }

    private void InitUserControls()
    {
      this.ucDateSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
      this.ucStockWorkOrderSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentFilter);
    }

    private void AddNewColumn()
    {
      this.myDataTable.Columns.Add(new DataColumn()
      {
        DataType = typeof (bool),
        AllowDBNull = true,
        Caption = "Check",
        ColumnName = "ToBeUpdate",
        DefaultValue = (object) false
      });
    }

    private void BasicSearch(bool isSearchAll)
    {
      if (!this.myInSearch)
      {
        this.myInSearch = true;
        BCE.XtraUtils.GridViewUtils.UpdateData(this.gridView1);
        this.gridControl1.DataSource = (object) null;
        try
        {
          this.gridControl1.MainView.UpdateCurrentRow();
          this.myReportingCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
          this.myCommand.DocumentListingBasicSearch(this.myReportingCriteria, CommonFunction.BuildSQLColumns(isSearchAll, this.gridView1.Columns.View), this.myDataTable, "ToBeUpdate");
          if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
            this.AddNewColumn();
          this.gridControl1.DataSource = (object) this.myDataTable;
        }
        finally
        {
          this.myInSearch = false;
        }
        FormStockWorkOrderPrintListing.FormInquiryEventArgs inquiryEventArgs1 = new FormStockWorkOrderPrintListing.FormInquiryEventArgs(this, this.myDataTable);
        ScriptObject scriptObject = this.myScriptObject;
        string name = "OnFormInquiry";
        System.Type[] types = new System.Type[1];
        int index1 = 0;
        System.Type type = inquiryEventArgs1.GetType();
        types[index1] = type;
        object[] objArray = new object[1];
        int index2 = 0;
        FormStockWorkOrderPrintListing.FormInquiryEventArgs inquiryEventArgs2 = inquiryEventArgs1;
        objArray[index2] = (object) inquiryEventArgs2;
        scriptObject.RunMethod(name, types, objArray);
      }
    }

    private void AdvanceSearch()
    {
      if (this.myCriteria == null)
        this.myCriteria = new AdvancedStockWorkOrderCriteria(this.myDBSetting);
      this.myCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
      using (FormAdvancedSearch formAdvancedSearch = new FormAdvancedSearch((SearchCriteria) this.myCriteria, this.myDBSetting))
      {
        if (formAdvancedSearch.ShowDialog((IWin32Window) this) == DialogResult.OK)
        {
          if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
            this.AddNewColumn();
          this.memoEdit_Criteria.Lines = this.myCriteria.BuildReadableTextArray();
          this.ucSearchResult1.KeepSearchResult = this.myCriteria.KeepSearchResult;
          Cursor current = Cursor.Current;
          Cursor.Current = Cursors.WaitCursor;
          this.myInSearch = true;
          try
          {
            this.myCommand.AdvanceSearch(this.myCriteria, CommonFunction.BuildSQLColumns(false, this.gridView1.Columns.View), this.myDataTable, "ToBeUpdate");
          }
          catch (AppException ex)
          {
            AppMessage.ShowErrorMessage(ex.Message);
          }
          finally
          {
            this.ucSearchResult1.CheckAll();
            this.myInSearch = false;
            Cursor.Current = current;
          }
          FormStockWorkOrderPrintListing.FormInquiryEventArgs inquiryEventArgs1 = new FormStockWorkOrderPrintListing.FormInquiryEventArgs(this, this.myDataTable);
          ScriptObject scriptObject = this.myScriptObject;
          string name = "OnFormInquiry";
          System.Type[] types = new System.Type[1];
          int index1 = 0;
          System.Type type = inquiryEventArgs1.GetType();
          types[index1] = type;
          object[] objArray = new object[1];
          int index2 = 0;
          FormStockWorkOrderPrintListing.FormInquiryEventArgs inquiryEventArgs2 = inquiryEventArgs1;
          objArray[index2] = (object) inquiryEventArgs2;
          scriptObject.RunMethod(name, types, objArray);
        }
      }
    }

    private string GenerateDocNosToString()
    {
      BCE.XtraUtils.GridViewUtils.UpdateData(this.gridView1);
      DataRow[] dataRowArray = this.myDataTable.Select("ToBeUpdate = True");
      if (dataRowArray.Length == 0)
      {
        AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_NoStockAssemblyOrderSelected, new object[0]));
        this.DialogResult = DialogResult.None;
        return (string) null;
      }
      else
      {
        string str = "";
        foreach (DataRow dataRow in dataRowArray)
        {
          if (str.Length != 0)
            str = str + ", ";
          str = str + "'" + dataRow["DocNo"].ToString() + "'";
        }
        return str;
      }
    }

    private void simpleButtonAdvanceSearch_Click(object sender, EventArgs e)
    {
      this.AdvanceSearch();
    }

    private void FormStockAssemblyList_Closing(object sender, CancelEventArgs e)
    {
      this.SaveCriteria();
    }

    private void ReloadAllColumns(object sender, EventArgs e)
    {
      this.BasicSearch(true);
    }

    private void sbtnInquiry_Click(object sender, EventArgs e)
    {
      this.BasicSearch(false);
      if (this.cbEditSortBy.Text != "")
      {
        if (this.cbEditSortBy.Text == "Date")
        {
          this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
          this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
        }
        else
        {
          this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
          this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
        }
      }
      this.ucSearchResult1.CheckAll();
      this.memoEdit_Criteria.Lines = this.myReportingCriteria.ReadableTextArray;
      this.sbtnToggleOptions.Enabled = this.gridControl1.DataSource != null;
      this.gridControl1.Focus();
      if (this.cbEditReportType.SelectedIndex == 0)
      {
        DBSetting dbSetting = this.myDBSetting;
        string docType = "";
        long docKey = 0L;
        long eventKey = 0L;
        // ISSUE: variable of a boxed type
        StockWorkOrderString local =  StockWorkOrderString.InquiredBatchPrintStockWorkOrder;
        object[] objArray = new object[1];
        int index = 0;
        string loginUserId = this.myCommand.UserAuthentication.LoginUserID;
        objArray[index] = (object) loginUserId;
        string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
        string text = this.memoEdit_Criteria.Text;
        Activity.Log(dbSetting, docType, docKey, eventKey, @string, text);
      }
      else
      {
        DBSetting dbSetting = this.myDBSetting;
        string docType = "";
        long docKey = 0L;
        long eventKey = 0L;
        // ISSUE: variable of a boxed type
        StockWorkOrderString local =  StockWorkOrderString.InquiredPrintStockWorkOrderListing;
        object[] objArray = new object[1];
        int index = 0;
        string loginUserId = this.myCommand.UserAuthentication.LoginUserID;
        objArray[index] = (object) loginUserId;
        string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
        string text = this.memoEdit_Criteria.Text;
        Activity.Log(dbSetting, docType, docKey, eventKey, @string, text);
      }
    }

    private void cbEditGroupBy_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.GroupBy = this.cbEditGroupBy.Text;
    }

    private void cbEditSortBy_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
      {
        if (this.myReportingCriteria.SortBy != this.cbEditSortBy.Text)
          this.myReportingCriteria.SortBy = this.cbEditSortBy.Text;
        if (this.cbEditSortBy.Text != "")
        {
          if (this.cbEditSortBy.Text == "Date")
          {
            this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
            this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
          }
          else
          {
            this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
            this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          }
        }
      }
    }

    private void cbEditReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
      this.myReportingCriteria.ReportTypeOption = this.cbEditReportType.SelectedIndex;
      FormStockWorkOrderPrintListing orderPrintListing = this;
      // ISSUE: variable of a boxed type
      StockAssemblyOrderStringId local =  StockAssemblyOrderStringId.Code_PrintStockAssemblyOrder;
      object[] objArray = new object[1];
      int index = 0;
      string text = this.cbEditReportType.Text;
      objArray[index] = (object) text;
      string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
      orderPrintListing.Text = @string;
      if (this.cbEditReportType.SelectedIndex == 0)
      {
        this.previewButton1.ReportType = "Stock Assembly Order Document";
        this.previewButton1.SetDBSetting(this.myDBSetting);
        this.printButton1.ReportType = "Stock Assembly Order Document";
        this.printButton1.SetDBSetting(this.myDBSetting);
      }
      else
      {
        this.previewButton1.ReportType = "Stock Assembly Order Listing";
        this.previewButton1.SetDBSetting(this.myDBSetting);
        this.printButton1.ReportType = "Stock Assembly Order Listing";
        this.printButton1.SetDBSetting(this.myDBSetting);
      }
    }

    private void cbCancelledOption_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.IsPrintCancelled = (CancelledDocumentOption) this.cbCancelledOption.SelectedIndex;
    }

    private void chkEditShowCriteria_CheckedChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria.Checked;
    }

    private void gridView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      GridView gridView = (GridView) sender;
      int[] selectedRows = gridView.GetSelectedRows();
      if (selectedRows != null && selectedRows.Length != 1)
      {
        for (int index = 0; index < selectedRows.Length; ++index)
          gridView.GetDataRow(selectedRows[index])["ToBeUpdate"] = (object) true;
      }
    }

    private string GetSelectedDocKeys()
    {
      BCE.XtraUtils.GridViewUtils.UpdateData(this.gridView1);
      if (this.gridView1.FocusedRowHandle >= 0 && this.myDataTable.Select("ToBeUpdate = True").Length != 0)
      {
        this.mySelectedDocKeylist = DataTableHelper.GetSelectedKeyArray("ToBeUpdate", "DocKey", this.myDataTable);
        return this.SelectedDocKeysInString;
      }
      else
        return "";
    }

    private void sbtnToggleOptions_Click(object sender, EventArgs e)
    {
      this.panelCriteria.Visible = !this.panelCriteria.Visible;
      if (this.panelCriteria.Visible)
        this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_HideOptions, new object[0]);
      else
        this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_ShowOptions, new object[0]);
    }

    private void sbtnClose_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void FormStockAssemblyPrintSearch_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == (Keys) 120)
      {
        if (this.sbtnInquiry.Enabled)
          this.sbtnInquiry.PerformClick();
      }
      else if (e.KeyCode == (Keys) 119)
      {
        if (this.previewButton1.Enabled)
          this.previewButton1.PerformClick();
      }
      else if (e.KeyCode == (Keys) 118)
      {
        if (this.printButton1.Enabled)
          this.printButton1.PerformClick();
      }
      else if (e.KeyCode == (Keys) 117 && this.sbtnToggleOptions.Enabled)
        this.sbtnToggleOptions.PerformClick();
    }

    private void gridView1_Layout(object sender, EventArgs e)
    {
      if (this.gridControl1.DataSource != null)
        this.InitGroupSummary();
    }

    private void repositoryItemTextEdit_Cancelled_FormatEditValue(object sender, ConvertEditValueEventArgs e)
    {
      if (e.Value != null)
      {
        if (e.Value.ToString() == "T")
          e.Value = (object) BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Cancelled, new object[0]);
        else
          e.Value = (object) "";
      }
    }

    private void previewButton1_Preview(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
    {
      string selectedDocKeys = this.GetSelectedDocKeys();
      if (selectedDocKeys.Length == 0)
      {
        AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_NoStockAssemblyOrderSelected, new object[0]));
        this.DialogResult = DialogResult.None;
      }
      else if (this.cbEditReportType.SelectedIndex == 0)
      {
        if (this.myCommand.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_DOC_REPORT_PREVIEW", (XtraForm) this))
        {
          if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
          {
            foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
            {
              if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "ASMORDER", BCE.Data.Convert.ToInt64(dataRow["DocKey"])) > 0)
              {
                AccessRight accessRight = this.myCommand.UserAuthentication.AccessRight;
                string cmdID = "MF_ASMORDER_PRINTED_PREVIEW";
                FormStockWorkOrderPrintListing orderPrintListing = this;
                // ISSUE: variable of a boxed type
                StockStringId local =  StockStringId.NoPermissionToPreviewPrintedDocument;
                object[] objArray = new object[1];
                int index = 0;
                string str = dataRow["DocNo"].ToString();
                objArray[index] = (object) str;
                string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
                if (!accessRight.IsAccessible(cmdID, (XtraForm) orderPrintListing, @string))
                  return;
              }
            }
          }
          ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString((Enum) StockWorkOrderString.BatchStockWorkOrder, new object[0]), "MF_ASMORDER_DOC_REPORT_PRINT", "MF_ASMORDER_DOC_REPORT_EXPORT", this.memoEdit_Criteria.Text);
          reportInfo.UpdatePrintCountTableName = "ASMORDER";
          reportInfo.CheckBeforePrintEvent += new CheckBeforeEventHandler(this.CheckBeforePrint);
          reportInfo.CheckBeforeExportEvent += new CheckBeforeEventHandler(this.CheckBeforeExport);
          ReportTool.PreviewReport("Stock Assembly Order Document", this.myCommand.GetReportDataSource(selectedDocKeys), this.myDBSetting, e.DefaultReport, false, this.myCommand.ReportOption, reportInfo);
        }
      }
      else if (this.cbEditReportType.SelectedIndex == 1 && this.myCommand.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_LISTING_REPORT_PREVIEW", (XtraForm) this))
        ReportTool.PreviewReport("Stock Assembly Order Listing", this.myCommand.GetDocumentListingReportDataSource(selectedDocKeys, this.StockWorkOrderReportingCriteria), this.myDBSetting, e.DefaultReport, false, this.myCommand.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString((Enum) StockWorkOrderString.StockWorkOrderListing, new object[0]), "MF_ASMORDER_LISTING_REPORT_PRINT", "MF_ASMORDER_LISTING_REPORT_EXPORT", this.memoEdit_Criteria.Text)
        {
          UpdatePrintCountTableName = "ASMORDER"
        });
    }

    private bool CheckBeforePrint(ReportInfo reportInfo)
    {
      if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
      {
        foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
        {
          if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "ASMORDER", BCE.Data.Convert.ToInt64(dataRow["DocKey"])) > 0)
          {
            AccessRight accessRight = this.myCommand.UserAuthentication.AccessRight;
            string cmdID = "MF_ASMORDER_PRINTED_PRINT";
            FormStockWorkOrderPrintListing orderPrintListing = this;
            // ISSUE: variable of a boxed type
            StockStringId local =  StockStringId.NoPermissionToPrintPrintedDocument;
            object[] objArray = new object[1];
            int index = 0;
            string str = dataRow["DocNo"].ToString();
            objArray[index] = (object) str;
            string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
            if (!accessRight.IsAccessible(cmdID, (XtraForm) orderPrintListing, @string))
              return false;
          }
        }
      }
      return true;
    }

    private bool CheckBeforeExport(ReportInfo reportInfo)
    {
      if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
      {
        foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
        {
          if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "ASMORDER", BCE.Data.Convert.ToInt64(dataRow["DocKey"])) > 0)
          {
            AccessRight accessRight = this.myCommand.UserAuthentication.AccessRight;
            string cmdID = "MF_ASMORDER_PRINTED_EXPORT";
            FormStockWorkOrderPrintListing orderPrintListing = this;
            // ISSUE: variable of a boxed type
            StockStringId local =  StockStringId.NoPermissionToExportPrintedDocument;
            object[] objArray = new object[1];
            int index = 0;
            string str = dataRow["DocNo"].ToString();
            objArray[index] = (object) str;
            string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
            if (!accessRight.IsAccessible(cmdID, (XtraForm) orderPrintListing, @string))
              return false;
          }
        }
      }
      return true;
    }

    private void printButton1_Print(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
    {
      string selectedDocKeys = this.GetSelectedDocKeys();
      if (selectedDocKeys.Length == 0)
      {
        AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_NoStockAssemblyOrderSelected, new object[0]));
        this.DialogResult = DialogResult.None;
      }
      else if (this.cbEditReportType.SelectedIndex == 0)
      {
        if (this.myCommand.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_DOC_REPORT_PRINT", (XtraForm) this))
        {
          if (SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
          {
            foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
            {
              if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "ASMORDER", BCE.Data.Convert.ToInt64(dataRow["DocKey"])) > 0)
              {
                AccessRight accessRight = this.myCommand.UserAuthentication.AccessRight;
                string cmdID = "MF_ASMORDER_PRINTED_PRINT";
                FormStockWorkOrderPrintListing orderPrintListing = this;
                // ISSUE: variable of a boxed type
                StockStringId local =  StockStringId.NoPermissionToPrintPrintedDocument;
                object[] objArray = new object[1];
                int index = 0;
                string str = dataRow["DocNo"].ToString();
                objArray[index] = (object) str;
                string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
                if (!accessRight.IsAccessible(cmdID, (XtraForm) orderPrintListing, @string))
                  return;
              }
            }
          }
          ReportTool.PrintReport("Stock Assembly Order Document", this.myCommand.GetReportDataSource(selectedDocKeys), this.myDBSetting, e.DefaultReport, this.myCommand.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString((Enum) StockWorkOrderString.BatchStockWorkOrder, new object[0]), "MF_ASMORDER_DOC_REPORT_PRINT", "MF_ASMORDER_DOC_REPORT_EXPORT", this.memoEdit_Criteria.Text)
          {
            UpdatePrintCountTableName = "ASMORDER"
          });
        }
      }
      else if (this.cbEditReportType.SelectedIndex == 1 && this.myCommand.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_LISTING_REPORT_PRINT", (XtraForm) this))
        ReportTool.PrintReport("Stock Assembly Order Listing", this.myCommand.GetDocumentListingReportDataSource(selectedDocKeys, this.StockWorkOrderReportingCriteria), this.myDBSetting, e.DefaultReport, this.myCommand.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString((Enum) StockWorkOrderString.StockWorkOrderListing, new object[0]), "MF_ASMORDER_LISTING_REPORT_PRINT", "MF_ASMORDER_LISTING_REPORT_EXPORT", this.memoEdit_Criteria.Text)
        {
          UpdatePrintCountTableName = "ASMORDER"
        });
    }

    private void barBtnDesignDocumentStyleReport_ItemClick(object sender, ItemClickEventArgs e)
    {
      ReportTool.DesignReport("Stock Assembly Order Document", this.myDBSetting);
    }

    private void barBtnDesignListingStyleReport_ItemClick(object sender, ItemClickEventArgs e)
    {
      ReportTool.DesignReport("Stock Assembly Order Listing", this.myDBSetting);
    }

    public virtual void RefreshDesignReport()
    {
      bool show = this.myCommand.UserAuthentication.AccessRight.IsAccessible("TOOLS_RPT_SHOW");
      this.barBtnDesignDocumentStyleReport.Visibility = XtraBarsUtils.ToBarItemVisibility(show);
      this.barBtnDesignListingStyleReport.Visibility = XtraBarsUtils.ToBarItemVisibility(show);
    }

    private void FormSAOPrintSearch_Load(object sender, EventArgs e)
    {
      this.FormInitialize();
    }

    private void FormInitialize()
    {
      FormStockWorkOrderPrintListing.FormInitializeEventArgs initializeEventArgs1 = new FormStockWorkOrderPrintListing.FormInitializeEventArgs(this);
      ScriptObject scriptObject = this.myScriptObject;
      string name = "OnFormInitialize";
      System.Type[] types = new System.Type[1];
      int index1 = 0;
      System.Type type = initializeEventArgs1.GetType();
      types[index1] = type;
      object[] objArray = new object[1];
      int index2 = 0;
      FormStockWorkOrderPrintListing.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
      objArray[index2] = (object) initializeEventArgs2;
      scriptObject.RunMethod(name, types, objArray);
    }

    private void gridView1_DoubleClick(object sender, EventArgs e)
    {
      if (this.myMouseDownHelper.IsLeftMouseDown && BCE.XtraUtils.GridViewUtils.GetGridHitInfo((GridView) sender).InRow && UserAuthentication.GetOrCreate(this.myDBSetting).AccessRight.IsAccessible("SYS_BHV_DRILLDOWN"))
        this.GoToDocument();
    }

    private void GoToDocument()
    {
      ColumnView columnView = (ColumnView) this.gridControl1.FocusedView;
      int focusedRowHandle = columnView.FocusedRowHandle;
      DataRow dataRow = columnView.GetDataRow(focusedRowHandle);
      if (dataRow != null)
        DocumentDispatcher.Open(this.myDBSetting, "AO", BCE.Data.Convert.ToInt64(dataRow["DocKey"]));
    }

    protected override void Dispose(bool disposing)
    {
      this.SaveCriteria();
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormStockWorkOrderPrintListing));
      this.panelCriteria = new PanelControl();
      this.cbEditReportType = new ComboBoxEdit();
      this.label9 = new Label();
      this.groupBox_SearchCriteria = new GroupControl();
      this.ucStockWorkOrderSelector1 = new UCStockAssemblyOrderSelector();
      this.cbCancelledOption = new ComboBoxEdit();
      this.label4 = new Label();
      this.label6 = new Label();
      this.label3 = new Label();
      this.ucDateSelector1 = new UCDateSelector();
      this.simpleButtonAdvanceSearch = new SimpleButton();
      this.gbReportOption = new GroupControl();
      this.chkEditShowCriteria = new CheckEdit();
      this.cbEditGroupBy = new ComboBoxEdit();
      this.label8 = new Label();
      this.label7 = new Label();
      this.cbEditSortBy = new ComboBoxEdit();
      this.tabControl1 = new XtraTabControl();
      this.tabPage1 = new XtraTabPage();
      this.gridControl1 = new GridControl();
      this.gridView1 = new GridView();
      this.colCheck = new GridColumn();
      this.repositoryItemCheckEdit1 = new RepositoryItemCheckEdit();
      this.colDocNo = new GridColumn();
      this.colDocDate = new GridColumn();
      this.colTotal = new GridColumn();
      this.colAssemblyCost = new GridColumn();
      this.colNetTotal = new GridColumn();
      this.colRemark1 = new GridColumn();
      this.colRemark2 = new GridColumn();
      this.colRemark3 = new GridColumn();
      this.colRemark4 = new GridColumn();
      this.colPrintCount = new GridColumn();
      this.colCancelled = new GridColumn();
      this.repositoryItemTextEdit_Cancelled = new RepositoryItemTextEdit();
      this.colLastModified = new GridColumn();
      this.colLastModifiedUserID = new GridColumn();
      this.colCreatedTimeStamp = new GridColumn();
      this.colCreatedUserID = new GridColumn();
      this.colRefDocNo = new GridColumn();
      this.colDescription = new GridColumn();
      this.colItemCode = new GridColumn();
      this.colLocation = new GridColumn();
      this.colBatchNo = new GridColumn();
      this.colProjNo = new GridColumn();
      this.colDeptNo = new GridColumn();
      this.colQty = new GridColumn();
      this.colExpCompletedDate = new GridColumn();
      this.ucSearchResult1 = new UCSearchResult();
      this.tabPage2 = new XtraTabPage();
      this.memoEdit_Criteria = new MemoEdit();
      this.panel_Center = new PanelControl();
      this.panelControl1 = new PanelControl();
      this.printButton1 = new PrintButton();
      this.previewButton1 = new PreviewButton();
      this.sbtnToggleOptions = new SimpleButton();
      this.sbtnClose = new SimpleButton();
      this.sbtnInquiry = new SimpleButton();
      this.barManager1 = new BarManager(this.components);
      this.bar2 = new Bar();
      this.barSubItem1 = new BarSubItem();
      this.barBtnDesignDocumentStyleReport = new BarButtonItem();
      this.barBtnDesignListingStyleReport = new BarButtonItem();
      this.barDockControlTop = new BarDockControl();
      this.barDockControlBottom = new BarDockControl();
      this.barDockControlLeft = new BarDockControl();
      this.barDockControlRight = new BarDockControl();
      this.panelHeader1 = new PanelHeader();
      this.panelCriteria.BeginInit();
      this.panelCriteria.SuspendLayout();
      this.cbEditReportType.Properties.BeginInit();
      this.groupBox_SearchCriteria.BeginInit();
      this.groupBox_SearchCriteria.SuspendLayout();
      this.cbCancelledOption.Properties.BeginInit();
      this.gbReportOption.BeginInit();
      this.gbReportOption.SuspendLayout();
      this.chkEditShowCriteria.Properties.BeginInit();
      this.cbEditGroupBy.Properties.BeginInit();
      this.cbEditSortBy.Properties.BeginInit();
      this.tabControl1.BeginInit();
      this.tabControl1.SuspendLayout();
      this.tabPage1.SuspendLayout();
      this.gridControl1.BeginInit();
      this.gridView1.BeginInit();
      this.repositoryItemCheckEdit1.BeginInit();
      this.repositoryItemTextEdit_Cancelled.BeginInit();
      this.tabPage2.SuspendLayout();
      this.memoEdit_Criteria.Properties.BeginInit();
      this.panel_Center.BeginInit();
      this.panel_Center.SuspendLayout();
      this.panelControl1.BeginInit();
      this.panelControl1.SuspendLayout();
      this.barManager1.BeginInit();
      this.SuspendLayout();
      componentResourceManager.ApplyResources((object) this.panelCriteria, "panelCriteria");
      this.panelCriteria.BorderStyle = BorderStyles.NoBorder;
      this.panelCriteria.Controls.Add((Control) this.cbEditReportType);
      this.panelCriteria.Controls.Add((Control) this.label9);
      this.panelCriteria.Controls.Add((Control) this.groupBox_SearchCriteria);
      this.panelCriteria.Controls.Add((Control) this.gbReportOption);
      this.panelCriteria.Name = "panelCriteria";
      componentResourceManager.ApplyResources((object) this.cbEditReportType, "cbEditReportType");
      this.cbEditReportType.Name = "cbEditReportType";
      this.cbEditReportType.Properties.AccessibleDescription = componentResourceManager.GetString("cbEditReportType.Properties.AccessibleDescription");
      this.cbEditReportType.Properties.AccessibleName = componentResourceManager.GetString("cbEditReportType.Properties.AccessibleName");
      this.cbEditReportType.Properties.AutoHeight = (bool) componentResourceManager.GetObject("cbEditReportType.Properties.AutoHeight");
      EditorButtonCollection buttons1 = this.cbEditReportType.Properties.Buttons;
      EditorButton[] buttons2 = new EditorButton[1];
      int index1 = 0;
      EditorButton editorButton1 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("cbEditReportType.Properties.Buttons"));
      buttons2[index1] = editorButton1;
      buttons1.AddRange(buttons2);
      this.cbEditReportType.Properties.NullValuePrompt = componentResourceManager.GetString("cbEditReportType.Properties.NullValuePrompt");
      this.cbEditReportType.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("cbEditReportType.Properties.NullValuePromptShowForEmptyValue");
      this.cbEditReportType.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
      this.cbEditReportType.SelectedIndexChanged += new EventHandler(this.cbEditReportType_SelectedIndexChanged);
      componentResourceManager.ApplyResources((object) this.label9, "label9");
      this.label9.Name = "label9";
      componentResourceManager.ApplyResources((object) this.groupBox_SearchCriteria, "groupBox_SearchCriteria");
      this.groupBox_SearchCriteria.Controls.Add((Control) this.ucStockWorkOrderSelector1);
      this.groupBox_SearchCriteria.Controls.Add((Control) this.cbCancelledOption);
      this.groupBox_SearchCriteria.Controls.Add((Control) this.label4);
      this.groupBox_SearchCriteria.Controls.Add((Control) this.label6);
      this.groupBox_SearchCriteria.Controls.Add((Control) this.label3);
      this.groupBox_SearchCriteria.Controls.Add((Control) this.ucDateSelector1);
      this.groupBox_SearchCriteria.Controls.Add((Control) this.simpleButtonAdvanceSearch);
      this.groupBox_SearchCriteria.Name = "groupBox_SearchCriteria";
      componentResourceManager.ApplyResources((object) this.ucStockWorkOrderSelector1, "ucStockWorkOrderSelector1");
      this.ucStockWorkOrderSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucStockWorkOrderSelector1.Appearance.BackColor");
      this.ucStockWorkOrderSelector1.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucStockWorkOrderSelector1.Appearance.GradientMode");
      this.ucStockWorkOrderSelector1.Appearance.Image = (Image) componentResourceManager.GetObject("ucStockWorkOrderSelector1.Appearance.Image");
      this.ucStockWorkOrderSelector1.Appearance.Options.UseBackColor = true;
      this.ucStockWorkOrderSelector1.Name = "ucStockWorkOrderSelector1";
      componentResourceManager.ApplyResources((object) this.cbCancelledOption, "cbCancelledOption");
      this.cbCancelledOption.Name = "cbCancelledOption";
      this.cbCancelledOption.Properties.AccessibleDescription = componentResourceManager.GetString("cbCancelledOption.Properties.AccessibleDescription");
      this.cbCancelledOption.Properties.AccessibleName = componentResourceManager.GetString("cbCancelledOption.Properties.AccessibleName");
      this.cbCancelledOption.Properties.AutoHeight = (bool) componentResourceManager.GetObject("cbCancelledOption.Properties.AutoHeight");
      EditorButtonCollection buttons3 = this.cbCancelledOption.Properties.Buttons;
      EditorButton[] buttons4 = new EditorButton[1];
      int index2 = 0;
      EditorButton editorButton2 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("cbCancelledOption.Properties.Buttons"));
      buttons4[index2] = editorButton2;
      buttons3.AddRange(buttons4);
      ComboBoxItemCollection items1 = this.cbCancelledOption.Properties.Items;
      object[] items2 = new object[3];
      int index3 = 0;
      string string1 = componentResourceManager.GetString("cbCancelledOption.Properties.Items");
      items2[index3] = (object) string1;
      int index4 = 1;
      string string2 = componentResourceManager.GetString("cbCancelledOption.Properties.Items1");
      items2[index4] = (object) string2;
      int index5 = 2;
      string string3 = componentResourceManager.GetString("cbCancelledOption.Properties.Items2");
      items2[index5] = (object) string3;
      items1.AddRange(items2);
      this.cbCancelledOption.Properties.NullValuePrompt = componentResourceManager.GetString("cbCancelledOption.Properties.NullValuePrompt");
      this.cbCancelledOption.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("cbCancelledOption.Properties.NullValuePromptShowForEmptyValue");
      this.cbCancelledOption.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
      this.cbCancelledOption.SelectedIndexChanged += new EventHandler(this.cbCancelledOption_SelectedIndexChanged);
      componentResourceManager.ApplyResources((object) this.label4, "label4");
      this.label4.Name = "label4";
      componentResourceManager.ApplyResources((object) this.label6, "label6");
      this.label6.Name = "label6";
      componentResourceManager.ApplyResources((object) this.label3, "label3");
      this.label3.Name = "label3";
      componentResourceManager.ApplyResources((object) this.ucDateSelector1, "ucDateSelector1");
      this.ucDateSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucDateSelector1.Appearance.BackColor");
      this.ucDateSelector1.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucDateSelector1.Appearance.GradientMode");
      this.ucDateSelector1.Appearance.Image = (Image) componentResourceManager.GetObject("ucDateSelector1.Appearance.Image");
      this.ucDateSelector1.Appearance.Options.UseBackColor = true;
      this.ucDateSelector1.Name = "ucDateSelector1";
      componentResourceManager.ApplyResources((object) this.simpleButtonAdvanceSearch, "simpleButtonAdvanceSearch");
      this.simpleButtonAdvanceSearch.Name = "simpleButtonAdvanceSearch";
      this.simpleButtonAdvanceSearch.Click += new EventHandler(this.simpleButtonAdvanceSearch_Click);
      componentResourceManager.ApplyResources((object) this.gbReportOption, "gbReportOption");
      this.gbReportOption.Controls.Add((Control) this.chkEditShowCriteria);
      this.gbReportOption.Controls.Add((Control) this.cbEditGroupBy);
      this.gbReportOption.Controls.Add((Control) this.label8);
      this.gbReportOption.Controls.Add((Control) this.label7);
      this.gbReportOption.Controls.Add((Control) this.cbEditSortBy);
      this.gbReportOption.Name = "gbReportOption";
      componentResourceManager.ApplyResources((object) this.chkEditShowCriteria, "chkEditShowCriteria");
      this.chkEditShowCriteria.Name = "chkEditShowCriteria";
      this.chkEditShowCriteria.Properties.AccessibleDescription = componentResourceManager.GetString("chkEditShowCriteria.Properties.AccessibleDescription");
      this.chkEditShowCriteria.Properties.AccessibleName = componentResourceManager.GetString("chkEditShowCriteria.Properties.AccessibleName");
      this.chkEditShowCriteria.Properties.AutoHeight = (bool) componentResourceManager.GetObject("chkEditShowCriteria.Properties.AutoHeight");
      this.chkEditShowCriteria.Properties.Caption = componentResourceManager.GetString("chkEditShowCriteria.Properties.Caption");
      this.chkEditShowCriteria.Properties.DisplayValueChecked = componentResourceManager.GetString("chkEditShowCriteria.Properties.DisplayValueChecked");
      this.chkEditShowCriteria.Properties.DisplayValueGrayed = componentResourceManager.GetString("chkEditShowCriteria.Properties.DisplayValueGrayed");
      this.chkEditShowCriteria.Properties.DisplayValueUnchecked = componentResourceManager.GetString("chkEditShowCriteria.Properties.DisplayValueUnchecked");
      this.chkEditShowCriteria.CheckedChanged += new EventHandler(this.chkEditShowCriteria_CheckedChanged);
      componentResourceManager.ApplyResources((object) this.cbEditGroupBy, "cbEditGroupBy");
      this.cbEditGroupBy.Name = "cbEditGroupBy";
      this.cbEditGroupBy.Properties.AccessibleDescription = componentResourceManager.GetString("cbEditGroupBy.Properties.AccessibleDescription");
      this.cbEditGroupBy.Properties.AccessibleName = componentResourceManager.GetString("cbEditGroupBy.Properties.AccessibleName");
      this.cbEditGroupBy.Properties.AutoHeight = (bool) componentResourceManager.GetObject("cbEditGroupBy.Properties.AutoHeight");
      EditorButtonCollection buttons5 = this.cbEditGroupBy.Properties.Buttons;
      EditorButton[] buttons6 = new EditorButton[1];
      int index6 = 0;
      EditorButton editorButton3 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("cbEditGroupBy.Properties.Buttons"));
      buttons6[index6] = editorButton3;
      buttons5.AddRange(buttons6);
      this.cbEditGroupBy.Properties.NullValuePrompt = componentResourceManager.GetString("cbEditGroupBy.Properties.NullValuePrompt");
      this.cbEditGroupBy.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("cbEditGroupBy.Properties.NullValuePromptShowForEmptyValue");
      this.cbEditGroupBy.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
      this.cbEditGroupBy.SelectedIndexChanged += new EventHandler(this.cbEditGroupBy_SelectedIndexChanged);
      componentResourceManager.ApplyResources((object) this.label8, "label8");
      this.label8.Name = "label8";
      componentResourceManager.ApplyResources((object) this.label7, "label7");
      this.label7.Name = "label7";
      componentResourceManager.ApplyResources((object) this.cbEditSortBy, "cbEditSortBy");
      this.cbEditSortBy.Name = "cbEditSortBy";
      this.cbEditSortBy.Properties.AccessibleDescription = componentResourceManager.GetString("cbEditSortBy.Properties.AccessibleDescription");
      this.cbEditSortBy.Properties.AccessibleName = componentResourceManager.GetString("cbEditSortBy.Properties.AccessibleName");
      this.cbEditSortBy.Properties.AutoHeight = (bool) componentResourceManager.GetObject("cbEditSortBy.Properties.AutoHeight");
      EditorButtonCollection buttons7 = this.cbEditSortBy.Properties.Buttons;
      EditorButton[] buttons8 = new EditorButton[1];
      int index7 = 0;
      EditorButton editorButton4 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("cbEditSortBy.Properties.Buttons"));
      buttons8[index7] = editorButton4;
      buttons7.AddRange(buttons8);
      this.cbEditSortBy.Properties.NullValuePrompt = componentResourceManager.GetString("cbEditSortBy.Properties.NullValuePrompt");
      this.cbEditSortBy.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("cbEditSortBy.Properties.NullValuePromptShowForEmptyValue");
      this.cbEditSortBy.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
      this.cbEditSortBy.SelectedIndexChanged += new EventHandler(this.cbEditSortBy_SelectedIndexChanged);
      componentResourceManager.ApplyResources((object) this.tabControl1, "tabControl1");
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedTabPage = this.tabPage1;
      XtraTabPageCollection tabPages = this.tabControl1.TabPages;
      XtraTabPage[] pages = new XtraTabPage[2];
      int index8 = 0;
      XtraTabPage xtraTabPage1 = this.tabPage1;
      pages[index8] = xtraTabPage1;
      int index9 = 1;
      XtraTabPage xtraTabPage2 = this.tabPage2;
      pages[index9] = xtraTabPage2;
      tabPages.AddRange(pages);
      componentResourceManager.ApplyResources((object) this.tabPage1, "tabPage1");
      this.tabPage1.Controls.Add((Control) this.gridControl1);
      this.tabPage1.Controls.Add((Control) this.ucSearchResult1);
      this.tabPage1.Name = "tabPage1";
      componentResourceManager.ApplyResources((object) this.gridControl1, "gridControl1");
      this.gridControl1.EmbeddedNavigator.AccessibleDescription = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.AccessibleDescription");
      this.gridControl1.EmbeddedNavigator.AccessibleName = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.AccessibleName");
      this.gridControl1.EmbeddedNavigator.AllowHtmlTextInToolTip = (DefaultBoolean) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.AllowHtmlTextInToolTip");
      this.gridControl1.EmbeddedNavigator.Anchor = (AnchorStyles) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.Anchor");
      this.gridControl1.EmbeddedNavigator.BackgroundImage = (Image) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.BackgroundImage");
      this.gridControl1.EmbeddedNavigator.BackgroundImageLayout = (ImageLayout) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.BackgroundImageLayout");
      this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
      this.gridControl1.EmbeddedNavigator.Buttons.Append.Hint = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.Buttons.Append.Hint");
      this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
      this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
      this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Hint = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Hint");
      this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
      this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
      this.gridControl1.EmbeddedNavigator.Buttons.Edit.Hint = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.Buttons.Edit.Hint");
      this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
      this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
      this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Hint = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.Buttons.EndEdit.Hint");
      this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
      this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
      this.gridControl1.EmbeddedNavigator.Buttons.Remove.Hint = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.Buttons.Remove.Hint");
      this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
      this.gridControl1.EmbeddedNavigator.ImeMode = (ImeMode) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.ImeMode");
      this.gridControl1.EmbeddedNavigator.MaximumSize = (Size) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.MaximumSize");
      this.gridControl1.EmbeddedNavigator.TextLocation = (NavigatorButtonsTextLocation) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.TextLocation");
      this.gridControl1.EmbeddedNavigator.ToolTip = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.ToolTip");
      this.gridControl1.EmbeddedNavigator.ToolTipIconType = (ToolTipIconType) componentResourceManager.GetObject("gridControl1.EmbeddedNavigator.ToolTipIconType");
      this.gridControl1.EmbeddedNavigator.ToolTipTitle = componentResourceManager.GetString("gridControl1.EmbeddedNavigator.ToolTipTitle");
      this.gridControl1.MainView = (BaseView) this.gridView1;
      this.gridControl1.Name = "gridControl1";
      RepositoryItemCollection repositoryItems = this.gridControl1.RepositoryItems;
      RepositoryItem[] items3 = new RepositoryItem[2];
      int index10 = 0;
      RepositoryItemCheckEdit repositoryItemCheckEdit = this.repositoryItemCheckEdit1;
      items3[index10] = (RepositoryItem) repositoryItemCheckEdit;
      int index11 = 1;
      RepositoryItemTextEdit repositoryItemTextEdit = this.repositoryItemTextEdit_Cancelled;
      items3[index11] = (RepositoryItem) repositoryItemTextEdit;
      repositoryItems.AddRange(items3);
      this.gridControl1.UseEmbeddedNavigator = true;
      ViewRepositoryCollection viewCollection = this.gridControl1.ViewCollection;
      BaseView[] views = new BaseView[1];
      int index12 = 0;
      GridView gridView = this.gridView1;
      views[index12] = (BaseView) gridView;
      viewCollection.AddRange(views);
      componentResourceManager.ApplyResources((object) this.gridView1, "gridView1");
      GridColumnCollection columns1 = this.gridView1.Columns;
      GridColumn[] columns2 = new GridColumn[25];
      int index13 = 0;
      GridColumn gridColumn1 = this.colCheck;
      columns2[index13] = gridColumn1;
      int index14 = 1;
      GridColumn gridColumn2 = this.colDocNo;
      columns2[index14] = gridColumn2;
      int index15 = 2;
      GridColumn gridColumn3 = this.colDocDate;
      columns2[index15] = gridColumn3;
      int index16 = 3;
      GridColumn gridColumn4 = this.colTotal;
      columns2[index16] = gridColumn4;
      int index17 = 4;
      GridColumn gridColumn5 = this.colAssemblyCost;
      columns2[index17] = gridColumn5;
      int index18 = 5;
      GridColumn gridColumn6 = this.colNetTotal;
      columns2[index18] = gridColumn6;
      int index19 = 6;
      GridColumn gridColumn7 = this.colRemark1;
      columns2[index19] = gridColumn7;
      int index20 = 7;
      GridColumn gridColumn8 = this.colRemark2;
      columns2[index20] = gridColumn8;
      int index21 = 8;
      GridColumn gridColumn9 = this.colRemark3;
      columns2[index21] = gridColumn9;
      int index22 = 9;
      GridColumn gridColumn10 = this.colRemark4;
      columns2[index22] = gridColumn10;
      int index23 = 10;
      GridColumn gridColumn11 = this.colPrintCount;
      columns2[index23] = gridColumn11;
      int index24 = 11;
      GridColumn gridColumn12 = this.colCancelled;
      columns2[index24] = gridColumn12;
      int index25 = 12;
      GridColumn gridColumn13 = this.colLastModified;
      columns2[index25] = gridColumn13;
      int index26 = 13;
      GridColumn gridColumn14 = this.colLastModifiedUserID;
      columns2[index26] = gridColumn14;
      int index27 = 14;
      GridColumn gridColumn15 = this.colCreatedTimeStamp;
      columns2[index27] = gridColumn15;
      int index28 = 15;
      GridColumn gridColumn16 = this.colCreatedUserID;
      columns2[index28] = gridColumn16;
      int index29 = 16;
      GridColumn gridColumn17 = this.colRefDocNo;
      columns2[index29] = gridColumn17;
      int index30 = 17;
      GridColumn gridColumn18 = this.colDescription;
      columns2[index30] = gridColumn18;
      int index31 = 18;
      GridColumn gridColumn19 = this.colItemCode;
      columns2[index31] = gridColumn19;
      int index32 = 19;
      GridColumn gridColumn20 = this.colLocation;
      columns2[index32] = gridColumn20;
      int index33 = 20;
      GridColumn gridColumn21 = this.colBatchNo;
      columns2[index33] = gridColumn21;
      int index34 = 21;
      GridColumn gridColumn22 = this.colProjNo;
      columns2[index34] = gridColumn22;
      int index35 = 22;
      GridColumn gridColumn23 = this.colDeptNo;
      columns2[index35] = gridColumn23;
      int index36 = 23;
      GridColumn gridColumn24 = this.colQty;
      columns2[index36] = gridColumn24;
      int index37 = 24;
      GridColumn gridColumn25 = this.colExpCompletedDate;
      columns2[index37] = gridColumn25;
      columns1.AddRange(columns2);
      this.gridView1.GridControl = this.gridControl1;
      this.gridView1.Name = "gridView1";
      this.gridView1.OptionsBehavior.AllowIncrementalSearch = true;
      this.gridView1.OptionsSelection.MultiSelect = true;
      this.gridView1.OptionsView.ShowFooter = true;
      this.gridView1.SelectionChanged += new SelectionChangedEventHandler(this.gridView1_SelectionChanged);
      this.gridView1.DoubleClick += new EventHandler(this.gridView1_DoubleClick);
      this.gridView1.Layout += new EventHandler(this.gridView1_Layout);
      componentResourceManager.ApplyResources((object) this.colCheck, "colCheck");
      this.colCheck.ColumnEdit = (RepositoryItem) this.repositoryItemCheckEdit1;
      this.colCheck.FieldName = "ToBeUpdate";
      this.colCheck.Name = "colCheck";
      this.colCheck.OptionsColumn.ShowInCustomizationForm = false;
      componentResourceManager.ApplyResources((object) this.repositoryItemCheckEdit1, "repositoryItemCheckEdit1");
      this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
      this.repositoryItemCheckEdit1.NullStyle = StyleIndeterminate.Unchecked;
      componentResourceManager.ApplyResources((object) this.colDocNo, "colDocNo");
      this.colDocNo.FieldName = "DocNo";
      this.colDocNo.Name = "colDocNo";
      this.colDocNo.OptionsColumn.AllowEdit = false;
      this.colDocNo.OptionsColumn.AllowGroup = DefaultBoolean.False;
      this.colDocNo.OptionsColumn.ShowInCustomizationForm = false;
      componentResourceManager.ApplyResources((object) this.colDocDate, "colDocDate");
      this.colDocDate.FieldName = "DocDate";
      this.colDocDate.Name = "colDocDate";
      this.colDocDate.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colTotal, "colTotal");
      this.colTotal.FieldName = "Total";
      this.colTotal.Name = "colTotal";
      this.colTotal.OptionsColumn.AllowEdit = false;
      GridColumnSummaryItemCollection summary = this.colTotal.Summary;
      GridSummaryItem[] items4 = new GridSummaryItem[1];
      int index38 = 0;
      GridColumnSummaryItem columnSummaryItem = new GridColumnSummaryItem((SummaryItemType) componentResourceManager.GetObject("colTotal.Summary"));
      items4[index38] = (GridSummaryItem) columnSummaryItem;
      summary.AddRange(items4);
      componentResourceManager.ApplyResources((object) this.colAssemblyCost, "colAssemblyCost");
      this.colAssemblyCost.FieldName = "AssemblyCost";
      this.colAssemblyCost.Name = "colAssemblyCost";
      this.colAssemblyCost.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colNetTotal, "colNetTotal");
      this.colNetTotal.FieldName = "NetTotal";
      this.colNetTotal.Name = "colNetTotal";
      this.colNetTotal.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark1, "colRemark1");
      this.colRemark1.FieldName = "Remark1";
      this.colRemark1.Name = "colRemark1";
      this.colRemark1.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark2, "colRemark2");
      this.colRemark2.FieldName = "Remark2";
      this.colRemark2.Name = "colRemark2";
      this.colRemark2.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark3, "colRemark3");
      this.colRemark3.FieldName = "Remark3";
      this.colRemark3.Name = "colRemark3";
      this.colRemark3.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark4, "colRemark4");
      this.colRemark4.FieldName = "Remark4";
      this.colRemark4.Name = "colRemark4";
      this.colRemark4.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colPrintCount, "colPrintCount");
      this.colPrintCount.FieldName = "PrintCount";
      this.colPrintCount.Name = "colPrintCount";
      this.colPrintCount.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colCancelled, "colCancelled");
      this.colCancelled.ColumnEdit = (RepositoryItem) this.repositoryItemTextEdit_Cancelled;
      this.colCancelled.FieldName = "Cancelled";
      this.colCancelled.Name = "colCancelled";
      this.colCancelled.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.repositoryItemTextEdit_Cancelled, "repositoryItemTextEdit_Cancelled");
      this.repositoryItemTextEdit_Cancelled.Mask.AutoComplete = (AutoCompleteType) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.AutoComplete");
      this.repositoryItemTextEdit_Cancelled.Mask.BeepOnError = (bool) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.BeepOnError");
      this.repositoryItemTextEdit_Cancelled.Mask.EditMask = componentResourceManager.GetString("repositoryItemTextEdit_Cancelled.Mask.EditMask");
      this.repositoryItemTextEdit_Cancelled.Mask.IgnoreMaskBlank = (bool) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.IgnoreMaskBlank");
      this.repositoryItemTextEdit_Cancelled.Mask.MaskType = (DevExpress.XtraEditors.Mask.MaskType) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.MaskType");
      this.repositoryItemTextEdit_Cancelled.Mask.PlaceHolder = (char) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.PlaceHolder");
      this.repositoryItemTextEdit_Cancelled.Mask.SaveLiteral = (bool) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.SaveLiteral");
      this.repositoryItemTextEdit_Cancelled.Mask.ShowPlaceHolders = (bool) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.ShowPlaceHolders");
      this.repositoryItemTextEdit_Cancelled.Mask.UseMaskAsDisplayFormat = (bool) componentResourceManager.GetObject("repositoryItemTextEdit_Cancelled.Mask.UseMaskAsDisplayFormat");
      this.repositoryItemTextEdit_Cancelled.Name = "repositoryItemTextEdit_Cancelled";
      this.repositoryItemTextEdit_Cancelled.FormatEditValue += new ConvertEditValueEventHandler(this.repositoryItemTextEdit_Cancelled_FormatEditValue);
      componentResourceManager.ApplyResources((object) this.colLastModified, "colLastModified");
      this.colLastModified.FieldName = "LastModified";
      this.colLastModified.Name = "colLastModified";
      this.colLastModified.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colLastModifiedUserID, "colLastModifiedUserID");
      this.colLastModifiedUserID.FieldName = "LastModifiedUserID";
      this.colLastModifiedUserID.Name = "colLastModifiedUserID";
      this.colLastModifiedUserID.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colCreatedTimeStamp, "colCreatedTimeStamp");
      this.colCreatedTimeStamp.FieldName = "CreatedTimeStamp";
      this.colCreatedTimeStamp.Name = "colCreatedTimeStamp";
      this.colCreatedTimeStamp.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colCreatedUserID, "colCreatedUserID");
      this.colCreatedUserID.FieldName = "CreatedUserID";
      this.colCreatedUserID.Name = "colCreatedUserID";
      this.colCreatedUserID.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRefDocNo, "colRefDocNo");
      this.colRefDocNo.FieldName = "RefDocNo";
      this.colRefDocNo.Name = "colRefDocNo";
      this.colRefDocNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDescription, "colDescription");
      this.colDescription.FieldName = "Description";
      this.colDescription.Name = "colDescription";
      this.colDescription.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colItemCode, "colItemCode");
      this.colItemCode.FieldName = "ItemCode";
      this.colItemCode.Name = "colItemCode";
      this.colItemCode.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colLocation, "colLocation");
      this.colLocation.FieldName = "Location";
      this.colLocation.Name = "colLocation";
      this.colLocation.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colBatchNo, "colBatchNo");
      this.colBatchNo.FieldName = "BatchNo";
      this.colBatchNo.Name = "colBatchNo";
      this.colBatchNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colProjNo, "colProjNo");
      this.colProjNo.FieldName = "ProjNo";
      this.colProjNo.Name = "colProjNo";
      this.colProjNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDeptNo, "colDeptNo");
      this.colDeptNo.FieldName = "DeptNo";
      this.colDeptNo.Name = "colDeptNo";
      this.colDeptNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colQty, "colQty");
      this.colQty.FieldName = "Qty";
      this.colQty.Name = "colQty";
      this.colQty.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colExpCompletedDate, "colExpCompletedDate");
      this.colExpCompletedDate.FieldName = "ExpectedCompletedDate";
      this.colExpCompletedDate.Name = "colExpCompletedDate";
      this.colExpCompletedDate.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.ucSearchResult1, "ucSearchResult1");
      this.ucSearchResult1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucSearchResult1.Appearance.BackColor");
      this.ucSearchResult1.Appearance.BackColor2 = (Color) componentResourceManager.GetObject("ucSearchResult1.Appearance.BackColor2");
      this.ucSearchResult1.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucSearchResult1.Appearance.GradientMode");
      this.ucSearchResult1.Appearance.Image = (Image) componentResourceManager.GetObject("ucSearchResult1.Appearance.Image");
      this.ucSearchResult1.Appearance.Options.UseBackColor = true;
      this.ucSearchResult1.LookAndFeel.UseDefaultLookAndFeel = false;
      this.ucSearchResult1.Name = "ucSearchResult1";
      componentResourceManager.ApplyResources((object) this.tabPage2, "tabPage2");
      this.tabPage2.Controls.Add((Control) this.memoEdit_Criteria);
      this.tabPage2.Name = "tabPage2";
      componentResourceManager.ApplyResources((object) this.memoEdit_Criteria, "memoEdit_Criteria");
      this.memoEdit_Criteria.Name = "memoEdit_Criteria";
      this.memoEdit_Criteria.Properties.AccessibleDescription = componentResourceManager.GetString("memoEdit_Criteria.Properties.AccessibleDescription");
      this.memoEdit_Criteria.Properties.AccessibleName = componentResourceManager.GetString("memoEdit_Criteria.Properties.AccessibleName");
      this.memoEdit_Criteria.Properties.Appearance.Font = (Font) componentResourceManager.GetObject("memoEdit_Criteria.Properties.Appearance.Font");
      this.memoEdit_Criteria.Properties.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("memoEdit_Criteria.Properties.Appearance.GradientMode");
      this.memoEdit_Criteria.Properties.Appearance.Image = (Image) componentResourceManager.GetObject("memoEdit_Criteria.Properties.Appearance.Image");
      this.memoEdit_Criteria.Properties.Appearance.Options.UseFont = true;
      this.memoEdit_Criteria.Properties.NullValuePrompt = componentResourceManager.GetString("memoEdit_Criteria.Properties.NullValuePrompt");
      this.memoEdit_Criteria.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("memoEdit_Criteria.Properties.NullValuePromptShowForEmptyValue");
      this.memoEdit_Criteria.Properties.ReadOnly = true;
      this.memoEdit_Criteria.UseOptimizedRendering = true;
      componentResourceManager.ApplyResources((object) this.panel_Center, "panel_Center");
      this.panel_Center.BorderStyle = BorderStyles.NoBorder;
      this.panel_Center.Controls.Add((Control) this.tabControl1);
      this.panel_Center.Name = "panel_Center";
      componentResourceManager.ApplyResources((object) this.panelControl1, "panelControl1");
      this.panelControl1.BorderStyle = BorderStyles.NoBorder;
      this.panelControl1.Controls.Add((Control) this.printButton1);
      this.panelControl1.Controls.Add((Control) this.previewButton1);
      this.panelControl1.Controls.Add((Control) this.sbtnToggleOptions);
      this.panelControl1.Controls.Add((Control) this.sbtnClose);
      this.panelControl1.Controls.Add((Control) this.sbtnInquiry);
      this.panelControl1.Name = "panelControl1";
      componentResourceManager.ApplyResources((object) this.printButton1, "printButton1");
      this.printButton1.Name = "printButton1";
      this.printButton1.ReportType = "";
      this.printButton1.Print += new PrintEventHandler(this.printButton1_Print);
      componentResourceManager.ApplyResources((object) this.previewButton1, "previewButton1");
      this.previewButton1.Name = "previewButton1";
      this.previewButton1.ReportType = "";
      this.previewButton1.Preview += new PrintEventHandler(this.previewButton1_Preview);
      componentResourceManager.ApplyResources((object) this.sbtnToggleOptions, "sbtnToggleOptions");
      this.sbtnToggleOptions.Name = "sbtnToggleOptions";
      this.sbtnToggleOptions.Click += new EventHandler(this.sbtnToggleOptions_Click);
      componentResourceManager.ApplyResources((object) this.sbtnClose, "sbtnClose");
      this.sbtnClose.DialogResult = DialogResult.Cancel;
      this.sbtnClose.Name = "sbtnClose";
      this.sbtnClose.Click += new EventHandler(this.sbtnClose_Click);
      componentResourceManager.ApplyResources((object) this.sbtnInquiry, "sbtnInquiry");
      this.sbtnInquiry.Name = "sbtnInquiry";
      this.sbtnInquiry.Click += new EventHandler(this.sbtnInquiry_Click);
      Bars bars1 = this.barManager1.Bars;
      Bar[] bars2 = new Bar[1];
      int index39 = 0;
      Bar bar = this.bar2;
      bars2[index39] = bar;
      bars1.AddRange(bars2);
      this.barManager1.DockControls.Add(this.barDockControlTop);
      this.barManager1.DockControls.Add(this.barDockControlBottom);
      this.barManager1.DockControls.Add(this.barDockControlLeft);
      this.barManager1.DockControls.Add(this.barDockControlRight);
      this.barManager1.Form = (Control) this;
      BarItems items5 = this.barManager1.Items;
      BarItem[] items6 = new BarItem[3];
      int index40 = 0;
      BarSubItem barSubItem = this.barSubItem1;
      items6[index40] = (BarItem) barSubItem;
      int index41 = 1;
      BarButtonItem barButtonItem1 = this.barBtnDesignDocumentStyleReport;
      items6[index41] = (BarItem) barButtonItem1;
      int index42 = 2;
      BarButtonItem barButtonItem2 = this.barBtnDesignListingStyleReport;
      items6[index42] = (BarItem) barButtonItem2;
      items5.AddRange(items6);
      this.barManager1.MainMenu = this.bar2;
      this.barManager1.MaxItemId = 3;
      this.bar2.BarName = "Main menu";
      this.bar2.DockCol = 0;
      this.bar2.DockRow = 0;
      this.bar2.DockStyle = BarDockStyle.Top;
      LinksInfo linksPersistInfo1 = this.bar2.LinksPersistInfo;
      LinkPersistInfo[] links1 = new LinkPersistInfo[1];
      int index43 = 0;
      LinkPersistInfo linkPersistInfo1 = new LinkPersistInfo((BarItem) this.barSubItem1, true);
      links1[index43] = linkPersistInfo1;
      linksPersistInfo1.AddRange(links1);
      this.bar2.OptionsBar.MultiLine = true;
      this.bar2.OptionsBar.UseWholeRow = true;
      componentResourceManager.ApplyResources((object) this.bar2, "bar2");
      componentResourceManager.ApplyResources((object) this.barSubItem1, "barSubItem1");
      this.barSubItem1.Id = 0;
      LinksInfo linksPersistInfo2 = this.barSubItem1.LinksPersistInfo;
      LinkPersistInfo[] links2 = new LinkPersistInfo[2];
      int index44 = 0;
      LinkPersistInfo linkPersistInfo2 = new LinkPersistInfo((BarItem) this.barBtnDesignDocumentStyleReport);
      links2[index44] = linkPersistInfo2;
      int index45 = 1;
      LinkPersistInfo linkPersistInfo3 = new LinkPersistInfo((BarItem) this.barBtnDesignListingStyleReport);
      links2[index45] = linkPersistInfo3;
      linksPersistInfo2.AddRange(links2);
      this.barSubItem1.MergeOrder = 1;
      this.barSubItem1.Name = "barSubItem1";
      componentResourceManager.ApplyResources((object) this.barBtnDesignDocumentStyleReport, "barBtnDesignDocumentStyleReport");
      this.barBtnDesignDocumentStyleReport.Id = 1;
      this.barBtnDesignDocumentStyleReport.Name = "barBtnDesignDocumentStyleReport";
      this.barBtnDesignDocumentStyleReport.ItemClick += new ItemClickEventHandler(this.barBtnDesignDocumentStyleReport_ItemClick);
      componentResourceManager.ApplyResources((object) this.barBtnDesignListingStyleReport, "barBtnDesignListingStyleReport");
      this.barBtnDesignListingStyleReport.Id = 2;
      this.barBtnDesignListingStyleReport.Name = "barBtnDesignListingStyleReport";
      this.barBtnDesignListingStyleReport.ItemClick += new ItemClickEventHandler(this.barBtnDesignListingStyleReport_ItemClick);
      componentResourceManager.ApplyResources((object) this.barDockControlTop, "barDockControlTop");
      this.barDockControlTop.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("barDockControlTop.Appearance.GradientMode");
      this.barDockControlTop.Appearance.Image = (Image) componentResourceManager.GetObject("barDockControlTop.Appearance.Image");
      this.barDockControlTop.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlBottom, "barDockControlBottom");
      this.barDockControlBottom.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("barDockControlBottom.Appearance.GradientMode");
      this.barDockControlBottom.Appearance.Image = (Image) componentResourceManager.GetObject("barDockControlBottom.Appearance.Image");
      this.barDockControlBottom.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlLeft, "barDockControlLeft");
      this.barDockControlLeft.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("barDockControlLeft.Appearance.GradientMode");
      this.barDockControlLeft.Appearance.Image = (Image) componentResourceManager.GetObject("barDockControlLeft.Appearance.Image");
      this.barDockControlLeft.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlRight, "barDockControlRight");
      this.barDockControlRight.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("barDockControlRight.Appearance.GradientMode");
      this.barDockControlRight.Appearance.Image = (Image) componentResourceManager.GetObject("barDockControlRight.Appearance.Image");
      this.barDockControlRight.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.panelHeader1, "panelHeader1");
      this.panelHeader1.HelpTopicId = "Stock_Assembly_Order.htm";
      this.panelHeader1.Name = "panelHeader1";
      componentResourceManager.ApplyResources((object) this, "$this");
      this.AutoScaleMode = AutoScaleMode.Dpi;
      this.CancelButton = (IButtonControl) this.sbtnClose;
      this.Controls.Add((Control) this.panel_Center);
      this.Controls.Add((Control) this.panelControl1);
      this.Controls.Add((Control) this.panelCriteria);
      this.Controls.Add((Control) this.panelHeader1);
      this.Controls.Add((Control) this.barDockControlLeft);
      this.Controls.Add((Control) this.barDockControlRight);
      this.Controls.Add((Control) this.barDockControlBottom);
      this.Controls.Add((Control) this.barDockControlTop);
      this.KeyPreview = true;
      this.Name = "FormStockWorkOrderPrintListing";
      this.ShowInTaskbar = false;
      this.SizeGripStyle = SizeGripStyle.Hide;
      this.Closing += new CancelEventHandler(this.FormStockAssemblyList_Closing);
      this.Load += new EventHandler(this.FormSAOPrintSearch_Load);
      this.KeyDown += new KeyEventHandler(this.FormStockAssemblyPrintSearch_KeyDown);
      this.panelCriteria.EndInit();
      this.panelCriteria.ResumeLayout(false);
      this.panelCriteria.PerformLayout();
      this.cbEditReportType.Properties.EndInit();
      this.groupBox_SearchCriteria.EndInit();
      this.groupBox_SearchCriteria.ResumeLayout(false);
      this.groupBox_SearchCriteria.PerformLayout();
      this.cbCancelledOption.Properties.EndInit();
      this.gbReportOption.EndInit();
      this.gbReportOption.ResumeLayout(false);
      this.gbReportOption.PerformLayout();
      this.chkEditShowCriteria.Properties.EndInit();
      this.cbEditGroupBy.Properties.EndInit();
      this.cbEditSortBy.Properties.EndInit();
      this.tabControl1.EndInit();
      this.tabControl1.ResumeLayout(false);
      this.tabPage1.ResumeLayout(false);
      this.gridControl1.EndInit();
      this.gridView1.EndInit();
      this.repositoryItemCheckEdit1.EndInit();
      this.repositoryItemTextEdit_Cancelled.EndInit();
      this.tabPage2.ResumeLayout(false);
      this.memoEdit_Criteria.Properties.EndInit();
      this.panel_Center.EndInit();
      this.panel_Center.ResumeLayout(false);
      this.panelControl1.EndInit();
      this.panelControl1.ResumeLayout(false);
      this.barManager1.EndInit();
      this.ResumeLayout(false);
    }

    public class FormEventArgs
    {
      private FormStockWorkOrderPrintListing myForm;

      public StockWorkOrderCommand Command
      {
        get
        {
          return this.myForm.myCommand;
        }
      }

      public PanelControl PanelCriteria
      {
        get
        {
          return this.myForm.panelCriteria;
        }
      }

      public PanelControl PanelButtons
      {
        get
        {
          return this.myForm.panelControl1;
        }
      }

      public XtraTabControl TabControl
      {
        get
        {
          return this.myForm.tabControl1;
        }
      }

      public GridControl GridControl
      {
        get
        {
          return this.myForm.gridControl1;
        }
      }

      public FormStockWorkOrderPrintListing Form
      {
        get
        {
          return this.myForm;
        }
      }

      public DBSetting DBSettig
      {
        get
        {
          return this.myForm.myDBSetting;
        }
      }

      public FormEventArgs(FormStockWorkOrderPrintListing form)
      {
        this.myForm = form;
      }
    }

    public class FormInitializeEventArgs : FormStockWorkOrderPrintListing.FormEventArgs
    {
      public FormInitializeEventArgs(FormStockWorkOrderPrintListing form)
        : base(form)
      {
      }
    }

    public class FormInquiryEventArgs : FormStockWorkOrderPrintListing.FormEventArgs
    {
      private DataTable myResultTable;

      public DataTable ResultTable
      {
        get
        {
          return this.myResultTable;
        }
      }

      public FormInquiryEventArgs(FormStockWorkOrderPrintListing form, DataTable resultTable)
        : base(form)
      {
        this.myResultTable = resultTable;
      }
    }
  }
}
