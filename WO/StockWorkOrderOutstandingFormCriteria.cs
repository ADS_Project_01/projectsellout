﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderOutstandingFormCriteria
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Invoicing;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.SearchFilter;
using BCE.Localization;
using System;
using System.Collections;

namespace RPASystem.WorkOrder
{
  [Serializable]
  public class StockWorkOrderOutstandingFormCriteria : SearchCriteria
  {
    protected FilterMode myFilterMode = FilterMode.BasicMode;
    private BCE.AutoCount.SearchFilter.Filter myDocumentNoFilter;
    private BCE.AutoCount.SearchFilter.Filter myBomItemCodeFilter;
    private BCE.AutoCount.SearchFilter.Filter myItemGroupFilter;
    private BCE.AutoCount.SearchFilter.Filter myItemTypeFilter;
    private BCE.AutoCount.SearchFilter.Filter myLocationFilter;
    private BCE.AutoCount.SearchFilter.Filter myProjectNoFilter;
    private BCE.AutoCount.SearchFilter.Filter myDepartmentNoFilter;
    private BCE.AutoCount.SearchFilter.Filter myItemCodeDetailFilter;
    private BCE.AutoCount.SearchFilter.Filter myItemGroupDetailFilter;
    private BCE.AutoCount.SearchFilter.Filter myItemTypeDetailFilter;
    private BCE.AutoCount.SearchFilter.Filter myLocationDetailFilter;
    private BCE.AutoCount.SearchFilter.Filter myProjectNoDetailFilter;
    private BCE.AutoCount.SearchFilter.Filter myDepartmentNoDetailFilter;
    private DateTime myFromDate;
    private DateTime myEndDate;
    private int mySortBy;
    private int myGroupBy;
    private bool myShowCriteria;
    private bool myAdvancedOptions;
    private StockWorkOrderOutstandingFormCriteria.PrintOption myPrintOption;

    public BCE.AutoCount.SearchFilter.Filter DocumentNoFilter
    {
      get
      {
        return this.myDocumentNoFilter;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter BomItemCodeFilter
    {
      get
      {
        return this.myBomItemCodeFilter;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter ItemGroupFilter
    {
      get
      {
        return this.myItemGroupFilter;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter ItemTypeFilter
    {
      get
      {
        return this.myItemTypeFilter;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter LocationFilter
    {
      get
      {
        return this.myLocationFilter;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter ProjectNoFilter
    {
      get
      {
        return this.myProjectNoFilter;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter DepartmentNoFilter
    {
      get
      {
        return this.myDepartmentNoFilter;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter ItemCodeDetailFilter
    {
      get
      {
        return this.myItemCodeDetailFilter;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter ItemGroupDetailFilter
    {
      get
      {
        return this.myItemGroupDetailFilter;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter ItemTypeDetailFilter
    {
      get
      {
        return this.myItemTypeDetailFilter;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter LocationDetailFilter
    {
      get
      {
        return this.myLocationDetailFilter;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter ProjectNoDetailFilter
    {
      get
      {
        return this.myProjectNoDetailFilter;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter DepartmentNoDetailFilter
    {
      get
      {
        return this.myDepartmentNoDetailFilter;
      }
    }

    public DateTime FromDate
    {
      get
      {
        return this.myFromDate;
      }
      set
      {
        this.myFromDate = value.Date;
      }
    }

    public DateTime EndDate
    {
      get
      {
        return this.myEndDate;
      }
      set
      {
        this.myEndDate = value.Date;
      }
    }

    public int SortBy
    {
      get
      {
        return this.mySortBy;
      }
      set
      {
        this.mySortBy = value;
      }
    }

    public int GroupBy
    {
      get
      {
        return this.myGroupBy;
      }
      set
      {
        this.myGroupBy = value;
      }
    }

    public bool ShowCriteria
    {
      get
      {
        return this.myShowCriteria;
      }
      set
      {
        this.myShowCriteria = value;
      }
    }

    public bool AdvancedOptions
    {
      get
      {
        return this.myAdvancedOptions;
      }
      set
      {
        this.myAdvancedOptions = value;
      }
    }

    public FilterMode FilterMode
    {
      get
      {
        return this.myFilterMode;
      }
      set
      {
        this.myFilterMode = value;
      }
    }

    public StockWorkOrderOutstandingFormCriteria.PrintOption OutstandingPrintingOption
    {
      get
      {
        return this.myPrintOption;
      }
      set
      {
        this.myPrintOption = value;
      }
    }

    public string[] ReadableTextArray
    {
      get
      {
        ArrayList arrayList1 = new ArrayList();
        string str1 = string.Empty;
        arrayList1.Add((object) ("From Date: " + this.myFromDate.ToLongDateString()));
        arrayList1.Add((object) ("               To Date: " + this.myEndDate.ToLongDateString()));
        string str2 = this.myDocumentNoFilter.BuildReadableText(false);
        if (str2.Length > 0)
          arrayList1.Add((object) ("               " + str2));
        string str3 = this.myItemTypeFilter.BuildReadableText(false);
        if (str3.Length > 0)
          arrayList1.Add((object) ("               " + str3));
        string str4 = this.myItemGroupFilter.BuildReadableText(false);
        if (str4.Length > 0)
          arrayList1.Add((object) ("               " + str4));
        string str5 = this.myLocationFilter.BuildReadableText(false);
        if (str5.Length > 0)
          arrayList1.Add((object) ("               " + str5));
        string str6 = this.myProjectNoFilter.BuildReadableText(false);
        if (str6.Length > 0)
          arrayList1.Add((object) ("               " + str6));
        string str7 = this.myDepartmentNoFilter.BuildReadableText(false);
        if (str7.Length > 0)
          arrayList1.Add((object) ("               " + str7));
        string str8 = this.myItemCodeDetailFilter.BuildReadableText(false);
        if (str8.Length > 0)
          arrayList1.Add((object) ("               " + str8));
        string str9 = this.myItemTypeDetailFilter.BuildReadableText(false);
        if (str9.Length > 0)
          arrayList1.Add((object) ("               " + str9));
        string str10 = this.myItemGroupDetailFilter.BuildReadableText(false);
        if (str10.Length > 0)
          arrayList1.Add((object) ("               " + str10));
        string str11 = this.myLocationDetailFilter.BuildReadableText(false);
        if (str11.Length > 0)
          arrayList1.Add((object) ("               " + str11));
        string str12 = this.myProjectNoDetailFilter.BuildReadableText(false);
        if (str12.Length > 0)
          arrayList1.Add((object) ("               " + str12));
        string str13 = this.myDepartmentNoDetailFilter.BuildReadableText(false);
        if (str13.Length > 0)
          arrayList1.Add((object) ("               " + str13));
        ArrayList arrayList2 = new ArrayList();
        if (arrayList1.Count == 0)
        {
          arrayList2.Add((object) "Filter Options: No Filter");
        }
        else
        {
          for (int index = 0; index < arrayList1.Count; ++index)
          {
            string str14 = arrayList1[index].ToString();
            if (index == 0)
              arrayList2.Add((object) ("Filter Options: " + str14));
            else
              arrayList2.Add((object) (" " + str14));
          }
        }
        if (this.mySortBy == 0)
          arrayList2.Add((object) "Report Options: Sort By: Document No");
        else
          arrayList2.Add((object) "Report Options: Sort By: Document Date");
        if (this.myGroupBy == 0)
          arrayList2.Add((object) "                Group By: None");
        else if (this.myGroupBy == 1)
          arrayList2.Add((object) "                Group By: Item Code");
        else if (this.myGroupBy == 2)
          arrayList2.Add((object) "                Group By: Item Group");
        else if (this.myGroupBy == 3)
          arrayList2.Add((object) "                Group By: Item Type");
        else if (this.myGroupBy == 4)
          arrayList2.Add((object) "                Group By: Location");
        else if (this.myGroupBy == 5)
          arrayList2.Add((object) "                Group By: Project No");
        else if (this.myGroupBy == 6)
          arrayList2.Add((object) "                Group By: Departmnet No");
        else if (this.myGroupBy == 7)
          arrayList2.Add((object) "                Group By: Date");
        else if (this.myGroupBy == 8)
          arrayList2.Add((object) "                Group By: Month");
        else if (this.myGroupBy == 9)
          arrayList2.Add((object) "                Group By: Year");
        return (string[]) arrayList2.ToArray(Type.GetType("System.String"));
      }
    }

    public StockWorkOrderOutstandingFormCriteria()
    {
      this.myDocumentNoFilter = new BCE.AutoCount.SearchFilter.Filter("ASMORDER", "DocNo", Localizer.GetString((Enum) StockAssemblyOrderStringId.DocNo, new object[0]), FilterControlType.StockAssemblyOrder);
      this.myBomItemCodeFilter = new BCE.AutoCount.SearchFilter.Filter("C", "ItemCode", Localizer.GetString((Enum) StockAssemblyOrderStringId.BomItemCode, new object[0]), FilterControlType.Item);
      this.myItemGroupFilter = new BCE.AutoCount.SearchFilter.Filter("C", "ItemGroup", Localizer.GetString((Enum) StockAssemblyOrderStringId.ItemGroup, new object[0]), FilterControlType.ItemGroup);
      this.myItemTypeFilter = new BCE.AutoCount.SearchFilter.Filter("C", "ItemType", Localizer.GetString((Enum) StockAssemblyOrderStringId.ItemType, new object[0]), FilterControlType.ItemType);
      this.myLocationFilter = new BCE.AutoCount.SearchFilter.Filter("ASMORDER", "Location", Localizer.GetString((Enum) StockAssemblyOrderStringId.Location, new object[0]), FilterControlType.Location);
      this.myProjectNoFilter = new BCE.AutoCount.SearchFilter.Filter("ASMORDER", "ProjNo", Localizer.GetString((Enum) StockAssemblyOrderStringId.ProjectNo, new object[0]), FilterControlType.Project);
      this.myDepartmentNoFilter = new BCE.AutoCount.SearchFilter.Filter("ASMORDER", "DeptNo", Localizer.GetString((Enum) StockAssemblyOrderStringId.DepartmentNo, new object[0]), FilterControlType.Department);
      this.myItemCodeDetailFilter = new BCE.AutoCount.SearchFilter.Filter("ASMORDERDTL", "ItemCode", Localizer.GetString((Enum) StockAssemblyOrderStringId.ItemCode, new object[0]), FilterControlType.Item);
      this.myItemGroupDetailFilter = new BCE.AutoCount.SearchFilter.Filter("D", "ItemGroup", Localizer.GetString((Enum) StockAssemblyOrderStringId.ItemGroup, new object[0]), FilterControlType.ItemGroup);
      this.myItemTypeDetailFilter = new BCE.AutoCount.SearchFilter.Filter("D", "ItemType", Localizer.GetString((Enum) StockAssemblyOrderStringId.ItemType, new object[0]), FilterControlType.ItemType);
      this.myLocationDetailFilter = new BCE.AutoCount.SearchFilter.Filter("ASMORDERDTL", "Location", Localizer.GetString((Enum) StockAssemblyOrderStringId.Location, new object[0]), FilterControlType.Location);
      this.myProjectNoDetailFilter = new BCE.AutoCount.SearchFilter.Filter("ASMORDERDTL", "ProjNo", Localizer.GetString((Enum) StockAssemblyOrderStringId.ProjectNo, new object[0]), FilterControlType.Project);
      this.myDepartmentNoDetailFilter = new BCE.AutoCount.SearchFilter.Filter("ASMORDERDTL", "DeptNo", Localizer.GetString((Enum) StockAssemblyOrderStringId.DepartmentNo, new object[0]), FilterControlType.Department);
      this.myAdvancedOptions = false;
      this.mySortBy = 0;
      this.myGroupBy = 0;
      this.myShowCriteria = true;
      this.KeepSearchResult = false;
    }

    public enum PrintOption
    {
      Outstanding,
      Compeleted,
    }
  }
}
