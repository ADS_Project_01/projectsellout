﻿// Type: BCE.AutoCount.Manufacturing.StockWorkOrder.FormStockWorkOrderEntry
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.ActivityStream;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Common;
using BCE.AutoCount.CommonForms;
using BCE.AutoCount.ContextException;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controls;
using BCE.AutoCount.Data;
using BCE.AutoCount.Document;
using BCE.AutoCount.DragDrop;
using BCE.AutoCount.Help;
using BCE.AutoCount.Inquiry;
using BCE.AutoCount.Inquiry.UserControls;
using BCE.AutoCount.Invoicing;
using BCE.AutoCount.LicenseControl;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.Manufacturing.BOMOptional;
using BCE.AutoCount.RegistryID.Misc;
using BCE.AutoCount.RegistryID.PrimaryKeyID;
using BCE.AutoCount.Report;
using BCE.AutoCount.Scripting;
using BCE.AutoCount.Settings;
using BCE.AutoCount.Stock;
using BCE.AutoCount.Stock.Item;
using BCE.AutoCount.Stock.StockUOMConversion;
using BCE.AutoCount.UDF;
using BCE.AutoCount.WinForms;
using BCE.AutoCount.XtraUtils;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
using BCE.Controls;
using BCE.Data;
using BCE.Localization;
using BCE.Misc;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace RPASystem.WorkOrder
{
  public class FormStockWorkOrderEntry : XtraForm
  {
    private DBRegistry myDBReg;
    private bool myUseLookupEditToInputItemCode;
    private bool mySkipExecuteFormActivated;
    private bool myLastValidationSuccess;
    private CustomizeGridLayout myGridLayout;
    private int myInProcessTempDocument;
    private StockHelper myStockHelper;
    private bool myHasDeactivated;
    private bool myItemCodeFromPopup;
    private bool myCanDrag;
    private RemarkName myRemarkName;
    private RemarkNameEntity myRemarkNameEntity;
    private MRUHelper myMRUHelper;
    private MRUHelper myMRUHelperRemark1;
    private MRUHelper myMRUHelperRemark2;
    private MRUHelper myMRUHelperRemark3;
    private MRUHelper myMRUHelperRemark4;
    private DataSet myLastDragDataSet;
    private StockWorkOrder myStockWorkOrder;
    private DBSetting myDBSetting;
    private UndoManager myMasterRecordUndo;
    private bool mySaveInKIV;
    private bool myIsLoadForm;
    private ScriptObject myScriptObject;
    private ItemBatchForReceiveLookupEditBuilder myMasterItemBatchLookupEditBuilder;
    private int myColLocationVisibleIndex;
    private int myColLocationVisibleIndex1;
    private int myColBatchNoVisibleIndex;
    private int myColBatchNoVisibleIndex1;
    private int myColProjNoVisibleIndex;
    private int myColDeptNoVisibleIndex;
    private UndoManager myDetailRecordUndo;
    private DocumentCarrier myDropDocCarrier;
    private int myDisableAutoSaveCounter;
    private bool myFilterByLocation;
    private bool myIsInstantInfoShown;
    private string myLastInquiryItemCode;
    private string myLastInquiryUOM;
    private UCInquiryStock myUCInquiry;
    private FormItemSearch myFormItemSearch;
    private FormStockWorkOrderEditOption myEditOption;
    private IContainer components;
    private bool myHasUnlinkLookupEditEventHandlers;
    private PreviewButton btnPreview;
    private PrintButton btnPrint;
    private UCInquiryStock ucInquiryStock1;
    private ExternalLinkBox externalLinkBox1;
    private BCE.Controls.MemoEdit memoEdtNote;
    private Navigator navigator;
    private Bar bar1;
    private BarButtonItem barbtnCheckTransferredToStatus;
    private BarButtonItem barBtnSaveInKIVFolder;
    private BarButtonItem barbtnTransferFromSO;
    private BarButtonItem barButtonItem1;
    private BarButtonItem barItemCopyFrom;
    private BarButtonItem barItemCopySelectedDetails;
    private BarButtonItem barItemCopyTo;
    private BarButtonItem barItemCopyWholeDocument;
    private BarButtonItem barItemPasteItemDetailOnly;
    private BarButtonItem barItemPasteWholeDocument;
    private BarButtonItem iEditDescriptionMRU;
    private BarButtonItem iUndoMaster;
    private BarDockControl barDockControlBottom;
    private BarDockControl barDockControlLeft;
    private BarDockControl barDockControlRight;
    private BarDockControl barDockControlTop;
    private BarManager barManager1;
    private BarSubItem barSubItem1;
    private BarSubItem barSubItem2;
    private BarSubItem barSubItem3;
    private CheckEdit chkedtChildItem;
    private CheckEdit chkedtNextRecord;
    private DateEdit dateEdtDate;
    private GroupControl grboxItemStock;
    private LookUpEdit luAssemblyItem;
    private LookUpEdit luBatchNo;
    private LookUpEdit luDeptNo;
    private LookUpEdit luEdtDocNoFormat;
    private LookUpEdit luLocation;
    private LookUpEdit luProject;
    private MRUEdit mruEdtDescription;
    private PanelControl panBatch;
    private PanelControl panBOMOptional;
    private PanelControl panBottom;
    private PanelControl panDept;
    private PanelControl panel2;
    private PanelControl panelControl1;
    private PanelControl panelHeader;
    private PanelControl panLocation;
    private PanelControl panProject;
    private PanelControl panTop;
    private RepositoryItemButtonEdit repBtnedtFurtherDesc;
    private RepositoryItemCheckEdit repositoryItemCheckEdit1;
    private RepositoryItemLookUpEdit repLuedtBatchNo;
    private RepositoryItemLookUpEdit repLuedtDeptNo;
    private RepositoryItemLookUpEdit repLuedtItem;
    private RepositoryItemLookUpEdit repLuedtLocation;
    private RepositoryItemLookUpEdit repLuedtProjNo;
    private RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
    private RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
    private SimpleButton sBtnAdd;
    private SimpleButton sBtnApplyBOMOptional;
    private SimpleButton sbtnCancel;
    private SimpleButton sbtnCancelDoc;
    private SimpleButton sbtnDelete;
    private SimpleButton sbtnEdit;
    private SimpleButton sBtnInsert;
    private SimpleButton sBtnMoveDown;
    private SimpleButton sBtnMoveUp;
    private SimpleButton sBtnRangeSetting;
    private SimpleButton sBtnRemove;
    private SimpleButton sbtnSave;
    private SimpleButton sbtnSavePreview;
    private SimpleButton sbtnSavePrint;
    private SimpleButton sBtnSearch;
    private SimpleButton sBtnSelectAllMain;
    private SimpleButton sBtnShowInstant;
    private SimpleButton sBtnUndoTree;
    private TextEdit textEdtExpectedCompletedDate;
    private TextEdit textEdtRefDocNo;
    private TextEdit textEdtTotal;
    private TextEdit txtedtAssemblyCost;
    private TextEdit txtedtNetTotal;
    private TextEdit txtedtQuantity;
    private TextEdit txtEdtStockAssemblyNo;
    private GridColumn colAvailableBal;
    private GridColumn colAvailableQty;
    private GridColumn colBatchNo;
    private GridColumn colDescription;
    private GridColumn colItemCode;
    private GridColumn colLocation;
    private GridColumn colOnHandBal;
    private GridColumn colOnHandQty;
    private GridColumn colReqQty;
    private GridColumn colUOM;
    private GridControl gctlRawMaterial;
    private GridView gridView1;
    private GridView gvRawMaterial;
    private XtraTabControl tabControl1;
    private XtraTabPage tabPageExternalLink;
    private XtraTabPage tabPageMainTree;
    private XtraTabPage tabPageNote;
    private XtraTabPage tabPageRawMaterialStatus;
    private TreeListColumn tlBatchNo;
    private TreeListColumn tlDeptNo;
    private TreeListColumn tlDesc;
    private TreeListColumn tlFurtherDescription;
    private TreeListColumn tlIsBOMItem;
    private TreeListColumn tlItemCode;
    private TreeListColumn tlItemCost;
    private TreeListColumn tlLocation;
    private TreeListColumn tlNumbering;
    private TreeListColumn tlOverheadCost;
    private TreeListColumn tlPrintOut;
    private TreeListColumn tlProjNo;
    private TreeListColumn tlQty;
    private TreeListColumn tlRate;
    private TreeListColumn tlRemark;
    private TreeListColumn tlSeq;
    private TreeListColumn tlSubTotal;
    private TreeList treeListDetail;
    private Label label2;
    private Label label4;
    private Label labelAssemblyItemDescription;
    private Label lblBatch;
    private Label lblDept;
    private Label lblLocation;
    private Label lblProj;
    private ImageList imageList1;
    private Label label10;
    private Label label11;
    private Label label12;
    private Label label9;
    private Label lblCancelled;
    private Label lblDate;
    private Label lblDescription;
    private Label lblRefDocNo;
    private Label lblStockAdjNo;
    private System.Windows.Forms.Timer timer1;
    private BarButtonItem barItemCopyAsTabDelimitedText;
    private XtraTabPage tabPageMoreHeader;
    private BarSubItem barSubItem4;
    private BarButtonItem iEditRemark1MRU;
    private BarButtonItem iEditRemark2MRU;
    private BarButtonItem iEditRemark3MRU;
    private BarButtonItem iEditRemark4MRU;
    private Label labelRemark4;
    private Label labelRemark3;
    private Label labelRemark2;
    private Label labelRemark1;
    private MRUEdit mruEdtRemark1;
    private MRUEdit mruEdtRemark2;
    private MRUEdit mruEdtRemark3;
    private MRUEdit mruEdtRemark4;
    private BarButtonItem barBtnConvertLevel;
    private System.Windows.Forms.Timer timer2;
    private SplitterControl splitterControl1;

    public FormStockWorkOrderEntry(StockWorkOrder stockWorkOrder)
      : this(stockWorkOrder, new FormStockWorkOrderEditOption())
    {
    }

    public FormStockWorkOrderEntry(StockWorkOrder stockWorkOrder, FormStockWorkOrderEditOption editOption)
    {
      if (stockWorkOrder == null)
        throw new ArgumentNullException("stockWorkOrder");
      else if (editOption == null)
      {
        throw new ArgumentNullException("editOption");
      }
      else
      {
        this.myEditOption = editOption;
        this.InitializeComponent();
        this.myDBSetting = stockWorkOrder.Command.DBSetting;
        this.myDBReg = stockWorkOrder.Command.DBReg;
        this.myScriptObject = ScriptManager.CreateObject(this.myDBSetting, "StockWorkOrderEditForm");
        this.SetUseLookupEditToInputItemCode();
        this.myStockHelper = StockHelper.Create(this.myDBSetting);
        this.btnPreview.ReportType = "Stock Assembly Order Document";
        this.btnPreview.SetDBSetting(this.myDBSetting);
        this.btnPrint.ReportType = "Stock Assembly Order Document";
        this.btnPrint.SetDBSetting(this.myDBSetting);
        this.InitRemark();
        this.InitMRUHelper();
        this.SetupLookupEdit();
        this.memoEdtNote.SetDBSetting(this.myDBSetting);
        WorkOrderKeepAfterSave assemblyKeepAfterSave = (WorkOrderKeepAfterSave) null;
        try
        {
          assemblyKeepAfterSave = (WorkOrderKeepAfterSave) PersistenceUtil.LoadUserSetting("StockWorkOrderCondition.setting");
        }
        catch
        {
        }
        if (assemblyKeepAfterSave != null)
        {
          this.chkedtNextRecord.Checked = assemblyKeepAfterSave.CheckKeepAfterSave;
          if (this.sBtnShowInstant.Text == BCE.Localization.Localizer.GetString( StockAssemblyOrderStringId.Code_ShowFooter, new object[0]))
            this.panBottom.Height = assemblyKeepAfterSave.InstantInfoHeight;
          if (assemblyKeepAfterSave.MaximizeWindow)
            this.WindowState = FormWindowState.Maximized;
        }
        this.myGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name, this.gvRawMaterial);
        BCE.AutoCount.Help.HelpProvider.SetHelpTopic(new System.Windows.Forms.HelpProvider(), (Control) this, "Stock_Assembly_Order.htm");
        FormStockWorkOrderEntry.FormInitializeEventArgs initializeEventArgs1 = new FormStockWorkOrderEntry.FormInitializeEventArgs(this, stockWorkOrder);
        ScriptObject scriptObject1 = this.myScriptObject;
        string name1 = "OnFormInitialize";
        System.Type[] types1 = new System.Type[1];
        int index1 = 0;
        System.Type type1 = initializeEventArgs1.GetType();
        types1[index1] = type1;
        object[] objArray1 = new object[1];
        int index2 = 0;
        FormStockWorkOrderEntry.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
        objArray1[index2] = (object) initializeEventArgs2;
        scriptObject1.RunMethod(name1, types1, objArray1);
        ScriptObject scriptObject2 = stockWorkOrder.ScriptObject;
        string name2 = "OnFormInitialize";
        System.Type[] types2 = new System.Type[1];
        int index3 = 0;
        System.Type type2 = initializeEventArgs1.GetType();
        types2[index3] = type2;
        object[] objArray2 = new object[1];
        int index4 = 0;
        FormStockWorkOrderEntry.FormInitializeEventArgs initializeEventArgs3 = initializeEventArgs1;
        objArray2[index4] = (object) initializeEventArgs3;
        scriptObject2.RunMethod(name2, types2, objArray2);
        this.SetStockWorkOrder(stockWorkOrder);
        this.LogActivity(this.myDBSetting, stockWorkOrder);
      }
    }

    [StartThreadForm]
    public void StartForm(StockWorkOrder stockWorkOrder)
    {
      this.StartForm(stockWorkOrder, new FormStockWorkOrderEditOption());
    }

    [StartThreadForm]
    public void StartForm(StockWorkOrder stockWorkOrder, FormStockWorkOrderEditOption editOption)
    {
      this.myEditOption = editOption;
      this.myDBSetting = stockWorkOrder.Command.DBSetting;
      this.myDBReg = stockWorkOrder.Command.DBReg;
      this.SetUseLookupEditToInputItemCode();
      this.SetStockWorkOrder(stockWorkOrder);
      this.LogActivity(this.myDBSetting, stockWorkOrder);
    }

    private void SetUseLookupEditToInputItemCode()
    {
      this.myUseLookupEditToInputItemCode = this.myDBReg.GetBoolean((IRegistryID) new UseLookupEditToInputItemCode());
      if (this.myUseLookupEditToInputItemCode)
      {
        if (this.repLuedtItem.Columns.Count == 0)
          DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.BuildLookupEdit(this.repLuedtItem, this.myDBSetting);
        this.tlItemCode.ColumnEdit = (RepositoryItem) this.repLuedtItem;
      }
      else
        this.tlItemCode.ColumnEdit = (RepositoryItem) null;
    }

    private void LogActivity(DBSetting dbSetting, StockWorkOrder stockWorkOrder)
    {
      if (stockWorkOrder.Action == StockWorkOrderAction.Edit)
      {
        DBSetting dbSetting1 = dbSetting;
        string docType = "AO";
        long docKey = stockWorkOrder.DocKey;
        long eventKey = 0L;
        // ISSUE: variable of a boxed type
        StockWorkOrderString  local = StockWorkOrderString.ViewStockWorkOrderInEditMode;
        object[] objArray = new object[2];
        int index1 = 0;
        string loginUserId = this.myStockWorkOrder.Command.UserAuthentication.LoginUserID;
        objArray[index1] = (object) loginUserId;
        int index2 = 1;
        string str = stockWorkOrder.DocNo.ToString();
        objArray[index2] = (object) str;
        string @string = BCE.Localization.Localizer.GetString( local, objArray);
        string detail = "";
        Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
      }
      else if (stockWorkOrder.Action == StockWorkOrderAction.View)
      {
        DBSetting dbSetting1 = dbSetting;
        string docType = "AO";
        long docKey = stockWorkOrder.DocKey;
        long eventKey = 0L;
        // ISSUE: variable of a boxed type
        StockWorkOrderString  local =  StockWorkOrderString.ViewStockWorkOrder;
        object[] objArray = new object[2];
        int index1 = 0;
        string loginUserId = this.myStockWorkOrder.Command.UserAuthentication.LoginUserID;
        objArray[index1] = (object) loginUserId;
        int index2 = 1;
        string str = stockWorkOrder.DocNo.ToString();
        objArray[index2] = (object) str;
        string @string = BCE.Localization.Localizer.GetString( local, objArray);
        string detail = "";
        Activity.Log(dbSetting1, docType, docKey, eventKey, @string, detail);
      }
    }

    private void SetStockWorkOrder(StockWorkOrder newStockWorkOrder)
    {
      if (this.myStockWorkOrder != newStockWorkOrder)
      {
        this.myStockWorkOrder = newStockWorkOrder;
        if (this.barBtnConvertLevel.Visibility == BarItemVisibility.Always)
        {
          if (this.myStockWorkOrder.IsMultilevel)
          {
            BarButtonItem barButtonItem = this.barBtnConvertLevel;
            // ISSUE: variable of a boxed type
            StockAssemblyStringId  local =  StockAssemblyStringId.ConvertToLevel;
            object[] objArray = new object[1];
            int index = 0;
            string str = "Single";
            objArray[index] = (object) str;
            string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
            barButtonItem.Caption = @string;
          }
          else
          {
            BarButtonItem barButtonItem = this.barBtnConvertLevel;
            // ISSUE: variable of a boxed type
            StockAssemblyStringId  local =  StockAssemblyStringId.ConvertToLevel;
            object[] objArray = new object[1];
            int index = 0;
            string str = "Multi";
            objArray[index] = (object) str;
            string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
            barButtonItem.Caption = @string;
          }
        }
        this.myStockWorkOrder.EnableAutoLoadItemDetail = true;
        this.BindingMasterData();
        this.treeListDetail.DataSource = (object) this.myStockWorkOrder.DataTableDetail;
        this.chkedtNextRecord.Visible = this.myStockWorkOrder.Action == StockWorkOrderAction.New && this.myEditOption.ShowProceedNewStockAssemblyOrder;
        this.SetControlState();
        this.FooterHeightAdjustment();
        this.RefreshLookupEdit();
        this.InitUndoManager();
        this.InitDelegate();
        if (this.myStockWorkOrder.DetailCount > 0)
          this.myStockHelper.GetBaseUOM(this.myStockWorkOrder.DataTableDetail.Rows[0]["ItemCode"].ToString());
        UDFUtil udfUtil = new UDFUtil(this.myDBSetting);
        TreeList treeList = this.treeListDetail;
        string tableName1 = "ASMORDERDTL";
        udfUtil.SetupTreeListControl(treeList, tableName1);
        XtraTabControl tabControl = this.tabControl1;
        string tableName2 = "ASMORDER";
        DataTable dataTableMaster = this.myStockWorkOrder.DataTableMaster;
        udfUtil.SetupTabControl(tabControl, tableName2, (object) dataTableMaster);
        this.splitterControl1.Visible = this.sBtnShowInstant.Text == BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_ShowFooter, new object[0]);
        this.mySaveInKIV = false;
        this.timer1.Interval = TempDocumentSetting.Default.AutoSaveSeconds * 1000;
        this.timer1.Enabled = true;
        this.Tag = (object) new FormStockWorkOrderEntry.MyFilterOption()
        {
          Document = this.myStockWorkOrder
        };
        this.FormInitialize();
        FormStockWorkOrderEntry.FormDataBindingEventArgs bindingEventArgs1 = new FormStockWorkOrderEntry.FormDataBindingEventArgs(this, this.myStockWorkOrder);
        ScriptObject scriptObject1 = this.myScriptObject;
        string name1 = "OnDataBinding";
        System.Type[] types1 = new System.Type[1];
        int index1 = 0;
        System.Type type1 = bindingEventArgs1.GetType();
        types1[index1] = type1;
        object[] objArray1 = new object[1];
        int index2 = 0;
        FormStockWorkOrderEntry.FormDataBindingEventArgs bindingEventArgs2 = bindingEventArgs1;
        objArray1[index2] = (object) bindingEventArgs2;
        scriptObject1.RunMethod(name1, types1, objArray1);
        ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
        string name2 = "OnDataBinding";
        System.Type[] types2 = new System.Type[1];
        int index3 = 0;
        System.Type type2 = bindingEventArgs1.GetType();
        types2[index3] = type2;
        object[] objArray2 = new object[1];
        int index4 = 0;
        FormStockWorkOrderEntry.FormDataBindingEventArgs bindingEventArgs3 = bindingEventArgs1;
        objArray2[index4] = (object) bindingEventArgs3;
        scriptObject2.RunMethod(name2, types2, objArray2);
      }
    }

    private void InitDelegate()
    {
      this.myStockWorkOrder.PauseUndoEvent += new PauseUndoDelegate(this.PauseUndo);
      this.myStockWorkOrder.ResumeUndoEvent += new ResumeUndoDelegate(this.ResumeUndo);
    }

    private void UnlinkDelegate()
    {
      this.myStockWorkOrder.PauseUndoEvent -= new PauseUndoDelegate(this.PauseUndo);
      this.myStockWorkOrder.ResumeUndoEvent -= new ResumeUndoDelegate(this.ResumeUndo);
    }

    private void FormInitialize()
    {
      if (this.myStockWorkOrder != null)
      {
        //BCE.AutoCount.Manufacturing.StockWorkOrder.FormInitializeEventArgs initializeEventArgs1 = new BCE.AutoCount.Manufacturing.StockWorkOrder.FormInitializeEventArgs(this.myStockWorkOrder, this.panelHeader, this.treeListDetail, this.tabControl1);
        //ScriptObject scriptObject = this.myStockWorkOrder.ScriptObject;
        //string name = "OnFormInitialize";
        //System.Type[] types = new System.Type[2];
        //int index1 = 0;
        //System.Type type1 = typeof (object);
        //types[index1] = type1;
        //int index2 = 1;
        //type2 = initializeEventArgs1.GetType();
        //types[index2] = type2;
        //object[] objArray = new object[2];
        //int index3 = 0;
        //FormStockWorkOrderEntry assemblyOrderEntry = this;
        //objArray[index3] = (object) assemblyOrderEntry;
        //int index4 = 1;
        //BCE.AutoCount.Manufacturing.StockWorkOrder.FormInitializeEventArgs initializeEventArgs2 = initializeEventArgs1;
        //objArray[index4] = (object) initializeEventArgs2;
        //scriptObject.RunMethod(name, types, objArray);
      }
    }

    private void CreateUCInquiry()
    {
      this.myUCInquiry = new UCInquiryStock();
      this.myUCInquiry.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.myUCInquiry.Location = new Point(0, 0);
      this.myUCInquiry.Name = "myUCInquiry";
      this.myUCInquiry.Size = new Size(this.label11.Left - 2, this.panBottom.Height);
      this.myUCInquiry.TabIndex = 6;
      this.myUCInquiry.TabStop = false;
      this.myUCInquiry.Visible = false;
      this.panBottom.Controls.Add((Control) this.myUCInquiry);
      this.myUCInquiry.Initialize(this.myDBSetting, "", "", "", "", InquiryType.Stock);
      this.ReloadInquiryUserControl();
    }

    private void ReloadInquiryUserControl()
    {
      if (this.myUCInquiry != null && this.myIsInstantInfoShown)
        this.myUCInquiry.Reload(this.myLastInquiryItemCode, this.myLastInquiryUOM, "", "");
    }

    private void BindingMasterData()
    {
      ControlsHelper.ClearDataBinding((IEnumerable) this.Controls);
      this.txtEdtStockAssemblyNo.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "DocNo"));
      this.textEdtRefDocNo.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "RefDocNo"));
      this.textEdtExpectedCompletedDate.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "ExpectedCompletedDate"));
      this.mruEdtDescription.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "Description"));
      this.memoEdtNote.DataBindings.Add(new Binding("rtf", (object) this.myStockWorkOrder.DataTableMaster, "Note"));
      this.textEdtTotal.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "Total"));
      this.txtedtAssemblyCost.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "AssemblyCost"));
      this.txtedtNetTotal.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "NetTotal"));
      this.txtedtQuantity.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "Qty"));
      this.dateEdtDate.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "DocDate"));
      this.luProject.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "ProjNo"));
      this.luAssemblyItem.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "ItemCode"));
      this.luDeptNo.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "DeptNo"));
      this.luBatchNo.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "BatchNo"));
      this.luLocation.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "Location"));
      this.externalLinkBox1.DataBindings.Add(new Binding("Links", (object) this.myStockWorkOrder.DataTableMaster, "ExternalLink"));
      this.mruEdtRemark1.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "Remark1"));
      this.mruEdtRemark2.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "Remark2"));
      this.mruEdtRemark3.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "Remark3"));
      this.mruEdtRemark4.DataBindings.Add(new Binding("EditValue", (object) this.myStockWorkOrder.DataTableMaster, "Remark4"));
      this.UpdateAssemblyItemDescription();
    }

    private void InitRemark()
    {
      this.myRemarkName = RemarkName.Create(this.myDBSetting);
      this.myRemarkNameEntity = this.myRemarkName.GetRemarkName("AS");
    }

    private void SetupRemark()
    {
      this.labelRemark1.Text = this.myRemarkNameEntity.GetRemarkName(1);
      this.labelRemark2.Text = this.myRemarkNameEntity.GetRemarkName(2);
      this.labelRemark3.Text = this.myRemarkNameEntity.GetRemarkName(3);
      this.labelRemark4.Text = this.myRemarkNameEntity.GetRemarkName(4);
      if (!this.myRemarkNameEntity.IsRemarkSupportMRU(1))
        this.iEditRemark1MRU.Enabled = false;
      if (!this.myRemarkNameEntity.IsRemarkSupportMRU(2))
        this.iEditRemark2MRU.Enabled = false;
      if (!this.myRemarkNameEntity.IsRemarkSupportMRU(3))
        this.iEditRemark3MRU.Enabled = false;
      if (!this.myRemarkNameEntity.IsRemarkSupportMRU(4))
        this.iEditRemark4MRU.Enabled = false;
    }

    private void InitMRUHelper()
    {
      this.myMRUHelper = MRUHelper.Create(this.myDBSetting, 13, this.mruEdtDescription.Properties);
      this.myMRUHelperRemark1 = MRUHelper.Create(this.myDBSetting, 46, this.mruEdtRemark1.Properties);
      this.myMRUHelperRemark2 = MRUHelper.Create(this.myDBSetting, 47, this.mruEdtRemark2.Properties);
      this.myMRUHelperRemark3 = MRUHelper.Create(this.myDBSetting, 48, this.mruEdtRemark3.Properties);
      this.myMRUHelperRemark4 = MRUHelper.Create(this.myDBSetting, 49, this.mruEdtRemark4.Properties);
    }

    private void InitUndoManager()
    {
      DataTable dataTableMaster = this.myStockWorkOrder.DataTableMaster;
      DataTable dataTableDetail = this.myStockWorkOrder.DataTableDetail;
      this.myMasterRecordUndo = new UndoManager(dataTableMaster, true);
      this.myMasterRecordUndo.ExcludeFields.Add((object) "Total");
      this.myMasterRecordUndo.ExcludeFields.Add((object) "NetTotal");
      this.myMasterRecordUndo.CanUndoChanged += new EventHandler(this.myMasterRecordUndo_CanUndoChanged);
      this.iUndoMaster.Enabled = this.myMasterRecordUndo.CanUndo;
      this.myDetailRecordUndo = new UndoManager(dataTableDetail, false, new FillRowValueDelegate(this.CalcSubTotal));
      this.myDetailRecordUndo.ExcludeFields.Add((object) "SubTotalCost");
      this.myDetailRecordUndo.CanUndoChanged += new EventHandler(this.myDetailRecordUndo_CanUndoChanged);
      this.sBtnUndoTree.Enabled = this.myDetailRecordUndo.CanUndo;
      this.myMasterRecordUndo.SaveState();
      this.myMasterRecordUndo.KeepCurrentChanges();
      this.myDetailRecordUndo.SaveState();
      this.myDetailRecordUndo.KeepCurrentChanges();
    }

    private void myMasterRecordUndo_CanUndoChanged(object sender, EventArgs e)
    {
      this.iUndoMaster.Enabled = this.myMasterRecordUndo.CanUndo;
    }

    private void myDetailRecordUndo_CanUndoChanged(object sender, EventArgs e)
    {
      this.sBtnUndoTree.Enabled = this.myDetailRecordUndo.CanUndo;
    }

    private void PauseUndo()
    {
      if (this.myMasterRecordUndo != null)
        this.myMasterRecordUndo.PauseCapture();
      if (this.myDetailRecordUndo != null)
        this.myDetailRecordUndo.PauseCapture();
      this.DisableAutoSave();
      this.treeListDetail.BeginUpdate();
    }

    private void ResumeUndo(bool captureChanges)
    {
      if (this.myMasterRecordUndo != null)
      {
        this.myMasterRecordUndo.ResumeCapture();
        if (!this.myMasterRecordUndo.IsPauseCapture())
          this.myMasterRecordUndo.SaveState();
        if (!captureChanges)
          this.myMasterRecordUndo.KeepCurrentChanges();
      }
      if (this.myDetailRecordUndo != null)
      {
        this.myDetailRecordUndo.ResumeCapture();
        if (!this.myDetailRecordUndo.IsPauseCapture())
          this.myDetailRecordUndo.SaveState();
        if (!captureChanges)
          this.myDetailRecordUndo.KeepCurrentChanges();
      }
      this.EnableAutoSave();
      this.treeListDetail.EndUpdate();
    }

    private void DisableAutoSave()
    {
      this.myDisableAutoSaveCounter = this.myDisableAutoSaveCounter + 1;
    }

    private void EnableAutoSave()
    {
      if (this.myDisableAutoSaveCounter > 0)
        this.myDisableAutoSaveCounter = this.myDisableAutoSaveCounter - 1;
    }

    private bool IsAutoSaveDisabled()
    {
      return this.myDisableAutoSaveCounter > 0;
    }

    private void SetDetailButtonState()
    {
      bool flag1 = this.myStockWorkOrder.Action == StockWorkOrderAction.View;
      bool flag2 = this.treeListDetail.Nodes.Count > 0;
      this.sBtnRemove.Enabled = !flag1 & flag2;
      this.sBtnMoveUp.Enabled = !flag1 & flag2;
      this.sBtnMoveDown.Enabled = !flag1 & flag2;
      this.sBtnInsert.Enabled = !flag1 & flag2;
      this.sBtnSelectAllMain.Enabled = !flag1 & flag2;
      this.panBOMOptional.Enabled = !flag1 & flag2;
    }

    private void AdjustBottomButtons()
    {
      Control[] controlArray = new Control[9];
      int index1 = 0;
      SimpleButton simpleButton1 = this.sbtnCancel;
      controlArray[index1] = (Control) simpleButton1;
      int index2 = 1;
      SimpleButton simpleButton2 = this.sbtnSavePrint;
      controlArray[index2] = (Control) simpleButton2;
      int index3 = 2;
      SimpleButton simpleButton3 = this.sbtnSavePreview;
      controlArray[index3] = (Control) simpleButton3;
      int index4 = 3;
      SimpleButton simpleButton4 = this.sbtnSave;
      controlArray[index4] = (Control) simpleButton4;
      int index5 = 4;
      SimpleButton simpleButton5 = this.sbtnDelete;
      controlArray[index5] = (Control) simpleButton5;
      int index6 = 5;
      SimpleButton simpleButton6 = this.sbtnCancelDoc;
      controlArray[index6] = (Control) simpleButton6;
      int index7 = 6;
      SimpleButton simpleButton7 = this.sbtnEdit;
      controlArray[index7] = (Control) simpleButton7;
      int index8 = 7;
      PrintButton printButton = this.btnPrint;
      controlArray[index8] = (Control) printButton;
      int index9 = 8;
      PreviewButton previewButton = this.btnPreview;
      controlArray[index9] = (Control) previewButton;
      int num = this.panel2.Width;
      foreach (Control control in controlArray)
      {
        if (control.Visible)
        {
          num = num - control.Width - 4;
          control.Left = num;
        }
      }
    }

    private void SetControlState()
    {
      bool bReadOnly = this.myStockWorkOrder.Action == StockWorkOrderAction.View;
      BCE.Controls.Utils.SetReadOnly((IEnumerable) this.Controls, bReadOnly);
      this.navigator.Visible = bReadOnly;
      this.sbtnSavePreview.Enabled = this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_DOC_REPORT_PREVIEW");
      this.sbtnSavePrint.Enabled = this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_DOC_REPORT_PRINT");
      this.sBtnAdd.Enabled = !bReadOnly;
      this.sBtnSearch.Enabled = !bReadOnly;
      this.sBtnRangeSetting.Enabled = !bReadOnly && this.treeListDetail.Selection.Count > 0;
      this.barbtnTransferFromSO.Enabled = !bReadOnly;
      this.treeListDetail.OptionsBehavior.Editable = !bReadOnly;
      this.barBtnConvertLevel.Enabled = !bReadOnly;
      this.SetDetailButtonState();
      this.btnPreview.Visible = bReadOnly;
      this.btnPrint.Visible = bReadOnly;
      this.sbtnEdit.Visible = bReadOnly;
      this.sbtnCancelDoc.Visible = bReadOnly;
      this.sbtnDelete.Visible = bReadOnly;
      this.sbtnSave.Visible = !bReadOnly;
      this.sbtnSavePreview.Visible = !bReadOnly;
      this.sbtnSavePrint.Visible = !bReadOnly;
      if (bReadOnly)
        this.sbtnCancel.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_Close, new object[0]);
      else
        this.sbtnCancel.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_Cancel, new object[0]);
      this.lblCancelled.Visible = this.myStockWorkOrder.Cancelled;
      if (this.myStockWorkOrder.Cancelled)
        this.sbtnCancelDoc.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_UncancelDocument, new object[0]);
      else
        this.sbtnCancelDoc.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_CancelDocument, new object[0]);
      this.panBOMOptional.Enabled = this.myStockWorkOrder.IsMultilevel;
      this.SetWindowCaption();
    }

    private void InvokeAccessRightChanged()
    {
      this.BeginInvoke((Delegate) new MethodInvoker(this.ShowDocNoFormatLookupEdit));
      this.SetModuleFeature();
    }

    private void ShowDocNoFormatLookupEdit()
    {
      if (this.myStockWorkOrder != null)
      {
        try
        {
          this.luEdtDocNoFormat.Visible = (string) this.myStockWorkOrder.DocNo == "<<New>>" && this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_CHANGE_DOCNO_FORMAT");
          this.txtEdtStockAssemblyNo.Enabled = this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_EDIT_DOCNO");
          this.myFilterByLocation = SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnableFilterByCurrentUserLocationInDataEntry;
          this.luLocation.Enabled = !this.myFilterByLocation;
          this.tlLocation.OptionsColumn.ReadOnly = this.myFilterByLocation;
        }
        catch (AppException ex)
        {
          AppMessage.ShowErrorMessage(ex.Message);
        }
      }
    }

    private void SetWindowCaption()
    {
      if (this.myStockWorkOrder != null)
      {
        this.ShowDocNoFormatLookupEdit();
        string str1;
        if ((string) this.myStockWorkOrder.DocNo == "<<New>>")
        {
          Document document = Document.CreateDocument(this.myDBSetting);
          // ISSUE: variable of a boxed type
          StockAssemblyOrderStringId  local =  StockAssemblyOrderStringId.Code_NextPossibleNo;
          object[] objArray = new object[1];
          int index = 0;
          string documentNo = document.GetDocumentNo("AO", this.luEdtDocNoFormat.Text, (DateTime) this.myStockWorkOrder.DocDate);
          objArray[index] = (object) documentNo;
          str1 = BCE.Localization.Localizer.GetString((Enum) local, objArray);
        }
        else
          str1 = string.Format(" - [{0}]", (object) this.myStockWorkOrder.DocNo);
        string str2 = this.myStockWorkOrder.Action != StockWorkOrderAction.View ? (this.myStockWorkOrder.Action != StockWorkOrderAction.Edit ? BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_NewStockAssemblyOrder, new object[0]) + str1 : BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_EditStockAssemblyOrder, new object[0]) + str1) : BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_ViewStockAssemblyOrder, new object[0]) + str1;
        string format = "{0} - {1} - {2} (Ver: {3})";
        object[] objArray1 = new object[4];
        int index1 = 0;
        string str3 = str2;
        objArray1[index1] = (object) str3;
        int index2 = 1;
        // ISSUE: variable of a boxed type
        DBString  local1 = this.myStockWorkOrder.Command.GeneralSetting.CompanyProfile.CompanyNameWithRemark;
        objArray1[index2] = (object) local1;
        int index3 = 2;
        string productName = OEM.GetCurrentOEM().ProductName;
        objArray1[index3] = (object) productName;
        int index4 = 3;
      //  string minorProductVersion = ProductVersion.GetMajorMinorProductVersion();
        //objArray1[index4] = (object) minorProductVersion;
        this.Text = BCE.AutoCount.WinForms.FormHelper.SetUniqueWindowCaption(string.Format(format, objArray1), (Form) this);
      }
    }

    private void InvokeModuleFeature(ModuleController controller)
    {
      this.BeginInvoke((Delegate) new MethodInvoker(this.SetModuleFeature));
    }

    private void SetModuleFeature()
    {
      ModuleController moduleController = ModuleControl.GetOrCreate(this.myDBSetting).ModuleController;
      bool flag = this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("SYS_BHV_VIEW_COST") && ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.BOM.Enable;
      this.barBtnConvertLevel.Visibility = moduleController.MultiLevelAssembly.Enable ? BarItemVisibility.Always : BarItemVisibility.Never;
      int num1 = this.txtedtQuantity.Left + this.txtedtQuantity.Width + 44;
      int num2 = num1;
      Point location;
      Size size;
      if (!moduleController.MultiLocationStock.Enable)
      {
        this.myColLocationVisibleIndex = this.tlLocation.VisibleIndex;
        this.tlLocation.VisibleIndex = -1;
      }
      else
      {
        this.tlLocation.VisibleIndex = this.myColLocationVisibleIndex;
        PanelControl panelControl = this.panLocation;
        int x = num1;
        location = this.panLocation.Location;
        int y = location.Y;
        Point point = new Point(x, y);
        panelControl.Location = point;
        int num3 = num1;
        size = this.panLocation.Size;
        int num4 = size.Width + 6;
        num1 = num3 + num4;
      }
      this.tlLocation.OptionsColumn.ShowInCustomizationForm = moduleController.MultiLocationStock.Enable;
      this.panLocation.Visible = moduleController.MultiLocationStock.Enable;
      if (!moduleController.BatchNo.Enable)
      {
        this.myColBatchNoVisibleIndex = this.tlBatchNo.VisibleIndex;
        this.tlBatchNo.VisibleIndex = -1;
      }
      else
      {
        this.tlBatchNo.VisibleIndex = this.myColBatchNoVisibleIndex;
        PanelControl panelControl = this.panBatch;
        int x = num1;
        location = this.panBatch.Location;
        int y = location.Y;
        Point point = new Point(x, y);
        panelControl.Location = point;
      }
      this.tlBatchNo.OptionsColumn.ShowInCustomizationForm = moduleController.BatchNo.Enable;
      this.panBatch.Visible = moduleController.BatchNo.Enable;
      if (!moduleController.Project.Enable)
      {
        this.myColProjNoVisibleIndex = this.tlProjNo.VisibleIndex;
        this.tlProjNo.VisibleIndex = -1;
      }
      else
      {
        this.tlProjNo.VisibleIndex = this.myColProjNoVisibleIndex;
        PanelControl panelControl = this.panProject;
        int x = num2;
        location = this.panProject.Location;
        int y = location.Y;
        Point point = new Point(x, y);
        panelControl.Location = point;
        int num3 = num2;
        size = this.panProject.Size;
        int num4 = size.Width + 6;
        num2 = num3 + num4;
      }
      this.tlProjNo.OptionsColumn.ShowInCustomizationForm = moduleController.Project.Enable;
      this.panProject.Visible = moduleController.Project.Enable;
      if (!moduleController.Department.Enable)
      {
        this.myColDeptNoVisibleIndex = this.tlDeptNo.VisibleIndex;
        this.tlDeptNo.VisibleIndex = -1;
      }
      else
      {
        this.tlDeptNo.VisibleIndex = this.myColDeptNoVisibleIndex;
        PanelControl panelControl = this.panDept;
        int x = num2;
        location = this.panDept.Location;
        int y = location.Y;
        Point point = new Point(x, y);
        panelControl.Location = point;
      }
      this.tlDeptNo.OptionsColumn.ShowInCustomizationForm = moduleController.Department.Enable;
      this.panDept.Visible = moduleController.Department.Enable;
      this.tlItemCost.Visible = flag;
      this.tlItemCost.OptionsColumn.ShowInCustomizationForm = flag;
      this.tlOverheadCost.Visible = flag;
      this.tlOverheadCost.OptionsColumn.ShowInCustomizationForm = flag;
      this.tlSubTotal.Visible = flag;
      this.tlSubTotal.OptionsColumn.ShowInCustomizationForm = flag;
      this.label12.Visible = flag;
      this.label11.Visible = flag;
      this.label10.Visible = flag;
      this.textEdtTotal.Visible = flag;
      this.txtedtAssemblyCost.Visible = flag;
      this.txtedtNetTotal.Visible = flag;
      this.FooterHeightAdjustment();
      if (this.myGridLayout != null)
        this.myGridLayout.SaveDefaultLayout();
    }

    private void FooterHeightAdjustment()
    {
      if (this.myStockWorkOrder != null && !this.myIsInstantInfoShown)
      {
        if ((!this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("SYS_BHV_VIEW_COST") ? 0 : (ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.BOM.Enable ? 1 : 0)) == 0)
          this.panBottom.Height = 0;
        else
          this.panBottom.Height = BCE.Data.Convert.ToInt32((object) Math.Round((double) this.AutoScaleDimensions.Height / 96.0 * 76.0));
      }
    }

    private void FormStockAssemblyEntry_Load(object sender, EventArgs e)
    {
      this.myIsLoadForm = true;
      this.myColLocationVisibleIndex = this.tlLocation.VisibleIndex;
      this.myColBatchNoVisibleIndex = this.tlBatchNo.VisibleIndex;
      this.myColProjNoVisibleIndex = this.tlProjNo.VisibleIndex;
      this.myColDeptNoVisibleIndex = this.tlDeptNo.VisibleIndex;
      this.SetModuleFeature();
      new CustomizeTreeListLayout(this.myDBSetting, this.Name, this.treeListDetail).PrintTreeList = false;
      this.tlLocation.OptionsColumn.ReadOnly = this.myFilterByLocation;
      ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.AddListener(new ModuleControllerListenerDelegate(this.InvokeModuleFeature), (Component) this);
      this.myStockWorkOrder.Command.UserAuthentication.AccessRight.AddListener(new AccessRightListenerDelegate(this.InvokeAccessRightChanged), (Component) this);
      this.InitFormControls();
      this.ucInquiryTreeReload();
      this.SetControlState();
      this.SetupRemark();
      this.luAssemblyItem.EditValueChanged += new EventHandler(this.luAssemblyItem_EditValueChanged);
    }

    private void EndCurrentEdit()
    {
      try
      {
        if (this.myStockWorkOrder != null)
        {
          CurrencyManager currencyManager = (CurrencyManager) this.BindingContext[(object) this.myStockWorkOrder.DataTableMaster];
          if (currencyManager != null)
          {
            currencyManager.EndCurrentEdit();
            currencyManager.Refresh();
          }
        }
      }
      catch (Exception ex)
      {
        AppMessage.ShowInformationMessage(ex.Message);
      }
    }

    private void luAssemblyItem_EditValueChanged(object sender, EventArgs e)
    {
      if (this.luAssemblyItem.EditValue != null && this.luAssemblyItem.EditValue != DBNull.Value && !(this.luAssemblyItem.EditValue.ToString() == ""))
      {
        this.myMasterItemBatchLookupEditBuilder.FilterItemCode(this.luAssemblyItem.EditValue.ToString(), (DateTime) this.myStockWorkOrder.DocDate);
        this.panBOMOptional.Enabled = this.myStockWorkOrder.IsMultilevel;
        this.UpdateAssemblyItemDescription();
        this.treeListDetail.BeginUpdate();
        try
        {
          this.BeginInvoke((Delegate) new MethodInvoker(this.EndCurrentEdit));
        }
        finally
        {
          this.treeListDetail.EndUpdate();
        }
        this.ucInquiryReloadAssemblyItem();
      }
    }

    private void SetupLookupEdit()
    {
      this.myMasterItemBatchLookupEditBuilder = new ItemBatchForReceiveLookupEditBuilder();
      this.myMasterItemBatchLookupEditBuilder.BuildLookupEdit(this.luBatchNo.Properties, this.myDBSetting);
      DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder.BuildLookupEdit(this.luProject.Properties, this.myDBSetting);
      DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DepartmentLookupEditBuilder.BuildLookupEdit(this.luDeptNo.Properties, this.myDBSetting);
      DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).LocationLookupEditBuilder.BuildLookupEdit(this.luLocation.Properties, this.myDBSetting);
      DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).BOMItemLookupEditBuilder.BuildLookupEdit(this.luAssemblyItem.Properties, this.myDBSetting);
      DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).StockAssemblyOrderDocNoFormatLookupEditBuilder.BuildLookupEdit(this.luEdtDocNoFormat.Properties, this.myDBSetting);
      DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchLookupEditBuilder.BuildLookupEdit(this.repLuedtBatchNo, this.myDBSetting);
      if (this.myUseLookupEditToInputItemCode && this.repLuedtItem.Columns.Count == 0)
        DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.BuildLookupEdit(this.repLuedtItem, this.myDBSetting);
      DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).LocationLookupEditBuilder.BuildLookupEdit(this.repLuedtLocation, this.myDBSetting);
      DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder.BuildLookupEdit(this.repLuedtProjNo, this.myDBSetting);
      DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DepartmentLookupEditBuilder.BuildLookupEdit(this.repLuedtDeptNo, this.myDBSetting);
    }

    private void UnlinkLookupEditEventHandlers()
    {
      if (this.luProject != null)
        DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder.UnlinkEventHandlers(this.luProject.Properties);
      if (this.luDeptNo != null)
        DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DepartmentLookupEditBuilder.UnlinkEventHandlers(this.luDeptNo.Properties);
      if (this.luLocation != null)
        DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).LocationLookupEditBuilder.UnlinkEventHandlers(this.luLocation.Properties);
      if (this.luAssemblyItem != null)
        DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).BOMItemLookupEditBuilder.UnlinkEventHandlers(this.luAssemblyItem.Properties);
      if (this.luEdtDocNoFormat != null)
        DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).StockAssemblyOrderDocNoFormatLookupEditBuilder.UnlinkEventHandlers(this.luEdtDocNoFormat.Properties);
      DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchLookupEditBuilder.UnlinkEventHandlers(this.repLuedtBatchNo);
      if (this.repLuedtItem.Columns.Count > 0)
        DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.UnlinkEventHandlers(this.repLuedtItem);
      DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).LocationLookupEditBuilder.UnlinkEventHandlers(this.repLuedtLocation);
      DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder.UnlinkEventHandlers(this.repLuedtProjNo);
      DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DepartmentLookupEditBuilder.UnlinkEventHandlers(this.repLuedtDeptNo);
    }

    private void RefreshLookupEdit()
    {
      try
      {
        this.myMasterItemBatchLookupEditBuilder.Refresh(this.luBatchNo.Properties);
        ProjectLookupEditBuilder lookupEditBuilder1 = DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ProjectLookupEditBuilder;
        RepositoryItemLookUpEdit[] lookupEdits1 = new RepositoryItemLookUpEdit[2];
        int index1 = 0;
        RepositoryItemLookUpEdit properties1 = this.luProject.Properties;
        lookupEdits1[index1] = properties1;
        int index2 = 1;
        RepositoryItemLookUpEdit repositoryItemLookUpEdit1 = this.repLuedtProjNo;
        lookupEdits1[index2] = repositoryItemLookUpEdit1;
        lookupEditBuilder1.Refresh(lookupEdits1);
        DepartmentLookupEditBuilder lookupEditBuilder2 = DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).DepartmentLookupEditBuilder;
        RepositoryItemLookUpEdit[] lookupEdits2 = new RepositoryItemLookUpEdit[2];
        int index3 = 0;
        RepositoryItemLookUpEdit properties2 = this.luDeptNo.Properties;
        lookupEdits2[index3] = properties2;
        int index4 = 1;
        RepositoryItemLookUpEdit repositoryItemLookUpEdit2 = this.repLuedtDeptNo;
        lookupEdits2[index4] = repositoryItemLookUpEdit2;
        lookupEditBuilder2.Refresh(lookupEdits2);
        LocationLookupEditBuilder lookupEditBuilder3 = DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).LocationLookupEditBuilder;
        RepositoryItemLookUpEdit[] lookupEdits3 = new RepositoryItemLookUpEdit[2];
        int index5 = 0;
        RepositoryItemLookUpEdit properties3 = this.luLocation.Properties;
        lookupEdits3[index5] = properties3;
        int index6 = 1;
        RepositoryItemLookUpEdit repositoryItemLookUpEdit3 = this.repLuedtLocation;
        lookupEdits3[index6] = repositoryItemLookUpEdit3;
        lookupEditBuilder3.Refresh(lookupEdits3);
        DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).BOMItemLookupEditBuilder.Refresh(this.luAssemblyItem.Properties);
        DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).StockAssemblyOrderDocNoFormatLookupEditBuilder.Refresh(this.luEdtDocNoFormat.Properties);
        DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchLookupEditBuilder.Refresh(this.repLuedtBatchNo);
        if (this.myUseLookupEditToInputItemCode && this.repLuedtItem.Columns.Count > 0)
          DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemLookupEditBuilder.Refresh(this.repLuedtItem);
      }
      catch (DataAccessException ex)
      {
        AppMessage.ShowErrorMessage(ex.Message);
      }
    }

    private void InitFormControls()
    {
      FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting);
      string fieldname1 = "Total";
      string fieldtype1 = "Price";
      formControlUtil.AddField(fieldname1, fieldtype1);
      string fieldname2 = "UnitCost";
      string fieldtype2 = "Cost";
      formControlUtil.AddField(fieldname2, fieldtype2);
      string fieldname3 = "ItemCost";
      string fieldtype3 = "Cost";
      formControlUtil.AddField(fieldname3, fieldtype3);
      string fieldname4 = "OverHeadCost";
      string fieldtype4 = "Cost";
      formControlUtil.AddField(fieldname4, fieldtype4);
      string fieldname5 = "SubTotalCost";
      string fieldtype5 = "Cost";
      formControlUtil.AddField(fieldname5, fieldtype5);
      string fieldname6 = "AssemblyCost";
      string fieldtype6 = "Cost";
      formControlUtil.AddField(fieldname6, fieldtype6);
      string fieldname7 = "Qty";
      string fieldtype7 = "Quantity";
      formControlUtil.AddField(fieldname7, fieldtype7);
      string fieldname8 = "Rate";
      string fieldtype8 = "Quantity";
      formControlUtil.AddField(fieldname8, fieldtype8);
      string fieldname9 = "SubTotal";
      string fieldtype9 = "Currency";
      formControlUtil.AddField(fieldname9, fieldtype9);
      string fieldname10 = "NetTotal";
      string fieldtype10 = "Currency";
      formControlUtil.AddField(fieldname10, fieldtype10);
      string fieldname11 = "ReqQty";
      string fieldtype11 = "Quantity";
      formControlUtil.AddField(fieldname11, fieldtype11);
      string fieldname12 = "OnHandQty";
      string fieldtype12 = "Quantity";
      formControlUtil.AddField(fieldname12, fieldtype12);
      string fieldname13 = "OnHandBal";
      string fieldtype13 = "Quantity";
      formControlUtil.AddField(fieldname13, fieldtype13);
      string fieldname14 = "AvailableQty";
      string fieldtype14 = "Quantity";
      formControlUtil.AddField(fieldname14, fieldtype14);
      string fieldname15 = "AvailableBal";
      string fieldtype15 = "Quantity";
      formControlUtil.AddField(fieldname15, fieldtype15);
      FormStockWorkOrderEntry assemblyOrderEntry = this;
      formControlUtil.InitControls((Control) assemblyOrderEntry);
      this.sBtnAdd.ToolTip = BCE.Localization.Localizer.GetString((Enum) BaseStringId.ToolTipAddDetail, new object[0]);
      this.sBtnInsert.ToolTip = BCE.Localization.Localizer.GetString((Enum) BaseStringId.ToolTipInsertBefore, new object[0]);
      this.sBtnRemove.ToolTip = BCE.Localization.Localizer.GetString((Enum) BaseStringId.ToolTipDeleteDetail, new object[0]);
      this.sBtnMoveUp.ToolTip = BCE.Localization.Localizer.GetString((Enum) BaseStringId.ToolTipMoveUp, new object[0]);
      this.sBtnMoveDown.ToolTip = BCE.Localization.Localizer.GetString((Enum) BaseStringId.ToolTipMoveDown, new object[0]);
      this.sBtnUndoTree.ToolTip = BCE.Localization.Localizer.GetString((Enum) BaseStringId.ToolTipUndoDetail, new object[0]);
      this.sBtnSelectAllMain.ToolTip = BCE.Localization.Localizer.GetString((Enum) BaseStringId.ToolTipSelectAllDetail, new object[0]);
      this.sBtnRangeSetting.ToolTip = BCE.Localization.Localizer.GetString((Enum) BaseStringId.ToolTipRangeSet, new object[0]);
      this.sBtnSearch.ToolTip = BCE.Localization.Localizer.GetString((Enum) BaseStringId.ToolTipItemSearch, new object[0]);
      this.sBtnShowInstant.ToolTip = BCE.Localization.Localizer.GetString((Enum) BaseStringId.ToolTipToggleInstantInfo, new object[0]);
      this.sbtnSave.ToolTip = BCE.Localization.Localizer.GetString((Enum) BaseStringId.ToolTipSave, new object[0]);
      this.sbtnSavePreview.ToolTip = BCE.Localization.Localizer.GetString((Enum) BaseStringId.ToolTipSaveAndPreview, new object[0]);
      this.sbtnSavePrint.ToolTip = BCE.Localization.Localizer.GetString((Enum) BaseStringId.ToolTipSaveAndPrint, new object[0]);
      this.sbtnCancel.ToolTip = BCE.Localization.Localizer.GetString((Enum) BaseStringId.ToolTipCancelOrClose, new object[0]);
    }

    private void FocusFirstControl()
    {
      this.mruEdtDescription.Focus();
    }

    private bool Save(SaveDocumentAction saveAction)
    {
      if (this.myStockWorkOrder == null || this.myStockWorkOrder.Action == StockWorkOrderAction.View || this.IsAutoSaveDisabled())
      {
        return false;
      }
      else
      {
        this.AcceptEdit();
        System.Windows.Forms.Application.DoEvents();
        bool flag = false;
        this.DisableAutoSave();
        FormStockWorkOrderEntry.FormBeforeSaveEventArgs beforeSaveEventArgs1 = new FormStockWorkOrderEntry.FormBeforeSaveEventArgs(this, this.myStockWorkOrder);
        ScriptObject scriptObject1 = this.myScriptObject;
        string name1 = "BeforeSave";
        System.Type[] types1 = new System.Type[1];
        int index1 = 0;
        System.Type type1 = beforeSaveEventArgs1.GetType();
        types1[index1] = type1;
        object[] objArray1 = new object[1];
        int index2 = 0;
        FormStockWorkOrderEntry.FormBeforeSaveEventArgs beforeSaveEventArgs2 = beforeSaveEventArgs1;
        objArray1[index2] = (object) beforeSaveEventArgs2;
        scriptObject1.RunMethod(name1, types1, objArray1);
        ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
        string name2 = "BeforeSave";
        System.Type[] types2 = new System.Type[1];
        int index3 = 0;
        System.Type type2 = beforeSaveEventArgs1.GetType();
        types2[index3] = type2;
        object[] objArray2 = new object[1];
        int index4 = 0;
        FormStockWorkOrderEntry.FormBeforeSaveEventArgs beforeSaveEventArgs3 = beforeSaveEventArgs1;
        objArray2[index4] = (object) beforeSaveEventArgs3;
        scriptObject2.RunMethod(name2, types2, objArray2);
        try
        {
          if (this.myMasterRecordUndo.CanUndo || this.myDetailRecordUndo.CanUndo || this.myStockWorkOrder.DataTableDetail.GetChanges() != null)
          {
            this.CheckToDeleteLastEmptyItem();
            foreach (DataRow dataRow in this.myStockWorkOrder.DataTableDetail.Select("IsBOMItem = 'T'"))
            {
              if (this.myStockWorkOrder.DataTableDetail.Select("ParentDtlKey=" + (object) BCE.Data.Convert.ToInt64(dataRow["DtlKey"])).Length == 0)
                dataRow["IsBOMItem"] = (object) "F";
            }
            if (ModuleControl.GetOrCreate(this.myDBSetting).ModuleController.AdvancedMultiUOM.Enable)
              UOMConversionHelper.Create(this.myDBSetting, "AO", this.luEdtDocNoFormat.Text, (DateTime) this.myStockWorkOrder.DocDate).ApplyUOMConversion(this.GetLeafRawMaterialTable(), this.myStockWorkOrder.DocKey, (string) this.myStockWorkOrder.DocNo);
            this.myStockWorkOrder.DocNoFormatName = this.luEdtDocNoFormat.Text;
            this.myStockWorkOrder.Save(this.myStockWorkOrder.Command.UserAuthentication.LoginUserID);
            this.myMRUHelper.SaveMRUItems();
            if (this.myRemarkNameEntity.IsRemarkSupportMRU(1) && this.myMRUHelperRemark1 != null)
              this.myMRUHelperRemark1.SaveMRUItems();
            if (this.myRemarkNameEntity.IsRemarkSupportMRU(2) && this.myMRUHelperRemark2 != null)
              this.myMRUHelperRemark2.SaveMRUItems();
            if (this.myRemarkNameEntity.IsRemarkSupportMRU(3) && this.myMRUHelperRemark3 != null)
              this.myMRUHelperRemark3.SaveMRUItems();
            if (this.myRemarkNameEntity.IsRemarkSupportMRU(4) && this.myMRUHelperRemark4 != null)
              this.myMRUHelperRemark4.SaveMRUItems();
          }
          if (saveAction != SaveDocumentAction.Save)
            this.PrintPreviewAfterSave(saveAction);
          flag = true;
        }
        catch (DBConcurrencyException ex)
        {
          DocumentEditHelper.HandleDBConcurrencyException(this.myDBSetting, "ASMOrder", this.myStockWorkOrder.DocKey);
        }
        catch (AppException ex)
        {
          AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
          if (ex.GetType() == typeof (TransferedQtyIsExceedLimit))
            this.txtedtQuantity.Focus();
        }
        finally
        {
          this.EnableAutoSave();
        }
        return flag;
      }
    }

    private void CheckToDeleteLastEmptyItem()
    {
      if (this.treeListDetail.Nodes.Count > 0)
      {
        int count = this.treeListDetail.Nodes.Count;
        if (this.treeListDetail.Nodes[count - 1][(object) "ItemCode"] == DBNull.Value && this.treeListDetail.Nodes[count - 1][(object) "Description"].ToString().Length == 0 && BCE.Data.Convert.ToDecimal(this.treeListDetail.Nodes[count - 1][(object) "SubTotal"]) == Decimal.Zero)
        {
          DataRow dataRow = this.myStockWorkOrder.DataTableDetail.Rows.Find(this.treeListDetail.Nodes[count - 1][(object) "DtlKey"]);
          if (dataRow != null)
            dataRow.Delete();
        }
      }
    }

    private void PrintPreviewAfterSave(SaveDocumentAction saveAction)
    {
      // ISSUE: variable of a boxed type
      StockWorkOrderString  local =  StockWorkOrderString.StockWorkOrder;
      object[] objArray1 = new object[1];
      int index1 = 0;
      string str = this.myStockWorkOrder.DocNo.ToString();
      objArray1[index1] = (object) str;
      ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString((Enum) local, objArray1), "MF_ASMORDER_DOC_REPORT_PRINT", "MF_ASMORDER_DOC_REPORT_EXPORT", "");
      reportInfo.DocType = "AO";
      reportInfo.DocKey = this.myStockWorkOrder.DocKey;
      reportInfo.UpdatePrintCountTableName = "ASMORDER";
      if (saveAction == SaveDocumentAction.SaveAndPreview)
      {
        reportInfo.CheckBeforePrintEvent += new CheckBeforeEventHandler(this.CheckBeforePrint);
        reportInfo.CheckBeforeExportEvent += new CheckBeforeEventHandler(this.CheckBeforeExport);
        BCE.AutoCount.Manufacturing.StockAssemblyOrder.BeforePreviewDocumentEventArgs documentEventArgs1 = new BCE.AutoCount.Manufacturing.StockAssemblyOrder.BeforePreviewDocumentEventArgs(reportInfo.EmailAndFaxInfo, reportInfo.DocKey, this.myDBSetting);
        ScriptObject scriptObject1 = this.myScriptObject;
        string name1 = "BeforePreviewDocument";
        System.Type[] types1 = new System.Type[1];
        int index2 = 0;
        System.Type type1 = documentEventArgs1.GetType();
        types1[index2] = type1;
        object[] objArray2 = new object[1];
        int index3 = 0;
        //BCE.AutoCount.Manufacturing.StockWorkOrder.BeforePreviewDocumentEventArgs documentEventArgs2 = documentEventArgs1;
       // objArray2[index3] = (object) documentEventArgs2;
       // scriptObject1.RunMethod(name1, types1, objArray2);
       // ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
       // string name2 = "BeforePreviewDocument";
       // System.Type[] types2 = new System.Type[1];
       // int index4 = 0;
       // System.Type type2 = documentEventArgs1.GetType();
       // types2[index4] = type2;
       // object[] objArray3 = new object[1];
       // int index5 = 0;
       //// BCE.AutoCount.Manufacturing.StockWorkOrder.BeforePreviewDocumentEventArgs documentEventArgs3 = documentEventArgs1;
       // objArray3[index5] = (object) documentEventArgs3;
       // scriptObject2.RunMethod(name2, types2, objArray3);
       // reportInfo.Tag = (object) documentEventArgs1;
        if (documentEventArgs1.AllowPreview)
          ReportTool.PreviewReport("Stock Assembly Order Document", this.myStockWorkOrder.Command.GetReportDataSource(this.myStockWorkOrder.DocKey), this.myDBSetting, true, true, this.myStockWorkOrder.Command.ReportOption, reportInfo);
      }
      else if (saveAction == SaveDocumentAction.SaveAndPrint)
        ReportTool.PrintReport("Stock Assembly Order Document", this.myStockWorkOrder.Command.GetReportDataSource(this.myStockWorkOrder.DocKey), this.myDBSetting, true, this.myStockWorkOrder.Command.ReportOption, reportInfo);
    }

    private DataTable GetLeafRawMaterialTable()
    {
      DataTable dataTable = this.myStockWorkOrder.DataTableDetail.Copy();
      for (int index = dataTable.Rows.Count - 1; index >= 0; --index)
      {
        DataRow row = dataTable.Rows[index];
        if (row.RowState == DataRowState.Deleted)
        {
          if (BCE.Data.Convert.TextToBoolean(row["IsBOMItem", DataRowVersion.Original]))
            dataTable.Rows.Remove(row);
        }
        else if (BCE.Data.Convert.TextToBoolean(row["IsBOMItem"]))
          dataTable.Rows.Remove(row);
      }
      return dataTable;
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      this.AcceptEdit();
      this.Close();
    }

    private void btnEdit_Click(object sender, EventArgs e)
    {
      if (this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_EDIT", (XtraForm) this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || (int) this.myStockWorkOrder.PrintCount <= 0 || this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_PRINTED_EDIT", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedEditPrintedDocument, new object[0]))))
      {
        try
        {
          this.myStockWorkOrder.Edit();
        }
        catch (StandardApplicationException ex)
        {
          AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
          return;
        }
        this.SetControlState();
        this.SwitchToEditMode();
        DBSetting dbSetting = this.myDBSetting;
        string docType = "AO";
        long docKey = this.myStockWorkOrder.DocKey;
        long eventKey = 0L;
        // ISSUE: variable of a boxed type
        StockWorkOrderString  local =  StockWorkOrderString.EditedStockWorkOrder;
        object[] objArray = new object[2];
        int index1 = 0;
        string loginUserId = this.myStockWorkOrder.Command.UserAuthentication.LoginUserID;
        objArray[index1] = (object) loginUserId;
        int index2 = 1;
        string str = this.myStockWorkOrder.DocNo.ToString();
        objArray[index2] = (object) str;
        string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
        string detail = "";
        Activity.Log(dbSetting, docType, docKey, eventKey, @string, detail);
      }
    }

    private void SwitchToEditMode()
    {
      if (this.myStockWorkOrder != null)
      {
        FormStockWorkOrderEntry.FormEventArgs formEventArgs1 = new FormStockWorkOrderEntry.FormEventArgs(this, this.myStockWorkOrder);
        ScriptObject scriptObject1 = this.myScriptObject;
        string name1 = "OnSwitchToEditMode";
        System.Type[] types1 = new System.Type[1];
        int index1 = 0;
        System.Type type1 = formEventArgs1.GetType();
        types1[index1] = type1;
        object[] objArray1 = new object[1];
        int index2 = 0;
        FormStockWorkOrderEntry.FormEventArgs formEventArgs2 = formEventArgs1;
        objArray1[index2] = (object) formEventArgs2;
        scriptObject1.RunMethod(name1, types1, objArray1);
        ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
        string name2 = "OnSwitchToEditMode";
        System.Type[] types2 = new System.Type[1];
        int index3 = 0;
        System.Type type2 = formEventArgs1.GetType();
        types2[index3] = type2;
        object[] objArray2 = new object[1];
        int index4 = 0;
        FormStockWorkOrderEntry.FormEventArgs formEventArgs3 = formEventArgs1;
        objArray2[index4] = (object) formEventArgs3;
        scriptObject2.RunMethod(name2, types2, objArray2);
        //FormEventArgs formEventArgs4 = new FormEventArgs(this.myStockWorkOrder, this.panelHeader, this.treeListDetail, this.tabControl1);
        //ScriptObject scriptObject3 = this.myStockWorkOrder.ScriptObject;
        //string name3 = "OnSwitchToEditMode";
        //System.Type[] types3 = new System.Type[2];
        //int index5 = 0;
        //System.Type type3 = typeof (object);
        //types3[index5] = type3;
        //int index6 = 1;
        //System.Type type4 = formEventArgs4.GetType();
        //types3[index6] = type4;
        //object[] objArray3 = new object[2];
        //int index7 = 0;
        //FormStockWorkOrderEntry assemblyOrderEntry = this;
        //objArray3[index7] = (object) assemblyOrderEntry;
        //int index8 = 1;
        //BCE.AutoCount.Manufacturing.StockWorkOrder.FormEventArgs formEventArgs5 = formEventArgs4;
        //objArray3[index8] = (object) formEventArgs5;
        //scriptObject3.RunMethod(name3, types3, objArray3);
      }
    }

    private void btnDelete_Click(object sender, EventArgs e)
    {
      if (this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_DELETE", (XtraForm) this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || (int) this.myStockWorkOrder.PrintCount <= 0 || this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_PRINTED_DELETE", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedDeletePrintedDocument, new object[0]))))
      {
        // ISSUE: variable of a boxed type
        StockAssemblyOrderStringId  local1 =  StockAssemblyOrderStringId.ConfirmMessage_DeleteStockAssemblyOrder;
        object[] objArray = new object[1];
        int index = 0;
        // ISSUE: variable of a boxed type
        DBString  local2 =this.myStockWorkOrder.DocNo;
        objArray[index] = (object) local2;
        if (AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString((Enum) local1, objArray)))
        {
          this.DisableAutoSave();
          try
          {
            BCE.Data.Convert.ToInt64(this.myStockWorkOrder.DataTableMaster.Rows[0]["DocKey"]);
            this.myStockWorkOrder.Delete();
            AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_DeleteSuccessfully, new object[0]));
            this.Close();
          }
          catch (DBConcurrencyException ex)
          {
            DocumentEditHelper.HandleDBConcurrencyExceptionWhenDeleteDocument();
          }
          catch (AppException ex)
          {
            AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
          }
          finally
          {
            this.EnableAutoSave();
          }
        }
      }
    }

    private void FormStockAssemblyEntry_Closing(object sender, CancelEventArgs e)
    {
      if (this.myStockWorkOrder != null)
      {
        if (this.myStockWorkOrder.Action != StockWorkOrderAction.View && this.Visible && !this.mySaveInKIV)
        {
          this.AcceptEdit();
          if (this.myMasterRecordUndo.CanUndo || this.myDetailRecordUndo.CanUndo || this.myStockWorkOrder.DataTableDetail.GetChanges() != null)
          {
            this.Activate();
            DialogResult dialogResult = AppMessage.ShowConfirmSaveChangesMessage();
            e.Cancel = dialogResult != DialogResult.Yes ? dialogResult != DialogResult.No : !this.Save(SaveDocumentAction.Save);
          }
        }
        if (!e.Cancel)
        {
          FormStockWorkOrderEntry.FormClosedEventArgs formClosedEventArgs1 = new FormStockWorkOrderEntry.FormClosedEventArgs(this, this.myStockWorkOrder);
          ScriptObject scriptObject1 = this.myScriptObject;
          string name1 = "OnFormClosed";
          System.Type[] types1 = new System.Type[1];
          int index1 = 0;
          System.Type type1 = formClosedEventArgs1.GetType();
          types1[index1] = type1;
          object[] objArray1 = new object[1];
          int index2 = 0;
          FormStockWorkOrderEntry.FormClosedEventArgs formClosedEventArgs2 = formClosedEventArgs1;
          objArray1[index2] = (object) formClosedEventArgs2;
          scriptObject1.RunMethod(name1, types1, objArray1);
          ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
          string name2 = "OnFormClosed";
          System.Type[] types2 = new System.Type[1];
          int index3 = 0;
          System.Type type2 = formClosedEventArgs1.GetType();
          types2[index3] = type2;
          object[] objArray2 = new object[1];
          int index4 = 0;
          FormStockWorkOrderEntry.FormClosedEventArgs formClosedEventArgs3 = formClosedEventArgs1;
          objArray2[index4] = (object) formClosedEventArgs3;
          scriptObject2.RunMethod(name2, types2, objArray2);
          this.Cleanup();
        }
      }
    }

    private void Cleanup()
    {
      this.timer1.Enabled = false;
      this.timer2.Enabled = false;
      if (this.Visible && !this.mySaveInKIV)
      {
        if (this.myStockWorkOrder.Action != StockWorkOrderAction.View)
        {
          try
          {
            this.DeleteTempDocument();
          }
          catch (Exception ex)
          {
            StandardExceptionHandler.WriteExceptionToErrorLog(ex);
          }
        }
      }
      this.treeListDetail.DataSource = (object) null;
      ControlsHelper.ClearAllDataBinding((IEnumerable) this.Controls);
      this.UnlinkDelegate();
      if (this.myStockWorkOrder != null)
      {
        this.myStockWorkOrder.PrepareForDispose();
        this.myStockWorkOrder = (StockWorkOrder) null;
      }
      if (this.myMasterRecordUndo != null)
      {
        this.myMasterRecordUndo.CanUndoChanged -= new EventHandler(this.myMasterRecordUndo_CanUndoChanged);
        this.myMasterRecordUndo.Dispose();
        this.myMasterRecordUndo = (UndoManager) null;
      }
      if (this.myDetailRecordUndo != null)
      {
        this.myDetailRecordUndo.CanUndoChanged -= new EventHandler(this.myDetailRecordUndo_CanUndoChanged);
        this.myDetailRecordUndo.Dispose();
        this.myDetailRecordUndo = (UndoManager) null;
      }
      if (this.timer1 != null)
        this.timer1.Dispose();
      if (this.timer2 != null)
        this.timer2.Dispose();
      if (this.myFormItemSearch != null)
      {
        this.myFormItemSearch.Dispose();
        this.myFormItemSearch = (FormItemSearch) null;
      }
    }

    private void chkedtNextRecord_CheckedChanged(object sender, EventArgs e)
    {
      this.SaveLocalSetting();
    }

    private void FormStockAssemblyEntry_KeyDown(object sender, KeyEventArgs e)
    {
      if (this.myStockWorkOrder != null)
      {
        if (e.Control && e.KeyCode == (Keys.LButton | Keys.ShiftKey | Keys.Space) && (this.sBtnShowInstant.Visible && this.sBtnShowInstant.Enabled))
        {
          this.sBtnShowInstant.PerformClick();
          e.Handled = true;
        }
        else if (e.Shift && e.Control)
        {
          if (e.KeyCode == (Keys) 77)
          {
            if (this.tabControl1.SelectedTabPage != this.tabPageMainTree)
              this.tabControl1.SelectedTabPage = this.tabPageMainTree;
            this.treeListDetail.Focus();
            e.Handled = true;
          }
          else if (e.KeyCode == (Keys) 69)
          {
            if (this.tabControl1.SelectedTabPage != this.tabPageExternalLink)
              this.tabControl1.SelectedTabPage = this.tabPageExternalLink;
            this.externalLinkBox1.Focus();
            e.Handled = true;
          }
          else if (e.KeyCode == (Keys) 78)
          {
            if (this.tabControl1.SelectedTabPage != this.tabPageNote)
              this.tabControl1.SelectedTabPage = this.tabPageNote;
            this.memoEdtNote.Focus();
            e.Handled = true;
          }
        }
        else if (this.myStockWorkOrder.Action != StockWorkOrderAction.View)
        {
          if (!e.Shift && !e.Control && !e.Alt)
          {
            if (e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space))
            {
              this.AddNewTreeDetail();
              this.treeListDetail.Focus();
              e.Handled = true;
            }
            else if (e.KeyCode == (Keys) 114)
            {
              this.sbtnSave.PerformClick();
              e.Handled = true;
            }
            else if (e.KeyCode == (Keys) 119)
            {
              this.sbtnSavePreview.PerformClick();
              e.Handled = true;
            }
            else if (e.KeyCode == (Keys) 118)
            {
              this.sbtnSavePrint.PerformClick();
              e.Handled = true;
            }
            else if (e.KeyCode == (Keys) 120 && this.sBtnSearch.Enabled)
            {
              this.sBtnSearch.PerformClick();
              e.Handled = true;
            }
            else if (e.KeyCode == (Keys) 123 && this.sBtnRangeSetting.Enabled)
            {
              this.sBtnRangeSetting.PerformClick();
              e.Handled = true;
            }
            else if (e.KeyCode == (Keys) 117)
            {
              Control control = this.ActiveControl;
              while (control != null && !(control is TabPage))
                control = control.Parent;
              if (control == null)
              {
                if (this.tabControl1.SelectedTabPage == this.tabPageMainTree)
                {
                  if (this.treeListDetail.CanFocus)
                    this.treeListDetail.Focus();
                }
                else if (this.tabControl1.SelectedTabPage == this.tabPageExternalLink)
                {
                  if (this.externalLinkBox1.CanFocus)
                    this.externalLinkBox1.Focus();
                }
                else if (this.memoEdtNote.CanFocus)
                  this.memoEdtNote.Focus();
              }
              else if (this.mruEdtDescription.CanFocus)
                this.mruEdtDescription.Focus();
            }
          }
          else if (e.Control && e.Alt)
          {
            if (e.KeyCode == (Keys) 65)
            {
              this.sBtnSelectAllMain.PerformClick();
              this.treeListDetail.Focus();
              e.Handled = true;
            }
          }
          else if (e.Alt && e.Shift)
          {
            if (e.KeyCode == (Keys.RButton | Keys.MButton | Keys.Space))
            {
              if (this.sBtnMoveUp.Enabled)
              {
                this.sBtnMoveUp.PerformClick();
                e.Handled = true;
              }
            }
            else if (e.KeyCode == (Keys.Back | Keys.Space) && this.sBtnMoveDown.Enabled)
            {
              this.sBtnMoveDown.PerformClick();
              e.Handled = true;
            }
          }
          else if (e.Control)
          {
            if (e.KeyCode == (Keys.RButton | Keys.MButton | Keys.Back | Keys.Space) && this.sBtnRemove.Enabled)
            {
              this.sBtnRemove.PerformClick();
              e.Handled = true;
            }
            else if (e.KeyCode == (Keys.LButton | Keys.MButton | Keys.Back | Keys.Space))
            {
              this.InsertTreeDetailBefore();
              this.treeListDetail.Focus();
              e.Handled = true;
            }
            else if (e.KeyCode == (Keys) 90 && this.sBtnUndoTree.Enabled)
            {
              this.sBtnUndoTree.PerformClick();
              e.Handled = true;
            }
            else if (e.KeyCode == (Keys) 70)
            {
              this.repBtnedtFurtherDesc_ButtonPressed((object) this.repBtnedtFurtherDesc, (ButtonPressedEventArgs) null);
              e.Handled = true;
            }
          }
        }
        else if (e.KeyCode == (Keys) 113 && this.sbtnEdit.Enabled)
        {
          this.sbtnEdit.PerformClick();
          e.Handled = true;
        }
        else if (e.KeyCode == (Keys) 119 && this.btnPreview.Enabled)
        {
          this.btnPreview.PerformClick();
          e.Handled = true;
        }
        else if (e.KeyCode == (Keys.RButton | Keys.MButton | Keys.Back | Keys.Space) && this.sbtnDelete.Enabled)
        {
          this.sbtnDelete.PerformClick();
          e.Handled = true;
        }
      }
    }

    private void FormStockAssemblyEntry_Activated(object sender, EventArgs e)
    {
      if (!this.mySkipExecuteFormActivated)
      {
        this.SetUseLookupEditToInputItemCode();
        if (this.myHasDeactivated)
          this.RefreshLookupEdit();
        else
          this.FocusFirstControl();
      }
    }

    private void btnCancelDoc_Click(object sender, EventArgs e)
    {
      if (this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_CANCEL", (XtraForm) this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || (int) this.myStockWorkOrder.PrintCount <= 0 || this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_PRINTED_CANCEL", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedCancelOrUncancelPrintedDocument, new object[0]))))
      {
        string string1;
        if (this.myStockWorkOrder.Cancelled)
        {
          // ISSUE: variable of a boxed type
          StockAssemblyOrderStringId  local1 = StockAssemblyOrderStringId.ConfirmMessage_UncancelStockAssemblyOrderAction;
          object[] objArray = new object[1];
          int index = 0;
          // ISSUE: variable of a boxed type
          DBString  local2 = this.myStockWorkOrder.DocNo;
          objArray[index] = (object) local2;
          string1 = BCE.Localization.Localizer.GetString((Enum) local1, objArray);
        }
        else
        {
          // ISSUE: variable of a boxed type
          StockAssemblyOrderStringId  local1 = StockAssemblyOrderStringId.ConfirmMessage_CancelStockAssemblyOrderAction;
          object[] objArray = new object[1];
          int index = 0;
          // ISSUE: variable of a boxed type
          DBString  local2 =  this.myStockWorkOrder.DocNo;
          objArray[index] = (object) local2;
          string1 = BCE.Localization.Localizer.GetString((Enum) local1, objArray);
        }
        if (AppMessage.ShowConfirmMessage(string1))
        {
          this.DisableAutoSave();
          try
          {
            if (this.myStockWorkOrder.Cancelled)
            {
              this.myStockWorkOrder.UncancelDocument(this.myStockWorkOrder.Command.UserAuthentication.LoginUserID);
              DBSetting dbSetting = this.myDBSetting;
              string docType = "AO";
              long docKey = this.myStockWorkOrder.DocKey;
              long eventKey = 0L;
              // ISSUE: variable of a boxed type
              StockWorkOrderString  local = StockWorkOrderString.UncancelStockWorkOrder;
              object[] objArray = new object[2];
              int index1 = 0;
              string loginUserId = this.myStockWorkOrder.Command.UserAuthentication.LoginUserID;
              objArray[index1] = (object) loginUserId;
              int index2 = 1;
              string str = this.myStockWorkOrder.DocNo.ToString();
              objArray[index2] = (object) str;
              string string2 = BCE.Localization.Localizer.GetString((Enum) local, objArray);
              string detail = "";
              Activity.Log(dbSetting, docType, docKey, eventKey, string2, detail);
            }
            else
            {
              this.myStockWorkOrder.CancelDocument(this.myStockWorkOrder.Command.UserAuthentication.LoginUserID);
              DBSetting dbSetting = this.myDBSetting;
              string docType = "AO";
              long docKey = this.myStockWorkOrder.DocKey;
              long eventKey = 0L;
              // ISSUE: variable of a boxed type
              StockWorkOrderString  local = StockWorkOrderString.CancelStockWorkOrder;
              object[] objArray = new object[2];
              int index1 = 0;
              string loginUserId = this.myStockWorkOrder.Command.UserAuthentication.LoginUserID;
              objArray[index1] = (object) loginUserId;
              int index2 = 1;
              string str = this.myStockWorkOrder.DocNo.ToString();
              objArray[index2] = (object) str;
              string string2 = BCE.Localization.Localizer.GetString((Enum) local, objArray);
              string detail = "";
              Activity.Log(dbSetting, docType, docKey, eventKey, string2, detail);
            }
            this.SetControlState();
          }
          catch (DBConcurrencyException ex)
          {
            DocumentEditHelper.HandleDBConcurrencyException(this.myDBSetting, "ASMOrder", this.myStockWorkOrder.DocKey);
          }
          catch (AppException ex)
          {
            AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
          }
          finally
          {
            this.EnableAutoSave();
          }
        }
      }
    }

    private void txtEdtStockAssemblyNo_EditValueChanged(object sender, EventArgs e)
    {
      this.SetWindowCaption();
    }

    private void ucInquiryTreeReload()
    {
      if (this.treeListDetail.FocusedNode != null && this.treeListDetail.Nodes.Count != 0)
      {
        this.myLastInquiryItemCode = this.treeListDetail.FocusedNode.GetDisplayText((object) "ItemCode");
        this.myLastInquiryUOM = this.myStockHelper.GetBaseUOM(this.myLastInquiryItemCode);
        this.ReloadInquiryUserControl();
      }
    }

    private void ucInquiryReloadAssemblyItem()
    {
      this.myLastInquiryItemCode = this.luAssemblyItem.Text;
      this.myLastInquiryUOM = this.myStockHelper.GetBaseUOM(this.myLastInquiryItemCode);
      this.ReloadInquiryUserControl();
    }

    private void AcceptEdit()
    {
      try
      {
        if (this.mruEdtDescription != null && this.mruEdtDescription.BindingManager != null)
          this.mruEdtDescription.BindingManager.EndCurrentEdit();
        this.UpdateTreeList();
      }
      catch (DataException ex)
      {
        AppMessage.ShowErrorMessage(ex.Message);
      }
    }

    private string GetSelectedDetailsXml()
    {
      DataSet selectedDetailsDataSet = this.GetSelectedDetailsDataSet();
      if (selectedDetailsDataSet != null)
      {
        StringWriter stringWriter = new StringWriter();
        selectedDetailsDataSet.WriteXml((TextWriter) stringWriter, XmlWriteMode.WriteSchema);
        return stringWriter.ToString();
      }
      else
        return "";
    }

    private DataSet GetSelectedDetailsDataSet()
    {
      this.AcceptEdit();
      if (this.treeListDetail.Selection.Count == 0)
      {
        return (DataSet) null;
      }
      else
      {
        DataTable dataTable1 = this.myStockWorkOrder.DataTableDetail.Copy();
        DataSet dataSet = this.myStockWorkOrder.StockWorkOrderDataSet.Clone();
        DataTable dataTable2 = dataSet.Tables[1];
        try
        {
          for (int index1 = 0; index1 < this.treeListDetail.Selection.Count; ++index1)
          {
            if (!dataTable2.Rows.Contains(this.treeListDetail.Selection[index1][(object) "DtlKey"]))
            {
              DataRow row = dataTable1.Rows.Find(this.treeListDetail.Selection[index1][(object) "DtlKey"]);
              dataTable2.ImportRow(row);
            }
            for (int index2 = 0; index2 < this.treeListDetail.Selection[index1].Nodes.Count; ++index2)
            {
              if (!dataTable2.Rows.Contains(this.treeListDetail.Selection[index1].Nodes[index2][(object) "DtlKey"]))
              {
                DataRow row = dataTable1.Rows.Find(this.treeListDetail.Selection[index1].Nodes[index2][(object) "DtlKey"]);
                dataTable2.ImportRow(row);
              }
            }
          }
        }
        catch (Exception ex)
        {
          AppMessage.ShowErrorMessage(ex.Message);
        }
        return dataSet;
      }
    }

    private void panelHeader_MouseDown(object sender, MouseEventArgs e)
    {
      if (this.myStockWorkOrder != null)
      {
        this.AcceptEdit();
        this.myLastDragDataSet = this.myStockWorkOrder.StockWorkOrderDataSet;
        int num = (int) this.DoDragDrop((object) new DocumentCarrier(this.myLastDragDataSet, true), DragDropEffects.Copy);
      }
    }

    private void FormStockAssemblyEntry_DragOver(object sender, DragEventArgs e)
    {
      if (this.myStockWorkOrder.Action == StockWorkOrderAction.View)
        e.Effect = DragDropEffects.None;
      else if (e.Data.GetDataPresent(typeof (DocumentCarrier)))
        e.Effect = DragDropEffects.Copy;
      else
        e.Effect = DragDropEffects.None;
    }

    private void Drop()
    {
      this.treeListDetail.BeginUpdate();
      try
      {
        this.myStockWorkOrder.ImportFromDataSet(this.myDropDocCarrier.CarriedDataSet, this.myDropDocCarrier.AcceptWholeDocument);
      }
      finally
      {
        this.treeListDetail.EndUpdate();
      }
    }

    private void FormStockAssemblyEntry_DragDrop(object sender, DragEventArgs e)
    {
      bool flag = false;
      if (e.Data.GetDataPresent(typeof (DocumentCarrier)))
      {
        this.myDropDocCarrier = (DocumentCarrier) e.Data.GetData(typeof (DocumentCarrier));
        if (this.myLastDragDataSet != this.myDropDocCarrier.CarriedDataSet)
        {
          this.AcceptEdit();
          if (!this.myDropDocCarrier.AcceptWholeDocument && this.myDropDocCarrier.CarriedDataSet.Tables.Count > 1)
          {
            if (this.myDropDocCarrier.CarriedDataSet.Tables[1].Select("IsBOMItem='T'").Length != 0)
              flag = true;
            if (flag && !this.myStockWorkOrder.IsMultilevel)
            {
              AppMessage.ShowInformationMessage(BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_MultilevelStatusNotMatched, new object[0]));
              return;
            }
          }
          this.ExecuteWithPauseUndo(new MethodInvoker(this.Drop));
        }
      }
    }

    private void barSubItem1_Popup(object sender, EventArgs e)
    {
      this.barItemCopySelectedDetails.Enabled = this.treeListDetail.Selection.Count > 0;
      this.barItemPasteItemDetailOnly.Enabled = this.myStockWorkOrder.Action != StockWorkOrderAction.View;
      this.barItemPasteWholeDocument.Enabled = this.myStockWorkOrder.Action != StockWorkOrderAction.View;
      this.barBtnSaveInKIVFolder.Enabled = this.myStockWorkOrder.Action != StockWorkOrderAction.View;
    }

    private void barItemCopyWholeDocument_ItemClick(object sender, ItemClickEventArgs e)
    {
      ClipboardHelper.SetDataObject((object) this.myStockWorkOrder.ExportAsXml());
    }

    private void barItemCopySelectedDetails_ItemClick(object sender, ItemClickEventArgs e)
    {
      ClipboardHelper.SetDataObject((object) this.GetSelectedDetailsXml());
    }

    private void PasteFromClipboard(bool wholeDocument)
    {
      IDataObject dataObject;
      try
      {
        dataObject = Clipboard.GetDataObject();
      }
      catch (Exception ex)
      {
        AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message + BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ErrorMessage_PleaseTryAgain, new object[0]));
        return;
      }
      if (dataObject.GetDataPresent(DataFormats.UnicodeText))
      {
        string str = (string) dataObject.GetData(DataFormats.UnicodeText);
        this.treeListDetail.BeginUpdate();
        try
        {
          this.myStockWorkOrder.ImportFromXml(str, wholeDocument);
        }
        catch (XmlException ex1)
        {
          try
          {
            if (!this.myStockWorkOrder.ImportFromTabDelimitedText(str, wholeDocument))
              AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_InvalidTextFormat, new object[0]));
          }
          catch (DataAccessException ex2)
          {
            AppMessage.ShowErrorMessage((IWin32Window) this, ex2.Message);
          }
        }
        finally
        {
          this.treeListDetail.EndUpdate();
        }
      }
    }

    private void PasteWholeDocument()
    {
      this.PasteFromClipboard(true);
    }

    private void barItemPasteWholeDocument_ItemClick(object sender, ItemClickEventArgs e)
    {
      this.ExecuteWithPauseUndo(new MethodInvoker(this.PasteWholeDocument));
    }

    private void PasteItemDetailOnly()
    {
      this.PasteFromClipboard(false);
    }

    private void barItemPasteItemDetailOnly_ItemClick(object sender, ItemClickEventArgs e)
    {
      this.ExecuteWithPauseUndo(new MethodInvoker(this.PasteItemDetailOnly));
    }

    private void barSubItem2_Popup(object sender, EventArgs e)
    {
      this.barItemCopyFrom.Enabled = this.myStockWorkOrder.Action != StockWorkOrderAction.View;
    }

    private void CopyFromStock()
    {
      using (FormStockWorkSearch stockAssemblySearch = new FormStockWorkSearch(this.myStockWorkOrder.Command, this.myDBSetting, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.CopyFromOtherStockAssemblyOrder, new object[0]), false))
      {
        if (stockAssemblySearch.ShowDialog((IWin32Window) this) == DialogResult.OK)
        {
          StockWorkOrder fromDoc = this.myStockWorkOrder.Command.Edit(stockAssemblySearch.SelectedDocKeys[0]);
          this.myStockWorkOrder.ImportFromDataSet(fromDoc.StockWorkOrderDataSet, true);
          FormStockWorkOrderEntry.AfterCopyFromOtherDocumentEventArgs documentEventArgs1 = new FormStockWorkOrderEntry.AfterCopyFromOtherDocumentEventArgs(this.myStockWorkOrder, fromDoc);
          ScriptObject scriptObject1 = this.myScriptObject;
          string name1 = "AfterCopyFromOtherDocument";
          System.Type[] types1 = new System.Type[1];
          int index1 = 0;
          System.Type type1 = documentEventArgs1.GetType();
          types1[index1] = type1;
          object[] objArray1 = new object[1];
          int index2 = 0;
          FormStockWorkOrderEntry.AfterCopyFromOtherDocumentEventArgs documentEventArgs2 = documentEventArgs1;
          objArray1[index2] = (object) documentEventArgs2;
          scriptObject1.RunMethod(name1, types1, objArray1);
          ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
          string name2 = "AfterCopyFromOtherDocument";
          System.Type[] types2 = new System.Type[1];
          int index3 = 0;
          System.Type type2 = documentEventArgs1.GetType();
          types2[index3] = type2;
          object[] objArray2 = new object[1];
          int index4 = 0;
          FormStockWorkOrderEntry.AfterCopyFromOtherDocumentEventArgs documentEventArgs3 = documentEventArgs1;
          objArray2[index4] = (object) documentEventArgs3;
          scriptObject2.RunMethod(name2, types2, objArray2);
        }
      }
    }

    private void barItemCopyFrom_ItemClick(object sender, ItemClickEventArgs e)
    {
      this.ExecuteWithPauseUndo(new MethodInvoker(this.CopyFromStock));
    }

    private void CopyToStock()
    {
      StockWorkOrder stockWorkOrder;
      try
      {
        if (!this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_NEW", true))
          return;
        else
          stockWorkOrder = this.myStockWorkOrder.Command.AddNew();
      }
      catch (DataAccessException ex)
      {
        AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
        stockWorkOrder = (StockWorkOrder) null;
      }
      if (stockWorkOrder != null)
      {
        stockWorkOrder.ImportFromDataSet(this.myStockWorkOrder.StockWorkOrderDataSet, true);
        FormStockWorkOrderEntry.AfterCopyToNewDocumentEventArgs documentEventArgs1 = new FormStockWorkOrderEntry.AfterCopyToNewDocumentEventArgs(stockWorkOrder, this.myStockWorkOrder);
        ScriptObject scriptObject1 = this.myScriptObject;
        string name1 = "AfterCopyToNewDocument";
        System.Type[] types1 = new System.Type[1];
        int index1 = 0;
        System.Type type1 = documentEventArgs1.GetType();
        types1[index1] = type1;
        object[] objArray1 = new object[1];
        int index2 = 0;
        FormStockWorkOrderEntry.AfterCopyToNewDocumentEventArgs documentEventArgs2 = documentEventArgs1;
        objArray1[index2] = (object) documentEventArgs2;
        scriptObject1.RunMethod(name1, types1, objArray1);
        ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
        string name2 = "AfterCopyToNewDocument";
        System.Type[] types2 = new System.Type[1];
        int index3 = 0;
        System.Type type2 = documentEventArgs1.GetType();
        types2[index3] = type2;
        object[] objArray2 = new object[1];
        int index4 = 0;
        FormStockWorkOrderEntry.AfterCopyToNewDocumentEventArgs documentEventArgs3 = documentEventArgs1;
        objArray2[index4] = (object) documentEventArgs3;
        scriptObject2.RunMethod(name2, types2, objArray2);
        FormStockWorkOrderCmd.StartEntryForm(stockWorkOrder);
      }
    }

    private void barItemCopyTo_ItemClick(object sender, ItemClickEventArgs e)
    {
      this.ExecuteWithPauseUndo(new MethodInvoker(this.CopyToStock));
    }

    private void UpdateAssemblyItemDescription()
    {
      if (this.luAssemblyItem.EditValue.ToString().Length == 0)
      {
        this.labelAssemblyItemDescription.Text = "";
      }
      else
      {
        DataTable dataTable = (DataTable) this.luAssemblyItem.Properties.DataSource;
        if (dataTable != null)
        {
          DataRow[] dataRowArray = dataTable.Select(string.Format("ItemCode = '{0}'", (object) StringHelper.ToSingleQuoteString(this.luAssemblyItem.EditValue.ToString())));
          if (dataRowArray.Length != 0)
            this.labelAssemblyItemDescription.Text = dataRowArray[0]["Description"].ToString();
          else
            this.labelAssemblyItemDescription.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ErrorMessage_CannotRetrieveCauseInactive, new object[0]);
        }
      }
    }

    private void iUndoMaster_ItemClick(object sender, ItemClickEventArgs e)
    {
      if (this.myStockWorkOrder.Action != StockWorkOrderAction.View)
      {
        Cursor current = Cursor.Current;
        Cursor.Current = Cursors.WaitCursor;
        try
        {
          this.myMasterRecordUndo.Undo();
        }
        finally
        {
          Cursor.Current = current;
        }
      }
    }

    private void CalcSubTotal(DataRow row, string[] columnChanged)
    {
      if (columnChanged.Length != 0)
        this.myStockWorkOrder.CalcSubTotal(row);
    }

    private void iEditMRUItem_ItemClick(object sender, ItemClickEventArgs e)
    {
      this.myMRUHelper.EditMRUItems();
    }

    private void ExecuteWithPauseUndo(MethodInvoker sDelegate)
    {
      Cursor current = Cursor.Current;
      Cursor.Current = Cursors.WaitCursor;
      if (this.myMasterRecordUndo != null)
        this.myMasterRecordUndo.PauseCapture();
      if (this.myDetailRecordUndo != null)
        this.myDetailRecordUndo.PauseCapture();
      try
      {
        sDelegate();
      }
      catch (AppException ex)
      {
        AppMessage.ShowErrorMessage((IWin32Window) this, ex.Message);
      }
      finally
      {
        if (this.myMasterRecordUndo != null)
        {
          this.myMasterRecordUndo.ResumeCapture();
          if (!this.myMasterRecordUndo.IsPauseCapture())
            this.myMasterRecordUndo.SaveState();
        }
        if (this.myDetailRecordUndo != null)
        {
          this.myDetailRecordUndo.ResumeCapture();
          if (!this.myDetailRecordUndo.IsPauseCapture())
            this.myDetailRecordUndo.SaveState();
        }
        Cursor.Current = current;
      }
    }

    private void luEdtDocNoFormat_EditValueChanged(object sender, EventArgs e)
    {
      this.SetWindowCaption();
    }

    private void repositoryItemLookUpEditItem_CloseUp(object sender, CloseUpEventArgs e)
    {
      if (e.AcceptValue)
        this.myItemCodeFromPopup = true;
    }

    private void FormStockAssemblyEntry_Deactivate(object sender, EventArgs e)
    {
      this.myHasDeactivated = true;
    }

    private void luAssemblyItem_Enter(object sender, EventArgs e)
    {
      this.ucInquiryReloadAssemblyItem();
      if (this.myStockWorkOrder.FromDocDtlKey > 0L)
        this.luAssemblyItem.Properties.ReadOnly = true;
      else
        this.luAssemblyItem.Properties.ReadOnly = false;
    }

    private void sbtnSave_Click(object sender, EventArgs e)
    {
      if (this.Save(SaveDocumentAction.Save))
        this.CheckContinueToNewDocument();
      else
        this.DialogResult = DialogResult.None;
    }

    private void CheckContinueToNewDocument()
    {
      if (this.chkedtNextRecord.Visible && this.chkedtNextRecord.Checked)
      {
        StockWorkOrder newStockWorkOrder = this.myStockWorkOrder.Command.AddNew();
        if (newStockWorkOrder != null)
        {
          this.SetStockWorkOrder(newStockWorkOrder);
          this.FocusFirstControl();
          return;
        }
      }
      this.Close();
    }

    private void sbtnSavePreview_Click(object sender, EventArgs e)
    {
      if (this.Save(SaveDocumentAction.SaveAndPreview))
        this.CheckContinueToNewDocument();
      else
        this.DialogResult = DialogResult.None;
    }

    private void sbtnSavePrint_Click(object sender, EventArgs e)
    {
      if (this.Save(SaveDocumentAction.SaveAndPrint))
        this.CheckContinueToNewDocument();
      else
        this.DialogResult = DialogResult.None;
    }

    private bool SaveTempDocument(string saveReason)
    {
      if (this.myInProcessTempDocument > 0)
      {
        return false;
      }
      else
      {
        FormStockWorkOrderEntry assemblyOrderEntry = this;
        bool lockTaken = false;
        try
        {
          Monitor.Enter((object) assemblyOrderEntry, ref lockTaken);
          this.myInProcessTempDocument = this.myInProcessTempDocument + 1;
          try
          {
            this.myStockWorkOrder.SaveToTempDocument(saveReason);
          }
          finally
          {
            if (this.myInProcessTempDocument > 0)
              this.myInProcessTempDocument = this.myInProcessTempDocument - 1;
          }
        }
        finally
        {
          if (lockTaken)
            Monitor.Exit((object) assemblyOrderEntry);
        }
        return true;
      }
    }

    private void DeleteTempDocument()
    {
      FormStockWorkOrderEntry assemblyOrderEntry = this;
      bool lockTaken = false;
      try
      {
        Monitor.Enter((object) assemblyOrderEntry, ref lockTaken);
        this.myInProcessTempDocument = this.myInProcessTempDocument + 1;
        try
        {
          TempDocument.Delete(this.myDBSetting, this.myStockWorkOrder.DocKey);
        }
        finally
        {
          if (this.myInProcessTempDocument > 0)
            this.myInProcessTempDocument = this.myInProcessTempDocument - 1;
        }
      }
      finally
      {
        if (lockTaken)
          Monitor.Exit((object) assemblyOrderEntry);
      }
    }

    private void barBtnSaveInKIVFolder_ItemClick(object sender, ItemClickEventArgs e)
    {
      this.AcceptEdit();
      try
      {
        if (this.SaveTempDocument("K.I.V."))
        {
          this.mySaveInKIV = true;
          this.Close();
        }
      }
      catch (AppException ex)
      {
        AppMessage.ShowErrorMessage(ex.Message);
      }
    }

    private void timer1_Tick(object sender, EventArgs e)
    {
      if (this.Visible && this.myStockWorkOrder.Action != StockWorkOrderAction.View && TempDocumentSetting.Default.EnableAutoSave)
      {
        if (!this.IsAutoSaveDisabled())
        {
          try
          {
            this.SaveTempDocument("Auto-save");
          }
          catch (Exception ex)
          {
            StandardExceptionHandler.WriteExceptionToErrorLog(ex);
          }
        }
      }
    }

    private void navigator_ButtonClick(object sender,BCE.Controls.NavigatorButtonClickEventArgs e)
    {
      StockWorkOrder newStockWorkOrder = (StockWorkOrder) null;
      try
      {
        if (e.ButtonType == BCE.Controls.NavigatorButtonType.First)
          newStockWorkOrder = this.myStockWorkOrder.Command.ViewFirst();
        else if (e.ButtonType == BCE.Controls.NavigatorButtonType.Prev)
          newStockWorkOrder = this.myStockWorkOrder.Command.ViewPrev((string) this.myStockWorkOrder.DocNo);
        else if (e.ButtonType == BCE.Controls.NavigatorButtonType.Next)
          newStockWorkOrder = this.myStockWorkOrder.Command.ViewNext((string) this.myStockWorkOrder.DocNo);
        else if (e.ButtonType == BCE.Controls.NavigatorButtonType.Last)
          newStockWorkOrder = this.myStockWorkOrder.Command.ViewLast();
      }
      catch (AppException ex)
      {
        AppMessage.ShowErrorMessage(ex.Message);
        return;
      }
      if (newStockWorkOrder != null)
        this.SetStockWorkOrder(newStockWorkOrder);
    }

    private void barbtnTransferFromSO_ItemClick(object sender, ItemClickEventArgs e)
    {
      Cursor current = Cursor.Current;
      Cursor.Current = Cursors.WaitCursor;
      this.treeListDetail.BeginUpdate();
      try
      {
        using (FormTransferDoc formTransferDoc = new FormTransferDoc(this.myStockWorkOrder))
        {
          int num = (int) formTransferDoc.ShowDialog();
        }
      }
      finally
      {
        this.treeListDetail.EndUpdate();
        Cursor.Current = current;
      }
    }

    private void barbtnCheckTransferredToStatus_ItemClick(object sender, ItemClickEventArgs e)
    {
      Cursor current = Cursor.Current;
      Cursor.Current = Cursors.WaitCursor;
      try
      {
        using (FormTransferredToStatus transferredToStatus = new FormTransferredToStatus(this.myStockWorkOrder))
        {
          int num = (int) transferredToStatus.ShowDialog();
        }
      }
      finally
      {
        Cursor.Current = current;
      }
    }

    private void previewButton1_Preview(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
    {
      if (this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_DOC_REPORT_PREVIEW", (XtraForm) this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || (int) this.myStockWorkOrder.PrintCount <= 0 || this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_PRINTED_PREVIEW", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedPreviewPrintedDocument, new object[0]))))
      {
        // ISSUE: variable of a boxed type
        StockWorkOrderString  local =  StockWorkOrderString.StockWorkOrder;
        object[] objArray1 = new object[1];
        int index1 = 0;
        string str = this.myStockWorkOrder.DocNo.ToString();
        objArray1[index1] = (object) str;
        ReportInfo reportInfo = new ReportInfo(BCE.Localization.Localizer.GetString((Enum) local, objArray1), "MF_ASMORDER_DOC_REPORT_PRINT", "MF_ASMORDER_DOC_REPORT_EXPORT", "");
        reportInfo.DocType = "AO";
        reportInfo.DocKey = this.myStockWorkOrder.DocKey;
        reportInfo.UpdatePrintCountTableName = "ASMORDER";
        reportInfo.CheckBeforePrintEvent += new CheckBeforeEventHandler(this.CheckBeforePrint);
        reportInfo.CheckBeforeExportEvent += new CheckBeforeEventHandler(this.CheckBeforeExport);
        reportInfo.EmailAndFaxInfo = StockWorkOrderCommand.GetEmailAndFaxInfo(this.myStockWorkOrder.DocKey, this.myDBSetting);
        //BCE.AutoCount.Manufacturing.StockWorkOrder.BeforePreviewDocumentEventArgs documentEventArgs1 = new BCE.AutoCount.Manufacturing.StockWorkOrder.BeforePreviewDocumentEventArgs(reportInfo.EmailAndFaxInfo, reportInfo.DocKey, this.myDBSetting);
        //ScriptObject scriptObject1 = this.myScriptObject;
        //string name1 = "BeforePreviewDocument";
        //System.Type[] types1 = new System.Type[1];
        //int index2 = 0;
        //System.Type type1 = documentEventArgs1.GetType();
        //types1[index2] = type1;
        //object[] objArray2 = new object[1];
        //int index3 = 0;
        //BCE.AutoCount.Manufacturing.StockWorkOrder.BeforePreviewDocumentEventArgs documentEventArgs2 = documentEventArgs1;
        //objArray2[index3] = (object) documentEventArgs2;
        //scriptObject1.RunMethod(name1, types1, objArray2);
        //ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
        //string name2 = "BeforePreviewDocument";
        //System.Type[] types2 = new System.Type[1];
        //int index4 = 0;
        //System.Type type2 = documentEventArgs1.GetType();
        //types2[index4] = type2;
        //object[] objArray3 = new object[1];
        //int index5 = 0;
        //BCE.AutoCount.Manufacturing.StockWorkOrder.BeforePreviewDocumentEventArgs documentEventArgs3 = documentEventArgs1;
        //objArray3[index5] = (object) documentEventArgs3;
        //scriptObject2.RunMethod(name2, types2, objArray3);
        //reportInfo.Tag = (object) documentEventArgs1;
        ReportTool.PreviewReport("Stock Assembly Order Document", this.myStockWorkOrder.Command.GetReportDataSource(this.myStockWorkOrder.DocKey), this.myDBSetting, e.DefaultReport, false, this.myStockWorkOrder.Command.ReportOption, reportInfo);
      }
    }

    private void printButton1_Print(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
    {
      if (this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_DOC_REPORT_PRINT", (XtraForm) this) && (!SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight || (int) this.myStockWorkOrder.PrintCount <= 0 || this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_PRINTED_PRINT", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedPrintPrintedDocument, new object[0]))))
      {
        // ISSUE: variable of a boxed type
        StockWorkOrderString  local =  StockWorkOrderString.StockWorkOrder;
        object[] objArray = new object[1];
        int index = 0;
        string str = this.myStockWorkOrder.DocNo.ToString();
        objArray[index] = (object) str;
        ReportTool.PrintReport("Stock Assembly Order Document", this.myStockWorkOrder.Command.GetReportDataSource(this.myStockWorkOrder.DocKey), this.myDBSetting, e.DefaultReport, this.myStockWorkOrder.Command.ReportOption, new ReportInfo(BCE.Localization.Localizer.GetString((Enum) local, objArray), "MF_ASMORDER_DOC_REPORT_PRINT", "MF_ASMORDER_DOC_REPORT_EXPORT", "")
        {
          DocType = "AO",
          DocKey = this.myStockWorkOrder.DocKey,
          UpdatePrintCountTableName = "ASMORDER",
          EmailAndFaxInfo = StockWorkOrderCommand.GetEmailAndFaxInfo(this.myStockWorkOrder.DocKey, this.myDBSetting)
        });
      }
    }

    private void sBtnAdd_Click(object sender, EventArgs e)
    {
      this.ExecuteWithPauseUndo(new MethodInvoker(this.AddNewTreeDetail));
    }

    private void repLuedtItem_EditValueChanged(object sender, EventArgs e)
    {
      if (this.myItemCodeFromPopup)
      {
        this.UpdateTreeList();
        this.myItemCodeFromPopup = false;
      }
      this.ucInquiryTreeReload();
    }

    private void AddNewTreeDetail()
    {
      if (this.tabControl1.SelectedTabPage != this.tabPageMainTree)
        this.tabControl1.SelectedTabPage = this.tabPageMainTree;
      this.UpdateTreeList();
      this.myStockWorkOrder.AddDetail();
      this.treeListDetail.Focus();
    }

    private void sBtnInsert_Click(object sender, EventArgs e)
    {
      this.ExecuteWithPauseUndo(new MethodInvoker(this.InsertTreeDetailBefore));
    }

    private void repLuedtItem_CloseUp(object sender, CloseUpEventArgs e)
    {
      if (e.AcceptValue)
        this.myItemCodeFromPopup = true;
    }

    private void InsertTreeDetailBefore()
    {
      this.UpdateTreeList();
      TreeListNode focusedNode = this.treeListDetail.FocusedNode;
      if (focusedNode != null)
      {
        this.myStockWorkOrder.InsertDetailBefore(focusedNode);
        this.SortTreeList();
        this.treeListDetail.Focus();
      }
    }

    private void UpdateTreeList()
    {
      this.treeListDetail.CloseEditor();
      this.treeListDetail.EndCurrentEdit();
    }

    private void SortTreeList()
    {
      this.treeListDetail.BeginSort();
      this.treeListDetail.EndSort();
    }

    private void sBtnRemove_Click(object sender, EventArgs e)
    {
      this.ExecuteWithPauseUndo(new MethodInvoker(this.DeleteTreeSelectedRows));
    }

    private void DeleteTreeSelectedRows()
    {
      this.UpdateTreeList();
      IEnumerator enumerator = this.treeListDetail.Selection.GetEnumerator();
      ArrayList arrayList = new ArrayList();
      while (enumerator.MoveNext())
      {
        TreeListNode treeListNode = (TreeListNode) enumerator.Current;
        arrayList.Add(treeListNode[(object) "DtlKey"]);
        if (BCE.Data.Convert.ToDecimal(treeListNode[(object) "TransferedQty"]) > Decimal.Zero)
        {
          FormStockWorkOrderEntry assemblyOrderEntry = this;
          // ISSUE: variable of a boxed type
          StockAssemblyOrderStringId  local = StockAssemblyOrderStringId.ErrorMessage_NotAllowDeleteCauseAlreadyTransfer;
          object[] objArray = new object[1];
          int index = 0;
          string str = treeListNode[(object) "ItemCode"].ToString();
          objArray[index] = (object) str;
          string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
          AppMessage.ShowErrorMessage((IWin32Window) assemblyOrderEntry, @string);
          return;
        }
      }
      if (arrayList.Count != 0 && AppMessage.ShowConfirmMessage(BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ConfirmMessage_DeleteSelectedRecord, new object[0])))
      {
        for (int index = 0; index < arrayList.Count; ++index)
        {
          DataRow dataRow = this.myStockWorkOrder.DataTableDetail.Rows.Find((object) System.Convert.ToInt64(arrayList[index]));
          if (dataRow != null)
            dataRow.Delete();
        }
        this.SetControlState();
      }
    }

    private void repLuedtBatchNo_QueryPopUp(object sender, CancelEventArgs e)
    {
      this.FilterTreeItemBatch();
    }

    private void FilterTreeItemBatch()
    {
      if (this.treeListDetail.FocusedColumn != null && this.treeListDetail.FocusedColumn.FieldName == "BatchNo")
        DefaultLookupEditBuilder.GetOrCreate(this.myDBSetting).ItemBatchLookupEditBuilder.FilterItemCode(this.treeListDetail.FocusedNode[(object) "ItemCode"].ToString(), (DateTime) this.myStockWorkOrder.DocDate);
    }

    private void sBtnMoveUp_Click(object sender, EventArgs e)
    {
      this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveUpNode));
    }

    private void MoveUpNode()
    {
      TreeListNodes treeListNodes = this.treeListDetail.FocusedNode.ParentNode == null ? this.treeListDetail.Nodes : this.treeListDetail.FocusedNode.ParentNode.Nodes;
      int num = treeListNodes.IndexOf(this.treeListDetail.FocusedNode);
      if (treeListNodes.Count >= 2 && num != 0)
      {
        object obj1 = treeListNodes[num - 1][(object) "Seq"];
        object obj2 = this.treeListDetail.FocusedNode[(object) "Seq"];
        this.treeListDetail.FocusedNode[(object) "Seq"] = obj1;
        treeListNodes[num - 1][(object) "Seq"] = obj2;
      }
    }

    private void sBtnMoveDown_Click(object sender, EventArgs e)
    {
      this.ExecuteWithPauseUndo(new MethodInvoker(this.MoveDownNode));
    }

    private void MoveDownNode()
    {
      TreeListNodes treeListNodes = this.treeListDetail.FocusedNode.ParentNode == null ? this.treeListDetail.Nodes : this.treeListDetail.FocusedNode.ParentNode.Nodes;
      int num = treeListNodes.IndexOf(this.treeListDetail.FocusedNode);
      if (treeListNodes.Count >= 2 && num != treeListNodes.Count - 1)
      {
        object obj1 = treeListNodes[num + 1][(object) "Seq"];
        object obj2 = this.treeListDetail.FocusedNode[(object) "Seq"];
        this.treeListDetail.FocusedNode[(object) "Seq"] = obj1;
        treeListNodes[num + 1][(object) "Seq"] = obj2;
      }
    }

    private void sBtnSelectAllMain_Click(object sender, EventArgs e)
    {
      ArrayList arrayList = new ArrayList();
      foreach (object obj in this.treeListDetail.Nodes)
        arrayList.Add(obj);
      this.treeListDetail.Selection.Set((IEnumerable) arrayList);
    }

    private void sBtnSearch_Click(object sender, EventArgs e)
    {
      this.ExecuteWithPauseUndo(new MethodInvoker(this.ItemSearchTree));
    }

    private void ItemSearchTree()
    {
      this.timer2.Stop();
      if (this.myFormItemSearch == null)
        this.myFormItemSearch = new FormItemSearch(this.myDBSetting, BCE.AutoCount.Stock.Item.Action.GeneralSearch);
      int num = (int) this.myFormItemSearch.ShowDialog((IWin32Window) this);
      if (this.myFormItemSearch.DialogResult == DialogResult.OK)
      {
        this.treeListDetail.BeginUpdate();
        try
        {
          foreach (StockItemListElement stockItemListElement in this.myFormItemSearch.ResultList)
          {
            this.myStockWorkOrder.BeginLoadDetailData();
            StockWorkOrderDetail assemblyOrderDetail = this.myStockWorkOrder.AddDetail();
            this.myStockWorkOrder.EndLoadDetailData();
            assemblyOrderDetail.ItemCode = stockItemListElement.ItemCode;
            if (stockItemListElement.Description != DBNull.Value)
              assemblyOrderDetail.Description = (DBString) stockItemListElement.Description.ToString();
            if (stockItemListElement.FurtherDescription != DBNull.Value)
              assemblyOrderDetail.FurtherDescription = (DBString) stockItemListElement.FurtherDescription.ToString();
            if (stockItemListElement.Cost != DBNull.Value)
              assemblyOrderDetail.ItemCost = stockItemListElement.Cost;
            assemblyOrderDetail.Location = (DBString) this.myStockWorkOrder.Command.UserAuthentication.MainLocation;
            StockWorkOrderRecalculateDetailEventArgs recalculateDetailEventArgs1 = new StockWorkOrderRecalculateDetailEventArgs(this.myDBSetting, assemblyOrderDetail.Row);
            ScriptObject scriptObject = this.myStockWorkOrder.ScriptObject;
            string name = "OnRecalculateDetail";
            System.Type[] types = new System.Type[1];
            int index1 = 0;
            System.Type type = recalculateDetailEventArgs1.GetType();
            types[index1] = type;
            object[] objArray = new object[1];
            int index2 = 0;
            StockWorkOrderRecalculateDetailEventArgs recalculateDetailEventArgs2 = recalculateDetailEventArgs1;
            objArray[index2] = (object) recalculateDetailEventArgs2;
            scriptObject.RunMethod(name, types, objArray);
            int seq = assemblyOrderDetail.Seq;
          }
        }
        finally
        {
          this.treeListDetail.EndUpdate();
        }
        this.treeListDetail.Focus();
      }
      this.timer2.Start();
    }

    private void sBtnUndoTree_Click(object sender, EventArgs e)
    {
      if (this.myStockWorkOrder.Action != StockWorkOrderAction.View)
      {
        Cursor current = Cursor.Current;
        Cursor.Current = Cursors.WaitCursor;
        try
        {
          this.treeListDetail.BeginUpdate();
          this.myStockWorkOrder.BeginLoadDetailData();
          this.myDetailRecordUndo.Undo();
          this.myStockWorkOrder.EndLoadDetailData();
          this.treeListDetail.EndUpdate();
        }
        finally
        {
          Cursor.Current = current;
        }
      }
    }

    private void sBtnRangeSetting_Click(object sender, EventArgs e)
    {
      this.ExecuteWithPauseUndo(new MethodInvoker(this.TreeRangeSet));
    }

    private void TreeRangeSet()
    {
      this.myStockWorkOrder.BeginLoadDetailData();
      IEnumerator enumerator = this.treeListDetail.Selection.GetEnumerator();
      TreeListNodes selectedNodes = new TreeListNodes(this.treeListDetail);
      while (enumerator.MoveNext())
      {
        TreeListNode node = (TreeListNode) enumerator.Current;
        this.CheckChildNodes(ref selectedNodes, node);
      }
      FormStockWorkOrderRangeSet.RangeSet((IWin32Window) this, this.treeListDetail.FocusedNode, selectedNodes, this.myDBSetting, new FillRowValueDelegate(this.CalcSubTotal));
      this.myStockWorkOrder.EndLoadDetailData();
    }

    private void CheckChildNodes(ref TreeListNodes selectedNodes, TreeListNode node)
    {
      selectedNodes.Add(node);
      foreach (TreeListNode node1 in node.Nodes)
        this.CheckChildNodes(ref selectedNodes, node1);
    }

    private void ToggleMultiLevel()
    {
      if (this.myIsLoadForm)
        this.BeginInvoke((Delegate) new MethodInvoker(this.EndCurrentEdit));
    }

    private void sBtnShowInstant_Click(object sender, EventArgs e)
    {
      this.myIsInstantInfoShown = !this.myIsInstantInfoShown;
      if (this.myUCInquiry == null)
        this.CreateUCInquiry();
      this.myUCInquiry.Visible = !this.myUCInquiry.Visible;
      this.splitterControl1.Visible = this.myUCInquiry.Visible;
      if (this.myUCInquiry.Visible)
      {
        this.panBottom.Height = BCE.Data.Convert.ToInt32((object) Math.Round((double) this.AutoScaleDimensions.Height / 96.0 * 156.0));
        this.sBtnShowInstant.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_ShowFooter, new object[0]);
      }
      else
      {
        this.FooterHeightAdjustment();
        this.sBtnShowInstant.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_ShowInstantInfo, new object[0]);
      }
    }

    private void treeListDetail_FocusedNodeChanged(object sender, FocusedNodeChangedEventArgs e)
    {
      if (this.treeListDetail.Nodes.Count != 0 && this.treeListDetail.FocusedNode != null)
      {
        this.FilterTreeItemBatch();
        this.ucInquiryTreeReload();
        this.SetDetailButtonState();
      }
    }

    private void sBtnApplyBOMOptional_Click(object sender, EventArgs e)
    {
      this.ApplyBOMOptional();
    }

    private void ApplyBOMOptional()
    {
      FormBOMOptional formBomOptional = (FormBOMOptional) null;
      TreeListNode treeListNode = (TreeListNode) null;
      TreeListNodes childNodes = (TreeListNodes) null;
      object docKey = (object) null;
      DataTable tableAsmbomOptional = this.myStockWorkOrder.DataTableASMBOMOptional;
      Decimal defaultQty = new Decimal();
      if (this.chkedtChildItem.Checked && this.treeListDetail.FocusedNode != null)
      {
        if (this.treeListDetail.FocusedNode[(object) "IsBOMItem"].ToString() == "T")
        {
          docKey = this.treeListDetail.FocusedNode[(object) "DtlKey"];
          formBomOptional = new FormBOMOptional(this.treeListDetail.FocusedNode[(object) "ItemCode"].ToString(), this.myDBSetting, (Decimal) this.myStockWorkOrder.Qty, tableAsmbomOptional, docKey);
          treeListNode = this.treeListDetail.FocusedNode;
          childNodes = this.treeListDetail.FocusedNode.Nodes;
          defaultQty = BCE.Data.Convert.ToDecimal(childNodes.ParentNode[(object) "Qty"]);
        }
      }
      else if (this.luAssemblyItem.EditValue != null)
      {
        docKey = this.myStockWorkOrder.DataTableMaster.Rows[0]["DocKey"];
        formBomOptional = new FormBOMOptional(this.luAssemblyItem.EditValue.ToString(), this.myDBSetting, (Decimal) this.myStockWorkOrder.Qty, tableAsmbomOptional, docKey);
        childNodes = this.treeListDetail.Nodes;
        defaultQty = BCE.Data.Convert.ToDecimal(this.txtedtQuantity.EditValue);
      }
      if (formBomOptional != null && formBomOptional.HvBOMOptional && formBomOptional.ShowDialog() == DialogResult.OK)
      {
        DataTable dtblBomOptional = formBomOptional.DtblBOMOptional;
        formBomOptional.Close();
        DataRow[] oldBOMOptional = tableAsmbomOptional.Select("DocKey=" + (object) BCE.Data.Convert.ToInt64(docKey));
        this.ApplyBOMOptional(dtblBomOptional, oldBOMOptional, childNodes, defaultQty, docKey);
        this.UpdateASMBOMOptional(dtblBomOptional, docKey);
        TreeListNodes treeListNodes = treeListNode != null ? treeListNode.Nodes : this.treeListDetail.Nodes;
        this.treeListDetail.BeginUpdate();
        try
        {
          for (int index = treeListNodes.Count - 1; index >= 0; --index)
          {
            TreeListNode node = treeListNodes[index];
            if (BCE.Data.Convert.ToDecimal(node[(object) "Qty"]) == Decimal.Zero)
              ((DataRowView) this.treeListDetail.GetDataRecordByNode(node)).Delete();
          }
        }
        finally
        {
          this.treeListDetail.EndUpdate();
        }
      }
    }

    private void UpdateASMBOMOptional(DataTable dtblSelectedBOMOptional, object docKey)
    {
      DataTable tableAsmbomOptional = this.myStockWorkOrder.DataTableASMBOMOptional;
      foreach (DataRow dataRow in dtblSelectedBOMOptional.Select("", "", DataViewRowState.ModifiedCurrent))
      {
        DataTable dataTable = tableAsmbomOptional;
        object[] objArray = new object[4];
        int index1 = 0;
        string str1 = "BOMOptionalKey=";
        objArray[index1] = (object) str1;
        int index2 = 1;
        // ISSUE: variable of a boxed type
        long  local1 =  BCE.Data.Convert.ToInt64(dataRow["BOMOptionalKey"]);
        objArray[index2] = (object) local1;
        int index3 = 2;
        string str2 = " AND DocKey=";
        objArray[index3] = (object) str2;
        int index4 = 3;
        // ISSUE: variable of a boxed type
        long  local2 =  BCE.Data.Convert.ToInt64(docKey);
        objArray[index4] = (object) local2;
        string filterExpression = string.Concat(objArray);
        DataRow[] dataRowArray = dataTable.Select(filterExpression);
        if (dataRowArray.Length == 0)
        {
          DataRow row = tableAsmbomOptional.NewRow();
          DBRegistry dbRegistry = DBRegistry.Create(this.myDBSetting);
          row["DtlKey"] = (object) dbRegistry.IncOne((IRegistryID) new GlobalUniqueKey());
          row["DocKey"] = (object) BCE.Data.Convert.ToInt64(docKey);
          row["BOMOptionalKey"] = (object) BCE.Data.Convert.ToInt64(dataRow["BOMOptionalKey"]);
          row["Qty"] = (object) BCE.Data.Convert.ToInt32(dataRow["Qty"]);
          tableAsmbomOptional.Rows.Add(row);
        }
        else if (BCE.Data.Convert.ToInt32(dataRow["Qty"]) == 0)
          dataRowArray[0].Delete();
        else
          dataRowArray[0]["Qty"] = (object) BCE.Data.Convert.ToInt32(dataRow["Qty"]);
      }
    }

    private void ApplyBOMOptional(DataTable dtblSelectedBOMOptional, DataRow[] oldBOMOptional, TreeListNodes childNodes, Decimal defaultQty, object docKey)
    {
      if (dtblSelectedBOMOptional.Rows.Count != 0)
      {
        DataTable bomOptionalItem = BOMOptionalCommandSQL.GetBOMOptionalItem(dtblSelectedBOMOptional, oldBOMOptional, this.myDBSetting);
        if (bomOptionalItem.Rows.Count != 0)
        {
          IEnumerator enumerator = childNodes.GetEnumerator();
          ArrayList ar = new ArrayList();
          while (enumerator.MoveNext())
            ar.Add(enumerator.Current);
          string[] strArray = new string[ar.Count];
          DataTable tableAsmbomOptional = this.myStockWorkOrder.DataTableASMBOMOptional;
          int num = 0;
          foreach (TreeListNode treeListNode in ar)
          {
            string filter = string.Format("SubItemCode='{0}'", (object) StringHelper.ToSingleQuoteString(treeListNode[(object) "ItemCode"].ToString()));
            object obj1 = bomOptionalItem.Compute("SUM(Qty)", filter);
            object obj2 = bomOptionalItem.Compute("SUM(OverheadCost)", filter);
            if (obj1 != DBNull.Value)
              treeListNode[(object) "Qty"] = (object) (BCE.Data.Convert.ToDecimal(treeListNode[(object) "Qty"]) + BCE.Data.Convert.ToDecimal(obj1));
            if (obj2 != DBNull.Value)
              treeListNode[(object) "OverHeadCost"] = (object) (BCE.Data.Convert.ToDecimal(treeListNode[(object) "OverHeadCost"]) + BCE.Data.Convert.ToDecimal(obj2));
            strArray[num++] = treeListNode[(object) "ItemCode"].ToString();
          }
          foreach (DataRow dataRow in (InternalDataCollectionBase) bomOptionalItem.Rows)
          {
            if (Array.IndexOf<object>((object[]) strArray, dataRow["SubItemCode"]) == -1)
              this.AddNewSubItem(ref ar, dataRow["SubItemCode"].ToString(), BCE.Data.Convert.ToDecimal(dataRow["Rate"]), BCE.Data.Convert.ToDecimal(dataRow["Qty"]), BCE.Data.Convert.ToDecimal(dataRow["OverheadCost"]));
          }
        }
      }
    }

    private void AddNewSubItem(ref ArrayList ar, string itemCode, Decimal Rate, Decimal Qty, Decimal overheadCost)
    {
      this.UpdateTreeList();
      if ((TreeListNode) ar[ar.Count - 1] != null)
      {
        this.myStockWorkOrder.AddDetail();
        this.SortTreeList();
        this.treeListDetail.Focus();
        TreeListNode focusedNode = this.treeListDetail.FocusedNode;
        focusedNode[(object) "ItemCode"] = (object) itemCode;
        focusedNode[(object) "Rate"] = (object) Rate;
        focusedNode[(object) "Qty"] = (object) Qty;
        focusedNode[(object) "OverheadCost"] = (object) overheadCost;
        ar.Add((object) focusedNode);
      }
    }

    private void repBtnedtFurtherDesc_ButtonPressed(object sender, ButtonPressedEventArgs e)
    {
      this.UpdateTreeList();
      if (this.treeListDetail.FocusedNode != null)
      {
        using (BCE.AutoCount.CommonForms.FormRichTextEditor formRichTextEditor = new BCE.AutoCount.CommonForms.FormRichTextEditor(this.myDBSetting, this.myStockWorkOrder.DataTableDetail, this.treeListDetail.FocusedNode.Id, "FurtherDescription", BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.FurtherDescription, new object[0]), this.myStockWorkOrder.Action != StockWorkOrderAction.View))
        {
          int num = (int) formRichTextEditor.ShowDialog((IWin32Window) this);
        }
      }
    }

    private void treeListDetail_FocusedColumnChanged(object sender,DevExpress.XtraTreeList.FocusedColumnChangedEventArgs e)
    {
      this.FilterTreeItemBatch();
    }

    private void treeListDetail_MouseDown(object sender, MouseEventArgs e)
    {
      if (this.treeListDetail.CalcHitInfo(new Point(e.X, e.Y)).Node != null)
        this.myCanDrag = true;
    }

    private void treeListDetail_MouseUp(object sender, MouseEventArgs e)
    {
      this.myCanDrag = false;
    }

    private void treeListDetail_MouseMove(object sender, MouseEventArgs e)
    {
      if ((e.Button & MouseButtons.Left) == MouseButtons.Left && this.myCanDrag && this.myStockWorkOrder != null)
      {
        this.AcceptEdit();
        this.myLastDragDataSet = this.treeListDetail.Selection.Count != 0 ? this.GetSelectedDetailsDataSet() : this.myStockWorkOrder.StockWorkOrderDataSet;
        if (this.myLastDragDataSet != null)
        {
          int num = (int) this.DoDragDrop((object) new DocumentCarrier(this.myLastDragDataSet, false), DragDropEffects.Copy);
        }
      }
    }

    private void treeListDetail_ShowingEditor(object sender, CancelEventArgs e)
    {
      this.myCanDrag = false;
    }

    private void SetModuleFeaturesGrid()
    {
      this.myColLocationVisibleIndex1 = this.colLocation.VisibleIndex;
      this.myColBatchNoVisibleIndex1 = this.colBatchNo.VisibleIndex;
      ModuleController moduleController = ModuleControl.GetOrCreate(this.myDBSetting).ModuleController;
      if (!moduleController.MultiLocationStock.Enable)
      {
        this.myColLocationVisibleIndex1 = this.colLocation.VisibleIndex;
        this.colLocation.Visible = false;
        this.colLocation.VisibleIndex = -1;
      }
      else
      {
        this.colLocation.Visible = true;
        this.colLocation.VisibleIndex = this.myColLocationVisibleIndex1;
      }
      this.colLocation.OptionsColumn.ShowInCustomizationForm = moduleController.MultiLocationStock.Enable;
      if (!moduleController.BatchNo.Enable)
      {
        this.myColBatchNoVisibleIndex1 = this.colBatchNo.VisibleIndex;
        this.colBatchNo.Visible = false;
        this.colBatchNo.VisibleIndex = -1;
      }
      else
      {
        this.colBatchNo.Visible = true;
        this.colBatchNo.VisibleIndex = this.myColBatchNoVisibleIndex1;
      }
      this.colBatchNo.OptionsColumn.ShowInCustomizationForm = moduleController.BatchNo.Enable;
    }

    private void tabControl1_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
    {
      if (this.tabControl1.SelectedTabPage == this.tabPageRawMaterialStatus)
      {
        Cursor current = Cursor.Current;
        Cursor.Current = Cursors.WaitCursor;
        try
        {
          this.SetModuleFeaturesGrid();
          this.UpdateTreeList();
          foreach (DataRow dataRow in this.myStockWorkOrder.DataTableDetail.Select("IsBOMItem = 'T'"))
          {
            if (this.myStockWorkOrder.DataTableDetail.Select("ParentDtlKey=" + (object) BCE.Data.Convert.ToInt64(dataRow["DtlKey"])).Length == 0)
              dataRow["IsBOMItem"] = (object) "F";
          }
          DataRow[] dataRowArray = this.myStockWorkOrder.DataTableDetail.Select("IsBOMItem='F'");
          if (dataRowArray.Length != 0)
          {
            DataTable dtblNonBOMItem = this.myStockWorkOrder.DataTableDetail.Clone();
            foreach (DataRow row in dataRowArray)
              dtblNonBOMItem.ImportRow(row);
            foreach (DataRow dataRow in (InternalDataCollectionBase) dtblNonBOMItem.Rows)
              dataRow["BatchNo"] = (object) dataRow["BatchNo"].ToString();
            this.gctlRawMaterial.DataSource = (object) StockStatus.GetStockStatus(this.myDBSetting, dtblNonBOMItem, (object) this.myStockWorkOrder.DocKey);
          }
        }
        finally
        {
          Cursor.Current = current;
        }
      }
    }

    private void treeListDetail_CustomDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e)
    {
      string[] strArray = new string[6];
      int index1 = 0;
      string str1 = "ItemCode";
      strArray[index1] = str1;
      int index2 = 1;
      string str2 = "UOM";
      strArray[index2] = str2;
      int index3 = 2;
      string str3 = "Location";
      strArray[index3] = str3;
      int index4 = 3;
      string str4 = "ProjNo";
      strArray[index4] = str4;
      int index5 = 4;
      string str5 = "DeptNo";
      strArray[index5] = str5;
      int index6 = 5;
      string str6 = "BatchNo";
      strArray[index6] = str6;
      foreach (string str7 in strArray)
      {
        if (e.Column.FieldName == str7)
        {
          object obj = e.Node[(object) e.Column];
          if (obj == null)
          {
            break;
          }
          else
          {
            e.CellText = obj.ToString();
            break;
          }
        }
      }
    }

    private void txtEdtStockAssemblyNo_KeyDown(object sender, KeyEventArgs e)
    {
      if ((e.Control && e.KeyCode == (Keys.Back | Keys.Space) || e.KeyCode == (Keys) 115) && this.luEdtDocNoFormat.Visible)
      {
        this.luEdtDocNoFormat.ShowPopup();
        this.luEdtDocNoFormat.Focus();
        e.Handled = true;
      }
    }

    private void FormStockWorkOrderEntry_VisibleChanged(object sender, EventArgs e)
    {
      if (this.Visible)
      {
        this.AdjustBottomButtons();
        FormStockWorkOrderEntry.FormShowEventArgs formShowEventArgs1 = new FormStockWorkOrderEntry.FormShowEventArgs(this, this.myStockWorkOrder);
        ScriptObject scriptObject1 = this.myScriptObject;
        string name1 = "OnFormShow";
        System.Type[] types1 = new System.Type[1];
        int index1 = 0;
        System.Type type1 = formShowEventArgs1.GetType();
        types1[index1] = type1;
        object[] objArray1 = new object[1];
        int index2 = 0;
        FormStockWorkOrderEntry.FormShowEventArgs formShowEventArgs2 = formShowEventArgs1;
        objArray1[index2] = (object) formShowEventArgs2;
        scriptObject1.RunMethod(name1, types1, objArray1);
        ScriptObject scriptObject2 = this.myStockWorkOrder.ScriptObject;
        string name2 = "OnFormShow";
        System.Type[] types2 = new System.Type[1];
        int index3 = 0;
        System.Type type2 = formShowEventArgs1.GetType();
        types2[index3] = type2;
        object[] objArray2 = new object[1];
        int index4 = 0;
        FormStockWorkOrderEntry.FormShowEventArgs formShowEventArgs3 = formShowEventArgs1;
        objArray2[index4] = (object) formShowEventArgs3;
        scriptObject2.RunMethod(name2, types2, objArray2);
      }
    }

    private bool CheckBeforePrint(ReportInfo reportInfo)
    {
      //if (!((StockWorkOrder.BeforePreviewDocumentEventArgs) reportInfo.Tag).AllowPrint || SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight && DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "ASMORDER", reportInfo.DocKey) > 0 && !this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_PRINTED_PRINT", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedPrintPrintedDocument, new object[0])))
      //  return false;
    //  else
        return true;
    }

    private bool CheckBeforeExport(ReportInfo reportInfo)
    {
      //if (!((StockWorkOrder.BeforePreviewDocumentEventArgs) reportInfo.Tag).AllowExport || SystemOptionPolicy.GetOrCreate(this.myDBSetting).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight && DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "ASMORDER", reportInfo.DocKey) > 0 && !this.myStockWorkOrder.Command.UserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_PRINTED_EXPORT", (XtraForm) this, BCE.Localization.Localizer.GetString((Enum) BaseStringId.ErrorAccessDeniedExportPrintedDocument, new object[0])))
       // return false;
      //else
        return true;
    }

    private void treeListDetail_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
    {
      if (sender != null && e.Value != null && e.Value != DBNull.Value)
      {
        TreeList treeList = sender as TreeList;
        this.mySkipExecuteFormActivated = true;
        try
        {
          if (treeList.FocusedColumn != null && treeList.FocusedColumn.FieldName == "ItemCode" && (!this.myUseLookupEditToInputItemCode && e.Value.ToString().Length > 0) && !FormPartialMatchItemCode.IsValidAndActiveItemCode(this.myDBSetting, e.Value.ToString()))
          {
            DataTable partialMatchItemCode1 = FormPartialMatchItemCode.GetPartialMatchItemCode(this.myDBSetting, e.Value.ToString());
            if (partialMatchItemCode1 == null || partialMatchItemCode1.Rows.Count == 0)
            {
              e.ErrorText = BCE.Localization.Localizer.GetString( FormPartialMatchItemCodeStringId.ErrorMessage_InvalidOrInactiveItemCode, new object[0]);
              AppMessage.ShowErrorMessage(e.ErrorText);
              e.Valid = false;
            }
            else if (partialMatchItemCode1.Rows.Count == 1)
            {
              e.Value = (object) partialMatchItemCode1.Rows[0]["ItemCode"].ToString();
            }
            else
            {
              using (FormPartialMatchItemCode partialMatchItemCode2 = new FormPartialMatchItemCode(this.myDBSetting, partialMatchItemCode1, e.Value.ToString(), true))
              {
                if (partialMatchItemCode2.ShowDialog() == DialogResult.OK)
                {
                  e.Value = (object) partialMatchItemCode2.SelectedItemCode;
                }
                else
                {
                  e.ErrorText = BCE.Localization.Localizer.GetString( FormPartialMatchItemCodeStringId.ErrorMessage_InvalidOrInactiveItemCode, new object[0]);
                  e.Valid = false;
                }
              }
            }
          }
        }
        finally
        {
          this.mySkipExecuteFormActivated = false;
        }
      }
    }

    private void dateEdtDate_Validating(object sender, CancelEventArgs e)
    {
      if (this.myStockWorkOrder.Command != null)
      {
        DateEdit dateEdit1 = sender as DateEdit;
        FiscalYear orCreate = FiscalYear.GetOrCreate(this.myDBSetting);
        if (!orCreate.IsValidTransactionDate(dateEdit1.DateTime))
        {
          DateTime firstDate;
          DateTime lastDate;
          if (orCreate.GetValidTransactionDateRange(out firstDate, out lastDate))
          {
            DateEdit dateEdit2 = dateEdit1;
            // ISSUE: variable of a boxed type
            InvoicingStringId  local =  InvoicingStringId.ErrorMessage_ValidDocumentDate;
            object[] objArray = new object[2];
            int index1 = 0;
            string str1 = this.myStockWorkOrder.Command.GeneralSetting.FormatDate(firstDate);
            objArray[index1] = (object) str1;
            int index2 = 1;
            string str2 = this.myStockWorkOrder.Command.GeneralSetting.FormatDate(lastDate);
            objArray[index2] = (object) str2;
            string @string = BCE.Localization.Localizer.GetString( local, objArray);
            dateEdit2.ErrorText = @string;
          }
          else
            dateEdit1.ErrorText = BCE.Localization.Localizer.GetString( InvoicingStringId.ErrorMessage_InvalidDocumentDate, new object[0]);
          e.Cancel = true;
          this.myLastValidationSuccess = false;
        }
        else
          this.myLastValidationSuccess = true;
      }
    }

    private void barItemCopyAsTabDelimitedText_ItemClick(object sender, ItemClickEventArgs e)
    {
      ClipboardHelper.SetDataObject((object) this.myStockWorkOrder.ExportAsTabDelimiterText());
      AppMessage.ShowInformationMessage(BCE.Localization.Localizer.GetString( StockAssemblyOrderStringId.InfoMessage_SwitchToExcelToPaste, new object[0]));
    }

    private void iEditRemark1MRU_ItemClick(object sender, ItemClickEventArgs e)
    {
      this.myMRUHelperRemark1.EditMRUItems();
    }

    private void iEditRemark2MRU_ItemClick(object sender, ItemClickEventArgs e)
    {
      this.myMRUHelperRemark2.EditMRUItems();
    }

    private void iEditRemark3MRU_ItemClick(object sender, ItemClickEventArgs e)
    {
      this.myMRUHelperRemark3.EditMRUItems();
    }

    private void iEditRemark4MRU_ItemClick(object sender, ItemClickEventArgs e)
    {
      this.myMRUHelperRemark4.EditMRUItems();
    }

    private void barBtnConvertLevel_ItemClick(object sender, ItemClickEventArgs e)
    {
      string str1 = !this.myStockWorkOrder.IsMultilevel ? "Multi" : "Single";
      int num1 = 7;
      FormStockWorkOrderEntry assemblyOrderEntry = this;
      // ISSUE: variable of a boxed type
      StockAssemblyStringId  local1 =  StockAssemblyStringId.ConfirmMessage_ChangeLevel;
      object[] objArray1 = new object[1];
      int index1 = 0;
      string str2 = str1;
      objArray1[index1] = (object) str2;
      string string1 = BCE.Localization.Localizer.GetString( local1, objArray1);
      string caption = "Change Level";
      int num2 = 4;
      int num3 = 48;
      int num4 = (int) XtraMessageBox.Show((IWin32Window) assemblyOrderEntry, string1, caption, (MessageBoxButtons) num2, (MessageBoxIcon) num3);
      if (num1 != num4)
      {
        this.myStockWorkOrder.IsMultilevel = !this.myStockWorkOrder.IsMultilevel;
        string str3 = !this.myStockWorkOrder.IsMultilevel ? "Multi" : "Single";
        BarButtonItem barButtonItem = this.barBtnConvertLevel;
        // ISSUE: variable of a boxed type
        StockAssemblyStringId  local2 =  StockAssemblyStringId.ConvertToLevel;
        object[] objArray2 = new object[1];
        int index2 = 0;
        string str4 = str3;
        objArray2[index2] = (object) str4;
        string string2 = BCE.Localization.Localizer.GetString( local2, objArray2);
        barButtonItem.Caption = string2;
        BaseRegistryID baseRegistryId = (BaseRegistryID) new EnableMultiLevelStockAssembly();
        baseRegistryId.NewValue = (object)  (this.myStockWorkOrder.IsMultilevel ? true : false);
        this.myDBReg.SetValue((IRegistryID) baseRegistryId);
        this.ToggleMultiLevel();
      }
    }

    private void timer2_Tick(object sender, EventArgs e)
    {
      this.timer2.Stop();
      if (this.myFormItemSearch != null)
      {
        this.myFormItemSearch.Dispose();
        this.myFormItemSearch = (FormItemSearch) null;
      }
    }

    private void splitterControl1_SplitterMoved(object sender, SplitterEventArgs e)
    {
      this.SaveLocalSetting();
    }

    private void SaveLocalSetting()
    {
      WorkOrderKeepAfterSave assemblyKeepAfterSave = new WorkOrderKeepAfterSave();
      int num1 = this.chkedtNextRecord.Checked ? 1 : 0;
      assemblyKeepAfterSave.CheckKeepAfterSave = num1 != 0;
      int height = this.panBottom.Height;
      assemblyKeepAfterSave.InstantInfoHeight = height;
      int num2 = this.WindowState == FormWindowState.Maximized ? 1 : 0;
      assemblyKeepAfterSave.MaximizeWindow = num2 != 0;
      string fileName = "StockWorkOrderCondition.setting";
      PersistenceUtil.SaveUserSetting((object) assemblyKeepAfterSave, fileName);
    }

    private void treeListDetail_SelectionChanged(object sender, EventArgs e)
    {
      if (this.myStockWorkOrder != null)
        this.sBtnRangeSetting.Enabled = this.myStockWorkOrder.Action != StockWorkOrderAction.View && this.treeListDetail.Selection.Count > 0;
    }

    protected override void Dispose(bool disposing)
    {
      if (!this.myHasUnlinkLookupEditEventHandlers)
      {
        this.UnlinkLookupEditEventHandlers();
        this.myHasUnlinkLookupEditEventHandlers = true;
      }
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormStockWorkOrderEntry));
      this.panelHeader = new PanelControl();
      this.grboxItemStock = new GroupControl();
      this.labelAssemblyItemDescription = new Label();
      this.panDept = new PanelControl();
      this.luDeptNo = new LookUpEdit();
      this.lblDept = new Label();
      this.panProject = new PanelControl();
      this.luProject = new LookUpEdit();
      this.lblProj = new Label();
      this.panLocation = new PanelControl();
      this.luLocation = new LookUpEdit();
      this.lblLocation = new Label();
      this.panBatch = new PanelControl();
      this.luBatchNo = new LookUpEdit();
      this.lblBatch = new Label();
      this.txtedtQuantity = new TextEdit();
      this.label4 = new Label();
      this.luAssemblyItem = new LookUpEdit();
      this.label2 = new Label();
      this.textEdtExpectedCompletedDate = new TextEdit();
      this.label9 = new Label();
      this.lblCancelled = new Label();
      this.textEdtRefDocNo = new TextEdit();
      this.dateEdtDate = new DateEdit();
      this.txtEdtStockAssemblyNo = new TextEdit();
      this.lblRefDocNo = new Label();
      this.lblStockAdjNo = new Label();
      this.lblDate = new Label();
      this.lblDescription = new Label();
      this.mruEdtDescription = new MRUEdit();
      this.luEdtDocNoFormat = new LookUpEdit();
      this.imageList1 = new ImageList(this.components);
      this.panel2 = new PanelControl();
      this.btnPrint = new PrintButton();
      this.sbtnCancel = new SimpleButton();
      this.sbtnSavePreview = new SimpleButton();
      this.btnPreview = new PreviewButton();
      this.sbtnCancelDoc = new SimpleButton();
      this.navigator = new Navigator();
      this.sbtnSave = new SimpleButton();
      this.sbtnDelete = new SimpleButton();
      this.sbtnSavePrint = new SimpleButton();
      this.chkedtNextRecord = new CheckEdit();
      this.sbtnEdit = new SimpleButton();
      this.tabControl1 = new XtraTabControl();
      this.tabPageMainTree = new XtraTabPage();
      this.splitterControl1 = new SplitterControl();
      this.treeListDetail = new TreeList();
      this.tlItemCode = new TreeListColumn();
      this.repLuedtItem = new RepositoryItemLookUpEdit();
      this.tlDesc = new TreeListColumn();
      this.tlFurtherDescription = new TreeListColumn();
      this.repBtnedtFurtherDesc = new RepositoryItemButtonEdit();
      this.tlLocation = new TreeListColumn();
      this.repLuedtLocation = new RepositoryItemLookUpEdit();
      this.tlBatchNo = new TreeListColumn();
      this.repLuedtBatchNo = new RepositoryItemLookUpEdit();
      this.tlProjNo = new TreeListColumn();
      this.repLuedtProjNo = new RepositoryItemLookUpEdit();
      this.tlDeptNo = new TreeListColumn();
      this.repLuedtDeptNo = new RepositoryItemLookUpEdit();
      this.tlRate = new TreeListColumn();
      this.tlQty = new TreeListColumn();
      this.tlItemCost = new TreeListColumn();
      this.tlOverheadCost = new TreeListColumn();
      this.tlSubTotal = new TreeListColumn();
      this.tlRemark = new TreeListColumn();
      this.tlPrintOut = new TreeListColumn();
      this.repositoryItemCheckEdit1 = new RepositoryItemCheckEdit();
      this.tlSeq = new TreeListColumn();
      this.tlNumbering = new TreeListColumn();
      this.tlIsBOMItem = new TreeListColumn();
      this.repositoryItemLookUpEdit1 = new RepositoryItemLookUpEdit();
      this.repositoryItemLookUpEdit2 = new RepositoryItemLookUpEdit();
      this.panBottom = new PanelControl();
      this.ucInquiryStock1 = new UCInquiryStock();
      this.txtedtNetTotal = new TextEdit();
      this.txtedtAssemblyCost = new TextEdit();
      this.label10 = new Label();
      this.label11 = new Label();
      this.label12 = new Label();
      this.textEdtTotal = new TextEdit();
      this.panTop = new PanelControl();
      this.panBOMOptional = new PanelControl();
      this.sBtnApplyBOMOptional = new SimpleButton();
      this.chkedtChildItem = new CheckEdit();
      this.sBtnShowInstant = new SimpleButton();
      this.sBtnRangeSetting = new SimpleButton();
      this.sBtnSelectAllMain = new SimpleButton();
      this.sBtnUndoTree = new SimpleButton();
      this.sBtnSearch = new SimpleButton();
      this.sBtnAdd = new SimpleButton();
      this.sBtnRemove = new SimpleButton();
      this.sBtnMoveDown = new SimpleButton();
      this.sBtnMoveUp = new SimpleButton();
      this.sBtnInsert = new SimpleButton();
      this.tabPageRawMaterialStatus = new XtraTabPage();
      this.gctlRawMaterial = new GridControl();
      this.gvRawMaterial = new GridView();
      this.colItemCode = new GridColumn();
      this.colDescription = new GridColumn();
      this.colLocation = new GridColumn();
      this.colUOM = new GridColumn();
      this.colBatchNo = new GridColumn();
      this.colReqQty = new GridColumn();
      this.colOnHandQty = new GridColumn();
      this.colOnHandBal = new GridColumn();
      this.colAvailableQty = new GridColumn();
      this.colAvailableBal = new GridColumn();
      this.gridView1 = new GridView();
      this.tabPageMoreHeader = new XtraTabPage();
      this.labelRemark4 = new Label();
      this.labelRemark3 = new Label();
      this.labelRemark2 = new Label();
      this.labelRemark1 = new Label();
      this.mruEdtRemark1 = new MRUEdit();
      this.mruEdtRemark2 = new MRUEdit();
      this.mruEdtRemark3 = new MRUEdit();
      this.mruEdtRemark4 = new MRUEdit();
      this.tabPageExternalLink = new XtraTabPage();
      this.externalLinkBox1 = new ExternalLinkBox();
      this.tabPageNote = new XtraTabPage();
      this.memoEdtNote = new BCE.Controls.MemoEdit();
      this.panelControl1 = new PanelControl();
      this.barManager1 = new BarManager(this.components);
      this.bar1 = new Bar();
      this.barSubItem1 = new BarSubItem();
      this.barItemCopyWholeDocument = new BarButtonItem();
      this.barItemCopySelectedDetails = new BarButtonItem();
      this.barItemCopyAsTabDelimitedText = new BarButtonItem();
      this.barItemPasteWholeDocument = new BarButtonItem();
      this.barItemPasteItemDetailOnly = new BarButtonItem();
      this.iUndoMaster = new BarButtonItem();
      this.barBtnSaveInKIVFolder = new BarButtonItem();
      this.barSubItem4 = new BarSubItem();
      this.iEditDescriptionMRU = new BarButtonItem();
      this.iEditRemark1MRU = new BarButtonItem();
      this.iEditRemark2MRU = new BarButtonItem();
      this.iEditRemark3MRU = new BarButtonItem();
      this.iEditRemark4MRU = new BarButtonItem();
      this.barBtnConvertLevel = new BarButtonItem();
      this.barSubItem2 = new BarSubItem();
      this.barItemCopyFrom = new BarButtonItem();
      this.barItemCopyTo = new BarButtonItem();
      this.barSubItem3 = new BarSubItem();
      this.barbtnTransferFromSO = new BarButtonItem();
      this.barbtnCheckTransferredToStatus = new BarButtonItem();
      this.barDockControlTop = new BarDockControl();
      this.barDockControlBottom = new BarDockControl();
      this.barDockControlLeft = new BarDockControl();
      this.barDockControlRight = new BarDockControl();
      this.barButtonItem1 = new BarButtonItem();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.timer2 = new System.Windows.Forms.Timer(this.components);
      this.panelHeader.BeginInit();
      this.panelHeader.SuspendLayout();
      this.grboxItemStock.BeginInit();
      this.grboxItemStock.SuspendLayout();
      this.panDept.BeginInit();
      this.panDept.SuspendLayout();
      this.luDeptNo.Properties.BeginInit();
      this.panProject.BeginInit();
      this.panProject.SuspendLayout();
      this.luProject.Properties.BeginInit();
      this.panLocation.BeginInit();
      this.panLocation.SuspendLayout();
      this.luLocation.Properties.BeginInit();
      this.panBatch.BeginInit();
      this.panBatch.SuspendLayout();
      this.luBatchNo.Properties.BeginInit();
      this.txtedtQuantity.Properties.BeginInit();
      this.luAssemblyItem.Properties.BeginInit();
      this.textEdtExpectedCompletedDate.Properties.BeginInit();
      this.textEdtRefDocNo.Properties.BeginInit();
      this.dateEdtDate.Properties.CalendarTimeProperties.BeginInit();
      this.dateEdtDate.Properties.BeginInit();
      this.txtEdtStockAssemblyNo.Properties.BeginInit();
      this.mruEdtDescription.Properties.BeginInit();
      this.luEdtDocNoFormat.Properties.BeginInit();
      this.panel2.BeginInit();
      this.panel2.SuspendLayout();
      this.chkedtNextRecord.Properties.BeginInit();
      this.tabControl1.BeginInit();
      this.tabControl1.SuspendLayout();
      this.tabPageMainTree.SuspendLayout();
      this.treeListDetail.BeginInit();
      this.repLuedtItem.BeginInit();
      this.repBtnedtFurtherDesc.BeginInit();
      this.repLuedtLocation.BeginInit();
      this.repLuedtBatchNo.BeginInit();
      this.repLuedtProjNo.BeginInit();
      this.repLuedtDeptNo.BeginInit();
      this.repositoryItemCheckEdit1.BeginInit();
      this.repositoryItemLookUpEdit1.BeginInit();
      this.repositoryItemLookUpEdit2.BeginInit();
      this.panBottom.BeginInit();
      this.panBottom.SuspendLayout();
      this.txtedtNetTotal.Properties.BeginInit();
      this.txtedtAssemblyCost.Properties.BeginInit();
      this.textEdtTotal.Properties.BeginInit();
      this.panTop.BeginInit();
      this.panTop.SuspendLayout();
      this.panBOMOptional.BeginInit();
      this.panBOMOptional.SuspendLayout();
      this.chkedtChildItem.Properties.BeginInit();
      this.tabPageRawMaterialStatus.SuspendLayout();
      this.gctlRawMaterial.BeginInit();
      this.gvRawMaterial.BeginInit();
      this.gridView1.BeginInit();
      this.tabPageMoreHeader.SuspendLayout();
      this.mruEdtRemark1.Properties.BeginInit();
      this.mruEdtRemark2.Properties.BeginInit();
      this.mruEdtRemark3.Properties.BeginInit();
      this.mruEdtRemark4.Properties.BeginInit();
      this.tabPageExternalLink.SuspendLayout();
      this.tabPageNote.SuspendLayout();
      this.panelControl1.BeginInit();
      this.panelControl1.SuspendLayout();
      this.barManager1.BeginInit();
      this.SuspendLayout();
      componentResourceManager.ApplyResources((object) this.panelHeader, "panelHeader");
      this.panelHeader.BorderStyle = BorderStyles.NoBorder;
      this.panelHeader.Controls.Add((Control) this.grboxItemStock);
      this.panelHeader.Controls.Add((Control) this.textEdtExpectedCompletedDate);
      this.panelHeader.Controls.Add((Control) this.label9);
      this.panelHeader.Controls.Add((Control) this.lblCancelled);
      this.panelHeader.Controls.Add((Control) this.textEdtRefDocNo);
      this.panelHeader.Controls.Add((Control) this.dateEdtDate);
      this.panelHeader.Controls.Add((Control) this.txtEdtStockAssemblyNo);
      this.panelHeader.Controls.Add((Control) this.lblRefDocNo);
      this.panelHeader.Controls.Add((Control) this.lblStockAdjNo);
      this.panelHeader.Controls.Add((Control) this.lblDate);
      this.panelHeader.Controls.Add((Control) this.lblDescription);
      this.panelHeader.Controls.Add((Control) this.mruEdtDescription);
      this.panelHeader.Controls.Add((Control) this.luEdtDocNoFormat);
      this.panelHeader.Name = "panelHeader";
      this.panelHeader.MouseDown += new MouseEventHandler(this.panelHeader_MouseDown);
      this.grboxItemStock.Controls.Add((Control) this.labelAssemblyItemDescription);
      this.grboxItemStock.Controls.Add((Control) this.panDept);
      this.grboxItemStock.Controls.Add((Control) this.panProject);
      this.grboxItemStock.Controls.Add((Control) this.panLocation);
      this.grboxItemStock.Controls.Add((Control) this.panBatch);
      this.grboxItemStock.Controls.Add((Control) this.txtedtQuantity);
      this.grboxItemStock.Controls.Add((Control) this.label4);
      this.grboxItemStock.Controls.Add((Control) this.luAssemblyItem);
      this.grboxItemStock.Controls.Add((Control) this.label2);
      componentResourceManager.ApplyResources((object) this.grboxItemStock, "grboxItemStock");
      this.grboxItemStock.Name = "grboxItemStock";
      componentResourceManager.ApplyResources((object) this.labelAssemblyItemDescription, "labelAssemblyItemDescription");
      this.labelAssemblyItemDescription.ForeColor = Color.Blue;
      this.labelAssemblyItemDescription.Name = "labelAssemblyItemDescription";
      this.panDept.BorderStyle = BorderStyles.NoBorder;
      this.panDept.Controls.Add((Control) this.luDeptNo);
      this.panDept.Controls.Add((Control) this.lblDept);
      componentResourceManager.ApplyResources((object) this.panDept, "panDept");
      this.panDept.Name = "panDept";
      componentResourceManager.ApplyResources((object) this.luDeptNo, "luDeptNo");
      this.luDeptNo.Name = "luDeptNo";
      EditorButtonCollection buttons1 = this.luDeptNo.Properties.Buttons;
      EditorButton[] buttons2 = new EditorButton[1];
      int index1 = 0;
      EditorButton editorButton1 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("luDeptNo.Properties.Buttons"));
      buttons2[index1] = editorButton1;
      buttons1.AddRange(buttons2);
      componentResourceManager.ApplyResources((object) this.lblDept, "lblDept");
      this.lblDept.BackColor = Color.Transparent;
      this.lblDept.Name = "lblDept";
      this.panProject.BorderStyle = BorderStyles.NoBorder;
      this.panProject.Controls.Add((Control) this.luProject);
      this.panProject.Controls.Add((Control) this.lblProj);
      componentResourceManager.ApplyResources((object) this.panProject, "panProject");
      this.panProject.Name = "panProject";
      componentResourceManager.ApplyResources((object) this.luProject, "luProject");
      this.luProject.Name = "luProject";
      EditorButtonCollection buttons3 = this.luProject.Properties.Buttons;
      EditorButton[] buttons4 = new EditorButton[1];
      int index2 = 0;
      EditorButton editorButton2 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("luProject.Properties.Buttons"));
      buttons4[index2] = editorButton2;
      buttons3.AddRange(buttons4);
      componentResourceManager.ApplyResources((object) this.lblProj, "lblProj");
      this.lblProj.BackColor = Color.Transparent;
      this.lblProj.Name = "lblProj";
      this.panLocation.BorderStyle = BorderStyles.NoBorder;
      this.panLocation.Controls.Add((Control) this.luLocation);
      this.panLocation.Controls.Add((Control) this.lblLocation);
      componentResourceManager.ApplyResources((object) this.panLocation, "panLocation");
      this.panLocation.Name = "panLocation";
      componentResourceManager.ApplyResources((object) this.luLocation, "luLocation");
      this.luLocation.Name = "luLocation";
      EditorButtonCollection buttons5 = this.luLocation.Properties.Buttons;
      EditorButton[] buttons6 = new EditorButton[1];
      int index3 = 0;
      EditorButton editorButton3 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("luLocation.Properties.Buttons"));
      buttons6[index3] = editorButton3;
      buttons5.AddRange(buttons6);
      componentResourceManager.ApplyResources((object) this.lblLocation, "lblLocation");
      this.lblLocation.BackColor = Color.Transparent;
      this.lblLocation.Name = "lblLocation";
      this.panBatch.BorderStyle = BorderStyles.NoBorder;
      this.panBatch.Controls.Add((Control) this.luBatchNo);
      this.panBatch.Controls.Add((Control) this.lblBatch);
      componentResourceManager.ApplyResources((object) this.panBatch, "panBatch");
      this.panBatch.Name = "panBatch";
      componentResourceManager.ApplyResources((object) this.luBatchNo, "luBatchNo");
      this.luBatchNo.Name = "luBatchNo";
      EditorButtonCollection buttons7 = this.luBatchNo.Properties.Buttons;
      EditorButton[] buttons8 = new EditorButton[1];
      int index4 = 0;
      EditorButton editorButton4 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("luBatchNo.Properties.Buttons"));
      buttons8[index4] = editorButton4;
      buttons7.AddRange(buttons8);
      this.luBatchNo.Properties.NullText = componentResourceManager.GetString("luBatchNo.Properties.NullText");
      componentResourceManager.ApplyResources((object) this.lblBatch, "lblBatch");
      this.lblBatch.BackColor = Color.Transparent;
      this.lblBatch.Name = "lblBatch";
      componentResourceManager.ApplyResources((object) this.txtedtQuantity, "txtedtQuantity");
      this.txtedtQuantity.Name = "txtedtQuantity";
      componentResourceManager.ApplyResources((object) this.label4, "label4");
      this.label4.BackColor = Color.Transparent;
      this.label4.Name = "label4";
      componentResourceManager.ApplyResources((object) this.luAssemblyItem, "luAssemblyItem");
      this.luAssemblyItem.Name = "luAssemblyItem";
      EditorButtonCollection buttons9 = this.luAssemblyItem.Properties.Buttons;
      EditorButton[] buttons10 = new EditorButton[1];
      int index5 = 0;
      EditorButton editorButton5 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("luAssemblyItem.Properties.Buttons"));
      buttons10[index5] = editorButton5;
      buttons9.AddRange(buttons10);
      this.luAssemblyItem.EditValueChanged += new EventHandler(this.luAssemblyItem_EditValueChanged);
      componentResourceManager.ApplyResources((object) this.label2, "label2");
      this.label2.BackColor = Color.Transparent;
      this.label2.Name = "label2";
      componentResourceManager.ApplyResources((object) this.textEdtExpectedCompletedDate, "textEdtExpectedCompletedDate");
      this.textEdtExpectedCompletedDate.Name = "textEdtExpectedCompletedDate";
      componentResourceManager.ApplyResources((object) this.label9, "label9");
      this.label9.BackColor = Color.Transparent;
      this.label9.Name = "label9";
      componentResourceManager.ApplyResources((object) this.lblCancelled, "lblCancelled");
      this.lblCancelled.BackColor = Color.Transparent;
      this.lblCancelled.ForeColor = Color.Red;
      this.lblCancelled.Name = "lblCancelled";
      componentResourceManager.ApplyResources((object) this.textEdtRefDocNo, "textEdtRefDocNo");
      this.textEdtRefDocNo.Name = "textEdtRefDocNo";
      componentResourceManager.ApplyResources((object) this.dateEdtDate, "dateEdtDate");
      this.dateEdtDate.Name = "dateEdtDate";
      EditorButtonCollection buttons11 = this.dateEdtDate.Properties.Buttons;
      EditorButton[] buttons12 = new EditorButton[1];
      int index6 = 0;
      EditorButton editorButton6 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("dateEdtDate.Properties.Buttons"));
      buttons12[index6] = editorButton6;
      buttons11.AddRange(buttons12);
      EditorButtonCollection buttons13 = this.dateEdtDate.Properties.CalendarTimeProperties.Buttons;
      EditorButton[] buttons14 = new EditorButton[1];
      int index7 = 0;
      EditorButton editorButton7 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("dateEdtDate.Properties.CalendarTimeProperties.Buttons"));
      buttons14[index7] = editorButton7;
      buttons13.AddRange(buttons14);
      this.dateEdtDate.Properties.NullDate = (object) "";
      this.dateEdtDate.Validating += new CancelEventHandler(this.dateEdtDate_Validating);
      this.dateEdtDate.Validated += new EventHandler(this.txtEdtStockAssemblyNo_EditValueChanged);
      componentResourceManager.ApplyResources((object) this.txtEdtStockAssemblyNo, "txtEdtStockAssemblyNo");
      this.txtEdtStockAssemblyNo.Name = "txtEdtStockAssemblyNo";
      this.txtEdtStockAssemblyNo.EditValueChanged += new EventHandler(this.txtEdtStockAssemblyNo_EditValueChanged);
      this.txtEdtStockAssemblyNo.KeyDown += new KeyEventHandler(this.txtEdtStockAssemblyNo_KeyDown);
      componentResourceManager.ApplyResources((object) this.lblRefDocNo, "lblRefDocNo");
      this.lblRefDocNo.BackColor = Color.Transparent;
      this.lblRefDocNo.Name = "lblRefDocNo";
      componentResourceManager.ApplyResources((object) this.lblStockAdjNo, "lblStockAdjNo");
      this.lblStockAdjNo.BackColor = Color.Transparent;
      this.lblStockAdjNo.Name = "lblStockAdjNo";
      componentResourceManager.ApplyResources((object) this.lblDate, "lblDate");
      this.lblDate.BackColor = Color.Transparent;
      this.lblDate.Name = "lblDate";
      componentResourceManager.ApplyResources((object) this.lblDescription, "lblDescription");
      this.lblDescription.BackColor = Color.Transparent;
      this.lblDescription.Name = "lblDescription";
      componentResourceManager.ApplyResources((object) this.mruEdtDescription, "mruEdtDescription");
      this.mruEdtDescription.Name = "mruEdtDescription";
      EditorButtonCollection buttons15 = this.mruEdtDescription.Properties.Buttons;
      EditorButton[] buttons16 = new EditorButton[1];
      int index8 = 0;
      EditorButton editorButton8 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("mruEdtDescription.Properties.Buttons"));
      buttons16[index8] = editorButton8;
      buttons15.AddRange(buttons16);
      componentResourceManager.ApplyResources((object) this.luEdtDocNoFormat, "luEdtDocNoFormat");
      this.luEdtDocNoFormat.Name = "luEdtDocNoFormat";
      EditorButtonCollection buttons17 = this.luEdtDocNoFormat.Properties.Buttons;
      EditorButton[] buttons18 = new EditorButton[1];
      int index9 = 0;
      EditorButton editorButton9 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("luEdtDocNoFormat.Properties.Buttons"));
      buttons18[index9] = editorButton9;
      buttons17.AddRange(buttons18);
      this.luEdtDocNoFormat.Properties.NullText = componentResourceManager.GetString("luEdtDocNoFormat.Properties.NullText");
      this.luEdtDocNoFormat.TabStop = false;
      this.luEdtDocNoFormat.EditValueChanged += new EventHandler(this.luEdtDocNoFormat_EditValueChanged);
      this.imageList1.ImageStream = (ImageListStreamer) componentResourceManager.GetObject("imageList1.ImageStream");
      this.imageList1.TransparentColor = Color.Magenta;
      this.imageList1.Images.SetKeyName(0, "");
      this.imageList1.Images.SetKeyName(1, "");
      this.imageList1.Images.SetKeyName(2, "");
      this.imageList1.Images.SetKeyName(3, "");
      this.imageList1.Images.SetKeyName(4, "");
      this.imageList1.Images.SetKeyName(5, "");
      this.imageList1.Images.SetKeyName(6, "");
      this.panel2.BorderStyle = BorderStyles.NoBorder;
      this.panel2.Controls.Add((Control) this.btnPrint);
      this.panel2.Controls.Add((Control) this.sbtnCancel);
      this.panel2.Controls.Add((Control) this.sbtnSavePreview);
      this.panel2.Controls.Add((Control) this.btnPreview);
      this.panel2.Controls.Add((Control) this.sbtnCancelDoc);
      this.panel2.Controls.Add((Control) this.navigator);
      this.panel2.Controls.Add((Control) this.sbtnSave);
      this.panel2.Controls.Add((Control) this.sbtnDelete);
      this.panel2.Controls.Add((Control) this.sbtnSavePrint);
      this.panel2.Controls.Add((Control) this.chkedtNextRecord);
      this.panel2.Controls.Add((Control) this.sbtnEdit);
      componentResourceManager.ApplyResources((object) this.panel2, "panel2");
      this.panel2.Name = "panel2";
      componentResourceManager.ApplyResources((object) this.btnPrint, "btnPrint");
      this.btnPrint.Name = "btnPrint";
      this.btnPrint.ReportType = "";
      this.btnPrint.Print += new PrintEventHandler(this.printButton1_Print);
      componentResourceManager.ApplyResources((object) this.sbtnCancel, "sbtnCancel");
      this.sbtnCancel.DialogResult = DialogResult.Cancel;
      this.sbtnCancel.Name = "sbtnCancel";
      this.sbtnCancel.Click += new EventHandler(this.btnCancel_Click);
      componentResourceManager.ApplyResources((object) this.sbtnSavePreview, "sbtnSavePreview");
      this.sbtnSavePreview.DialogResult = DialogResult.OK;
      this.sbtnSavePreview.Name = "sbtnSavePreview";
      this.sbtnSavePreview.Click += new EventHandler(this.sbtnSavePreview_Click);
      componentResourceManager.ApplyResources((object) this.btnPreview, "btnPreview");
      this.btnPreview.Name = "btnPreview";
      this.btnPreview.ReportType = "";
      this.btnPreview.Preview += new PrintEventHandler(this.previewButton1_Preview);
      componentResourceManager.ApplyResources((object) this.sbtnCancelDoc, "sbtnCancelDoc");
      this.sbtnCancelDoc.Name = "sbtnCancelDoc";
      this.sbtnCancelDoc.Click += new EventHandler(this.btnCancelDoc_Click);
      componentResourceManager.ApplyResources((object) this.navigator, "navigator");
      this.navigator.Name = "navigator";
            this.navigator.ButtonClick += new BCE.Controls.NavigatorButtonClickEventHandler(this.navigator_ButtonClick);
                //new BCE.Controls.NavigatorButtonClickEventHandler(this.navigator_ButtonClick);
      componentResourceManager.ApplyResources((object) this.sbtnSave, "sbtnSave");
      this.sbtnSave.DialogResult = DialogResult.OK;
      this.sbtnSave.Name = "sbtnSave";
      this.sbtnSave.Click += new EventHandler(this.sbtnSave_Click);
      componentResourceManager.ApplyResources((object) this.sbtnDelete, "sbtnDelete");
      this.sbtnDelete.Appearance.ForeColor = (Color) componentResourceManager.GetObject("sbtnDelete.Appearance.ForeColor");
      this.sbtnDelete.Appearance.Options.UseForeColor = true;
      this.sbtnDelete.Name = "sbtnDelete";
      this.sbtnDelete.Click += new EventHandler(this.btnDelete_Click);
      componentResourceManager.ApplyResources((object) this.sbtnSavePrint, "sbtnSavePrint");
      this.sbtnSavePrint.DialogResult = DialogResult.OK;
      this.sbtnSavePrint.Name = "sbtnSavePrint";
      this.sbtnSavePrint.Click += new EventHandler(this.sbtnSavePrint_Click);
      componentResourceManager.ApplyResources((object) this.chkedtNextRecord, "chkedtNextRecord");
      this.chkedtNextRecord.Name = "chkedtNextRecord";
      this.chkedtNextRecord.Properties.Caption = componentResourceManager.GetString("chkedtNextRecord.Properties.Caption");
      this.chkedtNextRecord.CheckedChanged += new EventHandler(this.chkedtNextRecord_CheckedChanged);
      componentResourceManager.ApplyResources((object) this.sbtnEdit, "sbtnEdit");
      this.sbtnEdit.Name = "sbtnEdit";
      this.sbtnEdit.Click += new EventHandler(this.btnEdit_Click);
      componentResourceManager.ApplyResources((object) this.tabControl1, "tabControl1");
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedTabPage = this.tabPageMainTree;
      XtraTabPageCollection tabPages = this.tabControl1.TabPages;
      XtraTabPage[] pages = new XtraTabPage[5];
      int index10 = 0;
      XtraTabPage xtraTabPage1 = this.tabPageMainTree;
      pages[index10] = xtraTabPage1;
      int index11 = 1;
      XtraTabPage xtraTabPage2 = this.tabPageRawMaterialStatus;
      pages[index11] = xtraTabPage2;
      int index12 = 2;
      XtraTabPage xtraTabPage3 = this.tabPageMoreHeader;
      pages[index12] = xtraTabPage3;
      int index13 = 3;
      XtraTabPage xtraTabPage4 = this.tabPageExternalLink;
      pages[index13] = xtraTabPage4;
      int index14 = 4;
      XtraTabPage xtraTabPage5 = this.tabPageNote;
      pages[index14] = xtraTabPage5;
      tabPages.AddRange(pages);
      this.tabControl1.SelectedPageChanged += new TabPageChangedEventHandler(this.tabControl1_SelectedPageChanged);
      this.tabPageMainTree.Controls.Add((Control) this.splitterControl1);
      this.tabPageMainTree.Controls.Add((Control) this.treeListDetail);
      this.tabPageMainTree.Controls.Add((Control) this.panBottom);
      this.tabPageMainTree.Controls.Add((Control) this.panTop);
      this.tabPageMainTree.Name = "tabPageMainTree";
      componentResourceManager.ApplyResources((object) this.tabPageMainTree, "tabPageMainTree");
      componentResourceManager.ApplyResources((object) this.splitterControl1, "splitterControl1");
      this.splitterControl1.Name = "splitterControl1";
      this.splitterControl1.TabStop = false;
      this.splitterControl1.SplitterMoved += new SplitterEventHandler(this.splitterControl1_SplitterMoved);
      TreeListColumnCollection columns1 = this.treeListDetail.Columns;
      TreeListColumn[] columns2 = new TreeListColumn[17];
      int index15 = 0;
      TreeListColumn treeListColumn1 = this.tlItemCode;
      columns2[index15] = treeListColumn1;
      int index16 = 1;
      TreeListColumn treeListColumn2 = this.tlDesc;
      columns2[index16] = treeListColumn2;
      int index17 = 2;
      TreeListColumn treeListColumn3 = this.tlFurtherDescription;
      columns2[index17] = treeListColumn3;
      int index18 = 3;
      TreeListColumn treeListColumn4 = this.tlLocation;
      columns2[index18] = treeListColumn4;
      int index19 = 4;
      TreeListColumn treeListColumn5 = this.tlBatchNo;
      columns2[index19] = treeListColumn5;
      int index20 = 5;
      TreeListColumn treeListColumn6 = this.tlProjNo;
      columns2[index20] = treeListColumn6;
      int index21 = 6;
      TreeListColumn treeListColumn7 = this.tlDeptNo;
      columns2[index21] = treeListColumn7;
      int index22 = 7;
      TreeListColumn treeListColumn8 = this.tlRate;
      columns2[index22] = treeListColumn8;
      int index23 = 8;
      TreeListColumn treeListColumn9 = this.tlQty;
      columns2[index23] = treeListColumn9;
      int index24 = 9;
      TreeListColumn treeListColumn10 = this.tlItemCost;
      columns2[index24] = treeListColumn10;
      int index25 = 10;
      TreeListColumn treeListColumn11 = this.tlOverheadCost;
      columns2[index25] = treeListColumn11;
      int index26 = 11;
      TreeListColumn treeListColumn12 = this.tlSubTotal;
      columns2[index26] = treeListColumn12;
      int index27 = 12;
      TreeListColumn treeListColumn13 = this.tlRemark;
      columns2[index27] = treeListColumn13;
      int index28 = 13;
      TreeListColumn treeListColumn14 = this.tlPrintOut;
      columns2[index28] = treeListColumn14;
      int index29 = 14;
      TreeListColumn treeListColumn15 = this.tlSeq;
      columns2[index29] = treeListColumn15;
      int index30 = 15;
      TreeListColumn treeListColumn16 = this.tlNumbering;
      columns2[index30] = treeListColumn16;
      int index31 = 16;
      TreeListColumn treeListColumn17 = this.tlIsBOMItem;
      columns2[index31] = treeListColumn17;
      columns1.AddRange(columns2);
      componentResourceManager.ApplyResources((object) this.treeListDetail, "treeListDetail");
      this.treeListDetail.KeyFieldName = "DtlKey";
      this.treeListDetail.Name = "treeListDetail";
      this.treeListDetail.OptionsBehavior.AutoFocusNewNode = true;
      this.treeListDetail.OptionsBehavior.CloseEditorOnLostFocus = false;
      this.treeListDetail.OptionsBehavior.EnterMovesNextColumn = true;
      this.treeListDetail.OptionsSelection.MultiSelect = true;
      this.treeListDetail.ParentFieldName = "ParentDtlKey";
      RepositoryItemCollection repositoryItems = this.treeListDetail.RepositoryItems;
      RepositoryItem[] items1 = new RepositoryItem[9];
      int index32 = 0;
      RepositoryItemLookUpEdit repositoryItemLookUpEdit1 = this.repLuedtItem;
      items1[index32] = (RepositoryItem) repositoryItemLookUpEdit1;
      int index33 = 1;
      RepositoryItemButtonEdit repositoryItemButtonEdit = this.repBtnedtFurtherDesc;
      items1[index33] = (RepositoryItem) repositoryItemButtonEdit;
      int index34 = 2;
      RepositoryItemLookUpEdit repositoryItemLookUpEdit2 = this.repLuedtLocation;
      items1[index34] = (RepositoryItem) repositoryItemLookUpEdit2;
      int index35 = 3;
      RepositoryItemLookUpEdit repositoryItemLookUpEdit3 = this.repositoryItemLookUpEdit1;
      items1[index35] = (RepositoryItem) repositoryItemLookUpEdit3;
      int index36 = 4;
      RepositoryItemLookUpEdit repositoryItemLookUpEdit4 = this.repLuedtBatchNo;
      items1[index36] = (RepositoryItem) repositoryItemLookUpEdit4;
      int index37 = 5;
      RepositoryItemLookUpEdit repositoryItemLookUpEdit5 = this.repositoryItemLookUpEdit2;
      items1[index37] = (RepositoryItem) repositoryItemLookUpEdit5;
      int index38 = 6;
      RepositoryItemLookUpEdit repositoryItemLookUpEdit6 = this.repLuedtProjNo;
      items1[index38] = (RepositoryItem) repositoryItemLookUpEdit6;
      int index39 = 7;
      RepositoryItemLookUpEdit repositoryItemLookUpEdit7 = this.repLuedtDeptNo;
      items1[index39] = (RepositoryItem) repositoryItemLookUpEdit7;
      int index40 = 8;
      RepositoryItemCheckEdit repositoryItemCheckEdit = this.repositoryItemCheckEdit1;
      items1[index40] = (RepositoryItem) repositoryItemCheckEdit;
      repositoryItems.AddRange(items1);
      this.treeListDetail.FocusedNodeChanged += new FocusedNodeChangedEventHandler(this.treeListDetail_FocusedNodeChanged);
      this.treeListDetail.FocusedColumnChanged += new DevExpress.XtraTreeList.FocusedColumnChangedEventHandler(this.treeListDetail_FocusedColumnChanged);
      this.treeListDetail.ValidatingEditor += new BaseContainerValidateEditorEventHandler(this.treeListDetail_ValidatingEditor);
      this.treeListDetail.SelectionChanged += new EventHandler(this.treeListDetail_SelectionChanged);
      this.treeListDetail.CustomDrawNodeCell += new CustomDrawNodeCellEventHandler(this.treeListDetail_CustomDrawNodeCell);
      this.treeListDetail.ShowingEditor += new CancelEventHandler(this.treeListDetail_ShowingEditor);
      this.treeListDetail.MouseDown += new MouseEventHandler(this.treeListDetail_MouseDown);
      this.treeListDetail.MouseMove += new MouseEventHandler(this.treeListDetail_MouseMove);
      this.treeListDetail.MouseUp += new MouseEventHandler(this.treeListDetail_MouseUp);
      componentResourceManager.ApplyResources((object) this.tlItemCode, "tlItemCode");
      this.tlItemCode.ColumnEdit = (RepositoryItem) this.repLuedtItem;
      this.tlItemCode.FieldName = "ItemCode";
      this.tlItemCode.Name = "tlItemCode";
      this.tlItemCode.OptionsColumn.AllowSort = false;
      componentResourceManager.ApplyResources((object) this.repLuedtItem, "repLuedtItem");
      EditorButtonCollection buttons19 = this.repLuedtItem.Buttons;
      EditorButton[] buttons20 = new EditorButton[1];
      int index41 = 0;
      EditorButton editorButton10 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("repLuedtItem.Buttons"));
      buttons20[index41] = editorButton10;
      buttons19.AddRange(buttons20);
      this.repLuedtItem.Name = "repLuedtItem";
      this.repLuedtItem.ValidateOnEnterKey = true;
      this.repLuedtItem.CloseUp += new CloseUpEventHandler(this.repLuedtItem_CloseUp);
      this.repLuedtItem.EditValueChanged += new EventHandler(this.repLuedtItem_EditValueChanged);
      componentResourceManager.ApplyResources((object) this.tlDesc, "tlDesc");
      this.tlDesc.FieldName = "Description";
      this.tlDesc.Name = "tlDesc";
      this.tlDesc.OptionsColumn.AllowSort = false;
      componentResourceManager.ApplyResources((object) this.tlFurtherDescription, "tlFurtherDescription");
      this.tlFurtherDescription.ColumnEdit = (RepositoryItem) this.repBtnedtFurtherDesc;
      this.tlFurtherDescription.FieldName = "FurtherDescription";
      this.tlFurtherDescription.Name = "tlFurtherDescription";
      this.tlFurtherDescription.OptionsColumn.AllowSort = false;
      componentResourceManager.ApplyResources((object) this.repBtnedtFurtherDesc, "repBtnedtFurtherDesc");
      EditorButtonCollection buttons21 = this.repBtnedtFurtherDesc.Buttons;
      EditorButton[] buttons22 = new EditorButton[1];
      int index42 = 0;
      EditorButton editorButton11 = new EditorButton();
      buttons22[index42] = editorButton11;
      buttons21.AddRange(buttons22);
      this.repBtnedtFurtherDesc.Name = "repBtnedtFurtherDesc";
      this.repBtnedtFurtherDesc.TextEditStyle = TextEditStyles.HideTextEditor;
      this.repBtnedtFurtherDesc.ButtonPressed += new ButtonPressedEventHandler(this.repBtnedtFurtherDesc_ButtonPressed);
      componentResourceManager.ApplyResources((object) this.tlLocation, "tlLocation");
      this.tlLocation.ColumnEdit = (RepositoryItem) this.repLuedtLocation;
      this.tlLocation.FieldName = "Location";
      this.tlLocation.Name = "tlLocation";
      this.tlLocation.OptionsColumn.AllowSort = false;
      componentResourceManager.ApplyResources((object) this.repLuedtLocation, "repLuedtLocation");
      EditorButtonCollection buttons23 = this.repLuedtLocation.Buttons;
      EditorButton[] buttons24 = new EditorButton[1];
      int index43 = 0;
      EditorButton editorButton12 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("repLuedtLocation.Buttons"));
      buttons24[index43] = editorButton12;
      buttons23.AddRange(buttons24);
      this.repLuedtLocation.Name = "repLuedtLocation";
      componentResourceManager.ApplyResources((object) this.tlBatchNo, "tlBatchNo");
      this.tlBatchNo.ColumnEdit = (RepositoryItem) this.repLuedtBatchNo;
      this.tlBatchNo.FieldName = "BatchNo";
      this.tlBatchNo.Name = "tlBatchNo";
      this.tlBatchNo.OptionsColumn.AllowSort = false;
      componentResourceManager.ApplyResources((object) this.repLuedtBatchNo, "repLuedtBatchNo");
      EditorButtonCollection buttons25 = this.repLuedtBatchNo.Buttons;
      EditorButton[] buttons26 = new EditorButton[1];
      int index44 = 0;
      EditorButton editorButton13 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("repLuedtBatchNo.Buttons"));
      buttons26[index44] = editorButton13;
      buttons25.AddRange(buttons26);
      this.repLuedtBatchNo.Name = "repLuedtBatchNo";
      this.repLuedtBatchNo.QueryPopUp += new CancelEventHandler(this.repLuedtBatchNo_QueryPopUp);
      componentResourceManager.ApplyResources((object) this.tlProjNo, "tlProjNo");
      this.tlProjNo.ColumnEdit = (RepositoryItem) this.repLuedtProjNo;
      this.tlProjNo.FieldName = "ProjNo";
      this.tlProjNo.Name = "tlProjNo";
      this.tlProjNo.OptionsColumn.AllowSort = false;
      componentResourceManager.ApplyResources((object) this.repLuedtProjNo, "repLuedtProjNo");
      EditorButtonCollection buttons27 = this.repLuedtProjNo.Buttons;
      EditorButton[] buttons28 = new EditorButton[1];
      int index45 = 0;
      EditorButton editorButton14 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("repLuedtProjNo.Buttons"));
      buttons28[index45] = editorButton14;
      buttons27.AddRange(buttons28);
      this.repLuedtProjNo.Name = "repLuedtProjNo";
      componentResourceManager.ApplyResources((object) this.tlDeptNo, "tlDeptNo");
      this.tlDeptNo.ColumnEdit = (RepositoryItem) this.repLuedtDeptNo;
      this.tlDeptNo.FieldName = "DeptNo";
      this.tlDeptNo.Name = "tlDeptNo";
      this.tlDeptNo.OptionsColumn.AllowSort = false;
      componentResourceManager.ApplyResources((object) this.repLuedtDeptNo, "repLuedtDeptNo");
      EditorButtonCollection buttons29 = this.repLuedtDeptNo.Buttons;
      EditorButton[] buttons30 = new EditorButton[1];
      int index46 = 0;
      EditorButton editorButton15 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("repLuedtDeptNo.Buttons"));
      buttons30[index46] = editorButton15;
      buttons29.AddRange(buttons30);
      this.repLuedtDeptNo.Name = "repLuedtDeptNo";
      componentResourceManager.ApplyResources((object) this.tlRate, "tlRate");
      this.tlRate.FieldName = "Rate";
      this.tlRate.Name = "tlRate";
      this.tlRate.OptionsColumn.AllowEdit = false;
      this.tlRate.OptionsColumn.AllowSort = false;
      componentResourceManager.ApplyResources((object) this.tlQty, "tlQty");
      this.tlQty.FieldName = "Qty";
      this.tlQty.Name = "tlQty";
      this.tlQty.OptionsColumn.AllowSort = false;
      componentResourceManager.ApplyResources((object) this.tlItemCost, "tlItemCost");
      this.tlItemCost.FieldName = "ItemCost";
      this.tlItemCost.Name = "tlItemCost";
      this.tlItemCost.OptionsColumn.AllowSort = false;
      componentResourceManager.ApplyResources((object) this.tlOverheadCost, "tlOverheadCost");
      this.tlOverheadCost.FieldName = "OverHeadCost";
      this.tlOverheadCost.Name = "tlOverheadCost";
      this.tlOverheadCost.OptionsColumn.AllowSort = false;
      componentResourceManager.ApplyResources((object) this.tlSubTotal, "tlSubTotal");
      this.tlSubTotal.FieldName = "SubTotalCost";
      this.tlSubTotal.Name = "tlSubTotal";
      this.tlSubTotal.OptionsColumn.AllowSort = false;
      componentResourceManager.ApplyResources((object) this.tlRemark, "tlRemark");
      this.tlRemark.FieldName = "Remark";
      this.tlRemark.Name = "tlRemark";
      this.tlRemark.OptionsColumn.AllowSort = false;
      componentResourceManager.ApplyResources((object) this.tlPrintOut, "tlPrintOut");
      this.tlPrintOut.ColumnEdit = (RepositoryItem) this.repositoryItemCheckEdit1;
      this.tlPrintOut.FieldName = "PrintOut";
      this.tlPrintOut.Name = "tlPrintOut";
      this.tlPrintOut.OptionsColumn.AllowSort = false;
      componentResourceManager.ApplyResources((object) this.repositoryItemCheckEdit1, "repositoryItemCheckEdit1");
      this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
      this.repositoryItemCheckEdit1.ValueChecked = (object) "T";
      this.repositoryItemCheckEdit1.ValueUnchecked = (object) "F";
      componentResourceManager.ApplyResources((object) this.tlSeq, "tlSeq");
      this.tlSeq.FieldName = "Seq";
      this.tlSeq.Name = "tlSeq";
      this.tlSeq.OptionsColumn.AllowSort = false;
      this.tlSeq.OptionsColumn.ShowInCustomizationForm = false;
      componentResourceManager.ApplyResources((object) this.tlNumbering, "tlNumbering");
      this.tlNumbering.FieldName = "Numbering";
      this.tlNumbering.Name = "tlNumbering";
      this.tlNumbering.OptionsColumn.AllowSort = false;
      componentResourceManager.ApplyResources((object) this.tlIsBOMItem, "tlIsBOMItem");
      this.tlIsBOMItem.ColumnEdit = (RepositoryItem) this.repositoryItemCheckEdit1;
      this.tlIsBOMItem.FieldName = "IsBOMItem";
      this.tlIsBOMItem.Name = "tlIsBOMItem";
      this.tlIsBOMItem.OptionsColumn.AllowSort = false;
      this.tlIsBOMItem.OptionsColumn.ShowInCustomizationForm = false;
      componentResourceManager.ApplyResources((object) this.repositoryItemLookUpEdit1, "repositoryItemLookUpEdit1");
      EditorButtonCollection buttons31 = this.repositoryItemLookUpEdit1.Buttons;
      EditorButton[] buttons32 = new EditorButton[1];
      int index47 = 0;
      EditorButton editorButton16 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("repositoryItemLookUpEdit1.Buttons"));
      buttons32[index47] = editorButton16;
      buttons31.AddRange(buttons32);
      this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
      componentResourceManager.ApplyResources((object) this.repositoryItemLookUpEdit2, "repositoryItemLookUpEdit2");
      EditorButtonCollection buttons33 = this.repositoryItemLookUpEdit2.Buttons;
      EditorButton[] buttons34 = new EditorButton[1];
      int index48 = 0;
      EditorButton editorButton17 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("repositoryItemLookUpEdit2.Buttons"));
      buttons34[index48] = editorButton17;
      buttons33.AddRange(buttons34);
      this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
      this.panBottom.BorderStyle = BorderStyles.NoBorder;
      this.panBottom.Controls.Add((Control) this.ucInquiryStock1);
      this.panBottom.Controls.Add((Control) this.txtedtNetTotal);
      this.panBottom.Controls.Add((Control) this.txtedtAssemblyCost);
      this.panBottom.Controls.Add((Control) this.label10);
      this.panBottom.Controls.Add((Control) this.label11);
      this.panBottom.Controls.Add((Control) this.label12);
      this.panBottom.Controls.Add((Control) this.textEdtTotal);
      componentResourceManager.ApplyResources((object) this.panBottom, "panBottom");
      this.panBottom.Name = "panBottom";
      componentResourceManager.ApplyResources((object) this.ucInquiryStock1, "ucInquiryStock1");
      this.ucInquiryStock1.Name = "ucInquiryStock1";
      this.ucInquiryStock1.TabStop = false;
      componentResourceManager.ApplyResources((object) this.txtedtNetTotal, "txtedtNetTotal");
      this.txtedtNetTotal.Name = "txtedtNetTotal";
      componentResourceManager.ApplyResources((object) this.txtedtAssemblyCost, "txtedtAssemblyCost");
      this.txtedtAssemblyCost.Name = "txtedtAssemblyCost";
      componentResourceManager.ApplyResources((object) this.label10, "label10");
      this.label10.BackColor = Color.Transparent;
      this.label10.Name = "label10";
      componentResourceManager.ApplyResources((object) this.label11, "label11");
      this.label11.BackColor = Color.Transparent;
      this.label11.Name = "label11";
      componentResourceManager.ApplyResources((object) this.label12, "label12");
      this.label12.BackColor = Color.Transparent;
      this.label12.Name = "label12";
      componentResourceManager.ApplyResources((object) this.textEdtTotal, "textEdtTotal");
      this.textEdtTotal.Name = "textEdtTotal";
      this.panTop.BorderStyle = BorderStyles.NoBorder;
      this.panTop.Controls.Add((Control) this.panBOMOptional);
      this.panTop.Controls.Add((Control) this.sBtnShowInstant);
      this.panTop.Controls.Add((Control) this.sBtnRangeSetting);
      this.panTop.Controls.Add((Control) this.sBtnSelectAllMain);
      this.panTop.Controls.Add((Control) this.sBtnUndoTree);
      this.panTop.Controls.Add((Control) this.sBtnSearch);
      this.panTop.Controls.Add((Control) this.sBtnAdd);
      this.panTop.Controls.Add((Control) this.sBtnRemove);
      this.panTop.Controls.Add((Control) this.sBtnMoveDown);
      this.panTop.Controls.Add((Control) this.sBtnMoveUp);
      this.panTop.Controls.Add((Control) this.sBtnInsert);
      componentResourceManager.ApplyResources((object) this.panTop, "panTop");
      this.panTop.Name = "panTop";
      this.panBOMOptional.BorderStyle = BorderStyles.NoBorder;
      this.panBOMOptional.Controls.Add((Control) this.sBtnApplyBOMOptional);
      this.panBOMOptional.Controls.Add((Control) this.chkedtChildItem);
      componentResourceManager.ApplyResources((object) this.panBOMOptional, "panBOMOptional");
      this.panBOMOptional.Name = "panBOMOptional";
      this.sBtnApplyBOMOptional.Appearance.Font = (Font) componentResourceManager.GetObject("sBtnApplyBOMOptional.Appearance.Font");
      this.sBtnApplyBOMOptional.Appearance.ForeColor = (Color) componentResourceManager.GetObject("sBtnApplyBOMOptional.Appearance.ForeColor");
      this.sBtnApplyBOMOptional.Appearance.Options.UseFont = true;
      this.sBtnApplyBOMOptional.Appearance.Options.UseForeColor = true;
      componentResourceManager.ApplyResources((object) this.sBtnApplyBOMOptional, "sBtnApplyBOMOptional");
      this.sBtnApplyBOMOptional.Name = "sBtnApplyBOMOptional";
      this.sBtnApplyBOMOptional.Click += new EventHandler(this.sBtnApplyBOMOptional_Click);
      componentResourceManager.ApplyResources((object) this.chkedtChildItem, "chkedtChildItem");
      this.chkedtChildItem.Name = "chkedtChildItem";
      this.chkedtChildItem.Properties.Appearance.Font = (Font) componentResourceManager.GetObject("chkedtChildItem.Properties.Appearance.Font");
      this.chkedtChildItem.Properties.Appearance.ForeColor = (Color) componentResourceManager.GetObject("chkedtChildItem.Properties.Appearance.ForeColor");
      this.chkedtChildItem.Properties.Appearance.Options.UseFont = true;
      this.chkedtChildItem.Properties.Appearance.Options.UseForeColor = true;
      this.chkedtChildItem.Properties.Caption = componentResourceManager.GetString("chkedtChildItem.Properties.Caption");
      componentResourceManager.ApplyResources((object) this.sBtnShowInstant, "sBtnShowInstant");
      this.sBtnShowInstant.Name = "sBtnShowInstant";
      this.sBtnShowInstant.TabStop = false;
      this.sBtnShowInstant.Click += new EventHandler(this.sBtnShowInstant_Click);
      this.sBtnRangeSetting.ImageIndex = 5;
      this.sBtnRangeSetting.ImageList = (object) this.imageList1;
      componentResourceManager.ApplyResources((object) this.sBtnRangeSetting, "sBtnRangeSetting");
      this.sBtnRangeSetting.Name = "sBtnRangeSetting";
      this.sBtnRangeSetting.TabStop = false;
      this.sBtnRangeSetting.Click += new EventHandler(this.sBtnRangeSetting_Click);
      this.sBtnSelectAllMain.ImageIndex = 4;
      this.sBtnSelectAllMain.ImageList = (object) this.imageList1;
      componentResourceManager.ApplyResources((object) this.sBtnSelectAllMain, "sBtnSelectAllMain");
      this.sBtnSelectAllMain.Name = "sBtnSelectAllMain";
      this.sBtnSelectAllMain.TabStop = false;
      this.sBtnSelectAllMain.Click += new EventHandler(this.sBtnSelectAllMain_Click);
      this.sBtnUndoTree.ImageIndex = 3;
      this.sBtnUndoTree.ImageList = (object) this.imageList1;
      componentResourceManager.ApplyResources((object) this.sBtnUndoTree, "sBtnUndoTree");
      this.sBtnUndoTree.Name = "sBtnUndoTree";
      this.sBtnUndoTree.TabStop = false;
      this.sBtnUndoTree.Click += new EventHandler(this.sBtnUndoTree_Click);
      this.sBtnSearch.ImageIndex = 6;
      this.sBtnSearch.ImageList = (object) this.imageList1;
      componentResourceManager.ApplyResources((object) this.sBtnSearch, "sBtnSearch");
      this.sBtnSearch.Name = "sBtnSearch";
      this.sBtnSearch.TabStop = false;
      this.sBtnSearch.Click += new EventHandler(this.sBtnSearch_Click);
      this.sBtnAdd.ImageIndex = 0;
      this.sBtnAdd.ImageList = (object) this.imageList1;
      componentResourceManager.ApplyResources((object) this.sBtnAdd, "sBtnAdd");
      this.sBtnAdd.Name = "sBtnAdd";
      this.sBtnAdd.TabStop = false;
      this.sBtnAdd.Click += new EventHandler(this.sBtnAdd_Click);
      this.sBtnRemove.ImageIndex = 1;
      this.sBtnRemove.ImageList = (object) this.imageList1;
      componentResourceManager.ApplyResources((object) this.sBtnRemove, "sBtnRemove");
      this.sBtnRemove.Name = "sBtnRemove";
      this.sBtnRemove.TabStop = false;
      this.sBtnRemove.Click += new EventHandler(this.sBtnRemove_Click);
      this.sBtnMoveDown.Image = (Image) componentResourceManager.GetObject("sBtnMoveDown.Image");
      componentResourceManager.ApplyResources((object) this.sBtnMoveDown, "sBtnMoveDown");
      this.sBtnMoveDown.Name = "sBtnMoveDown";
      this.sBtnMoveDown.TabStop = false;
      this.sBtnMoveDown.Click += new EventHandler(this.sBtnMoveDown_Click);
      this.sBtnMoveUp.Image = (Image) componentResourceManager.GetObject("sBtnMoveUp.Image");
      componentResourceManager.ApplyResources((object) this.sBtnMoveUp, "sBtnMoveUp");
      this.sBtnMoveUp.Name = "sBtnMoveUp";
      this.sBtnMoveUp.TabStop = false;
      this.sBtnMoveUp.Click += new EventHandler(this.sBtnMoveUp_Click);
      this.sBtnInsert.ImageIndex = 2;
      this.sBtnInsert.ImageList = (object) this.imageList1;
      componentResourceManager.ApplyResources((object) this.sBtnInsert, "sBtnInsert");
      this.sBtnInsert.Name = "sBtnInsert";
      this.sBtnInsert.TabStop = false;
      this.sBtnInsert.Click += new EventHandler(this.sBtnInsert_Click);
      this.tabPageRawMaterialStatus.Controls.Add((Control) this.gctlRawMaterial);
      this.tabPageRawMaterialStatus.Name = "tabPageRawMaterialStatus";
      componentResourceManager.ApplyResources((object) this.tabPageRawMaterialStatus, "tabPageRawMaterialStatus");
      componentResourceManager.ApplyResources((object) this.gctlRawMaterial, "gctlRawMaterial");
      this.gctlRawMaterial.EmbeddedNavigator.Buttons.Append.Visible = false;
      this.gctlRawMaterial.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
      this.gctlRawMaterial.EmbeddedNavigator.Buttons.Edit.Visible = false;
      this.gctlRawMaterial.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
      this.gctlRawMaterial.EmbeddedNavigator.Buttons.Remove.Visible = false;
      this.gctlRawMaterial.MainView = (BaseView) this.gvRawMaterial;
      this.gctlRawMaterial.Name = "gctlRawMaterial";
      this.gctlRawMaterial.UseEmbeddedNavigator = true;
      ViewRepositoryCollection viewCollection = this.gctlRawMaterial.ViewCollection;
      BaseView[] views = new BaseView[2];
      int index49 = 0;
      GridView gridView1 = this.gvRawMaterial;
      views[index49] = (BaseView) gridView1;
      int index50 = 1;
      GridView gridView2 = this.gridView1;
      views[index50] = (BaseView) gridView2;
      viewCollection.AddRange(views);
      GridColumnCollection columns3 = this.gvRawMaterial.Columns;
      GridColumn[] columns4 = new GridColumn[10];
      int index51 = 0;
      GridColumn gridColumn1 = this.colItemCode;
      columns4[index51] = gridColumn1;
      int index52 = 1;
      GridColumn gridColumn2 = this.colDescription;
      columns4[index52] = gridColumn2;
      int index53 = 2;
      GridColumn gridColumn3 = this.colLocation;
      columns4[index53] = gridColumn3;
      int index54 = 3;
      GridColumn gridColumn4 = this.colUOM;
      columns4[index54] = gridColumn4;
      int index55 = 4;
      GridColumn gridColumn5 = this.colBatchNo;
      columns4[index55] = gridColumn5;
      int index56 = 5;
      GridColumn gridColumn6 = this.colReqQty;
      columns4[index56] = gridColumn6;
      int index57 = 6;
      GridColumn gridColumn7 = this.colOnHandQty;
      columns4[index57] = gridColumn7;
      int index58 = 7;
      GridColumn gridColumn8 = this.colOnHandBal;
      columns4[index58] = gridColumn8;
      int index59 = 8;
      GridColumn gridColumn9 = this.colAvailableQty;
      columns4[index59] = gridColumn9;
      int index60 = 9;
      GridColumn gridColumn10 = this.colAvailableBal;
      columns4[index60] = gridColumn10;
      columns3.AddRange(columns4);
      this.gvRawMaterial.GridControl = this.gctlRawMaterial;
      this.gvRawMaterial.Name = "gvRawMaterial";
      this.gvRawMaterial.OptionsBehavior.Editable = false;
      componentResourceManager.ApplyResources((object) this.colItemCode, "colItemCode");
      this.colItemCode.FieldName = "ItemCode";
      this.colItemCode.Name = "colItemCode";
      componentResourceManager.ApplyResources((object) this.colDescription, "colDescription");
      this.colDescription.FieldName = "Description";
      this.colDescription.Name = "colDescription";
      componentResourceManager.ApplyResources((object) this.colLocation, "colLocation");
      this.colLocation.FieldName = "Location";
      this.colLocation.Name = "colLocation";
      componentResourceManager.ApplyResources((object) this.colUOM, "colUOM");
      this.colUOM.FieldName = "UOM";
      this.colUOM.Name = "colUOM";
      componentResourceManager.ApplyResources((object) this.colBatchNo, "colBatchNo");
      this.colBatchNo.FieldName = "BatchNo";
      this.colBatchNo.Name = "colBatchNo";
      componentResourceManager.ApplyResources((object) this.colReqQty, "colReqQty");
      this.colReqQty.FieldName = "ReqQty";
      this.colReqQty.Name = "colReqQty";
      componentResourceManager.ApplyResources((object) this.colOnHandQty, "colOnHandQty");
      this.colOnHandQty.FieldName = "OnHandQty";
      this.colOnHandQty.Name = "colOnHandQty";
      componentResourceManager.ApplyResources((object) this.colOnHandBal, "colOnHandBal");
      this.colOnHandBal.FieldName = "OnHandBal";
      this.colOnHandBal.Name = "colOnHandBal";
      componentResourceManager.ApplyResources((object) this.colAvailableQty, "colAvailableQty");
      this.colAvailableQty.FieldName = "AvailableQty";
      this.colAvailableQty.Name = "colAvailableQty";
      componentResourceManager.ApplyResources((object) this.colAvailableBal, "colAvailableBal");
      this.colAvailableBal.FieldName = "AvailableBal";
      this.colAvailableBal.Name = "colAvailableBal";
      this.gridView1.GridControl = this.gctlRawMaterial;
      this.gridView1.Name = "gridView1";
      this.tabPageMoreHeader.Controls.Add((Control) this.labelRemark4);
      this.tabPageMoreHeader.Controls.Add((Control) this.labelRemark3);
      this.tabPageMoreHeader.Controls.Add((Control) this.labelRemark2);
      this.tabPageMoreHeader.Controls.Add((Control) this.labelRemark1);
      this.tabPageMoreHeader.Controls.Add((Control) this.mruEdtRemark1);
      this.tabPageMoreHeader.Controls.Add((Control) this.mruEdtRemark2);
      this.tabPageMoreHeader.Controls.Add((Control) this.mruEdtRemark3);
      this.tabPageMoreHeader.Controls.Add((Control) this.mruEdtRemark4);
      this.tabPageMoreHeader.Name = "tabPageMoreHeader";
      componentResourceManager.ApplyResources((object) this.tabPageMoreHeader, "tabPageMoreHeader");
      componentResourceManager.ApplyResources((object) this.labelRemark4, "labelRemark4");
      this.labelRemark4.BackColor = Color.Transparent;
      this.labelRemark4.Name = "labelRemark4";
      componentResourceManager.ApplyResources((object) this.labelRemark3, "labelRemark3");
      this.labelRemark3.BackColor = Color.Transparent;
      this.labelRemark3.Name = "labelRemark3";
      componentResourceManager.ApplyResources((object) this.labelRemark2, "labelRemark2");
      this.labelRemark2.BackColor = Color.Transparent;
      this.labelRemark2.Name = "labelRemark2";
      componentResourceManager.ApplyResources((object) this.labelRemark1, "labelRemark1");
      this.labelRemark1.BackColor = Color.Transparent;
      this.labelRemark1.Name = "labelRemark1";
      componentResourceManager.ApplyResources((object) this.mruEdtRemark1, "mruEdtRemark1");
      this.mruEdtRemark1.Name = "mruEdtRemark1";
      EditorButtonCollection buttons35 = this.mruEdtRemark1.Properties.Buttons;
      EditorButton[] buttons36 = new EditorButton[1];
      int index61 = 0;
      EditorButton editorButton18 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("mruEdtRemark1.Properties.Buttons"));
      buttons36[index61] = editorButton18;
      buttons35.AddRange(buttons36);
      componentResourceManager.ApplyResources((object) this.mruEdtRemark2, "mruEdtRemark2");
      this.mruEdtRemark2.Name = "mruEdtRemark2";
      EditorButtonCollection buttons37 = this.mruEdtRemark2.Properties.Buttons;
      EditorButton[] buttons38 = new EditorButton[1];
      int index62 = 0;
      EditorButton editorButton19 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("mruEdtRemark2.Properties.Buttons"));
      buttons38[index62] = editorButton19;
      buttons37.AddRange(buttons38);
      componentResourceManager.ApplyResources((object) this.mruEdtRemark3, "mruEdtRemark3");
      this.mruEdtRemark3.Name = "mruEdtRemark3";
      EditorButtonCollection buttons39 = this.mruEdtRemark3.Properties.Buttons;
      EditorButton[] buttons40 = new EditorButton[1];
      int index63 = 0;
      EditorButton editorButton20 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("mruEdtRemark3.Properties.Buttons"));
      buttons40[index63] = editorButton20;
      buttons39.AddRange(buttons40);
      MRUEditItemCollection items2 = this.mruEdtRemark3.Properties.Items;
      object[] items3 = new object[1];
      int index64 = 0;
      string string1 = componentResourceManager.GetString("mruEdtRemark3.Properties.Items");
      items3[index64] = (object) string1;
      items2.AddRange(items3);
      componentResourceManager.ApplyResources((object) this.mruEdtRemark4, "mruEdtRemark4");
      this.mruEdtRemark4.Name = "mruEdtRemark4";
      EditorButtonCollection buttons41 = this.mruEdtRemark4.Properties.Buttons;
      EditorButton[] buttons42 = new EditorButton[1];
      int index65 = 0;
      EditorButton editorButton21 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("mruEdtRemark4.Properties.Buttons"));
      buttons42[index65] = editorButton21;
      buttons41.AddRange(buttons42);
      MRUEditItemCollection items4 = this.mruEdtRemark4.Properties.Items;
      object[] items5 = new object[1];
      int index66 = 0;
      string string2 = componentResourceManager.GetString("mruEdtRemark4.Properties.Items");
      items5[index66] = (object) string2;
      items4.AddRange(items5);
      this.tabPageExternalLink.Controls.Add((Control) this.externalLinkBox1);
      this.tabPageExternalLink.Name = "tabPageExternalLink";
      componentResourceManager.ApplyResources((object) this.tabPageExternalLink, "tabPageExternalLink");
      componentResourceManager.ApplyResources((object) this.externalLinkBox1, "externalLinkBox1");
      this.externalLinkBox1.Name = "externalLinkBox1";
      this.tabPageNote.Controls.Add((Control) this.memoEdtNote);
      this.tabPageNote.Name = "tabPageNote";
      componentResourceManager.ApplyResources((object) this.tabPageNote, "tabPageNote");
      componentResourceManager.ApplyResources((object) this.memoEdtNote, "memoEdtNote");
      this.memoEdtNote.Name = "memoEdtNote";
      this.panelControl1.BorderStyle = BorderStyles.NoBorder;
      this.panelControl1.Controls.Add((Control) this.tabControl1);
      componentResourceManager.ApplyResources((object) this.panelControl1, "panelControl1");
      this.panelControl1.Name = "panelControl1";
      this.barManager1.AllowCustomization = false;
      this.barManager1.AllowQuickCustomization = false;
      Bars bars1 = this.barManager1.Bars;
      Bar[] bars2 = new Bar[1];
      int index67 = 0;
      Bar bar = this.bar1;
      bars2[index67] = bar;
      bars1.AddRange(bars2);
      this.barManager1.DockControls.Add(this.barDockControlTop);
      this.barManager1.DockControls.Add(this.barDockControlBottom);
      this.barManager1.DockControls.Add(this.barDockControlLeft);
      this.barManager1.DockControls.Add(this.barDockControlRight);
      this.barManager1.Form = (Control) this;
      BarItems items6 = this.barManager1.Items;
      BarItem[] items7 = new BarItem[22];
      int index68 = 0;
      BarSubItem barSubItem1 = this.barSubItem1;
      items7[index68] = (BarItem) barSubItem1;
      int index69 = 1;
      BarButtonItem barButtonItem1 = this.barItemCopyWholeDocument;
      items7[index69] = (BarItem) barButtonItem1;
      int index70 = 2;
      BarButtonItem barButtonItem2 = this.barItemPasteWholeDocument;
      items7[index70] = (BarItem) barButtonItem2;
      int index71 = 3;
      BarButtonItem barButtonItem3 = this.barItemPasteItemDetailOnly;
      items7[index71] = (BarItem) barButtonItem3;
      int index72 = 4;
      BarSubItem barSubItem2 = this.barSubItem2;
      items7[index72] = (BarItem) barSubItem2;
      int index73 = 5;
      BarButtonItem barButtonItem4 = this.barItemCopyFrom;
      items7[index73] = (BarItem) barButtonItem4;
      int index74 = 6;
      BarButtonItem barButtonItem5 = this.barItemCopyTo;
      items7[index74] = (BarItem) barButtonItem5;
      int index75 = 7;
      BarButtonItem barButtonItem6 = this.barItemCopySelectedDetails;
      items7[index75] = (BarItem) barButtonItem6;
      int index76 = 8;
      BarButtonItem barButtonItem7 = this.iUndoMaster;
      items7[index76] = (BarItem) barButtonItem7;
      int index77 = 9;
      BarButtonItem barButtonItem8 = this.iEditDescriptionMRU;
      items7[index77] = (BarItem) barButtonItem8;
      int index78 = 10;
      BarButtonItem barButtonItem9 = this.barBtnSaveInKIVFolder;
      items7[index78] = (BarItem) barButtonItem9;
      int index79 = 11;
      BarButtonItem barButtonItem10 = this.barButtonItem1;
      items7[index79] = (BarItem) barButtonItem10;
      int index80 = 12;
      BarSubItem barSubItem3 = this.barSubItem3;
      items7[index80] = (BarItem) barSubItem3;
      int index81 = 13;
      BarButtonItem barButtonItem11 = this.barbtnTransferFromSO;
      items7[index81] = (BarItem) barButtonItem11;
      int index82 = 14;
      BarButtonItem barButtonItem12 = this.barbtnCheckTransferredToStatus;
      items7[index82] = (BarItem) barButtonItem12;
      int index83 = 15;
      BarButtonItem barButtonItem13 = this.barItemCopyAsTabDelimitedText;
      items7[index83] = (BarItem) barButtonItem13;
      int index84 = 16;
      BarSubItem barSubItem4 = this.barSubItem4;
      items7[index84] = (BarItem) barSubItem4;
      int index85 = 17;
      BarButtonItem barButtonItem14 = this.iEditRemark1MRU;
      items7[index85] = (BarItem) barButtonItem14;
      int index86 = 18;
      BarButtonItem barButtonItem15 = this.iEditRemark2MRU;
      items7[index86] = (BarItem) barButtonItem15;
      int index87 = 19;
      BarButtonItem barButtonItem16 = this.iEditRemark3MRU;
      items7[index87] = (BarItem) barButtonItem16;
      int index88 = 20;
      BarButtonItem barButtonItem17 = this.iEditRemark4MRU;
      items7[index88] = (BarItem) barButtonItem17;
      int index89 = 21;
      BarButtonItem barButtonItem18 = this.barBtnConvertLevel;
      items7[index89] = (BarItem) barButtonItem18;
      items6.AddRange(items7);
      this.barManager1.MainMenu = this.bar1;
      this.barManager1.MaxItemId = 23;
      this.bar1.BarName = "Custom 1";
      this.bar1.CanDockStyle = BarCanDockStyle.Top;
      this.bar1.DockCol = 0;
      this.bar1.DockRow = 0;
      this.bar1.DockStyle = BarDockStyle.Top;
      this.bar1.FloatLocation = new Point(783, 366);
      LinksInfo linksPersistInfo1 = this.bar1.LinksPersistInfo;
      LinkPersistInfo[] links1 = new LinkPersistInfo[3];
      int index90 = 0;
      LinkPersistInfo linkPersistInfo1 = new LinkPersistInfo((BarItem) this.barSubItem1);
      links1[index90] = linkPersistInfo1;
      int index91 = 1;
      LinkPersistInfo linkPersistInfo2 = new LinkPersistInfo((BarItem) this.barSubItem2);
      links1[index91] = linkPersistInfo2;
      int index92 = 2;
      LinkPersistInfo linkPersistInfo3 = new LinkPersistInfo((BarItem) this.barSubItem3);
      links1[index92] = linkPersistInfo3;
      linksPersistInfo1.AddRange(links1);
      this.bar1.OptionsBar.AllowQuickCustomization = false;
      this.bar1.OptionsBar.DrawDragBorder = false;
      this.bar1.OptionsBar.MultiLine = true;
      this.bar1.OptionsBar.UseWholeRow = true;
      componentResourceManager.ApplyResources((object) this.bar1, "bar1");
      componentResourceManager.ApplyResources((object) this.barSubItem1, "barSubItem1");
      this.barSubItem1.Id = 0;
      LinksInfo linksPersistInfo2 = this.barSubItem1.LinksPersistInfo;
      LinkPersistInfo[] links2 = new LinkPersistInfo[9];
      int index93 = 0;
      LinkPersistInfo linkPersistInfo4 = new LinkPersistInfo((BarItem) this.barItemCopyWholeDocument);
      links2[index93] = linkPersistInfo4;
      int index94 = 1;
      LinkPersistInfo linkPersistInfo5 = new LinkPersistInfo((BarItem) this.barItemCopySelectedDetails);
      links2[index94] = linkPersistInfo5;
      int index95 = 2;
      LinkPersistInfo linkPersistInfo6 = new LinkPersistInfo((BarItem) this.barItemCopyAsTabDelimitedText);
      links2[index95] = linkPersistInfo6;
      int index96 = 3;
      LinkPersistInfo linkPersistInfo7 = new LinkPersistInfo((BarItem) this.barItemPasteWholeDocument, true);
      links2[index96] = linkPersistInfo7;
      int index97 = 4;
      LinkPersistInfo linkPersistInfo8 = new LinkPersistInfo((BarItem) this.barItemPasteItemDetailOnly);
      links2[index97] = linkPersistInfo8;
      int index98 = 5;
      LinkPersistInfo linkPersistInfo9 = new LinkPersistInfo((BarItem) this.iUndoMaster, true);
      links2[index98] = linkPersistInfo9;
      int index99 = 6;
      LinkPersistInfo linkPersistInfo10 = new LinkPersistInfo((BarItem) this.barBtnSaveInKIVFolder, true);
      links2[index99] = linkPersistInfo10;
      int index100 = 7;
      LinkPersistInfo linkPersistInfo11 = new LinkPersistInfo((BarItem) this.barSubItem4, true);
      links2[index100] = linkPersistInfo11;
      int index101 = 8;
      LinkPersistInfo linkPersistInfo12 = new LinkPersistInfo((BarItem) this.barBtnConvertLevel);
      links2[index101] = linkPersistInfo12;
      linksPersistInfo2.AddRange(links2);
      this.barSubItem1.Name = "barSubItem1";
      this.barSubItem1.Popup += new EventHandler(this.barSubItem1_Popup);
      componentResourceManager.ApplyResources((object) this.barItemCopyWholeDocument, "barItemCopyWholeDocument");
      this.barItemCopyWholeDocument.Id = 1;
      this.barItemCopyWholeDocument.Name = "barItemCopyWholeDocument";
      this.barItemCopyWholeDocument.ItemClick += new ItemClickEventHandler(this.barItemCopyWholeDocument_ItemClick);
      componentResourceManager.ApplyResources((object) this.barItemCopySelectedDetails, "barItemCopySelectedDetails");
      this.barItemCopySelectedDetails.Id = 7;
      this.barItemCopySelectedDetails.Name = "barItemCopySelectedDetails";
      this.barItemCopySelectedDetails.ItemClick += new ItemClickEventHandler(this.barItemCopySelectedDetails_ItemClick);
      componentResourceManager.ApplyResources((object) this.barItemCopyAsTabDelimitedText, "barItemCopyAsTabDelimitedText");
      this.barItemCopyAsTabDelimitedText.Id = 16;
      this.barItemCopyAsTabDelimitedText.Name = "barItemCopyAsTabDelimitedText";
      this.barItemCopyAsTabDelimitedText.ItemClick += new ItemClickEventHandler(this.barItemCopyAsTabDelimitedText_ItemClick);
      componentResourceManager.ApplyResources((object) this.barItemPasteWholeDocument, "barItemPasteWholeDocument");
      this.barItemPasteWholeDocument.Id = 2;
      this.barItemPasteWholeDocument.Name = "barItemPasteWholeDocument";
      this.barItemPasteWholeDocument.ItemClick += new ItemClickEventHandler(this.barItemPasteWholeDocument_ItemClick);
      componentResourceManager.ApplyResources((object) this.barItemPasteItemDetailOnly, "barItemPasteItemDetailOnly");
      this.barItemPasteItemDetailOnly.Id = 3;
      this.barItemPasteItemDetailOnly.Name = "barItemPasteItemDetailOnly";
      this.barItemPasteItemDetailOnly.ItemClick += new ItemClickEventHandler(this.barItemPasteItemDetailOnly_ItemClick);
      componentResourceManager.ApplyResources((object) this.iUndoMaster, "iUndoMaster");
      this.iUndoMaster.Id = 8;
      this.iUndoMaster.Name = "iUndoMaster";
      this.iUndoMaster.ItemClick += new ItemClickEventHandler(this.iUndoMaster_ItemClick);
      componentResourceManager.ApplyResources((object) this.barBtnSaveInKIVFolder, "barBtnSaveInKIVFolder");
      this.barBtnSaveInKIVFolder.Id = 11;
      this.barBtnSaveInKIVFolder.Name = "barBtnSaveInKIVFolder";
      this.barBtnSaveInKIVFolder.ItemClick += new ItemClickEventHandler(this.barBtnSaveInKIVFolder_ItemClick);
      componentResourceManager.ApplyResources((object) this.barSubItem4, "barSubItem4");
      this.barSubItem4.Id = 17;
      LinksInfo linksPersistInfo3 = this.barSubItem4.LinksPersistInfo;
      LinkPersistInfo[] links3 = new LinkPersistInfo[5];
      int index102 = 0;
      LinkPersistInfo linkPersistInfo13 = new LinkPersistInfo((BarItem) this.iEditDescriptionMRU);
      links3[index102] = linkPersistInfo13;
      int index103 = 1;
      LinkPersistInfo linkPersistInfo14 = new LinkPersistInfo((BarItem) this.iEditRemark1MRU);
      links3[index103] = linkPersistInfo14;
      int index104 = 2;
      LinkPersistInfo linkPersistInfo15 = new LinkPersistInfo((BarItem) this.iEditRemark2MRU);
      links3[index104] = linkPersistInfo15;
      int index105 = 3;
      LinkPersistInfo linkPersistInfo16 = new LinkPersistInfo((BarItem) this.iEditRemark3MRU);
      links3[index105] = linkPersistInfo16;
      int index106 = 4;
      LinkPersistInfo linkPersistInfo17 = new LinkPersistInfo((BarItem) this.iEditRemark4MRU);
      links3[index106] = linkPersistInfo17;
      linksPersistInfo3.AddRange(links3);
      this.barSubItem4.Name = "barSubItem4";
      componentResourceManager.ApplyResources((object) this.iEditDescriptionMRU, "iEditDescriptionMRU");
      this.iEditDescriptionMRU.Id = 10;
      this.iEditDescriptionMRU.Name = "iEditDescriptionMRU";
      this.iEditDescriptionMRU.ItemClick += new ItemClickEventHandler(this.iEditMRUItem_ItemClick);
      componentResourceManager.ApplyResources((object) this.iEditRemark1MRU, "iEditRemark1MRU");
      this.iEditRemark1MRU.Id = 18;
      this.iEditRemark1MRU.Name = "iEditRemark1MRU";
      this.iEditRemark1MRU.ItemClick += new ItemClickEventHandler(this.iEditRemark1MRU_ItemClick);
      componentResourceManager.ApplyResources((object) this.iEditRemark2MRU, "iEditRemark2MRU");
      this.iEditRemark2MRU.Id = 19;
      this.iEditRemark2MRU.Name = "iEditRemark2MRU";
      this.iEditRemark2MRU.ItemClick += new ItemClickEventHandler(this.iEditRemark2MRU_ItemClick);
      componentResourceManager.ApplyResources((object) this.iEditRemark3MRU, "iEditRemark3MRU");
      this.iEditRemark3MRU.Id = 20;
      this.iEditRemark3MRU.Name = "iEditRemark3MRU";
      this.iEditRemark3MRU.ItemClick += new ItemClickEventHandler(this.iEditRemark3MRU_ItemClick);
      componentResourceManager.ApplyResources((object) this.iEditRemark4MRU, "iEditRemark4MRU");
      this.iEditRemark4MRU.Id = 21;
      this.iEditRemark4MRU.Name = "iEditRemark4MRU";
      this.iEditRemark4MRU.ItemClick += new ItemClickEventHandler(this.iEditRemark4MRU_ItemClick);
      componentResourceManager.ApplyResources((object) this.barBtnConvertLevel, "barBtnConvertLevel");
      this.barBtnConvertLevel.Id = 22;
      this.barBtnConvertLevel.Name = "barBtnConvertLevel";
      this.barBtnConvertLevel.ItemClick += new ItemClickEventHandler(this.barBtnConvertLevel_ItemClick);
      componentResourceManager.ApplyResources((object) this.barSubItem2, "barSubItem2");
      this.barSubItem2.Id = 4;
      LinksInfo linksPersistInfo4 = this.barSubItem2.LinksPersistInfo;
      LinkPersistInfo[] links4 = new LinkPersistInfo[2];
      int index107 = 0;
      LinkPersistInfo linkPersistInfo18 = new LinkPersistInfo((BarItem) this.barItemCopyFrom);
      links4[index107] = linkPersistInfo18;
      int index108 = 1;
      LinkPersistInfo linkPersistInfo19 = new LinkPersistInfo((BarItem) this.barItemCopyTo);
      links4[index108] = linkPersistInfo19;
      linksPersistInfo4.AddRange(links4);
      this.barSubItem2.Name = "barSubItem2";
      this.barSubItem2.Popup += new EventHandler(this.barSubItem2_Popup);
      componentResourceManager.ApplyResources((object) this.barItemCopyFrom, "barItemCopyFrom");
      this.barItemCopyFrom.Id = 5;
      this.barItemCopyFrom.Name = "barItemCopyFrom";
      this.barItemCopyFrom.ItemClick += new ItemClickEventHandler(this.barItemCopyFrom_ItemClick);
      componentResourceManager.ApplyResources((object) this.barItemCopyTo, "barItemCopyTo");
      this.barItemCopyTo.Id = 6;
      this.barItemCopyTo.Name = "barItemCopyTo";
      this.barItemCopyTo.ItemClick += new ItemClickEventHandler(this.barItemCopyTo_ItemClick);
      componentResourceManager.ApplyResources((object) this.barSubItem3, "barSubItem3");
      this.barSubItem3.Id = 13;
      LinksInfo linksPersistInfo5 = this.barSubItem3.LinksPersistInfo;
      LinkPersistInfo[] links5 = new LinkPersistInfo[2];
      int index109 = 0;
      LinkPersistInfo linkPersistInfo20 = new LinkPersistInfo((BarItem) this.barbtnTransferFromSO);
      links5[index109] = linkPersistInfo20;
      int index110 = 1;
      LinkPersistInfo linkPersistInfo21 = new LinkPersistInfo((BarItem) this.barbtnCheckTransferredToStatus, true);
      links5[index110] = linkPersistInfo21;
      linksPersistInfo5.AddRange(links5);
      this.barSubItem3.Name = "barSubItem3";
      componentResourceManager.ApplyResources((object) this.barbtnTransferFromSO, "barbtnTransferFromSO");
      this.barbtnTransferFromSO.Id = 14;
      this.barbtnTransferFromSO.Name = "barbtnTransferFromSO";
      this.barbtnTransferFromSO.ItemClick += new ItemClickEventHandler(this.barbtnTransferFromSO_ItemClick);
      componentResourceManager.ApplyResources((object) this.barbtnCheckTransferredToStatus, "barbtnCheckTransferredToStatus");
      this.barbtnCheckTransferredToStatus.Id = 15;
      this.barbtnCheckTransferredToStatus.Name = "barbtnCheckTransferredToStatus";
      this.barbtnCheckTransferredToStatus.ItemClick += new ItemClickEventHandler(this.barbtnCheckTransferredToStatus_ItemClick);
      this.barDockControlTop.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlTop, "barDockControlTop");
      this.barDockControlBottom.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlBottom, "barDockControlBottom");
      this.barDockControlLeft.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlLeft, "barDockControlLeft");
      this.barDockControlRight.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlRight, "barDockControlRight");
      componentResourceManager.ApplyResources((object) this.barButtonItem1, "barButtonItem1");
      this.barButtonItem1.Id = 12;
      this.barButtonItem1.Name = "barButtonItem1";
      this.timer1.Interval = 10000;
      this.timer1.Tick += new EventHandler(this.timer1_Tick);
      this.timer2.Interval = 300000;
      this.timer2.Tick += new EventHandler(this.timer2_Tick);
      this.AllowDrop = true;
      componentResourceManager.ApplyResources((object) this, "$this");
      this.AutoScaleMode = AutoScaleMode.Dpi;
      this.CancelButton = (IButtonControl) this.sbtnCancel;
      this.Controls.Add((Control) this.panelControl1);
      this.Controls.Add((Control) this.panelHeader);
      this.Controls.Add((Control) this.panel2);
      this.Controls.Add((Control) this.barDockControlLeft);
      this.Controls.Add((Control) this.barDockControlRight);
      this.Controls.Add((Control) this.barDockControlBottom);
      this.Controls.Add((Control) this.barDockControlTop);
      this.KeyPreview = true;
      this.Name = "FormStockWorkOrderEntry";
      this.Activated += new EventHandler(this.FormStockAssemblyEntry_Activated);
      this.Closing += new CancelEventHandler(this.FormStockAssemblyEntry_Closing);
      this.Deactivate += new EventHandler(this.FormStockAssemblyEntry_Deactivate);
      this.Load += new EventHandler(this.FormStockAssemblyEntry_Load);
      this.VisibleChanged += new EventHandler(this.FormStockWorkOrderEntry_VisibleChanged);
      this.DragDrop += new DragEventHandler(this.FormStockAssemblyEntry_DragDrop);
      this.DragOver += new DragEventHandler(this.FormStockAssemblyEntry_DragOver);
      this.KeyDown += new KeyEventHandler(this.FormStockAssemblyEntry_KeyDown);
      this.panelHeader.EndInit();
      this.panelHeader.ResumeLayout(false);
      this.panelHeader.PerformLayout();
      this.grboxItemStock.EndInit();
      this.grboxItemStock.ResumeLayout(false);
      this.grboxItemStock.PerformLayout();
      this.panDept.EndInit();
      this.panDept.ResumeLayout(false);
      this.panDept.PerformLayout();
      this.luDeptNo.Properties.EndInit();
      this.panProject.EndInit();
      this.panProject.ResumeLayout(false);
      this.panProject.PerformLayout();
      this.luProject.Properties.EndInit();
      this.panLocation.EndInit();
      this.panLocation.ResumeLayout(false);
      this.panLocation.PerformLayout();
      this.luLocation.Properties.EndInit();
      this.panBatch.EndInit();
      this.panBatch.ResumeLayout(false);
      this.panBatch.PerformLayout();
      this.luBatchNo.Properties.EndInit();
      this.txtedtQuantity.Properties.EndInit();
      this.luAssemblyItem.Properties.EndInit();
      this.textEdtExpectedCompletedDate.Properties.EndInit();
      this.textEdtRefDocNo.Properties.EndInit();
      this.dateEdtDate.Properties.CalendarTimeProperties.EndInit();
      this.dateEdtDate.Properties.EndInit();
      this.txtEdtStockAssemblyNo.Properties.EndInit();
      this.mruEdtDescription.Properties.EndInit();
      this.luEdtDocNoFormat.Properties.EndInit();
      this.panel2.EndInit();
      this.panel2.ResumeLayout(false);
      this.chkedtNextRecord.Properties.EndInit();
      this.tabControl1.EndInit();
      this.tabControl1.ResumeLayout(false);
      this.tabPageMainTree.ResumeLayout(false);
      this.treeListDetail.EndInit();
      this.repLuedtItem.EndInit();
      this.repBtnedtFurtherDesc.EndInit();
      this.repLuedtLocation.EndInit();
      this.repLuedtBatchNo.EndInit();
      this.repLuedtProjNo.EndInit();
      this.repLuedtDeptNo.EndInit();
      this.repositoryItemCheckEdit1.EndInit();
      this.repositoryItemLookUpEdit1.EndInit();
      this.repositoryItemLookUpEdit2.EndInit();
      this.panBottom.EndInit();
      this.panBottom.ResumeLayout(false);
      this.panBottom.PerformLayout();
      this.txtedtNetTotal.Properties.EndInit();
      this.txtedtAssemblyCost.Properties.EndInit();
      this.textEdtTotal.Properties.EndInit();
      this.panTop.EndInit();
      this.panTop.ResumeLayout(false);
      this.panBOMOptional.EndInit();
      this.panBOMOptional.ResumeLayout(false);
      this.chkedtChildItem.Properties.EndInit();
      this.tabPageRawMaterialStatus.ResumeLayout(false);
      this.gctlRawMaterial.EndInit();
      this.gvRawMaterial.EndInit();
      this.gridView1.EndInit();
      this.tabPageMoreHeader.ResumeLayout(false);
      this.tabPageMoreHeader.PerformLayout();
      this.mruEdtRemark1.Properties.EndInit();
      this.mruEdtRemark2.Properties.EndInit();
      this.mruEdtRemark3.Properties.EndInit();
      this.mruEdtRemark4.Properties.EndInit();
      this.tabPageExternalLink.ResumeLayout(false);
      this.tabPageNote.ResumeLayout(false);
      this.panelControl1.EndInit();
      this.panelControl1.ResumeLayout(false);
      this.barManager1.EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

        private void Navigator_ButtonClick(object sender, BCE.Controls.NavigatorButtonClickEventArgs e)
        {
            throw new NotImplementedException();
        }

        private class MyFilterOption : EnterKeyMessageFilter.FilterOption
    {
      private StockWorkOrder myDocument;

      public StockWorkOrder Document
      {
        get
        {
          return this.myDocument;
        }
        set
        {
          this.myDocument = value;
        }
      }

      public MyFilterOption()
      {
        this.AddGridControl("myGridControl");
        this.SetNoFilterTabPage("tabPageExternalLink");
        this.SetNoFilterTabPage("tabPageNote");
      }

      protected override bool CanDelete(DataRow row)
      {
        if (this.myDocument != null && this.myDocument.Action == StockWorkOrderAction.View || (row["ItemCode"] != DBNull.Value || row["Description"].ToString().Length != 0))
          return false;
        else
          return BCE.Data.Convert.ToDecimal(row["SubTotal"]) == Decimal.Zero;
      }
    }

    public class FormEventArgs
    {
      private FormStockWorkOrderEntry myForm;
      private StockWorkOrder myStockWorkOrder;

      public StockWorkOrder StockWorkOrder
      {
        get
        {
          return this.myStockWorkOrder;
        }
      }

      public FormStockWorkOrderEntry Form
      {
        get
        {
          return this.myForm;
        }
      }

      public PanelControl HeaderPanel
      {
        get
        {
          return this.myForm.panelHeader;
        }
      }

      public TreeList TreeList
      {
        get
        {
          return this.myForm.treeListDetail;
        }
      }

      public XtraTabControl TabControl
      {
        get
        {
          return this.myForm.tabControl1;
        }
      }

      public DataTable MasterTable
      {
        get
        {
          return this.myStockWorkOrder.DataTableMaster;
        }
      }

      public DataTable DetailTable
      {
        get
        {
          return this.myStockWorkOrder.DataTableDetail;
        }
      }

      public EditWindowMode EditWindowMode
      {
        get
        {
          if (this.myStockWorkOrder.Action == StockWorkOrderAction.New)
            return EditWindowMode.New;
          else if (this.myStockWorkOrder.Action == StockWorkOrderAction.Edit)
            return EditWindowMode.Edit;
          else
            return EditWindowMode.View;
        }
      }

      public DBSetting DBSetting
      {
        get
        {
          return this.myForm.myDBSetting;
        }
      }

      public FormEventArgs(FormStockWorkOrderEntry form, StockWorkOrder doc)
      {
        this.myForm = form;
        this.myStockWorkOrder = doc;
      }
    }

    public class FormInitializeEventArgs : FormStockWorkOrderEntry.FormEventArgs
    {
      public FormInitializeEventArgs(FormStockWorkOrderEntry form, StockWorkOrder doc)
        : base(form, doc)
      {
      }
    }

    public class FormDataBindingEventArgs : FormStockWorkOrderEntry.FormEventArgs
    {
      public FormDataBindingEventArgs(FormStockWorkOrderEntry form, StockWorkOrder doc)
        : base(form, doc)
      {
      }
    }

    public class FormShowEventArgs : FormStockWorkOrderEntry.FormEventArgs
    {
      public FormShowEventArgs(FormStockWorkOrderEntry form, StockWorkOrder doc)
        : base(form, doc)
      {
      }
    }

    public class FormClosedEventArgs : FormStockWorkOrderEntry.FormEventArgs
    {
      public FormClosedEventArgs(FormStockWorkOrderEntry form, StockWorkOrder doc)
        : base(form, doc)
      {
      }
    }

    public class FormBeforeSaveEventArgs : FormStockWorkOrderEntry.FormEventArgs
    {
      public FormBeforeSaveEventArgs(FormStockWorkOrderEntry form, StockWorkOrder doc)
        : base(form, doc)
      {
      }
    }

    public class AfterCopyToNewDocumentEventArgs : StockWorkOrderEventArgs
    {
      private StockWorkOrder myFromStock;

      public StockWorkOrderRecord FromMasterRecord
      {
        get
        {
          return new StockWorkOrderRecord(this.myFromStock);
        }
      }

      internal AfterCopyToNewDocumentEventArgs(StockWorkOrder doc, StockWorkOrder fromDoc)
        : base(doc)
      {
        this.myFromStock = fromDoc;
      }
    }

    public class AfterCopyFromOtherDocumentEventArgs : StockWorkOrderEventArgs
    {
      private StockWorkOrder myFromStock;

      public StockWorkOrderRecord FromMasterRecord
      {
        get
        {
          return new StockWorkOrderRecord(this.myFromStock);
        }
      }

      internal AfterCopyFromOtherDocumentEventArgs(StockWorkOrder doc, StockWorkOrder fromDoc)
        : base(doc)
      {
        this.myFromStock = fromDoc;
      }
    }
  }
}
