﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.FormTransferredToStatus
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount;
using BCE.AutoCount.XtraUtils;
using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System.ComponentModel;
using System.Windows.Forms;

namespace RPASystem.WorkOrder
{
  public class FormTransferredToStatus : XtraForm
  {
    private StockWorkOrder myStockAssemblyOrder;
    private IContainer components;
    private PanelControl panelControl1;
    private SimpleButton sbtnOK;
    private GridColumn colAssemblyCost;
    private GridColumn colDesc1;
    private GridColumn colDescription;
    private GridColumn colDocDate;
    private GridColumn colDocNo;
    private GridColumn colItemCode;
    private GridColumn colItemCode1;
    private GridColumn colItemCost1;
    private GridColumn colNetTotal;
    private GridColumn colOverHeadCost1;
    private GridColumn colQty;
    private GridColumn colQty1;
    private GridColumn colRate1;
    private GridColumn colSeq1;
    private GridColumn colSubTotalCost;
    private GridColumn colTotal;
    private GridControl gridctrTransfer;
    private GridView gvDetail;
    private GridView gvMaster;

    public FormTransferredToStatus(StockWorkOrder aStockAssemblyOrder)
    {
      this.InitializeComponent();
      this.Icon = BCE.AutoCount.Application.Icon;
      this.myStockAssemblyOrder = aStockAssemblyOrder;
      this.InitFormControls();
      this.gridctrTransfer.LevelTree.Nodes.Clear();
      this.gridctrTransfer.LevelTree.Nodes.Add("MasterDetail", (BaseView) this.gvDetail);
      this.gridctrTransfer.DataSource = (object) this.myStockAssemblyOrder.Command.LoadPartialTransferStatus(aStockAssemblyOrder.DocKey).Tables["Master"];
    }

    private void InitFormControls()
    {
      FormControlUtil formControlUtil = new FormControlUtil(this.myStockAssemblyOrder.Command.DBSetting, false);
      string fieldname1 = "Qty";
      string fieldtype1 = "Quantity";
      formControlUtil.AddField(fieldname1, fieldtype1);
      string fieldname2 = "DocDate";
      string fieldtype2 = "Date";
      formControlUtil.AddField(fieldname2, fieldtype2);
      string fieldname3 = "AssemblyCost";
      string fieldtype3 = "Cost";
      formControlUtil.AddField(fieldname3, fieldtype3);
      string fieldname4 = "Total";
      string fieldtype4 = "Cost";
      formControlUtil.AddField(fieldname4, fieldtype4);
      string fieldname5 = "NetTotal";
      string fieldtype5 = "Cost";
      formControlUtil.AddField(fieldname5, fieldtype5);
      string fieldname6 = "DocDate";
      string fieldtype6 = "Date";
      formControlUtil.AddField(fieldname6, fieldtype6);
      string fieldname7 = "SubTotalCost";
      string fieldtype7 = "Cost";
      formControlUtil.AddField(fieldname7, fieldtype7);
      string fieldname8 = "Rate";
      string fieldtype8 = "Quantity";
      formControlUtil.AddField(fieldname8, fieldtype8);
      FormTransferredToStatus transferredToStatus = this;
      formControlUtil.InitControls((Control) transferredToStatus);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormTransferredToStatus));
      GridLevelNode gridLevelNode1 = new GridLevelNode();
      this.gvDetail = new GridView();
      this.colItemCode1 = new GridColumn();
      this.colDesc1 = new GridColumn();
      this.colRate1 = new GridColumn();
      this.colQty1 = new GridColumn();
      this.colItemCost1 = new GridColumn();
      this.colOverHeadCost1 = new GridColumn();
      this.colSubTotalCost = new GridColumn();
      this.colSeq1 = new GridColumn();
      this.gridctrTransfer = new GridControl();
      this.gvMaster = new GridView();
      this.colDocNo = new GridColumn();
      this.colDocDate = new GridColumn();
      this.colDescription = new GridColumn();
      this.colItemCode = new GridColumn();
      this.colQty = new GridColumn();
      this.colTotal = new GridColumn();
      this.colAssemblyCost = new GridColumn();
      this.colNetTotal = new GridColumn();
      this.panelControl1 = new PanelControl();
      this.sbtnOK = new SimpleButton();
      this.gvDetail.BeginInit();
      this.gridctrTransfer.BeginInit();
      this.gvMaster.BeginInit();
      this.panelControl1.BeginInit();
      this.panelControl1.SuspendLayout();
      this.SuspendLayout();
      GridColumnCollection columns1 = this.gvDetail.Columns;
      GridColumn[] columns2 = new GridColumn[8];
      int index1 = 0;
      GridColumn gridColumn1 = this.colItemCode1;
      columns2[index1] = gridColumn1;
      int index2 = 1;
      GridColumn gridColumn2 = this.colDesc1;
      columns2[index2] = gridColumn2;
      int index3 = 2;
      GridColumn gridColumn3 = this.colRate1;
      columns2[index3] = gridColumn3;
      int index4 = 3;
      GridColumn gridColumn4 = this.colQty1;
      columns2[index4] = gridColumn4;
      int index5 = 4;
      GridColumn gridColumn5 = this.colItemCost1;
      columns2[index5] = gridColumn5;
      int index6 = 5;
      GridColumn gridColumn6 = this.colOverHeadCost1;
      columns2[index6] = gridColumn6;
      int index7 = 6;
      GridColumn gridColumn7 = this.colSubTotalCost;
      columns2[index7] = gridColumn7;
      int index8 = 7;
      GridColumn gridColumn8 = this.colSeq1;
      columns2[index8] = gridColumn8;
      columns1.AddRange(columns2);
      this.gvDetail.GridControl = this.gridctrTransfer;
      this.gvDetail.Name = "gvDetail";
      this.gvDetail.OptionsBehavior.Editable = false;
      this.gvDetail.OptionsCustomization.AllowFilter = false;
      this.gvDetail.OptionsDetail.AllowZoomDetail = false;
      this.gvDetail.OptionsView.ShowGroupPanel = false;
      GridColumnSortInfoCollection sortInfo = this.gvDetail.SortInfo;
      GridColumnSortInfo[] sortInfos = new GridColumnSortInfo[1];
      int index9 = 0;
      GridColumnSortInfo gridColumnSortInfo = new GridColumnSortInfo(this.colSeq1, ColumnSortOrder.Ascending);
      sortInfos[index9] = gridColumnSortInfo;
      sortInfo.AddRange(sortInfos);
      componentResourceManager.ApplyResources((object) this.colItemCode1, "colItemCode1");
      this.colItemCode1.FieldName = "ItemCode";
      this.colItemCode1.Name = "colItemCode1";
      componentResourceManager.ApplyResources((object) this.colDesc1, "colDesc1");
      this.colDesc1.FieldName = "Description";
      this.colDesc1.Name = "colDesc1";
      componentResourceManager.ApplyResources((object) this.colRate1, "colRate1");
      this.colRate1.FieldName = "Rate";
      this.colRate1.Name = "colRate1";
      componentResourceManager.ApplyResources((object) this.colQty1, "colQty1");
      this.colQty1.FieldName = "Qty";
      this.colQty1.Name = "colQty1";
      componentResourceManager.ApplyResources((object) this.colItemCost1, "colItemCost1");
      this.colItemCost1.FieldName = "ItemCost";
      this.colItemCost1.Name = "colItemCost1";
      componentResourceManager.ApplyResources((object) this.colOverHeadCost1, "colOverHeadCost1");
      this.colOverHeadCost1.FieldName = "OverHeadCost";
      this.colOverHeadCost1.Name = "colOverHeadCost1";
      componentResourceManager.ApplyResources((object) this.colSubTotalCost, "colSubTotalCost");
      this.colSubTotalCost.FieldName = "OverHeadCost";
      this.colSubTotalCost.Name = "colSubTotalCost";
      componentResourceManager.ApplyResources((object) this.colSeq1, "colSeq1");
      this.colSeq1.FieldName = "Seq";
      this.colSeq1.Name = "colSeq1";
      this.colSeq1.OptionsColumn.ShowInCustomizationForm = false;
      componentResourceManager.ApplyResources((object) this.gridctrTransfer, "gridctrTransfer");
      gridLevelNode1.LevelTemplate = (BaseView) this.gvDetail;
      gridLevelNode1.RelationName = "Level1";
      GridLevelNodeCollection nodes1 = this.gridctrTransfer.LevelTree.Nodes;
      GridLevelNode[] nodes2 = new GridLevelNode[1];
      int index10 = 0;
      GridLevelNode gridLevelNode2 = gridLevelNode1;
      nodes2[index10] = gridLevelNode2;
      nodes1.AddRange(nodes2);
      this.gridctrTransfer.MainView = (BaseView) this.gvMaster;
      this.gridctrTransfer.Name = "gridctrTransfer";
      ViewRepositoryCollection viewCollection = this.gridctrTransfer.ViewCollection;
      BaseView[] views = new BaseView[2];
      int index11 = 0;
      GridView gridView1 = this.gvMaster;
      views[index11] = (BaseView) gridView1;
      int index12 = 1;
      GridView gridView2 = this.gvDetail;
      views[index12] = (BaseView) gridView2;
      viewCollection.AddRange(views);
      GridColumnCollection columns3 = this.gvMaster.Columns;
      GridColumn[] columns4 = new GridColumn[8];
      int index13 = 0;
      GridColumn gridColumn9 = this.colDocNo;
      columns4[index13] = gridColumn9;
      int index14 = 1;
      GridColumn gridColumn10 = this.colDocDate;
      columns4[index14] = gridColumn10;
      int index15 = 2;
      GridColumn gridColumn11 = this.colDescription;
      columns4[index15] = gridColumn11;
      int index16 = 3;
      GridColumn gridColumn12 = this.colItemCode;
      columns4[index16] = gridColumn12;
      int index17 = 4;
      GridColumn gridColumn13 = this.colQty;
      columns4[index17] = gridColumn13;
      int index18 = 5;
      GridColumn gridColumn14 = this.colTotal;
      columns4[index18] = gridColumn14;
      int index19 = 6;
      GridColumn gridColumn15 = this.colAssemblyCost;
      columns4[index19] = gridColumn15;
      int index20 = 7;
      GridColumn gridColumn16 = this.colNetTotal;
      columns4[index20] = gridColumn16;
      columns3.AddRange(columns4);
      this.gvMaster.GridControl = this.gridctrTransfer;
      this.gvMaster.Name = "gvMaster";
      this.gvMaster.OptionsBehavior.Editable = false;
      this.gvMaster.OptionsCustomization.AllowFilter = false;
      this.gvMaster.OptionsDetail.AllowZoomDetail = false;
      componentResourceManager.ApplyResources((object) this.colDocNo, "colDocNo");
      this.colDocNo.FieldName = "DocNo";
      this.colDocNo.Name = "colDocNo";
      componentResourceManager.ApplyResources((object) this.colDocDate, "colDocDate");
      this.colDocDate.FieldName = "DocDate";
      this.colDocDate.Name = "colDocDate";
      componentResourceManager.ApplyResources((object) this.colDescription, "colDescription");
      this.colDescription.FieldName = "Description";
      this.colDescription.Name = "colDescription";
      componentResourceManager.ApplyResources((object) this.colItemCode, "colItemCode");
      this.colItemCode.FieldName = "ItemCode";
      this.colItemCode.Name = "colItemCode";
      componentResourceManager.ApplyResources((object) this.colQty, "colQty");
      this.colQty.FieldName = "Qty";
      this.colQty.Name = "colQty";
      componentResourceManager.ApplyResources((object) this.colTotal, "colTotal");
      this.colTotal.FieldName = "Total";
      this.colTotal.Name = "colTotal";
      componentResourceManager.ApplyResources((object) this.colAssemblyCost, "colAssemblyCost");
      this.colAssemblyCost.FieldName = "AssemblyCost";
      this.colAssemblyCost.Name = "colAssemblyCost";
      componentResourceManager.ApplyResources((object) this.colNetTotal, "colNetTotal");
      this.colNetTotal.FieldName = "NetTotal";
      this.colNetTotal.Name = "colNetTotal";
      this.panelControl1.BorderStyle = BorderStyles.NoBorder;
      this.panelControl1.Controls.Add((Control) this.sbtnOK);
      componentResourceManager.ApplyResources((object) this.panelControl1, "panelControl1");
      this.panelControl1.Name = "panelControl1";
      componentResourceManager.ApplyResources((object) this.sbtnOK, "sbtnOK");
      this.sbtnOK.DialogResult = DialogResult.OK;
      this.sbtnOK.Name = "sbtnOK";
      componentResourceManager.ApplyResources((object) this, "$this");
      this.AutoScaleMode = AutoScaleMode.Dpi;
      this.Controls.Add((Control) this.gridctrTransfer);
      this.Controls.Add((Control) this.panelControl1);
      this.Name = "FormTransferredToStatus";
      this.ShowInTaskbar = false;
      this.gvDetail.EndInit();
      this.gridctrTransfer.EndInit();
      this.gvMaster.EndInit();
      this.panelControl1.EndInit();
      this.panelControl1.ResumeLayout(false);
      this.ResumeLayout(false);
    }
  }
}
