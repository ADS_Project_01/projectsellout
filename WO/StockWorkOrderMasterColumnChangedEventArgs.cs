﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderMasterColumnChangedEventArgs
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Data;

namespace RPASystem.WorkOrder
{
  public class StockWorkOrderMasterColumnChangedEventArgs
  {
    private string myColumnName;
    private StockWorkOrder myStock;

    public string ChangedColumnName
    {
      get
      {
        return this.myColumnName;
      }
    }

    public StockWorkOrderRecord MasterRecord
    {
      get
      {
        return new StockWorkOrderRecord(this.myStock);
      }
    }

    public DBSetting DBSetting
    {
      get
      {
        return this.myStock.Command.DBSetting;
      }
    }

    internal StockWorkOrderMasterColumnChangedEventArgs(string columnName, StockWorkOrder doc)
    {
      this.myColumnName = columnName;
      this.myStock = doc;
    }
  }
}
