﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderDetailReportingCriteria
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.SearchFilter;
using BCE.Localization;
using System;
using System.Collections;

namespace RPASystem.WorkOrder
{
  [Serializable]
  public class StockWorkOrderDetailReportingCriteria : StockWorkOrderReportingCriteria
  {
    protected BCE.AutoCount.SearchFilter.Filter myItemCode;
    protected BCE.AutoCount.SearchFilter.Filter myItemGroup;
    protected BCE.AutoCount.SearchFilter.Filter myItemType;
    protected BCE.AutoCount.SearchFilter.Filter myLocation;
    protected BCE.AutoCount.SearchFilter.Filter myProjNo;
    protected BCE.AutoCount.SearchFilter.Filter myDeptNo;
    private bool myAdvancedOptions;
    private bool myCheckAll;

    public BCE.AutoCount.SearchFilter.Filter ItemCodeFilter
    {
      get
      {
        return this.myItemCode;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter ItemGroupFilter
    {
      get
      {
        return this.myItemGroup;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter ItemTypeFilter
    {
      get
      {
        return this.myItemType;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter LocationFilter
    {
      get
      {
        return this.myLocation;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter ProjecNoFilter
    {
      get
      {
        return this.myProjNo;
      }
    }

    public BCE.AutoCount.SearchFilter.Filter DeptNoFilter
    {
      get
      {
        return this.myDeptNo;
      }
    }

    public bool AdvancedOptions
    {
      get
      {
        return this.myAdvancedOptions;
      }
      set
      {
        this.myAdvancedOptions = value;
      }
    }

    public bool CheckAll
    {
      get
      {
        return this.myCheckAll;
      }
      set
      {
        this.myCheckAll = value;
      }
    }

    public new string[] ReadableTextArray
    {
      get
      {
        ArrayList arrayList1 = new ArrayList();
        string str1 = "";
        string str2 = this.myDateFilter.BuildReadableText(false);
        if (str2.Length > 0)
        {
          arrayList1.Add((object) (str1 + "Report Date: " + str2));
          str1 = "               ";
        }
        string str3 = this.myDocumentNoFilter.BuildReadableText(false);
        if (str3.Length > 0)
        {
          arrayList1.Add((object) (str1 + str3));
          str1 = "               ";
        }
        string str4 = this.myItemCode.BuildReadableText(false);
        if (str4.Length > 0)
        {
          arrayList1.Add((object) (str1 + str4));
          str1 = "               ";
        }
        string str5 = this.myItemGroup.BuildReadableText(false);
        if (str5.Length > 0)
        {
          arrayList1.Add((object) (str1 + str5));
          str1 = "               ";
        }
        string str6 = this.myItemType.BuildReadableText(false);
        if (str6.Length > 0)
        {
          arrayList1.Add((object) (str1 + str6));
          str1 = "               ";
        }
        string str7 = this.myLocation.BuildReadableText(false);
        if (str7.Length > 0)
        {
          arrayList1.Add((object) (str1 + str7));
          str1 = "               ";
        }
        string str8 = this.myProjNo.BuildReadableText(false);
        if (str8.Length > 0)
        {
          arrayList1.Add((object) (str1 + str8));
          str1 = "               ";
        }
        string str9 = this.myDeptNo.BuildReadableText(false);
        if (str9.Length > 0)
        {
          arrayList1.Add((object) (str1 + str9));
          str1 = "               ";
        }
        string str10;
        if (this.myIsPrintCancelled == CancelledDocumentOption.UnCancelled)
        {
          arrayList1.Add((object) (str1 + "Cancelled Status: Show Uncancelled"));
          str10 = "               ";
        }
        else if (this.myIsPrintCancelled == CancelledDocumentOption.Cancelled)
        {
          arrayList1.Add((object) (str1 + "Cancelled Status: Show Cancelled"));
          str10 = "               ";
        }
        else
        {
          arrayList1.Add((object) (str1 + "Cancelled Status: Show All"));
          str10 = "               ";
        }
        ArrayList arrayList2 = new ArrayList();
        if (arrayList1.Count == 0)
        {
          arrayList2.Add((object) "Filter Options: No Filter");
        }
        else
        {
          for (int index = 0; index < arrayList1.Count; ++index)
          {
            string str11 = arrayList1[index].ToString();
            if (index == 0)
              arrayList2.Add((object) ("Filter Options: " + str11.Trim()));
            else
              arrayList2.Add((object) (" " + str11));
          }
        }
        if (this.mySortBy == "Document No")
          arrayList2.Add((object) "Report Options: Sort By: Document No");
        else if (this.mySortBy == "Date")
          arrayList2.Add((object) "Report Options: Sort By: Document Date");
        else if (this.mySortBy == "Item Code")
          arrayList2.Add((object) "Report Options: Sort By: Item Code");
        else if (this.mySortBy == "Item Group")
          arrayList2.Add((object) "Report Options: Sort By: Item Group");
        else if (this.mySortBy == "Item Type")
          arrayList2.Add((object) "Report Options: Sort By: Item Type");
        else if (this.mySortBy == "Location")
          arrayList2.Add((object) "Report Options: Sort By: Location");
        else if (this.mySortBy == "Project No")
          arrayList2.Add((object) "Report Options: Sort By: Project No");
        else if (this.mySortBy == "Department No")
          arrayList2.Add((object) "Report Options: Sort By: Department No");
        if (this.myGroupBy == "None")
          arrayList2.Add((object) "                Group By: None");
        else if (this.myGroupBy == "Date")
          arrayList2.Add((object) "                Group By: Date");
        else if (this.myGroupBy == "Month")
          arrayList2.Add((object) "                Group By: Month");
        else if (this.myGroupBy == "Year")
          arrayList2.Add((object) "                Group By: Year");
        else if (this.myGroupBy == "Item Code")
          arrayList2.Add((object) "                Group By: Item Code");
        else if (this.myGroupBy == "Item Group")
          arrayList2.Add((object) "                Group By: Item Group");
        else if (this.myGroupBy == "Item Type")
          arrayList2.Add((object) "                Group By: Item Type");
        else if (this.myGroupBy == "Location")
          arrayList2.Add((object) "                Group By: Location");
        else if (this.myGroupBy == "Project No")
          arrayList2.Add((object) "                Group By: Project No");
        else if (this.myGroupBy == "Department No")
          arrayList2.Add((object) "                Group By: Department No");
        return (string[]) arrayList2.ToArray(Type.GetType("System.String"));
      }
    }

    public StockWorkOrderDetailReportingCriteria()
    {
      this.myItemCode = new BCE.AutoCount.SearchFilter.Filter("ASMORDERDTL", "ItemCode", Localizer.GetString((Enum) StockAssemblyOrderStringId.ItemCode, new object[0]), FilterControlType.Item);
      this.myItemGroup = new BCE.AutoCount.SearchFilter.Filter("D", "ItemGroup", Localizer.GetString((Enum) StockAssemblyOrderStringId.ItemGroup, new object[0]), FilterControlType.ItemGroup);
      this.myItemType = new BCE.AutoCount.SearchFilter.Filter("D", "ItemType", Localizer.GetString((Enum) StockAssemblyOrderStringId.ItemType, new object[0]), FilterControlType.ItemType);
      this.myLocation = new BCE.AutoCount.SearchFilter.Filter("ASMORDERDTL", "Location", Localizer.GetString((Enum) StockAssemblyOrderStringId.Location, new object[0]), FilterControlType.Location);
      this.myProjNo = new BCE.AutoCount.SearchFilter.Filter("ASMORDERDTL", "ProjNo", Localizer.GetString((Enum) StockAssemblyOrderStringId.ProjectNo, new object[0]), FilterControlType.Project);
      this.myDeptNo = new BCE.AutoCount.SearchFilter.Filter("ASMORDERDTL", "DeptNo", Localizer.GetString((Enum) StockAssemblyOrderStringId.DepartmentNo, new object[0]), FilterControlType.Department);
      this.myAdvancedOptions = false;
      this.myGroupBy = "None";
      this.mySortBy = "Date";
    }
  }
}
