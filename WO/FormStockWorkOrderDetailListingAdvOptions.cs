﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.FormStockAssemblyOrderDetailListingAdvOptions
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.SearchFilter;
using BCE.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTab;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace RPASystem.WorkOrder
{
  public class FormStockWorkOrderDetailListingAdvOptions : XtraForm
  {
    private StockWorkOrderDetailReportingCriteria myReportingCriteriaDetail;
    private DBSetting myDBSetting;
    private IContainer components;
    private UCAllDepartmentsSelector ucDepartmentSelector1;
    private UCItemGroupSelector ucItemGroupSelector1;
    private UCItemTypeSelector ucItemTypeSelector1;
    private UCLocationSelector ucLocationSelector1;
    private UCAllProjectsSelector ucProjectSelector1;
    private ComboBoxEdit cbCancelledOption;
    private GroupControl gbFilter;
    private PanelControl panelControl1;
    private PanelControl panelControl2;
    private SimpleButton sbtnClose;
    private SimpleButton sbtnOK;
    private XtraTabControl xtraTabControl1;
    private XtraTabPage xtraTabPage1;
    private XtraTabPage xtraTabPage2;
    private Label label1;
    private Label label2;
    private Label label3;
    private Label label4;
    private Label label5;
    private Label label6;

    public object IsPrintCancelled
    {
      get
      {
        return this.cbCancelledOption.SelectedItem;
      }
    }

    public FormStockWorkOrderDetailListingAdvOptions(DBSetting dbSetting, StockWorkOrderDetailReportingCriteria myReportingCriteria)
    {
      this.InitializeComponent();
      this.Icon = BCE.AutoCount.Application.Icon;
      this.myReportingCriteriaDetail = myReportingCriteria;
      this.myDBSetting = dbSetting;
      this.ucItemGroupSelector1.Initialize(this.myDBSetting, myReportingCriteria.ItemGroupFilter);
      this.ucItemTypeSelector1.Initialize(this.myDBSetting, myReportingCriteria.ItemTypeFilter);
      this.ucLocationSelector1.Initialize(this.myDBSetting, myReportingCriteria.LocationFilter);
      this.ucProjectSelector1.Initialize(this.myDBSetting, myReportingCriteria.ProjecNoFilter);
      this.ucDepartmentSelector1.Initialize(this.myDBSetting, myReportingCriteria.DeptNoFilter);
    }

    public void SetFilterByLocation(bool filterByLocation)
    {
      this.ucLocationSelector1.Enabled = !filterByLocation;
    }

    private void InitializeSettings(StockWorkOrderDetailReportingCriteria myReportingCriteria)
    {
      this.ucItemGroupSelector1.ApplyFilter(myReportingCriteria.ItemGroupFilter);
      this.ucItemTypeSelector1.ApplyFilter(myReportingCriteria.ItemTypeFilter);
      this.ucLocationSelector1.ApplyFilter(myReportingCriteria.LocationFilter);
      this.ucProjectSelector1.ApplyFilter(myReportingCriteria.ProjecNoFilter);
      this.ucDepartmentSelector1.ApplyFilter(myReportingCriteria.DeptNoFilter);
      if (myReportingCriteria.IsPrintCancelled == CancelledDocumentOption.All)
        this.cbCancelledOption.SelectedIndex = 0;
      else if (myReportingCriteria.IsPrintCancelled == CancelledDocumentOption.Cancelled)
        this.cbCancelledOption.SelectedIndex = 1;
      else
        this.cbCancelledOption.SelectedIndex = 2;
    }

    private void FormStockAssemblyDetailListingAdvOptions_Load(object sender, EventArgs e)
    {
      this.InitializeSettings(this.myReportingCriteriaDetail);
    }

    private void sbtnOK_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.OK;
    }

    private void sbtnClose_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormStockWorkOrderDetailListingAdvOptions));
      this.panelControl1 = new PanelControl();
      this.gbFilter = new GroupControl();
      this.xtraTabControl1 = new XtraTabControl();
      this.xtraTabPage1 = new XtraTabPage();
      this.cbCancelledOption = new ComboBoxEdit();
      this.label1 = new Label();
      this.xtraTabPage2 = new XtraTabPage();
      this.ucProjectSelector1 = new UCAllProjectsSelector();
      this.ucLocationSelector1 = new UCLocationSelector();
      this.ucItemTypeSelector1 = new UCItemTypeSelector();
      this.ucItemGroupSelector1 = new UCItemGroupSelector();
      this.label2 = new Label();
      this.label4 = new Label();
      this.label3 = new Label();
      this.label6 = new Label();
      this.label5 = new Label();
      this.ucDepartmentSelector1 = new UCAllDepartmentsSelector();
      this.panelControl2 = new PanelControl();
      this.sbtnClose = new SimpleButton();
      this.sbtnOK = new SimpleButton();
      this.panelControl1.BeginInit();
      this.panelControl1.SuspendLayout();
      this.gbFilter.BeginInit();
      this.gbFilter.SuspendLayout();
      this.xtraTabControl1.BeginInit();
      this.xtraTabControl1.SuspendLayout();
      this.xtraTabPage1.SuspendLayout();
      this.cbCancelledOption.Properties.BeginInit();
      this.xtraTabPage2.SuspendLayout();
      this.panelControl2.BeginInit();
      this.panelControl2.SuspendLayout();
      this.SuspendLayout();
      this.panelControl1.BorderStyle = BorderStyles.NoBorder;
      this.panelControl1.Controls.Add((Control) this.gbFilter);
      componentResourceManager.ApplyResources((object) this.panelControl1, "panelControl1");
      this.panelControl1.Name = "panelControl1";
      this.gbFilter.Controls.Add((Control) this.xtraTabControl1);
      componentResourceManager.ApplyResources((object) this.gbFilter, "gbFilter");
      this.gbFilter.Name = "gbFilter";
      componentResourceManager.ApplyResources((object) this.xtraTabControl1, "xtraTabControl1");
      this.xtraTabControl1.Name = "xtraTabControl1";
      this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
      XtraTabPageCollection tabPages = this.xtraTabControl1.TabPages;
      XtraTabPage[] pages = new XtraTabPage[2];
      int index1 = 0;
      XtraTabPage xtraTabPage1 = this.xtraTabPage1;
      pages[index1] = xtraTabPage1;
      int index2 = 1;
      XtraTabPage xtraTabPage2 = this.xtraTabPage2;
      pages[index2] = xtraTabPage2;
      tabPages.AddRange(pages);
      this.xtraTabPage1.Controls.Add((Control) this.cbCancelledOption);
      this.xtraTabPage1.Controls.Add((Control) this.label1);
      this.xtraTabPage1.Name = "xtraTabPage1";
      componentResourceManager.ApplyResources((object) this.xtraTabPage1, "xtraTabPage1");
      componentResourceManager.ApplyResources((object) this.cbCancelledOption, "cbCancelledOption");
      this.cbCancelledOption.Name = "cbCancelledOption";
      EditorButtonCollection buttons1 = this.cbCancelledOption.Properties.Buttons;
      EditorButton[] buttons2 = new EditorButton[1];
      int index3 = 0;
      EditorButton editorButton = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("cbCancelledOption.Properties.Buttons"));
      buttons2[index3] = editorButton;
      buttons1.AddRange(buttons2);
      ComboBoxItemCollection items1 = this.cbCancelledOption.Properties.Items;
      object[] items2 = new object[3];
      int index4 = 0;
      string string1 = componentResourceManager.GetString("cbCancelledOption.Properties.Items");
      items2[index4] = (object) string1;
      int index5 = 1;
      string string2 = componentResourceManager.GetString("cbCancelledOption.Properties.Items1");
      items2[index5] = (object) string2;
      int index6 = 2;
      string string3 = componentResourceManager.GetString("cbCancelledOption.Properties.Items2");
      items2[index6] = (object) string3;
      items1.AddRange(items2);
      this.cbCancelledOption.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
      componentResourceManager.ApplyResources((object) this.label1, "label1");
      this.label1.Name = "label1";
      this.xtraTabPage2.Controls.Add((Control) this.ucProjectSelector1);
      this.xtraTabPage2.Controls.Add((Control) this.ucLocationSelector1);
      this.xtraTabPage2.Controls.Add((Control) this.ucItemTypeSelector1);
      this.xtraTabPage2.Controls.Add((Control) this.ucItemGroupSelector1);
      this.xtraTabPage2.Controls.Add((Control) this.label2);
      this.xtraTabPage2.Controls.Add((Control) this.label4);
      this.xtraTabPage2.Controls.Add((Control) this.label3);
      this.xtraTabPage2.Controls.Add((Control) this.label6);
      this.xtraTabPage2.Controls.Add((Control) this.label5);
      this.xtraTabPage2.Controls.Add((Control) this.ucDepartmentSelector1);
      this.xtraTabPage2.Name = "xtraTabPage2";
      componentResourceManager.ApplyResources((object) this.xtraTabPage2, "xtraTabPage2");
      this.ucProjectSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucProjectSelector1.Appearance.BackColor");
      this.ucProjectSelector1.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucProjectSelector1, "ucProjectSelector1");
      this.ucProjectSelector1.Name = "ucProjectSelector1";
      this.ucLocationSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucLocationSelector1.Appearance.BackColor");
      this.ucLocationSelector1.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucLocationSelector1, "ucLocationSelector1");
      this.ucLocationSelector1.LocationType = UCLocationType.ItemLocation;
      this.ucLocationSelector1.Name = "ucLocationSelector1";
      this.ucItemTypeSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucItemTypeSelector1.Appearance.BackColor");
      this.ucItemTypeSelector1.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucItemTypeSelector1, "ucItemTypeSelector1");
      this.ucItemTypeSelector1.Name = "ucItemTypeSelector1";
      this.ucItemGroupSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucItemGroupSelector1.Appearance.BackColor");
      this.ucItemGroupSelector1.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucItemGroupSelector1, "ucItemGroupSelector1");
      this.ucItemGroupSelector1.Name = "ucItemGroupSelector1";
      componentResourceManager.ApplyResources((object) this.label2, "label2");
      this.label2.Name = "label2";
      componentResourceManager.ApplyResources((object) this.label4, "label4");
      this.label4.Name = "label4";
      componentResourceManager.ApplyResources((object) this.label3, "label3");
      this.label3.Name = "label3";
      componentResourceManager.ApplyResources((object) this.label6, "label6");
      this.label6.Name = "label6";
      componentResourceManager.ApplyResources((object) this.label5, "label5");
      this.label5.Name = "label5";
      this.ucDepartmentSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucDepartmentSelector1.Appearance.BackColor");
      this.ucDepartmentSelector1.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucDepartmentSelector1, "ucDepartmentSelector1");
      this.ucDepartmentSelector1.Name = "ucDepartmentSelector1";
      this.panelControl2.BorderStyle = BorderStyles.NoBorder;
      this.panelControl2.Controls.Add((Control) this.sbtnClose);
      this.panelControl2.Controls.Add((Control) this.sbtnOK);
      componentResourceManager.ApplyResources((object) this.panelControl2, "panelControl2");
      this.panelControl2.Name = "panelControl2";
      this.sbtnClose.DialogResult = DialogResult.Cancel;
      componentResourceManager.ApplyResources((object) this.sbtnClose, "sbtnClose");
      this.sbtnClose.Name = "sbtnClose";
      this.sbtnClose.Click += new EventHandler(this.sbtnClose_Click);
      componentResourceManager.ApplyResources((object) this.sbtnOK, "sbtnOK");
      this.sbtnOK.Name = "sbtnOK";
      this.sbtnOK.Click += new EventHandler(this.sbtnOK_Click);
      componentResourceManager.ApplyResources((object) this, "$this");
      this.AutoScaleMode = AutoScaleMode.Dpi;
      this.CancelButton = (IButtonControl) this.sbtnClose;
      this.Controls.Add((Control) this.panelControl2);
      this.Controls.Add((Control) this.panelControl1);
      this.FormBorderStyle = FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.Name = "FormStockAssemblyOrderDetailListingAdvOptions";
      this.Load += new EventHandler(this.FormStockAssemblyDetailListingAdvOptions_Load);
      this.panelControl1.EndInit();
      this.panelControl1.ResumeLayout(false);
      this.gbFilter.EndInit();
      this.gbFilter.ResumeLayout(false);
      this.xtraTabControl1.EndInit();
      this.xtraTabControl1.ResumeLayout(false);
      this.xtraTabPage1.ResumeLayout(false);
      this.xtraTabPage1.PerformLayout();
      this.cbCancelledOption.Properties.EndInit();
      this.xtraTabPage2.ResumeLayout(false);
      this.xtraTabPage2.PerformLayout();
      this.panelControl2.EndInit();
      this.panelControl2.ResumeLayout(false);
      this.ResumeLayout(false);
    }
  }
}
