﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderDetailListingReportTypeHandler
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.AutoCount;
using BCE.Data;

namespace RPASystem.WorkOrder
{
  public class StockWorkOrderDetailListingReportTypeHandler : StockWorkOrderReportTypeHandler
  {
    public override object GetDesignerDataSource(DBSetting dbSetting)
    {
      BasicReportOption reportOption = (BasicReportOption) null;
      try
      {
        reportOption = (BasicReportOption) PersistenceUtil.LoadUserSetting("StockAssemblyOrderReportOption.setting");
      }
      catch
      {
      }
      if (reportOption == null)
        reportOption = new BasicReportOption();
      return StockWorkOrderReportCommand.Create(dbSetting, reportOption).GetDetailListingReportDesignerDataSource();
    }
  }
}
