﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.FormStockAssemblyOrderRangeSet
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.XtraUtils.LookupEditBuilder;
using BCE.Data;
using BCE.Localization;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraTreeList.Nodes;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace RPASystem.WorkOrder
{
  public class FormStockWorkOrderRangeSet : XtraForm
  {
    private DBSetting myDBSetting;
    private IContainer components;
    private CheckEdit chkEdtDeptNo;
    private CheckEdit chkEdtDescription;
    private CheckEdit chkEdtItemCode;
    private CheckEdit chkEdtLocation;
    private CheckEdit chkEdtOverheadCost;
    private CheckEdit chkEdtProjNo;
    private CheckEdit chkEdtQty;
    private CheckEdit chkEdtUnitCost;
    private LookUpEdit luDeptNo;
    private LookUpEdit luItemCode;
    private LookUpEdit luLocation;
    private LookUpEdit luProjNo;
    private PanelControl panelControl1;
    private SimpleButton sbtnCancel;
    private SimpleButton sbtnOK;
    private TextEdit edtDescription;
    private TextEdit edtOverheadCost;
    private TextEdit edtQty;
    private TextEdit edtUnitCost;
    private Label label1;
    private Label lblAffectedRows;

    public bool SelectItemCode
    {
      get
      {
        return this.chkEdtItemCode.Checked;
      }
    }

    public bool SelectLocation
    {
      get
      {
        return this.chkEdtLocation.Checked;
      }
    }

    public bool SelectProjNo
    {
      get
      {
        return this.chkEdtProjNo.Checked;
      }
    }

    public bool SelectDeptNo
    {
      get
      {
        return this.chkEdtDeptNo.Checked;
      }
    }

    public bool SelectQty
    {
      get
      {
        return this.chkEdtQty.Checked;
      }
    }

    public bool SelectUnitCost
    {
      get
      {
        return this.chkEdtUnitCost.Checked;
      }
    }

    public bool SelectOverheadCost
    {
      get
      {
        return this.chkEdtOverheadCost.Checked;
      }
    }

    public bool SelectDescription
    {
      get
      {
        return this.chkEdtDescription.Checked;
      }
    }

    public object ItemCode
    {
      get
      {
        return this.luItemCode.EditValue;
      }
      set
      {
        this.luItemCode.EditValue = value;
      }
    }

    public object StockLocation
    {
      get
      {
        return this.luLocation.EditValue;
      }
      set
      {
        this.luLocation.EditValue = value;
      }
    }

    public object ProjNo
    {
      get
      {
        return this.luProjNo.EditValue;
      }
      set
      {
        this.luProjNo.EditValue = value;
      }
    }

    public object DeptNo
    {
      get
      {
        return this.luDeptNo.EditValue;
      }
      set
      {
        this.luDeptNo.EditValue = value;
      }
    }

    public object Qty
    {
      get
      {
        return this.edtQty.EditValue;
      }
      set
      {
        this.edtQty.EditValue = value;
      }
    }

    public object UnitCost
    {
      get
      {
        return this.edtUnitCost.EditValue;
      }
      set
      {
        this.edtUnitCost.EditValue = value;
      }
    }

    public object OverheadCost
    {
      get
      {
        return this.edtOverheadCost.EditValue;
      }
      set
      {
        this.edtOverheadCost.EditValue = value;
      }
    }

    public object Description
    {
      get
      {
        return this.edtDescription.EditValue;
      }
      set
      {
        this.edtDescription.EditValue = value;
      }
    }

    public int TotalRowsAffected
    {
      set
      {
        Label label = this.lblAffectedRows;
        StockAssemblyOrderStringId local =  StockAssemblyOrderStringId.ShowMessage_TotalRowsUpdated;
        object[] objArray = new object[1];
        int index = 0;
        string str = value.ToString();
        objArray[index] = (object) str;
        string @string = BCE.Localization.Localizer.GetString((Enum) local, objArray);
        label.Text = @string;
      }
    }

    public FormStockWorkOrderRangeSet(DBSetting dbSetting)
    {
      this.InitializeComponent();
      this.myDBSetting = dbSetting;
    }

    public static void RangeSet(IWin32Window owner, TreeListNode focusNode, TreeListNodes selectedNodes, DBSetting dbSetting, FillRowValueDelegate fillRowValueDelegate)
    {
      using (FormStockWorkOrderRangeSet assemblyOrderRangeSet = new FormStockWorkOrderRangeSet(dbSetting))
      {
        if (focusNode != null)
        {
          assemblyOrderRangeSet.ItemCode = focusNode[(object) "ItemCode"];
          assemblyOrderRangeSet.StockLocation = focusNode[(object) "Location"];
          assemblyOrderRangeSet.ProjNo = focusNode[(object) "ProjNo"];
          assemblyOrderRangeSet.DeptNo = focusNode[(object) "DeptNo"];
          assemblyOrderRangeSet.Qty = focusNode[(object) "Qty"];
          assemblyOrderRangeSet.UnitCost = focusNode[(object) "ItemCost"];
          assemblyOrderRangeSet.OverheadCost = focusNode[(object) "OverheadCost"];
          assemblyOrderRangeSet.Description = focusNode[(object) "Description"];
        }
        assemblyOrderRangeSet.TotalRowsAffected = selectedNodes.Count;
        if (assemblyOrderRangeSet.ShowDialog(owner) == DialogResult.OK)
        {
          int length = 0;
          if (assemblyOrderRangeSet.SelectItemCode)
            ++length;
          if (assemblyOrderRangeSet.SelectDescription)
            ++length;
          if (assemblyOrderRangeSet.SelectProjNo)
            ++length;
          if (assemblyOrderRangeSet.SelectDeptNo)
            ++length;
          if (assemblyOrderRangeSet.SelectQty)
            ++length;
          if (assemblyOrderRangeSet.SelectUnitCost)
            ++length;
          if (assemblyOrderRangeSet.SelectOverheadCost)
            ++length;
          string[] strArray1 = new string[length];
          int num1 = 0;
          if (assemblyOrderRangeSet.SelectItemCode)
            strArray1[num1++] = "ItemCode";
          if (assemblyOrderRangeSet.SelectDescription)
            strArray1[num1++] = "Description";
          if (assemblyOrderRangeSet.SelectProjNo)
            strArray1[num1++] = "ProjNo";
          if (assemblyOrderRangeSet.SelectDeptNo)
            strArray1[num1++] = "DeptNo";
          if (assemblyOrderRangeSet.SelectQty)
            strArray1[num1++] = "Qty";
          if (assemblyOrderRangeSet.SelectUnitCost)
            strArray1[num1++] = "ItemCost";
          if (assemblyOrderRangeSet.SelectOverheadCost)
          {
            string[] strArray2 = strArray1;
            int index = num1;
            int num2 = 1;
            int num3 = index + num2;
            string str = "OverheadCost";
            strArray2[index] = str;
          }
          foreach (TreeListNode treeListNode in selectedNodes)
          {
            if (assemblyOrderRangeSet.SelectItemCode)
              treeListNode[(object) "ItemCode"] = assemblyOrderRangeSet.ItemCode;
            if (assemblyOrderRangeSet.SelectDescription)
              treeListNode[(object) "Description"] = assemblyOrderRangeSet.Description;
            if (assemblyOrderRangeSet.SelectLocation)
              treeListNode[(object) "Location"] = assemblyOrderRangeSet.StockLocation;
            if (assemblyOrderRangeSet.SelectProjNo)
              treeListNode[(object) "ProjNo"] = assemblyOrderRangeSet.ProjNo;
            if (assemblyOrderRangeSet.SelectDeptNo)
              treeListNode[(object) "DeptNo"] = assemblyOrderRangeSet.DeptNo;
            if (assemblyOrderRangeSet.SelectQty)
              treeListNode[(object) "Qty"] = assemblyOrderRangeSet.Qty;
            if (assemblyOrderRangeSet.SelectUnitCost)
              treeListNode[(object) "ItemCost"] = assemblyOrderRangeSet.UnitCost;
            if (assemblyOrderRangeSet.SelectOverheadCost)
              treeListNode[(object) "OverheadCost"] = assemblyOrderRangeSet.OverheadCost;
          }
        }
      }
    }

    private void FormStockDocumentBulkSet_Load(object sender, EventArgs e)
    {
      this.Icon = BCE.AutoCount.Application.Icon;
      ItemLookupEditBuilder.Create().BuildLookupEdit(this.luItemCode.Properties, this.myDBSetting);
      new LocationLookupEditBuilder().BuildLookupEdit(this.luLocation.Properties, this.myDBSetting);
      new ProjectLookupEditBuilder().BuildLookupEdit(this.luProjNo.Properties, this.myDBSetting);
      new DepartmentLookupEditBuilder().BuildLookupEdit(this.luDeptNo.Properties, this.myDBSetting);
    }

    private void sbtnOK_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.None;
      if (!this.chkEdtItemCode.Checked && !this.chkEdtLocation.Checked && (!this.chkEdtProjNo.Checked && !this.chkEdtDeptNo.Checked) && (!this.chkEdtQty.Checked && !this.chkEdtUnitCost.Checked && !this.chkEdtDescription.Checked))
      {
        AppMessage.ShowErrorMessage(BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ErrorMessage_CheckBox, new object[0]));
        this.chkEdtLocation.Focus();
      }
      else
      {
        if (this.chkEdtQty.Checked)
        {
          try
          {
            BCE.Data.Convert.ToDecimal(this.edtQty.EditValue);
          }
          catch
          {
            AppMessage.ShowErrorMessage(BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ErrorMessage_InvalidQty, new object[0]));
            this.edtQty.Focus();
            return;
          }
        }
        if (this.chkEdtUnitCost.Checked)
        {
          try
          {
            BCE.Data.Convert.ToDecimal(this.edtUnitCost.EditValue);
          }
          catch
          {
            AppMessage.ShowErrorMessage(BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ErrorMessage_InvalidItemCost, new object[0]));
            this.edtUnitCost.Focus();
            return;
          }
        }
        if (this.chkEdtOverheadCost.Checked)
        {
          try
          {
            BCE.Data.Convert.ToDecimal(this.edtOverheadCost.EditValue);
          }
          catch
          {
            AppMessage.ShowErrorMessage(BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ErrorMessage_InvalidOverheadCost, new object[0]));
            this.edtOverheadCost.Focus();
            return;
          }
        }
        this.DialogResult = DialogResult.OK;
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormStockWorkOrderRangeSet));
      this.chkEdtLocation = new CheckEdit();
      this.label1 = new Label();
      this.chkEdtProjNo = new CheckEdit();
      this.chkEdtDeptNo = new CheckEdit();
      this.chkEdtQty = new CheckEdit();
      this.chkEdtUnitCost = new CheckEdit();
      this.luLocation = new LookUpEdit();
      this.luProjNo = new LookUpEdit();
      this.luDeptNo = new LookUpEdit();
      this.edtQty = new TextEdit();
      this.edtUnitCost = new TextEdit();
      this.sbtnOK = new SimpleButton();
      this.sbtnCancel = new SimpleButton();
      this.chkEdtItemCode = new CheckEdit();
      this.luItemCode = new LookUpEdit();
      this.chkEdtDescription = new CheckEdit();
      this.edtDescription = new TextEdit();
      this.lblAffectedRows = new Label();
      this.edtOverheadCost = new TextEdit();
      this.chkEdtOverheadCost = new CheckEdit();
      this.panelControl1 = new PanelControl();
      this.chkEdtLocation.Properties.BeginInit();
      this.chkEdtProjNo.Properties.BeginInit();
      this.chkEdtDeptNo.Properties.BeginInit();
      this.chkEdtQty.Properties.BeginInit();
      this.chkEdtUnitCost.Properties.BeginInit();
      this.luLocation.Properties.BeginInit();
      this.luProjNo.Properties.BeginInit();
      this.luDeptNo.Properties.BeginInit();
      this.edtQty.Properties.BeginInit();
      this.edtUnitCost.Properties.BeginInit();
      this.chkEdtItemCode.Properties.BeginInit();
      this.luItemCode.Properties.BeginInit();
      this.chkEdtDescription.Properties.BeginInit();
      this.edtDescription.Properties.BeginInit();
      this.edtOverheadCost.Properties.BeginInit();
      this.chkEdtOverheadCost.Properties.BeginInit();
      this.panelControl1.BeginInit();
      this.panelControl1.SuspendLayout();
      this.SuspendLayout();
      componentResourceManager.ApplyResources((object) this.chkEdtLocation, "chkEdtLocation");
      this.chkEdtLocation.Name = "chkEdtLocation";
      this.chkEdtLocation.Properties.AccessibleDescription = componentResourceManager.GetString("chkEdtLocation.Properties.AccessibleDescription");
      this.chkEdtLocation.Properties.AccessibleName = componentResourceManager.GetString("chkEdtLocation.Properties.AccessibleName");
      this.chkEdtLocation.Properties.AutoHeight = (bool) componentResourceManager.GetObject("chkEdtLocation.Properties.AutoHeight");
      this.chkEdtLocation.Properties.Caption = componentResourceManager.GetString("chkEdtLocation.Properties.Caption");
      this.chkEdtLocation.Properties.DisplayValueChecked = componentResourceManager.GetString("chkEdtLocation.Properties.DisplayValueChecked");
      this.chkEdtLocation.Properties.DisplayValueGrayed = componentResourceManager.GetString("chkEdtLocation.Properties.DisplayValueGrayed");
      this.chkEdtLocation.Properties.DisplayValueUnchecked = componentResourceManager.GetString("chkEdtLocation.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.label1, "label1");
      this.label1.Name = "label1";
      componentResourceManager.ApplyResources((object) this.chkEdtProjNo, "chkEdtProjNo");
      this.chkEdtProjNo.Name = "chkEdtProjNo";
      this.chkEdtProjNo.Properties.AccessibleDescription = componentResourceManager.GetString("chkEdtProjNo.Properties.AccessibleDescription");
      this.chkEdtProjNo.Properties.AccessibleName = componentResourceManager.GetString("chkEdtProjNo.Properties.AccessibleName");
      this.chkEdtProjNo.Properties.AutoHeight = (bool) componentResourceManager.GetObject("chkEdtProjNo.Properties.AutoHeight");
      this.chkEdtProjNo.Properties.Caption = componentResourceManager.GetString("chkEdtProjNo.Properties.Caption");
      this.chkEdtProjNo.Properties.DisplayValueChecked = componentResourceManager.GetString("chkEdtProjNo.Properties.DisplayValueChecked");
      this.chkEdtProjNo.Properties.DisplayValueGrayed = componentResourceManager.GetString("chkEdtProjNo.Properties.DisplayValueGrayed");
      this.chkEdtProjNo.Properties.DisplayValueUnchecked = componentResourceManager.GetString("chkEdtProjNo.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.chkEdtDeptNo, "chkEdtDeptNo");
      this.chkEdtDeptNo.Name = "chkEdtDeptNo";
      this.chkEdtDeptNo.Properties.AccessibleDescription = componentResourceManager.GetString("chkEdtDeptNo.Properties.AccessibleDescription");
      this.chkEdtDeptNo.Properties.AccessibleName = componentResourceManager.GetString("chkEdtDeptNo.Properties.AccessibleName");
      this.chkEdtDeptNo.Properties.AutoHeight = (bool) componentResourceManager.GetObject("chkEdtDeptNo.Properties.AutoHeight");
      this.chkEdtDeptNo.Properties.Caption = componentResourceManager.GetString("chkEdtDeptNo.Properties.Caption");
      this.chkEdtDeptNo.Properties.DisplayValueChecked = componentResourceManager.GetString("chkEdtDeptNo.Properties.DisplayValueChecked");
      this.chkEdtDeptNo.Properties.DisplayValueGrayed = componentResourceManager.GetString("chkEdtDeptNo.Properties.DisplayValueGrayed");
      this.chkEdtDeptNo.Properties.DisplayValueUnchecked = componentResourceManager.GetString("chkEdtDeptNo.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.chkEdtQty, "chkEdtQty");
      this.chkEdtQty.Name = "chkEdtQty";
      this.chkEdtQty.Properties.AccessibleDescription = componentResourceManager.GetString("chkEdtQty.Properties.AccessibleDescription");
      this.chkEdtQty.Properties.AccessibleName = componentResourceManager.GetString("chkEdtQty.Properties.AccessibleName");
      this.chkEdtQty.Properties.AutoHeight = (bool) componentResourceManager.GetObject("chkEdtQty.Properties.AutoHeight");
      this.chkEdtQty.Properties.Caption = componentResourceManager.GetString("chkEdtQty.Properties.Caption");
      this.chkEdtQty.Properties.DisplayValueChecked = componentResourceManager.GetString("chkEdtQty.Properties.DisplayValueChecked");
      this.chkEdtQty.Properties.DisplayValueGrayed = componentResourceManager.GetString("chkEdtQty.Properties.DisplayValueGrayed");
      this.chkEdtQty.Properties.DisplayValueUnchecked = componentResourceManager.GetString("chkEdtQty.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.chkEdtUnitCost, "chkEdtUnitCost");
      this.chkEdtUnitCost.Name = "chkEdtUnitCost";
      this.chkEdtUnitCost.Properties.AccessibleDescription = componentResourceManager.GetString("chkEdtUnitCost.Properties.AccessibleDescription");
      this.chkEdtUnitCost.Properties.AccessibleName = componentResourceManager.GetString("chkEdtUnitCost.Properties.AccessibleName");
      this.chkEdtUnitCost.Properties.AutoHeight = (bool) componentResourceManager.GetObject("chkEdtUnitCost.Properties.AutoHeight");
      this.chkEdtUnitCost.Properties.Caption = componentResourceManager.GetString("chkEdtUnitCost.Properties.Caption");
      this.chkEdtUnitCost.Properties.DisplayValueChecked = componentResourceManager.GetString("chkEdtUnitCost.Properties.DisplayValueChecked");
      this.chkEdtUnitCost.Properties.DisplayValueGrayed = componentResourceManager.GetString("chkEdtUnitCost.Properties.DisplayValueGrayed");
      this.chkEdtUnitCost.Properties.DisplayValueUnchecked = componentResourceManager.GetString("chkEdtUnitCost.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.luLocation, "luLocation");
      this.luLocation.Name = "luLocation";
      this.luLocation.Properties.AccessibleDescription = componentResourceManager.GetString("luLocation.Properties.AccessibleDescription");
      this.luLocation.Properties.AccessibleName = componentResourceManager.GetString("luLocation.Properties.AccessibleName");
      this.luLocation.Properties.AutoHeight = (bool) componentResourceManager.GetObject("luLocation.Properties.AutoHeight");
      EditorButtonCollection buttons1 = this.luLocation.Properties.Buttons;
      EditorButton[] buttons2 = new EditorButton[1];
      int index1 = 0;
      EditorButton editorButton1 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("luLocation.Properties.Buttons"));
      buttons2[index1] = editorButton1;
      buttons1.AddRange(buttons2);
      this.luLocation.Properties.NullValuePrompt = componentResourceManager.GetString("luLocation.Properties.NullValuePrompt");
      this.luLocation.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("luLocation.Properties.NullValuePromptShowForEmptyValue");
      componentResourceManager.ApplyResources((object) this.luProjNo, "luProjNo");
      this.luProjNo.Name = "luProjNo";
      this.luProjNo.Properties.AccessibleDescription = componentResourceManager.GetString("luProjNo.Properties.AccessibleDescription");
      this.luProjNo.Properties.AccessibleName = componentResourceManager.GetString("luProjNo.Properties.AccessibleName");
      this.luProjNo.Properties.AutoHeight = (bool) componentResourceManager.GetObject("luProjNo.Properties.AutoHeight");
      EditorButtonCollection buttons3 = this.luProjNo.Properties.Buttons;
      EditorButton[] buttons4 = new EditorButton[1];
      int index2 = 0;
      EditorButton editorButton2 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("luProjNo.Properties.Buttons"));
      buttons4[index2] = editorButton2;
      buttons3.AddRange(buttons4);
      this.luProjNo.Properties.NullValuePrompt = componentResourceManager.GetString("luProjNo.Properties.NullValuePrompt");
      this.luProjNo.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("luProjNo.Properties.NullValuePromptShowForEmptyValue");
      componentResourceManager.ApplyResources((object) this.luDeptNo, "luDeptNo");
      this.luDeptNo.Name = "luDeptNo";
      this.luDeptNo.Properties.AccessibleDescription = componentResourceManager.GetString("luDeptNo.Properties.AccessibleDescription");
      this.luDeptNo.Properties.AccessibleName = componentResourceManager.GetString("luDeptNo.Properties.AccessibleName");
      this.luDeptNo.Properties.AutoHeight = (bool) componentResourceManager.GetObject("luDeptNo.Properties.AutoHeight");
      EditorButtonCollection buttons5 = this.luDeptNo.Properties.Buttons;
      EditorButton[] buttons6 = new EditorButton[1];
      int index3 = 0;
      EditorButton editorButton3 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("luDeptNo.Properties.Buttons"));
      buttons6[index3] = editorButton3;
      buttons5.AddRange(buttons6);
      this.luDeptNo.Properties.NullValuePrompt = componentResourceManager.GetString("luDeptNo.Properties.NullValuePrompt");
      this.luDeptNo.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("luDeptNo.Properties.NullValuePromptShowForEmptyValue");
      componentResourceManager.ApplyResources((object) this.edtQty, "edtQty");
      this.edtQty.Name = "edtQty";
      this.edtQty.Properties.AccessibleDescription = componentResourceManager.GetString("edtQty.Properties.AccessibleDescription");
      this.edtQty.Properties.AccessibleName = componentResourceManager.GetString("edtQty.Properties.AccessibleName");
      this.edtQty.Properties.AutoHeight = (bool) componentResourceManager.GetObject("edtQty.Properties.AutoHeight");
      this.edtQty.Properties.Mask.AutoComplete = (AutoCompleteType) componentResourceManager.GetObject("edtQty.Properties.Mask.AutoComplete");
      this.edtQty.Properties.Mask.BeepOnError = (bool) componentResourceManager.GetObject("edtQty.Properties.Mask.BeepOnError");
      this.edtQty.Properties.Mask.EditMask = componentResourceManager.GetString("edtQty.Properties.Mask.EditMask");
      this.edtQty.Properties.Mask.IgnoreMaskBlank = (bool) componentResourceManager.GetObject("edtQty.Properties.Mask.IgnoreMaskBlank");
      this.edtQty.Properties.Mask.MaskType = (MaskType) componentResourceManager.GetObject("edtQty.Properties.Mask.MaskType");
      this.edtQty.Properties.Mask.PlaceHolder = (char) componentResourceManager.GetObject("edtQty.Properties.Mask.PlaceHolder");
      this.edtQty.Properties.Mask.SaveLiteral = (bool) componentResourceManager.GetObject("edtQty.Properties.Mask.SaveLiteral");
      this.edtQty.Properties.Mask.ShowPlaceHolders = (bool) componentResourceManager.GetObject("edtQty.Properties.Mask.ShowPlaceHolders");
      this.edtQty.Properties.Mask.UseMaskAsDisplayFormat = (bool) componentResourceManager.GetObject("edtQty.Properties.Mask.UseMaskAsDisplayFormat");
      this.edtQty.Properties.NullValuePrompt = componentResourceManager.GetString("edtQty.Properties.NullValuePrompt");
      this.edtQty.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("edtQty.Properties.NullValuePromptShowForEmptyValue");
      componentResourceManager.ApplyResources((object) this.edtUnitCost, "edtUnitCost");
      this.edtUnitCost.Name = "edtUnitCost";
      this.edtUnitCost.Properties.AccessibleDescription = componentResourceManager.GetString("edtUnitCost.Properties.AccessibleDescription");
      this.edtUnitCost.Properties.AccessibleName = componentResourceManager.GetString("edtUnitCost.Properties.AccessibleName");
      this.edtUnitCost.Properties.AutoHeight = (bool) componentResourceManager.GetObject("edtUnitCost.Properties.AutoHeight");
      this.edtUnitCost.Properties.Mask.AutoComplete = (AutoCompleteType) componentResourceManager.GetObject("edtUnitCost.Properties.Mask.AutoComplete");
      this.edtUnitCost.Properties.Mask.BeepOnError = (bool) componentResourceManager.GetObject("edtUnitCost.Properties.Mask.BeepOnError");
      this.edtUnitCost.Properties.Mask.EditMask = componentResourceManager.GetString("edtUnitCost.Properties.Mask.EditMask");
      this.edtUnitCost.Properties.Mask.IgnoreMaskBlank = (bool) componentResourceManager.GetObject("edtUnitCost.Properties.Mask.IgnoreMaskBlank");
      this.edtUnitCost.Properties.Mask.MaskType = (MaskType) componentResourceManager.GetObject("edtUnitCost.Properties.Mask.MaskType");
      this.edtUnitCost.Properties.Mask.PlaceHolder = (char) componentResourceManager.GetObject("edtUnitCost.Properties.Mask.PlaceHolder");
      this.edtUnitCost.Properties.Mask.SaveLiteral = (bool) componentResourceManager.GetObject("edtUnitCost.Properties.Mask.SaveLiteral");
      this.edtUnitCost.Properties.Mask.ShowPlaceHolders = (bool) componentResourceManager.GetObject("edtUnitCost.Properties.Mask.ShowPlaceHolders");
      this.edtUnitCost.Properties.Mask.UseMaskAsDisplayFormat = (bool) componentResourceManager.GetObject("edtUnitCost.Properties.Mask.UseMaskAsDisplayFormat");
      this.edtUnitCost.Properties.NullValuePrompt = componentResourceManager.GetString("edtUnitCost.Properties.NullValuePrompt");
      this.edtUnitCost.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("edtUnitCost.Properties.NullValuePromptShowForEmptyValue");
      componentResourceManager.ApplyResources((object) this.sbtnOK, "sbtnOK");
      this.sbtnOK.DialogResult = DialogResult.OK;
      this.sbtnOK.Name = "sbtnOK";
      this.sbtnOK.Click += new EventHandler(this.sbtnOK_Click);
      componentResourceManager.ApplyResources((object) this.sbtnCancel, "sbtnCancel");
      this.sbtnCancel.DialogResult = DialogResult.Cancel;
      this.sbtnCancel.Name = "sbtnCancel";
      componentResourceManager.ApplyResources((object) this.chkEdtItemCode, "chkEdtItemCode");
      this.chkEdtItemCode.Name = "chkEdtItemCode";
      this.chkEdtItemCode.Properties.AccessibleDescription = componentResourceManager.GetString("chkEdtItemCode.Properties.AccessibleDescription");
      this.chkEdtItemCode.Properties.AccessibleName = componentResourceManager.GetString("chkEdtItemCode.Properties.AccessibleName");
      this.chkEdtItemCode.Properties.AutoHeight = (bool) componentResourceManager.GetObject("chkEdtItemCode.Properties.AutoHeight");
      this.chkEdtItemCode.Properties.Caption = componentResourceManager.GetString("chkEdtItemCode.Properties.Caption");
      this.chkEdtItemCode.Properties.DisplayValueChecked = componentResourceManager.GetString("chkEdtItemCode.Properties.DisplayValueChecked");
      this.chkEdtItemCode.Properties.DisplayValueGrayed = componentResourceManager.GetString("chkEdtItemCode.Properties.DisplayValueGrayed");
      this.chkEdtItemCode.Properties.DisplayValueUnchecked = componentResourceManager.GetString("chkEdtItemCode.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.luItemCode, "luItemCode");
      this.luItemCode.Name = "luItemCode";
      this.luItemCode.Properties.AccessibleDescription = componentResourceManager.GetString("luItemCode.Properties.AccessibleDescription");
      this.luItemCode.Properties.AccessibleName = componentResourceManager.GetString("luItemCode.Properties.AccessibleName");
      this.luItemCode.Properties.AutoHeight = (bool) componentResourceManager.GetObject("luItemCode.Properties.AutoHeight");
      EditorButtonCollection buttons7 = this.luItemCode.Properties.Buttons;
      EditorButton[] buttons8 = new EditorButton[1];
      int index4 = 0;
      EditorButton editorButton4 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("luItemCode.Properties.Buttons"));
      buttons8[index4] = editorButton4;
      buttons7.AddRange(buttons8);
      this.luItemCode.Properties.NullValuePrompt = componentResourceManager.GetString("luItemCode.Properties.NullValuePrompt");
      this.luItemCode.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("luItemCode.Properties.NullValuePromptShowForEmptyValue");
      componentResourceManager.ApplyResources((object) this.chkEdtDescription, "chkEdtDescription");
      this.chkEdtDescription.Name = "chkEdtDescription";
      this.chkEdtDescription.Properties.AccessibleDescription = componentResourceManager.GetString("chkEdtDescription.Properties.AccessibleDescription");
      this.chkEdtDescription.Properties.AccessibleName = componentResourceManager.GetString("chkEdtDescription.Properties.AccessibleName");
      this.chkEdtDescription.Properties.AutoHeight = (bool) componentResourceManager.GetObject("chkEdtDescription.Properties.AutoHeight");
      this.chkEdtDescription.Properties.Caption = componentResourceManager.GetString("chkEdtDescription.Properties.Caption");
      this.chkEdtDescription.Properties.DisplayValueChecked = componentResourceManager.GetString("chkEdtDescription.Properties.DisplayValueChecked");
      this.chkEdtDescription.Properties.DisplayValueGrayed = componentResourceManager.GetString("chkEdtDescription.Properties.DisplayValueGrayed");
      this.chkEdtDescription.Properties.DisplayValueUnchecked = componentResourceManager.GetString("chkEdtDescription.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.edtDescription, "edtDescription");
      this.edtDescription.Name = "edtDescription";
      this.edtDescription.Properties.AccessibleDescription = componentResourceManager.GetString("edtDescription.Properties.AccessibleDescription");
      this.edtDescription.Properties.AccessibleName = componentResourceManager.GetString("edtDescription.Properties.AccessibleName");
      this.edtDescription.Properties.AutoHeight = (bool) componentResourceManager.GetObject("edtDescription.Properties.AutoHeight");
      this.edtDescription.Properties.Mask.AutoComplete = (AutoCompleteType) componentResourceManager.GetObject("edtDescription.Properties.Mask.AutoComplete");
      this.edtDescription.Properties.Mask.BeepOnError = (bool) componentResourceManager.GetObject("edtDescription.Properties.Mask.BeepOnError");
      this.edtDescription.Properties.Mask.EditMask = componentResourceManager.GetString("edtDescription.Properties.Mask.EditMask");
      this.edtDescription.Properties.Mask.IgnoreMaskBlank = (bool) componentResourceManager.GetObject("edtDescription.Properties.Mask.IgnoreMaskBlank");
      this.edtDescription.Properties.Mask.MaskType = (MaskType) componentResourceManager.GetObject("edtDescription.Properties.Mask.MaskType");
      this.edtDescription.Properties.Mask.PlaceHolder = (char) componentResourceManager.GetObject("edtDescription.Properties.Mask.PlaceHolder");
      this.edtDescription.Properties.Mask.SaveLiteral = (bool) componentResourceManager.GetObject("edtDescription.Properties.Mask.SaveLiteral");
      this.edtDescription.Properties.Mask.ShowPlaceHolders = (bool) componentResourceManager.GetObject("edtDescription.Properties.Mask.ShowPlaceHolders");
      this.edtDescription.Properties.Mask.UseMaskAsDisplayFormat = (bool) componentResourceManager.GetObject("edtDescription.Properties.Mask.UseMaskAsDisplayFormat");
      this.edtDescription.Properties.NullValuePrompt = componentResourceManager.GetString("edtDescription.Properties.NullValuePrompt");
      this.edtDescription.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("edtDescription.Properties.NullValuePromptShowForEmptyValue");
      componentResourceManager.ApplyResources((object) this.lblAffectedRows, "lblAffectedRows");
      this.lblAffectedRows.Name = "lblAffectedRows";
      componentResourceManager.ApplyResources((object) this.edtOverheadCost, "edtOverheadCost");
      this.edtOverheadCost.Name = "edtOverheadCost";
      this.edtOverheadCost.Properties.AccessibleDescription = componentResourceManager.GetString("edtOverheadCost.Properties.AccessibleDescription");
      this.edtOverheadCost.Properties.AccessibleName = componentResourceManager.GetString("edtOverheadCost.Properties.AccessibleName");
      this.edtOverheadCost.Properties.AutoHeight = (bool) componentResourceManager.GetObject("edtOverheadCost.Properties.AutoHeight");
      this.edtOverheadCost.Properties.Mask.AutoComplete = (AutoCompleteType) componentResourceManager.GetObject("edtOverheadCost.Properties.Mask.AutoComplete");
      this.edtOverheadCost.Properties.Mask.BeepOnError = (bool) componentResourceManager.GetObject("edtOverheadCost.Properties.Mask.BeepOnError");
      this.edtOverheadCost.Properties.Mask.EditMask = componentResourceManager.GetString("edtOverheadCost.Properties.Mask.EditMask");
      this.edtOverheadCost.Properties.Mask.IgnoreMaskBlank = (bool) componentResourceManager.GetObject("edtOverheadCost.Properties.Mask.IgnoreMaskBlank");
      this.edtOverheadCost.Properties.Mask.MaskType = (MaskType) componentResourceManager.GetObject("edtOverheadCost.Properties.Mask.MaskType");
      this.edtOverheadCost.Properties.Mask.PlaceHolder = (char) componentResourceManager.GetObject("edtOverheadCost.Properties.Mask.PlaceHolder");
      this.edtOverheadCost.Properties.Mask.SaveLiteral = (bool) componentResourceManager.GetObject("edtOverheadCost.Properties.Mask.SaveLiteral");
      this.edtOverheadCost.Properties.Mask.ShowPlaceHolders = (bool) componentResourceManager.GetObject("edtOverheadCost.Properties.Mask.ShowPlaceHolders");
      this.edtOverheadCost.Properties.Mask.UseMaskAsDisplayFormat = (bool) componentResourceManager.GetObject("edtOverheadCost.Properties.Mask.UseMaskAsDisplayFormat");
      this.edtOverheadCost.Properties.NullValuePrompt = componentResourceManager.GetString("edtOverheadCost.Properties.NullValuePrompt");
      this.edtOverheadCost.Properties.NullValuePromptShowForEmptyValue = (bool) componentResourceManager.GetObject("edtOverheadCost.Properties.NullValuePromptShowForEmptyValue");
      componentResourceManager.ApplyResources((object) this.chkEdtOverheadCost, "chkEdtOverheadCost");
      this.chkEdtOverheadCost.Name = "chkEdtOverheadCost";
      this.chkEdtOverheadCost.Properties.AccessibleDescription = componentResourceManager.GetString("chkEdtOverheadCost.Properties.AccessibleDescription");
      this.chkEdtOverheadCost.Properties.AccessibleName = componentResourceManager.GetString("chkEdtOverheadCost.Properties.AccessibleName");
      this.chkEdtOverheadCost.Properties.AutoHeight = (bool) componentResourceManager.GetObject("chkEdtOverheadCost.Properties.AutoHeight");
      this.chkEdtOverheadCost.Properties.Caption = componentResourceManager.GetString("chkEdtOverheadCost.Properties.Caption");
      this.chkEdtOverheadCost.Properties.DisplayValueChecked = componentResourceManager.GetString("chkEdtOverheadCost.Properties.DisplayValueChecked");
      this.chkEdtOverheadCost.Properties.DisplayValueGrayed = componentResourceManager.GetString("chkEdtOverheadCost.Properties.DisplayValueGrayed");
      this.chkEdtOverheadCost.Properties.DisplayValueUnchecked = componentResourceManager.GetString("chkEdtOverheadCost.Properties.DisplayValueUnchecked");
      componentResourceManager.ApplyResources((object) this.panelControl1, "panelControl1");
      this.panelControl1.BorderStyle = BorderStyles.NoBorder;
      this.panelControl1.Controls.Add((Control) this.edtQty);
      this.panelControl1.Controls.Add((Control) this.luDeptNo);
      this.panelControl1.Controls.Add((Control) this.luProjNo);
      this.panelControl1.Controls.Add((Control) this.luLocation);
      this.panelControl1.Controls.Add((Control) this.chkEdtUnitCost);
      this.panelControl1.Controls.Add((Control) this.chkEdtQty);
      this.panelControl1.Controls.Add((Control) this.chkEdtDeptNo);
      this.panelControl1.Controls.Add((Control) this.chkEdtProjNo);
      this.panelControl1.Controls.Add((Control) this.label1);
      this.panelControl1.Controls.Add((Control) this.chkEdtLocation);
      this.panelControl1.Controls.Add((Control) this.chkEdtItemCode);
      this.panelControl1.Controls.Add((Control) this.luItemCode);
      this.panelControl1.Controls.Add((Control) this.chkEdtDescription);
      this.panelControl1.Controls.Add((Control) this.edtDescription);
      this.panelControl1.Controls.Add((Control) this.lblAffectedRows);
      this.panelControl1.Controls.Add((Control) this.edtOverheadCost);
      this.panelControl1.Controls.Add((Control) this.chkEdtOverheadCost);
      this.panelControl1.Controls.Add((Control) this.sbtnCancel);
      this.panelControl1.Controls.Add((Control) this.sbtnOK);
      this.panelControl1.Controls.Add((Control) this.edtUnitCost);
      this.panelControl1.Name = "panelControl1";
      componentResourceManager.ApplyResources((object) this, "$this");
      this.AutoScaleMode = AutoScaleMode.Dpi;
      this.CancelButton = (IButtonControl) this.sbtnCancel;
      this.Controls.Add((Control) this.panelControl1);
      this.FormBorderStyle = FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "FormStockAssemblyOrderRangeSet";
      this.Load += new EventHandler(this.FormStockDocumentBulkSet_Load);
      this.chkEdtLocation.Properties.EndInit();
      this.chkEdtProjNo.Properties.EndInit();
      this.chkEdtDeptNo.Properties.EndInit();
      this.chkEdtQty.Properties.EndInit();
      this.chkEdtUnitCost.Properties.EndInit();
      this.luLocation.Properties.EndInit();
      this.luProjNo.Properties.EndInit();
      this.luDeptNo.Properties.EndInit();
      this.edtQty.Properties.EndInit();
      this.edtUnitCost.Properties.EndInit();
      this.chkEdtItemCode.Properties.EndInit();
      this.luItemCode.Properties.EndInit();
      this.chkEdtDescription.Properties.EndInit();
      this.edtDescription.Properties.EndInit();
      this.edtOverheadCost.Properties.EndInit();
      this.chkEdtOverheadCost.Properties.EndInit();
      this.panelControl1.EndInit();
      this.panelControl1.ResumeLayout(false);
      this.panelControl1.PerformLayout();
      this.ResumeLayout(false);
    }
  }
}
