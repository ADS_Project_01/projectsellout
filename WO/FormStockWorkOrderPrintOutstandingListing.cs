﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.FormStockAssemblyOrderPrintOutstandingListing
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.Authentication;
using BCE.AutoCount.Controller;
using BCE.AutoCount.Controls;
using BCE.AutoCount.FilterUI;
using BCE.AutoCount.Help;
using BCE.AutoCount.Invoicing;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.Report;
using BCE.AutoCount.XtraUtils;
using BCE.Data;
using BCE.Localization;
using BCE.XtraUtils;
using DevExpress.Data;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace RPASystem.WorkOrder
{
  [SingleInstanceThreadForm]
  public class FormStockWorkOrderPrintOutstandingListing : XtraForm
  {
    private DBSetting myDBSetting;
    private StockWorkOrderOutstandingReport myCommand;
    private StockWorkOrderOutstandingFormCriteria myReportingCriteria;
    private DataTable myDataTable;
    private DataTable myDetailDataTable;
    private DataSet myDataSet;
    private bool myInSearch;
    private bool myLoadAllColumns;
    private UserAuthentication myUserAuthentication;
    private MouseDownHelper myMouseDownHelper;
    private IContainer components;
    private PanelHeader panelHeader1;
    private PreviewButton previewButton1;
    private PrintButton printButton1;
    private UCBOMItemSelector ucbomItemSelector1;
    private UCBOMItemSelector ucbomItemSelector2;
    private UCAllDepartmentsSelector ucDepartmentSelector1;
    private UCAllDepartmentsSelector ucDepartmentSelector2;
    private UCItemGroupSelector ucItemGroupSelector1;
    private UCItemGroupSelector ucItemGroupSelector2;
    private UCItemSelector ucItemSelector1;
    private UCItemTypeSelector ucItemTypeSelector1;
    private UCItemTypeSelector ucItemTypeSelector2;
    private UCLocationSelector ucLocationSelector1;
    private UCLocationSelector ucLocationSelector2;
    private UCAllProjectsSelector ucProjectSelector1;
    private UCAllProjectsSelector ucProjectSelector2;
    private UCSearchResult ucSearchResult1;
    private UCStockAssemblyOrderSelector ucStockAssemblyOrderSelector1;
    private UCStockAssemblyOrderSelector ucStockAssemblyOrderSelector2;
    private Bar bar2;
    private BarButtonItem barbtnAdvancedFilter;
    private BarButtonItem barBtnDesignOutstandingListingReport;
    private BarDockControl barDockControlBottom;
    private BarDockControl barDockControlLeft;
    private BarDockControl barDockControlRight;
    private BarDockControl barDockControlTop;
    private BarManager barManager1;
    private BarSubItem barSubItem1;
    private CheckEdit chkEditShowCriteria;
    private CheckEdit chkEditShowCriteriaBasic;
    private ComboBoxEdit cbGroupBy;
    private ComboBoxEdit cbGroupByBasic;
    private ComboBoxEdit cbSortBy;
    private ComboBoxEdit cbSortByBasic;
    private DateEdit dateEditFromAdv;
    private DateEdit dateEditFromBasic;
    private DateEdit dateEditToAdv;
    private DateEdit dateEditToBasic;
    private GroupControl gbFilter;
    private GroupControl gbPrint;
    private GroupControl gbReport;
    private GroupControl groupControl1;
    private GroupControl groupControl2;
    private GroupControl groupControl3;
    private MemoEdit memoEdit_Criteria;
    private PanelControl panelAdv;
    private PanelControl panelBasic;
    private PanelControl panelCenter;
    private PanelControl panelControl4;
    private RadioGroup radioGroup1;
    private RadioGroup radioGroup2;
    private RepositoryItemCheckEdit repositoryItemCheckEdit1;
    private SimpleButton sbtnAdvOptions;
    private SimpleButton sbtnClose;
    private SimpleButton sbtnInquiry;
    private SimpleButton sbtnToggleOptions;
    private GridColumn colAssemblyCost;
    private GridColumn colBatchNo;
    private GridColumn colBatchNo1;
    private GridColumn colCreatedTimeStamp;
    private GridColumn colCreatedUserID;
    private GridColumn colDeptNo;
    private GridColumn colDeptNo1;
    private GridColumn colDescription;
    private GridColumn colDescription1;
    private GridColumn colDocDate;
    private GridColumn colDocDate1;
    private GridColumn colDocNo;
    private GridColumn colDocNo1;
    private GridColumn colDocType;
    private GridColumn colFurtherDescription1;
    private GridColumn colItemCode;
    private GridColumn colItemCode1;
    private GridColumn colItemCost;
    private GridColumn colLastModified;
    private GridColumn colLastModifiedUserID;
    private GridColumn colLocation;
    private GridColumn colLocation1;
    private GridColumn colNetTotal;
    private GridColumn colNote;
    private GridColumn colOverHeadCost;
    private GridColumn colPrintOut;
    private GridColumn colProjNo;
    private GridColumn colProjNo1;
    private GridColumn colQty;
    private GridColumn colQty1;
    private GridColumn colQty2;
    private GridColumn colRate;
    private GridColumn colRate1;
    private GridColumn colRemainingQty;
    private GridColumn colRemark;
    private GridColumn colRemark1;
    private GridColumn colRemark2;
    private GridColumn colRemark3;
    private GridColumn colRemark4;
    private GridColumn colSubTotalCost;
    private GridColumn colToBeUpdate;
    private GridColumn colTotal;
    private GridColumn colTransferedQty;
    private GridControl gridControl1;
    private GridView gridView1;
    private GridView gridView2;
    private GridView gridView3;
    private XtraTabControl xtraTabControl1;
    private XtraTabControl xtraTabControl2;
    private XtraTabPage xtraTabPage1;
    private XtraTabPage xtraTabPage2;
    private XtraTabPage xtraTabPage3;
    private XtraTabPage xtraTabPage4;
    private Label label1;
    private Label label10;
    private Label label11;
    private Label label12;
    private Label label13;
    private Label label14;
    private Label label15;
    private Label label16;
    private Label label17;
    private Label label18;
    private Label label19;
    private Label label2;
    private Label label20;
    private Label label21;
    private Label label22;
    private Label label23;
    private Label label24;
    private Label label3;
    private Label label5;
    private Label label6;
    private Label label7;
    private Label label8;
    private Label label9;
    private GridColumn colFGQty;
    private GridColumn colOutStandingFGQty;
    private RepositoryItemTextEdit repositoryItemTextEdit1;
    private GridColumn colExpCompletedDate;

    public string SelectedDocKeysInString
    {
      get
      {
        return Utils.ArrayToString(new ArrayList((ICollection) CommonFunction.GetDocKeyList("ToBeUpdate", this.myDataTable)));
      }
    }

    public FormStockWorkOrderPrintOutstandingListing(DBSetting dbSetting)
    {
      this.InitializeComponent();
      this.myCommand = StockWorkOrderOutstandingReport.Create(dbSetting);
      this.myUserAuthentication = UserAuthentication.GetOrCreate(dbSetting);
      this.Init();
      this.InitUserControls();
      this.InitFormControls();
      this.printButton1.ReportType = "Outstanding Stock Assembly Order";
      this.printButton1.SetDBSetting(dbSetting);
      this.previewButton1.ReportType = "Outstanding Stock Assembly Order";
      this.previewButton1.SetDBSetting(dbSetting);
      this.RefreshDesignReport();
      this.myMouseDownHelper = new MouseDownHelper();
      this.myMouseDownHelper.Init(this.gridView1);
      this.myUserAuthentication.AccessRight.AddListener(new AccessRightListenerDelegate(this.RefreshDesignReport), (Component) this);
      BCE.AutoCount.Help.HelpProvider.SetHelpTopic(this.panelHeader1, "Stock_Assembly_Order.htm");
    }

    private void Init()
    {
      this.myDBSetting = this.myCommand.DBSetting;
      this.myDataTable = new DataTable("Master");
      this.myDetailDataTable = new DataTable("Detail");
      this.myDataSet = new DataSet();
      this.myDataSet.Tables.Add(this.myDataTable);
      this.myDataSet.Tables.Add(this.myDetailDataTable);
      this.BuildDataRelation();
      this.gridControl1.DataSource = (object) this.myDataTable;
      this.gridControl1.LevelTree.Nodes.Clear();
      this.gridControl1.LevelTree.Nodes.Add("MasterDetailRelation", (BaseView) this.gridView2);
      this.gridControl1.LevelTree.Nodes["MasterDetailRelation"].Nodes.Add("Detail_PartialTransferToTable", (BaseView) this.gridView3);
      this.gridView2.ViewCaption = "Detail";
      this.gridView3.ViewCaption = "Transfer Status";
      this.LoadCriteria();
      this.ucSearchResult1.Initialize(this.gridView1, "ToBeUpdate");
      CustomizeGridLayout customizeGridLayout = new CustomizeGridLayout(this.myDBSetting, this.Name, this.gridView1, new EventHandler(this.ReloadAllColumns));
      this.Tag = (object) EnterKeyMessageFilter.NoFilter;
      this.AddNewColumn();
      if (this.myReportingCriteria.FilterMode == FilterMode.BasicMode)
      {
        this.panelAdv.Visible = false;
        this.panelBasic.Visible = true;
        this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_SwitchToAdvancedOptions, new object[0]);
      }
      else
      {
        this.panelAdv.Visible = true;
        this.panelBasic.Visible = false;
        this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_SwitchToBasicOptions, new object[0]);
      }
      DateEdit dateEdit1 = this.dateEditFromAdv;
      DateTime now = DateTime.Now;
      int year1 = now.Year;
      now = DateTime.Now;
      int month1 = now.Month;
      int day1 = 1;
      DateTime dateTime1 = new DateTime(year1, month1, day1);
      dateEdit1.DateTime = dateTime1;
      DateEdit dateEdit2 = this.dateEditFromBasic;
      now = DateTime.Now;
      int year2 = now.Year;
      now = DateTime.Now;
      int month2 = now.Month;
      int day2 = 1;
      DateTime dateTime2 = new DateTime(year2, month2, day2);
      dateEdit2.DateTime = dateTime2;
      DateEdit dateEdit3 = this.dateEditToAdv;
      now = DateTime.Now;
      DateTime date1 = now.Date;
      dateEdit3.DateTime = date1;
      DateEdit dateEdit4 = this.dateEditToBasic;
      now = DateTime.Now;
      DateTime date2 = now.Date;
      dateEdit4.DateTime = date2;
    }

    private void InitFormControls()
    {
      FormControlUtil formControlUtil = new FormControlUtil(this.myDBSetting);
      string fieldname1 = "Total";
      string fieldtype1 = "Currency";
      formControlUtil.AddField(fieldname1, fieldtype1);
      string fieldname2 = "AssemblyCost";
      string fieldtype2 = "Currency";
      formControlUtil.AddField(fieldname2, fieldtype2);
      string fieldname3 = "NetTotal";
      string fieldtype3 = "Currency";
      formControlUtil.AddField(fieldname3, fieldtype3);
      string fieldname4 = "DocDate";
      string fieldtype4 = "Date";
      formControlUtil.AddField(fieldname4, fieldtype4);
      string fieldname5 = "Qty";
      string fieldtype5 = "Quantity";
      formControlUtil.AddField(fieldname5, fieldtype5);
      string fieldname6 = "ItemCost";
      string fieldtype6 = "Currency";
      formControlUtil.AddField(fieldname6, fieldtype6);
      string fieldname7 = "OverHeadCost";
      string fieldtype7 = "Currency";
      formControlUtil.AddField(fieldname7, fieldtype7);
      string fieldname8 = "SubTotalCost";
      string fieldtype8 = "Currency";
      formControlUtil.AddField(fieldname8, fieldtype8);
      string fieldname9 = "Rate";
      string fieldtype9 = "CurrencyRate";
      formControlUtil.AddField(fieldname9, fieldtype9);
      string fieldname10 = "RemainingQty";
      string fieldtype10 = "Quantity";
      formControlUtil.AddField(fieldname10, fieldtype10);
      string fieldname11 = "TransferedQty";
      string fieldtype11 = "Quantity";
      formControlUtil.AddField(fieldname11, fieldtype11);
      string fieldname12 = "Qty2";
      string fieldtype12 = "Quantity";
      formControlUtil.AddField(fieldname12, fieldtype12);
      string fieldname13 = "LastModified";
      string fieldtype13 = "DateTime";
      formControlUtil.AddField(fieldname13, fieldtype13);
      string fieldname14 = "CreatedTimeStamp";
      string fieldtype14 = "DateTime";
      formControlUtil.AddField(fieldname14, fieldtype14);
      string fieldname15 = "OutstandingFGQty";
      string fieldtype15 = "Quantity";
      formControlUtil.AddField(fieldname15, fieldtype15);
      FormStockWorkOrderPrintOutstandingListing outstandingListing = this;
      formControlUtil.InitControls((Control) outstandingListing);
    }

    private void SaveCriteria()
    {
      this.AssignCriteria();
      PersistenceUtil.SaveCriteriaData(this.myDBSetting, (CustomSetting) this.myReportingCriteria, "StockAssemblyOrderOutstandingListingReport.setting");
    }

    private void LoadCriteria()
    {
      this.myReportingCriteria = (StockWorkOrderOutstandingFormCriteria) PersistenceUtil.LoadCriteriaData(this.myDBSetting, "StockAssemblyOrderOutstandingListingReport.setting");
      if (this.myReportingCriteria == null)
        this.myReportingCriteria = new StockWorkOrderOutstandingFormCriteria();
      this.cbGroupBy.SelectedIndex = this.myReportingCriteria.GroupBy;
      this.cbGroupByBasic.SelectedIndex = this.myReportingCriteria.GroupBy;
      this.cbGroupBy.SelectedIndex = this.myReportingCriteria.SortBy;
      this.cbGroupByBasic.SelectedIndex = this.myReportingCriteria.SortBy;
      this.ucSearchResult1.KeepSearchResult = this.myReportingCriteria.KeepSearchResult;
      this.chkEditShowCriteria.Checked = this.myReportingCriteria.ShowCriteria;
      this.chkEditShowCriteriaBasic.Checked = this.myReportingCriteria.ShowCriteria;
      this.radioGroup1.SelectedIndex = this.myReportingCriteria.OutstandingPrintingOption == StockWorkOrderOutstandingFormCriteria.PrintOption.Outstanding ? 0 : 1;
      this.radioGroup2.SelectedIndex = this.radioGroup1.SelectedIndex;
    }

    private void AssignCriteria()
    {
      if (this.myReportingCriteria != null)
      {
        this.myReportingCriteria.FilterMode = this.panelBasic.Visible ? FilterMode.BasicMode : FilterMode.AdvanceMode;
        this.myReportingCriteria.OutstandingPrintingOption = this.radioGroup1.SelectedIndex == 0 ? StockWorkOrderOutstandingFormCriteria.PrintOption.Outstanding : StockWorkOrderOutstandingFormCriteria.PrintOption.Compeleted;
        if (this.myReportingCriteria.FilterMode == FilterMode.AdvanceMode)
        {
          this.myReportingCriteria.FromDate = this.dateEditFromAdv.DateTime;
          this.myReportingCriteria.EndDate = this.dateEditToAdv.DateTime;
          this.myReportingCriteria.GroupBy = this.cbGroupBy.SelectedIndex;
          this.myReportingCriteria.SortBy = this.cbSortBy.SelectedIndex;
          this.myReportingCriteria.ShowCriteria = this.chkEditShowCriteria.Checked;
        }
        else
        {
          this.myReportingCriteria.FromDate = this.dateEditFromBasic.DateTime;
          this.myReportingCriteria.EndDate = this.dateEditToBasic.DateTime;
          this.myReportingCriteria.GroupBy = this.cbGroupByBasic.SelectedIndex;
          this.myReportingCriteria.SortBy = this.cbSortByBasic.SelectedIndex;
          this.myReportingCriteria.ShowCriteria = this.chkEditShowCriteriaBasic.Checked;
        }
      }
    }

    private void InitUserControls()
    {
      this.ucStockAssemblyOrderSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentNoFilter);
      this.ucStockAssemblyOrderSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DocumentNoFilter);
      this.ucbomItemSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.BomItemCodeFilter);
      this.ucbomItemSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.BomItemCodeFilter);
      this.ucItemGroupSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemGroupFilter);
      this.ucItemTypeSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemTypeFilter);
      this.ucLocationSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.LocationFilter);
      this.ucProjectSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ProjectNoFilter);
      this.ucDepartmentSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DepartmentNoFilter);
      this.ucItemSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemCodeDetailFilter);
      this.ucItemGroupSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.ItemGroupDetailFilter);
      this.ucItemTypeSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.ItemTypeDetailFilter);
      this.ucLocationSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.LocationDetailFilter);
      this.ucProjectSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.ProjectNoDetailFilter);
      this.ucDepartmentSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DepartmentNoDetailFilter);
    }

    private void RefreshUserControlWhenSwitchFromMoreOption()
    {
      this.ucItemGroupSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemGroupFilter);
      this.ucItemTypeSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemTypeFilter);
      this.ucLocationSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.LocationFilter);
      this.ucProjectSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ProjectNoFilter);
      this.ucDepartmentSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DepartmentNoFilter);
      this.ucItemSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.ItemCodeDetailFilter);
      this.ucItemGroupSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.ItemGroupDetailFilter);
      this.ucItemTypeSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.ItemTypeDetailFilter);
      this.ucLocationSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.LocationDetailFilter);
      this.ucProjectSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.ProjectNoDetailFilter);
      this.ucDepartmentSelector2.Initialize(this.myDBSetting, this.myReportingCriteria.DepartmentNoDetailFilter);
    }

    private void ReloadAllColumns(object sender, EventArgs e)
    {
      if (!this.myLoadAllColumns)
        this.BasicSearch(true);
      this.myLoadAllColumns = true;
    }

    private void AddNewColumn()
    {
      DataColumn column = new DataColumn();
      column.DataType = typeof (bool);
      column.AllowDBNull = true;
      column.Caption = "Check";
      column.ColumnName = "ToBeUpdate";
      column.DefaultValue = (object) false;
      if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
        this.myDataTable.Columns.Add(column);
    }

    private void BuildDataRelation()
    {
      if (!this.myDataSet.Relations.Contains("MasterDetailRelation") && this.myDetailDataTable.Rows.Count > 0)
        this.myDataSet.Relations.Add(new DataRelation("MasterDetailRelation", this.myDataTable.Columns["DocKey"], this.myDetailDataTable.Columns["DocKey"], false));
    }

    private void BasicSearch(bool isSearchAll)
    {
      if (!this.myInSearch)
      {
        this.AssignCriteria();
        this.myInSearch = true;
        BCE.XtraUtils.GridViewUtils.UpdateData(this.gridView1);
        try
        {
          this.gridControl1.MainView.UpdateCurrentRow();
          this.myReportingCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
          this.myCommand.BasicSearch(this.myReportingCriteria, CommonFunction.BuildSQLColumns(isSearchAll, this.gridView1.Columns.View).Replace(",OutStandingFGQty", ""), this.myDataSet, "ToBeUpdate");
          this.BuildDataRelation();
          this.gridControl1.DataSource = (object) null;
          this.gridControl1.ForceInitialize();
          this.gridControl1.DataSource = (object) this.myDataTable;
          this.gridControl1.RefreshDataSource();
          this.gridControl1.ForceInitialize();
        }
        catch (Exception ex)
        {
          AppMessage.ShowErrorMessage(ex.Message);
        }
        finally
        {
          this.myInSearch = false;
        }
      }
    }

    private void sbtnInquiry_Click(object sender, EventArgs e)
    {
      this.sbtnToggleOptions.Enabled = true;
      this.BasicSearch(this.myLoadAllColumns);
      if (this.myReportingCriteria.FilterMode == FilterMode.AdvanceMode)
      {
        if (this.cbSortBy.SelectedIndex == 0)
        {
          this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
        }
        else if (this.cbSortBy.SelectedIndex == 1)
        {
          this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
          this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
        }
      }
      else if (this.cbSortByBasic.SelectedIndex == 0)
      {
        this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
        this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
      }
      else if (this.cbSortByBasic.SelectedIndex == 1)
      {
        this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
        this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
      }
      this.ucSearchResult1.CheckAll();
      this.memoEdit_Criteria.Lines = this.myReportingCriteria.ReadableTextArray;
    }

    private void FormStockAssemblyOrderPrintOutstandingListing_Load(object sender, EventArgs e)
    {
    }

    private void FormStockAssemblyOrderPrintOutstandingListing_Closing(object sender, CancelEventArgs e)
    {
      this.SaveCriteria();
    }

    private void gridView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      GridView gridView = (GridView) sender;
      int[] selectedRows = gridView.GetSelectedRows();
      if (selectedRows != null && selectedRows.Length != 0 && selectedRows.Length != 1)
      {
        for (int index = 0; index < selectedRows.Length; ++index)
          gridView.GetDataRow(selectedRows[index])["ToBeUpdate"] = (object) true;
      }
    }

    private void barbtnAdvancedFilter_ItemClick(object sender, ItemClickEventArgs e)
    {
      this.SwitchMode();
    }

    private void SwitchMode()
    {
      this.AssignCriteria();
      if (this.myReportingCriteria.FilterMode == FilterMode.AdvanceMode)
      {
        this.panelBasic.Visible = true;
        this.panelAdv.Visible = false;
        this.myReportingCriteria.FilterMode = FilterMode.BasicMode;
        this.dateEditFromBasic.DateTime = this.myReportingCriteria.FromDate;
        this.dateEditToBasic.DateTime = this.myReportingCriteria.EndDate;
        this.ucStockAssemblyOrderSelector2.ApplyFilter(this.myReportingCriteria.DocumentNoFilter);
        this.ucbomItemSelector2.ApplyFilter(this.myReportingCriteria.BomItemCodeFilter);
        this.cbGroupByBasic.EditValue = this.cbGroupBy.EditValue;
        this.cbSortByBasic.EditValue = this.cbSortBy.EditValue;
        this.chkEditShowCriteriaBasic.EditValue = this.chkEditShowCriteria.EditValue;
        this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_SwitchToAdvancedOptions, new object[0]);
      }
      else
      {
        this.panelBasic.Visible = false;
        this.panelAdv.Visible = true;
        this.myReportingCriteria.FilterMode = FilterMode.AdvanceMode;
        this.barbtnAdvancedFilter.Caption = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_SwitchToBasicOptions, new object[0]);
        this.dateEditFromAdv.DateTime = this.myReportingCriteria.FromDate;
        this.dateEditToAdv.DateTime = this.myReportingCriteria.EndDate;
        this.ucStockAssemblyOrderSelector1.ApplyFilter(this.myReportingCriteria.DocumentNoFilter);
        this.ucbomItemSelector1.ApplyFilter(this.myReportingCriteria.BomItemCodeFilter);
        this.cbGroupBy.EditValue = this.cbGroupByBasic.EditValue;
        this.cbSortBy.EditValue = this.cbSortByBasic.EditValue;
        this.chkEditShowCriteria.EditValue = this.chkEditShowCriteriaBasic.EditValue;
      }
    }

    private void dateEditFromAdv_EditValueChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.FromDate = this.dateEditFromAdv.DateTime;
    }

    private void dateEditToAdv_EditValueChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.FromDate = this.dateEditToAdv.DateTime;
    }

    private void dateEditFromBasic_EditValueChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.FromDate = this.dateEditFromBasic.DateTime;
    }

    private void dateEditToBasic_EditValueChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.FromDate = this.dateEditToBasic.DateTime;
    }

    private void previewButton1_Preview(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
    {
      if (this.myUserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_OUTLIST_REPORT_PREVIEW", true))
      {
        BCE.XtraUtils.GridViewUtils.UpdateData(this.gridView1);
        if (this.gridView1.FocusedRowHandle >= 0)
        {
          if (this.myDataTable.Select("ToBeUpdate = True").Length == 0)
          {
            AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_NoOutstandingStockAssemblySelected, new object[0]));
            this.DialogResult = DialogResult.None;
          }
          else
          {
            this.AssignCriteria();
            this.myCommand.PreviewOutstandingListingReport(this.SelectedDocKeysInString, this.myReportingCriteria, e.DefaultReport);
          }
        }
      }
    }

    private void printButton1_Print(object sender, BCE.AutoCount.Controls.PrintEventArgs e)
    {
      if (this.myUserAuthentication.AccessRight.IsAccessible("MF_ASMORDER_OUTLIST_REPORT_PRINT", true))
      {
        BCE.XtraUtils.GridViewUtils.UpdateData(this.gridView1);
        if (this.gridView1.FocusedRowHandle >= 0)
        {
          if (this.myDataTable.Select("ToBeUpdate = True").Length == 0)
          {
            AppMessage.ShowMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ShowMessage_NoOutstandingStockAssemblySelected, new object[0]));
            this.DialogResult = DialogResult.None;
          }
          else
          {
            this.AssignCriteria();
            this.myCommand.PrintOutstandingListingReport(this.SelectedDocKeysInString, this.myReportingCriteria, e.DefaultReport);
          }
        }
      }
    }

    private void sbtnToggleOptions_Click(object sender, EventArgs e)
    {
      if (this.myReportingCriteria.FilterMode == FilterMode.AdvanceMode)
      {
        this.panelAdv.Visible = !this.panelAdv.Visible;
        if (this.panelAdv.Visible)
          this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_HideOptions, new object[0]);
        else
          this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_ShowOptions, new object[0]);
      }
      else
      {
        this.panelBasic.Visible = !this.panelBasic.Visible;
        if (this.panelBasic.Visible)
          this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_HideOptions, new object[0]);
        else
          this.sbtnToggleOptions.Text = BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.Code_ShowOptions, new object[0]);
      }
    }

    private void sbtnClose_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void FormStockAssemblyOrderPrintOutstandingListing_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == (Keys) 120)
      {
        if (this.sbtnInquiry.Enabled)
          this.sbtnInquiry.PerformClick();
      }
      else if (e.KeyCode == (Keys) 119)
      {
        if (this.previewButton1.Enabled)
          this.previewButton1.PerformClick();
      }
      else if (e.KeyCode == (Keys) 118)
      {
        if (this.printButton1.Enabled)
          this.printButton1.PerformClick();
      }
      else if (e.KeyCode == (Keys) 117 && this.sbtnToggleOptions.Enabled)
        this.sbtnToggleOptions.PerformClick();
    }

    private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.OutstandingPrintingOption = this.radioGroup1.SelectedIndex == 0 ? StockWorkOrderOutstandingFormCriteria.PrintOption.Outstanding : StockWorkOrderOutstandingFormCriteria.PrintOption.Compeleted;
    }

    private void radioGroup2_SelectedIndexChanged(object sender, EventArgs e)
    {
      this.myReportingCriteria.OutstandingPrintingOption = this.radioGroup2.SelectedIndex == 0 ? StockWorkOrderOutstandingFormCriteria.PrintOption.Outstanding : StockWorkOrderOutstandingFormCriteria.PrintOption.Compeleted;
      this.radioGroup1.SelectedIndex = this.radioGroup2.SelectedIndex;
    }

    private void cbGroupBy_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.GroupBy = this.cbGroupBy.SelectedIndex;
    }

    private void cbGroupByBasic_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
        this.myReportingCriteria.GroupBy = this.cbGroupByBasic.SelectedIndex;
    }

    private void cbSortBy_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
      {
        if (this.myReportingCriteria.SortBy != this.cbSortBy.SelectedIndex)
          this.myReportingCriteria.SortBy = this.cbSortBy.SelectedIndex;
        if (this.cbSortBy.SelectedIndex == 0)
        {
          this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
        }
        else if (this.cbSortBy.SelectedIndex == 1)
        {
          this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
          this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
        }
      }
    }

    private void cbSortByBasic_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.myReportingCriteria != null)
      {
        if (this.myReportingCriteria.SortBy != this.cbGroupByBasic.SelectedIndex)
          this.myReportingCriteria.SortBy = this.cbGroupByBasic.SelectedIndex;
        if (this.cbSortByBasic.SelectedIndex == 0)
        {
          this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
          this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
        }
        else if (this.cbSortByBasic.SelectedIndex == 1)
        {
          this.gridView1.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
          this.gridView1.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
        }
      }
    }

    private void sbtnAdvOptions_Click(object sender, EventArgs e)
    {
      using (FormStockWorkOrderOustandingReportMoreOptions reportMoreOptions = new FormStockWorkOrderOustandingReportMoreOptions(this.myReportingCriteria, this.myDBSetting))
      {
        if (reportMoreOptions.ShowDialog((IWin32Window) this) == DialogResult.OK)
        {
          this.cbGroupBy.SelectedIndex = this.myReportingCriteria.GroupBy;
          this.cbGroupByBasic.SelectedIndex = this.myReportingCriteria.GroupBy;
          this.cbSortBy.SelectedIndex = this.myReportingCriteria.SortBy;
          this.cbSortByBasic.SelectedIndex = this.myReportingCriteria.SortBy;
          this.ucSearchResult1.KeepSearchResult = this.myReportingCriteria.KeepSearchResult;
          this.chkEditShowCriteria.Checked = this.myReportingCriteria.ShowCriteria;
          this.chkEditShowCriteriaBasic.Checked = this.myReportingCriteria.ShowCriteria;
          this.radioGroup1.SelectedIndex = this.myReportingCriteria.OutstandingPrintingOption == StockWorkOrderOutstandingFormCriteria.PrintOption.Outstanding ? 0 : 1;
          this.RefreshUserControlWhenSwitchFromMoreOption();
        }
      }
    }

    private void barBtnDesignOutstandingListingReport_ItemClick(object sender, ItemClickEventArgs e)
    {
      ReportTool.DesignReport("Outstanding Stock Assembly Order", this.myDBSetting);
    }

    public virtual void RefreshDesignReport()
    {
      this.barBtnDesignOutstandingListingReport.Visibility = XtraBarsUtils.ToBarItemVisibility(this.myUserAuthentication.AccessRight.IsAccessible("TOOLS_RPT_SHOW"));
    }

    private void gridView1_DoubleClick(object sender, EventArgs e)
    {
      if (this.myMouseDownHelper.IsLeftMouseDown && BCE.XtraUtils.GridViewUtils.GetGridHitInfo((GridView) sender).InRow && UserAuthentication.GetOrCreate(this.myDBSetting).AccessRight.IsAccessible("SYS_BHV_DRILLDOWN"))
        this.GoToDocument();
    }

    private void GoToDocument()
    {
      ColumnView columnView = (ColumnView) this.gridControl1.FocusedView;
      int focusedRowHandle = columnView.FocusedRowHandle;
      DataRow dataRow = columnView.GetDataRow(focusedRowHandle);
      if (dataRow != null)
        DocumentDispatcher.Open(this.myDBSetting, "AO", BCE.Data.Convert.ToInt64(dataRow["DocKey"]));
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormStockWorkOrderPrintOutstandingListing));
      GridLevelNode gridLevelNode1 = new GridLevelNode();
      GridLevelNode gridLevelNode2 = new GridLevelNode();
      this.gridView2 = new GridView();
      this.colItemCode1 = new GridColumn();
      this.colLocation1 = new GridColumn();
      this.colBatchNo1 = new GridColumn();
      this.colDescription1 = new GridColumn();
      this.colFurtherDescription1 = new GridColumn();
      this.colProjNo1 = new GridColumn();
      this.colDeptNo1 = new GridColumn();
      this.colRate = new GridColumn();
      this.colQty = new GridColumn();
      this.colTransferedQty = new GridColumn();
      this.colItemCost = new GridColumn();
      this.colOverHeadCost = new GridColumn();
      this.colSubTotalCost = new GridColumn();
      this.colRemark = new GridColumn();
      this.colPrintOut = new GridColumn();
      this.colRemainingQty = new GridColumn();
      this.gridControl1 = new GridControl();
      this.gridView3 = new GridView();
      this.colDocType = new GridColumn();
      this.colDocNo1 = new GridColumn();
      this.colDocDate1 = new GridColumn();
      this.colQty1 = new GridColumn();
      this.colQty2 = new GridColumn();
      this.colRate1 = new GridColumn();
      this.gridView1 = new GridView();
      this.colToBeUpdate = new GridColumn();
      this.repositoryItemCheckEdit1 = new RepositoryItemCheckEdit();
      this.colDocNo = new GridColumn();
      this.colDocDate = new GridColumn();
      this.colDescription = new GridColumn();
      this.colItemCode = new GridColumn();
      this.colLocation = new GridColumn();
      this.colBatchNo = new GridColumn();
      this.colProjNo = new GridColumn();
      this.colDeptNo = new GridColumn();
      this.colTotal = new GridColumn();
      this.colAssemblyCost = new GridColumn();
      this.colNetTotal = new GridColumn();
      this.colNote = new GridColumn();
      this.colRemark1 = new GridColumn();
      this.colRemark2 = new GridColumn();
      this.colRemark3 = new GridColumn();
      this.colRemark4 = new GridColumn();
      this.colLastModified = new GridColumn();
      this.colLastModifiedUserID = new GridColumn();
      this.colCreatedTimeStamp = new GridColumn();
      this.colCreatedUserID = new GridColumn();
      this.colFGQty = new GridColumn();
      this.colOutStandingFGQty = new GridColumn();
      this.colExpCompletedDate = new GridColumn();
      this.repositoryItemTextEdit1 = new RepositoryItemTextEdit();
      this.panelAdv = new PanelControl();
      this.gbPrint = new GroupControl();
      this.radioGroup1 = new RadioGroup();
      this.gbReport = new GroupControl();
      this.chkEditShowCriteria = new CheckEdit();
      this.cbSortBy = new ComboBoxEdit();
      this.cbGroupBy = new ComboBoxEdit();
      this.label17 = new Label();
      this.label18 = new Label();
      this.gbFilter = new GroupControl();
      this.xtraTabControl1 = new XtraTabControl();
      this.xtraTabPage1 = new XtraTabPage();
      this.ucbomItemSelector1 = new UCBOMItemSelector();
      this.dateEditFromAdv = new DateEdit();
      this.dateEditToAdv = new DateEdit();
      this.ucDepartmentSelector1 = new UCAllDepartmentsSelector();
      this.ucLocationSelector1 = new UCLocationSelector();
      this.ucItemGroupSelector1 = new UCItemGroupSelector();
      this.ucItemTypeSelector1 = new UCItemTypeSelector();
      this.ucStockAssemblyOrderSelector1 = new UCStockAssemblyOrderSelector();
      this.label9 = new Label();
      this.label8 = new Label();
      this.label7 = new Label();
      this.label6 = new Label();
      this.label5 = new Label();
      this.label3 = new Label();
      this.label2 = new Label();
      this.label1 = new Label();
      this.ucProjectSelector1 = new UCAllProjectsSelector();
      this.label16 = new Label();
      this.xtraTabPage2 = new XtraTabPage();
      this.ucItemSelector1 = new UCItemSelector();
      this.ucDepartmentSelector2 = new UCAllDepartmentsSelector();
      this.ucProjectSelector2 = new UCAllProjectsSelector();
      this.ucLocationSelector2 = new UCLocationSelector();
      this.ucItemTypeSelector2 = new UCItemTypeSelector();
      this.ucItemGroupSelector2 = new UCItemGroupSelector();
      this.label10 = new Label();
      this.label11 = new Label();
      this.label12 = new Label();
      this.label13 = new Label();
      this.label14 = new Label();
      this.label15 = new Label();
      this.panelBasic = new PanelControl();
      this.groupControl3 = new GroupControl();
      this.radioGroup2 = new RadioGroup();
      this.groupControl2 = new GroupControl();
      this.chkEditShowCriteriaBasic = new CheckEdit();
      this.cbSortByBasic = new ComboBoxEdit();
      this.cbGroupByBasic = new ComboBoxEdit();
      this.label23 = new Label();
      this.label24 = new Label();
      this.groupControl1 = new GroupControl();
      this.ucbomItemSelector2 = new UCBOMItemSelector();
      this.sbtnAdvOptions = new SimpleButton();
      this.ucStockAssemblyOrderSelector2 = new UCStockAssemblyOrderSelector();
      this.dateEditToBasic = new DateEdit();
      this.dateEditFromBasic = new DateEdit();
      this.label22 = new Label();
      this.label19 = new Label();
      this.label20 = new Label();
      this.label21 = new Label();
      this.panelCenter = new PanelControl();
      this.sbtnClose = new SimpleButton();
      this.sbtnToggleOptions = new SimpleButton();
      this.printButton1 = new PrintButton();
      this.previewButton1 = new PreviewButton();
      this.sbtnInquiry = new SimpleButton();
      this.panelControl4 = new PanelControl();
      this.xtraTabControl2 = new XtraTabControl();
      this.xtraTabPage3 = new XtraTabPage();
      this.ucSearchResult1 = new UCSearchResult();
      this.xtraTabPage4 = new XtraTabPage();
      this.memoEdit_Criteria = new MemoEdit();
      this.barManager1 = new BarManager(this.components);
      this.bar2 = new Bar();
      this.barSubItem1 = new BarSubItem();
      this.barBtnDesignOutstandingListingReport = new BarButtonItem();
      this.barbtnAdvancedFilter = new BarButtonItem();
      this.barDockControlTop = new BarDockControl();
      this.barDockControlBottom = new BarDockControl();
      this.barDockControlLeft = new BarDockControl();
      this.barDockControlRight = new BarDockControl();
      this.panelHeader1 = new PanelHeader();
      this.gridView2.BeginInit();
      this.gridControl1.BeginInit();
      this.gridView3.BeginInit();
      this.gridView1.BeginInit();
      this.repositoryItemCheckEdit1.BeginInit();
      this.repositoryItemTextEdit1.BeginInit();
      this.panelAdv.BeginInit();
      this.panelAdv.SuspendLayout();
      this.gbPrint.BeginInit();
      this.gbPrint.SuspendLayout();
      this.radioGroup1.Properties.BeginInit();
      this.gbReport.BeginInit();
      this.gbReport.SuspendLayout();
      this.chkEditShowCriteria.Properties.BeginInit();
      this.cbSortBy.Properties.BeginInit();
      this.cbGroupBy.Properties.BeginInit();
      this.gbFilter.BeginInit();
      this.gbFilter.SuspendLayout();
      this.xtraTabControl1.BeginInit();
      this.xtraTabControl1.SuspendLayout();
      this.xtraTabPage1.SuspendLayout();
      this.dateEditFromAdv.Properties.CalendarTimeProperties.BeginInit();
      this.dateEditFromAdv.Properties.BeginInit();
      this.dateEditToAdv.Properties.CalendarTimeProperties.BeginInit();
      this.dateEditToAdv.Properties.BeginInit();
      this.xtraTabPage2.SuspendLayout();
      this.panelBasic.BeginInit();
      this.panelBasic.SuspendLayout();
      this.groupControl3.BeginInit();
      this.groupControl3.SuspendLayout();
      this.radioGroup2.Properties.BeginInit();
      this.groupControl2.BeginInit();
      this.groupControl2.SuspendLayout();
      this.chkEditShowCriteriaBasic.Properties.BeginInit();
      this.cbSortByBasic.Properties.BeginInit();
      this.cbGroupByBasic.Properties.BeginInit();
      this.groupControl1.BeginInit();
      this.groupControl1.SuspendLayout();
      this.dateEditToBasic.Properties.CalendarTimeProperties.BeginInit();
      this.dateEditToBasic.Properties.BeginInit();
      this.dateEditFromBasic.Properties.CalendarTimeProperties.BeginInit();
      this.dateEditFromBasic.Properties.BeginInit();
      this.panelCenter.BeginInit();
      this.panelCenter.SuspendLayout();
      this.panelControl4.BeginInit();
      this.panelControl4.SuspendLayout();
      this.xtraTabControl2.BeginInit();
      this.xtraTabControl2.SuspendLayout();
      this.xtraTabPage3.SuspendLayout();
      this.xtraTabPage4.SuspendLayout();
      this.memoEdit_Criteria.Properties.BeginInit();
      this.barManager1.BeginInit();
      this.SuspendLayout();
      GridColumnCollection columns1 = this.gridView2.Columns;
      GridColumn[] columns2 = new GridColumn[16];
      int index1 = 0;
      GridColumn gridColumn1 = this.colItemCode1;
      columns2[index1] = gridColumn1;
      int index2 = 1;
      GridColumn gridColumn2 = this.colLocation1;
      columns2[index2] = gridColumn2;
      int index3 = 2;
      GridColumn gridColumn3 = this.colBatchNo1;
      columns2[index3] = gridColumn3;
      int index4 = 3;
      GridColumn gridColumn4 = this.colDescription1;
      columns2[index4] = gridColumn4;
      int index5 = 4;
      GridColumn gridColumn5 = this.colFurtherDescription1;
      columns2[index5] = gridColumn5;
      int index6 = 5;
      GridColumn gridColumn6 = this.colProjNo1;
      columns2[index6] = gridColumn6;
      int index7 = 6;
      GridColumn gridColumn7 = this.colDeptNo1;
      columns2[index7] = gridColumn7;
      int index8 = 7;
      GridColumn gridColumn8 = this.colRate;
      columns2[index8] = gridColumn8;
      int index9 = 8;
      GridColumn gridColumn9 = this.colQty;
      columns2[index9] = gridColumn9;
      int index10 = 9;
      GridColumn gridColumn10 = this.colTransferedQty;
      columns2[index10] = gridColumn10;
      int index11 = 10;
      GridColumn gridColumn11 = this.colItemCost;
      columns2[index11] = gridColumn11;
      int index12 = 11;
      GridColumn gridColumn12 = this.colOverHeadCost;
      columns2[index12] = gridColumn12;
      int index13 = 12;
      GridColumn gridColumn13 = this.colSubTotalCost;
      columns2[index13] = gridColumn13;
      int index14 = 13;
      GridColumn gridColumn14 = this.colRemark;
      columns2[index14] = gridColumn14;
      int index15 = 14;
      GridColumn gridColumn15 = this.colPrintOut;
      columns2[index15] = gridColumn15;
      int index16 = 15;
      GridColumn gridColumn16 = this.colRemainingQty;
      columns2[index16] = gridColumn16;
      columns1.AddRange(columns2);
      this.gridView2.GridControl = this.gridControl1;
      this.gridView2.Name = "gridView2";
      this.gridView2.OptionsView.ShowGroupPanel = false;
      componentResourceManager.ApplyResources((object) this.colItemCode1, "colItemCode1");
      this.colItemCode1.FieldName = "ItemCode";
      this.colItemCode1.Name = "colItemCode1";
      this.colItemCode1.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colLocation1, "colLocation1");
      this.colLocation1.FieldName = "Location";
      this.colLocation1.Name = "colLocation1";
      this.colLocation1.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colBatchNo1, "colBatchNo1");
      this.colBatchNo1.FieldName = "BatchNo";
      this.colBatchNo1.Name = "colBatchNo1";
      this.colBatchNo1.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDescription1, "colDescription1");
      this.colDescription1.FieldName = "Description";
      this.colDescription1.Name = "colDescription1";
      this.colDescription1.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colFurtherDescription1, "colFurtherDescription1");
      this.colFurtherDescription1.FieldName = "FurtherDescription";
      this.colFurtherDescription1.Name = "colFurtherDescription1";
      this.colFurtherDescription1.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colProjNo1, "colProjNo1");
      this.colProjNo1.FieldName = "ProjNo";
      this.colProjNo1.Name = "colProjNo1";
      this.colProjNo1.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDeptNo1, "colDeptNo1");
      this.colDeptNo1.FieldName = "DeptNo";
      this.colDeptNo1.Name = "colDeptNo1";
      this.colDeptNo1.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRate, "colRate");
      this.colRate.FieldName = "Rate";
      this.colRate.Name = "colRate";
      this.colRate.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colQty, "colQty");
      this.colQty.FieldName = "Qty";
      this.colQty.Name = "colQty";
      this.colQty.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colTransferedQty, "colTransferedQty");
      this.colTransferedQty.FieldName = "TransferedQty";
      this.colTransferedQty.Name = "colTransferedQty";
      this.colTransferedQty.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colItemCost, "colItemCost");
      this.colItemCost.FieldName = "ItemCost";
      this.colItemCost.Name = "colItemCost";
      this.colItemCost.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colOverHeadCost, "colOverHeadCost");
      this.colOverHeadCost.FieldName = "OverHeadCost";
      this.colOverHeadCost.Name = "colOverHeadCost";
      this.colOverHeadCost.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colSubTotalCost, "colSubTotalCost");
      this.colSubTotalCost.FieldName = "SubTotalCost";
      this.colSubTotalCost.Name = "colSubTotalCost";
      this.colSubTotalCost.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark, "colRemark");
      this.colRemark.FieldName = "Remark";
      this.colRemark.Name = "colRemark";
      this.colRemark.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colPrintOut, "colPrintOut");
      this.colPrintOut.FieldName = "PrintOut";
      this.colPrintOut.Name = "colPrintOut";
      this.colPrintOut.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemainingQty, "colRemainingQty");
      this.colRemainingQty.FieldName = "RemainingQty";
      this.colRemainingQty.Name = "colRemainingQty";
      this.colRemainingQty.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.gridControl1, "gridControl1");
      this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
      this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
      this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
      this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
      this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
      gridLevelNode1.LevelTemplate = (BaseView) this.gridView2;
      gridLevelNode1.RelationName = "Level1";
      gridLevelNode2.LevelTemplate = (BaseView) this.gridView3;
      gridLevelNode2.RelationName = "Level2";
      GridLevelNodeCollection nodes1 = this.gridControl1.LevelTree.Nodes;
      GridLevelNode[] nodes2 = new GridLevelNode[2];
      int index17 = 0;
      GridLevelNode gridLevelNode3 = gridLevelNode1;
      nodes2[index17] = gridLevelNode3;
      int index18 = 1;
      GridLevelNode gridLevelNode4 = gridLevelNode2;
      nodes2[index18] = gridLevelNode4;
      nodes1.AddRange(nodes2);
      this.gridControl1.MainView = (BaseView) this.gridView1;
      this.gridControl1.Name = "gridControl1";
      RepositoryItemCollection repositoryItems = this.gridControl1.RepositoryItems;
      RepositoryItem[] items1 = new RepositoryItem[2];
      int index19 = 0;
      RepositoryItemCheckEdit repositoryItemCheckEdit = this.repositoryItemCheckEdit1;
      items1[index19] = (RepositoryItem) repositoryItemCheckEdit;
      int index20 = 1;
      RepositoryItemTextEdit repositoryItemTextEdit = this.repositoryItemTextEdit1;
      items1[index20] = (RepositoryItem) repositoryItemTextEdit;
      repositoryItems.AddRange(items1);
      this.gridControl1.UseEmbeddedNavigator = true;
      ViewRepositoryCollection viewCollection = this.gridControl1.ViewCollection;
      BaseView[] views = new BaseView[3];
      int index21 = 0;
      GridView gridView1 = this.gridView3;
      views[index21] = (BaseView) gridView1;
      int index22 = 1;
      GridView gridView2 = this.gridView1;
      views[index22] = (BaseView) gridView2;
      int index23 = 2;
      GridView gridView3 = this.gridView2;
      views[index23] = (BaseView) gridView3;
      viewCollection.AddRange(views);
      GridColumnCollection columns3 = this.gridView3.Columns;
      GridColumn[] columns4 = new GridColumn[6];
      int index24 = 0;
      GridColumn gridColumn17 = this.colDocType;
      columns4[index24] = gridColumn17;
      int index25 = 1;
      GridColumn gridColumn18 = this.colDocNo1;
      columns4[index25] = gridColumn18;
      int index26 = 2;
      GridColumn gridColumn19 = this.colDocDate1;
      columns4[index26] = gridColumn19;
      int index27 = 3;
      GridColumn gridColumn20 = this.colQty1;
      columns4[index27] = gridColumn20;
      int index28 = 4;
      GridColumn gridColumn21 = this.colQty2;
      columns4[index28] = gridColumn21;
      int index29 = 5;
      GridColumn gridColumn22 = this.colRate1;
      columns4[index29] = gridColumn22;
      columns3.AddRange(columns4);
      this.gridView3.GridControl = this.gridControl1;
      this.gridView3.Name = "gridView3";
      this.gridView3.OptionsView.ShowGroupPanel = false;
      componentResourceManager.ApplyResources((object) this.colDocType, "colDocType");
      this.colDocType.FieldName = "DocType";
      this.colDocType.Name = "colDocType";
      this.colDocType.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDocNo1, "colDocNo1");
      this.colDocNo1.FieldName = "DocNo";
      this.colDocNo1.Name = "colDocNo1";
      this.colDocNo1.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDocDate1, "colDocDate1");
      this.colDocDate1.FieldName = "DocDate";
      this.colDocDate1.Name = "colDocDate1";
      this.colDocDate1.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colQty1, "colQty1");
      this.colQty1.FieldName = "Qty";
      this.colQty1.Name = "colQty1";
      this.colQty1.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colQty2, "colQty2");
      this.colQty2.FieldName = "Qty2";
      this.colQty2.Name = "colQty2";
      this.colQty2.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRate1, "colRate1");
      this.colRate1.FieldName = "Rate";
      this.colRate1.Name = "colRate1";
      this.colRate1.OptionsColumn.AllowEdit = false;
      GridColumnCollection columns5 = this.gridView1.Columns;
      GridColumn[] columns6 = new GridColumn[24];
      int index30 = 0;
      GridColumn gridColumn23 = this.colToBeUpdate;
      columns6[index30] = gridColumn23;
      int index31 = 1;
      GridColumn gridColumn24 = this.colDocNo;
      columns6[index31] = gridColumn24;
      int index32 = 2;
      GridColumn gridColumn25 = this.colDocDate;
      columns6[index32] = gridColumn25;
      int index33 = 3;
      GridColumn gridColumn26 = this.colDescription;
      columns6[index33] = gridColumn26;
      int index34 = 4;
      GridColumn gridColumn27 = this.colItemCode;
      columns6[index34] = gridColumn27;
      int index35 = 5;
      GridColumn gridColumn28 = this.colLocation;
      columns6[index35] = gridColumn28;
      int index36 = 6;
      GridColumn gridColumn29 = this.colBatchNo;
      columns6[index36] = gridColumn29;
      int index37 = 7;
      GridColumn gridColumn30 = this.colProjNo;
      columns6[index37] = gridColumn30;
      int index38 = 8;
      GridColumn gridColumn31 = this.colDeptNo;
      columns6[index38] = gridColumn31;
      int index39 = 9;
      GridColumn gridColumn32 = this.colTotal;
      columns6[index39] = gridColumn32;
      int index40 = 10;
      GridColumn gridColumn33 = this.colAssemblyCost;
      columns6[index40] = gridColumn33;
      int index41 = 11;
      GridColumn gridColumn34 = this.colNetTotal;
      columns6[index41] = gridColumn34;
      int index42 = 12;
      GridColumn gridColumn35 = this.colNote;
      columns6[index42] = gridColumn35;
      int index43 = 13;
      GridColumn gridColumn36 = this.colRemark1;
      columns6[index43] = gridColumn36;
      int index44 = 14;
      GridColumn gridColumn37 = this.colRemark2;
      columns6[index44] = gridColumn37;
      int index45 = 15;
      GridColumn gridColumn38 = this.colRemark3;
      columns6[index45] = gridColumn38;
      int index46 = 16;
      GridColumn gridColumn39 = this.colRemark4;
      columns6[index46] = gridColumn39;
      int index47 = 17;
      GridColumn gridColumn40 = this.colLastModified;
      columns6[index47] = gridColumn40;
      int index48 = 18;
      GridColumn gridColumn41 = this.colLastModifiedUserID;
      columns6[index48] = gridColumn41;
      int index49 = 19;
      GridColumn gridColumn42 = this.colCreatedTimeStamp;
      columns6[index49] = gridColumn42;
      int index50 = 20;
      GridColumn gridColumn43 = this.colCreatedUserID;
      columns6[index50] = gridColumn43;
      int index51 = 21;
      GridColumn gridColumn44 = this.colFGQty;
      columns6[index51] = gridColumn44;
      int index52 = 22;
      GridColumn gridColumn45 = this.colOutStandingFGQty;
      columns6[index52] = gridColumn45;
      int index53 = 23;
      GridColumn gridColumn46 = this.colExpCompletedDate;
      columns6[index53] = gridColumn46;
      columns5.AddRange(columns6);
      this.gridView1.GridControl = this.gridControl1;
      this.gridView1.Name = "gridView1";
      this.gridView1.OptionsBehavior.AllowIncrementalSearch = true;
      this.gridView1.OptionsSelection.MultiSelect = true;
      this.gridView1.OptionsView.ShowFooter = true;
      this.gridView1.SelectionChanged += new SelectionChangedEventHandler(this.gridView1_SelectionChanged);
      this.gridView1.DoubleClick += new EventHandler(this.gridView1_DoubleClick);
      this.colToBeUpdate.ColumnEdit = (RepositoryItem) this.repositoryItemCheckEdit1;
      this.colToBeUpdate.FieldName = "ToBeUpdate";
      this.colToBeUpdate.Name = "colToBeUpdate";
      componentResourceManager.ApplyResources((object) this.colToBeUpdate, "colToBeUpdate");
      componentResourceManager.ApplyResources((object) this.repositoryItemCheckEdit1, "repositoryItemCheckEdit1");
      this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
      componentResourceManager.ApplyResources((object) this.colDocNo, "colDocNo");
      this.colDocNo.FieldName = "DocNo";
      this.colDocNo.Name = "colDocNo";
      this.colDocNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDocDate, "colDocDate");
      this.colDocDate.FieldName = "DocDate";
      this.colDocDate.Name = "colDocDate";
      this.colDocDate.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDescription, "colDescription");
      this.colDescription.FieldName = "Description";
      this.colDescription.Name = "colDescription";
      this.colDescription.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colItemCode, "colItemCode");
      this.colItemCode.FieldName = "ItemCode";
      this.colItemCode.Name = "colItemCode";
      this.colItemCode.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colLocation, "colLocation");
      this.colLocation.FieldName = "Location";
      this.colLocation.Name = "colLocation";
      this.colLocation.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colBatchNo, "colBatchNo");
      this.colBatchNo.FieldName = "BatchNo";
      this.colBatchNo.Name = "colBatchNo";
      this.colBatchNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colProjNo, "colProjNo");
      this.colProjNo.FieldName = "ProjNo";
      this.colProjNo.Name = "colProjNo";
      this.colProjNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDeptNo, "colDeptNo");
      this.colDeptNo.FieldName = "DeptNo";
      this.colDeptNo.Name = "colDeptNo";
      this.colDeptNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colTotal, "colTotal");
      this.colTotal.FieldName = "Total";
      this.colTotal.Name = "colTotal";
      this.colTotal.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colAssemblyCost, "colAssemblyCost");
      this.colAssemblyCost.FieldName = "AssemblyCost";
      this.colAssemblyCost.Name = "colAssemblyCost";
      this.colAssemblyCost.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colNetTotal, "colNetTotal");
      this.colNetTotal.FieldName = "NetTotal";
      this.colNetTotal.Name = "colNetTotal";
      this.colNetTotal.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colNote, "colNote");
      this.colNote.FieldName = "Note";
      this.colNote.Name = "colNote";
      this.colNote.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark1, "colRemark1");
      this.colRemark1.FieldName = "Remark1";
      this.colRemark1.Name = "colRemark1";
      this.colRemark1.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark2, "colRemark2");
      this.colRemark2.FieldName = "Remark2";
      this.colRemark2.Name = "colRemark2";
      this.colRemark2.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark3, "colRemark3");
      this.colRemark3.FieldName = "Remark3";
      this.colRemark3.Name = "colRemark3";
      this.colRemark3.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colRemark4, "colRemark4");
      this.colRemark4.FieldName = "Remark4";
      this.colRemark4.Name = "colRemark4";
      this.colRemark4.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colLastModified, "colLastModified");
      this.colLastModified.FieldName = "LastModified";
      this.colLastModified.Name = "colLastModified";
      this.colLastModified.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colLastModifiedUserID, "colLastModifiedUserID");
      this.colLastModifiedUserID.FieldName = "LastModifiedUserID";
      this.colLastModifiedUserID.Name = "colLastModifiedUserID";
      this.colLastModifiedUserID.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colCreatedTimeStamp, "colCreatedTimeStamp");
      this.colCreatedTimeStamp.FieldName = "CreatedTimeStamp";
      this.colCreatedTimeStamp.Name = "colCreatedTimeStamp";
      this.colCreatedTimeStamp.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colCreatedUserID, "colCreatedUserID");
      this.colCreatedUserID.FieldName = "CreatedUserID";
      this.colCreatedUserID.Name = "colCreatedUserID";
      this.colCreatedUserID.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colFGQty, "colFGQty");
      this.colFGQty.FieldName = "Qty";
      this.colFGQty.Name = "colFGQty";
      this.colFGQty.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colOutStandingFGQty, "colOutStandingFGQty");
      this.colOutStandingFGQty.FieldName = "OutStandingFGQty";
      this.colOutStandingFGQty.Name = "colOutStandingFGQty";
      this.colOutStandingFGQty.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colExpCompletedDate, "colExpCompletedDate");
      this.colExpCompletedDate.FieldName = "ExpectedCompletedDate";
      this.colExpCompletedDate.Name = "colExpCompletedDate";
      this.colExpCompletedDate.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.repositoryItemTextEdit1, "repositoryItemTextEdit1");
      this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
      this.panelAdv.BorderStyle = BorderStyles.NoBorder;
      this.panelAdv.Controls.Add((Control) this.gbPrint);
      this.panelAdv.Controls.Add((Control) this.gbReport);
      this.panelAdv.Controls.Add((Control) this.gbFilter);
      componentResourceManager.ApplyResources((object) this.panelAdv, "panelAdv");
      this.panelAdv.Name = "panelAdv";
      this.gbPrint.Controls.Add((Control) this.radioGroup1);
      componentResourceManager.ApplyResources((object) this.gbPrint, "gbPrint");
      this.gbPrint.Name = "gbPrint";
      componentResourceManager.ApplyResources((object) this.radioGroup1, "radioGroup1");
      this.radioGroup1.Name = "radioGroup1";
      RadioGroupItemCollection items2 = this.radioGroup1.Properties.Items;
      RadioGroupItem[] items3 = new RadioGroupItem[2];
      int index54 = 0;
      RadioGroupItem radioGroupItem1 = new RadioGroupItem(componentResourceManager.GetObject("radioGroup1.Properties.Items"), componentResourceManager.GetString("radioGroup1.Properties.Items1"));
      items3[index54] = radioGroupItem1;
      int index55 = 1;
      RadioGroupItem radioGroupItem2 = new RadioGroupItem(componentResourceManager.GetObject("radioGroup1.Properties.Items2"), componentResourceManager.GetString("radioGroup1.Properties.Items3"));
      items3[index55] = radioGroupItem2;
      items2.AddRange(items3);
      this.radioGroup1.SelectedIndexChanged += new EventHandler(this.radioGroup1_SelectedIndexChanged);
      this.gbReport.Controls.Add((Control) this.chkEditShowCriteria);
      this.gbReport.Controls.Add((Control) this.cbSortBy);
      this.gbReport.Controls.Add((Control) this.cbGroupBy);
      this.gbReport.Controls.Add((Control) this.label17);
      this.gbReport.Controls.Add((Control) this.label18);
      componentResourceManager.ApplyResources((object) this.gbReport, "gbReport");
      this.gbReport.Name = "gbReport";
      componentResourceManager.ApplyResources((object) this.chkEditShowCriteria, "chkEditShowCriteria");
      this.chkEditShowCriteria.Name = "chkEditShowCriteria";
      this.chkEditShowCriteria.Properties.Caption = componentResourceManager.GetString("chkEditShowCriteria.Properties.Caption");
      componentResourceManager.ApplyResources((object) this.cbSortBy, "cbSortBy");
      this.cbSortBy.Name = "cbSortBy";
      EditorButtonCollection buttons1 = this.cbSortBy.Properties.Buttons;
      EditorButton[] buttons2 = new EditorButton[1];
      int index56 = 0;
      EditorButton editorButton1 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("cbSortBy.Properties.Buttons"));
      buttons2[index56] = editorButton1;
      buttons1.AddRange(buttons2);
      ComboBoxItemCollection items4 = this.cbSortBy.Properties.Items;
      object[] items5 = new object[2];
      int index57 = 0;
      string string1 = componentResourceManager.GetString("cbSortBy.Properties.Items");
      items5[index57] = (object) string1;
      int index58 = 1;
      string string2 = componentResourceManager.GetString("cbSortBy.Properties.Items1");
      items5[index58] = (object) string2;
      items4.AddRange(items5);
      this.cbSortBy.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
      this.cbSortBy.SelectedIndexChanged += new EventHandler(this.cbSortBy_SelectedIndexChanged);
      componentResourceManager.ApplyResources((object) this.cbGroupBy, "cbGroupBy");
      this.cbGroupBy.Name = "cbGroupBy";
      EditorButtonCollection buttons3 = this.cbGroupBy.Properties.Buttons;
      EditorButton[] buttons4 = new EditorButton[1];
      int index59 = 0;
      EditorButton editorButton2 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("cbGroupBy.Properties.Buttons"));
      buttons4[index59] = editorButton2;
      buttons3.AddRange(buttons4);
      this.cbGroupBy.Properties.DropDownRows = 10;
      ComboBoxItemCollection items6 = this.cbGroupBy.Properties.Items;
      object[] items7 = new object[10];
      int index60 = 0;
      string string3 = componentResourceManager.GetString("cbGroupBy.Properties.Items");
      items7[index60] = (object) string3;
      int index61 = 1;
      string string4 = componentResourceManager.GetString("cbGroupBy.Properties.Items1");
      items7[index61] = (object) string4;
      int index62 = 2;
      string string5 = componentResourceManager.GetString("cbGroupBy.Properties.Items2");
      items7[index62] = (object) string5;
      int index63 = 3;
      string string6 = componentResourceManager.GetString("cbGroupBy.Properties.Items3");
      items7[index63] = (object) string6;
      int index64 = 4;
      string string7 = componentResourceManager.GetString("cbGroupBy.Properties.Items4");
      items7[index64] = (object) string7;
      int index65 = 5;
      string string8 = componentResourceManager.GetString("cbGroupBy.Properties.Items5");
      items7[index65] = (object) string8;
      int index66 = 6;
      string string9 = componentResourceManager.GetString("cbGroupBy.Properties.Items6");
      items7[index66] = (object) string9;
      int index67 = 7;
      string string10 = componentResourceManager.GetString("cbGroupBy.Properties.Items7");
      items7[index67] = (object) string10;
      int index68 = 8;
      string string11 = componentResourceManager.GetString("cbGroupBy.Properties.Items8");
      items7[index68] = (object) string11;
      int index69 = 9;
      string string12 = componentResourceManager.GetString("cbGroupBy.Properties.Items9");
      items7[index69] = (object) string12;
      items6.AddRange(items7);
      this.cbGroupBy.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
      this.cbGroupBy.SelectedIndexChanged += new EventHandler(this.cbGroupBy_SelectedIndexChanged);
      componentResourceManager.ApplyResources((object) this.label17, "label17");
      this.label17.Name = "label17";
      componentResourceManager.ApplyResources((object) this.label18, "label18");
      this.label18.Name = "label18";
      this.gbFilter.Controls.Add((Control) this.xtraTabControl1);
      componentResourceManager.ApplyResources((object) this.gbFilter, "gbFilter");
      this.gbFilter.Name = "gbFilter";
      componentResourceManager.ApplyResources((object) this.xtraTabControl1, "xtraTabControl1");
      this.xtraTabControl1.Name = "xtraTabControl1";
      this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
      XtraTabPageCollection tabPages1 = this.xtraTabControl1.TabPages;
      XtraTabPage[] pages1 = new XtraTabPage[2];
      int index70 = 0;
      XtraTabPage xtraTabPage1 = this.xtraTabPage1;
      pages1[index70] = xtraTabPage1;
      int index71 = 1;
      XtraTabPage xtraTabPage2 = this.xtraTabPage2;
      pages1[index71] = xtraTabPage2;
      tabPages1.AddRange(pages1);
      this.xtraTabPage1.Controls.Add((Control) this.ucbomItemSelector1);
      this.xtraTabPage1.Controls.Add((Control) this.dateEditFromAdv);
      this.xtraTabPage1.Controls.Add((Control) this.dateEditToAdv);
      this.xtraTabPage1.Controls.Add((Control) this.ucDepartmentSelector1);
      this.xtraTabPage1.Controls.Add((Control) this.ucLocationSelector1);
      this.xtraTabPage1.Controls.Add((Control) this.ucItemGroupSelector1);
      this.xtraTabPage1.Controls.Add((Control) this.ucItemTypeSelector1);
      this.xtraTabPage1.Controls.Add((Control) this.ucStockAssemblyOrderSelector1);
      this.xtraTabPage1.Controls.Add((Control) this.label9);
      this.xtraTabPage1.Controls.Add((Control) this.label8);
      this.xtraTabPage1.Controls.Add((Control) this.label7);
      this.xtraTabPage1.Controls.Add((Control) this.label6);
      this.xtraTabPage1.Controls.Add((Control) this.label5);
      this.xtraTabPage1.Controls.Add((Control) this.label3);
      this.xtraTabPage1.Controls.Add((Control) this.label2);
      this.xtraTabPage1.Controls.Add((Control) this.label1);
      this.xtraTabPage1.Controls.Add((Control) this.ucProjectSelector1);
      this.xtraTabPage1.Controls.Add((Control) this.label16);
      this.xtraTabPage1.Name = "xtraTabPage1";
      componentResourceManager.ApplyResources((object) this.xtraTabPage1, "xtraTabPage1");
      componentResourceManager.ApplyResources((object) this.ucbomItemSelector1, "ucbomItemSelector1");
      this.ucbomItemSelector1.Name = "ucbomItemSelector1";
      componentResourceManager.ApplyResources((object) this.dateEditFromAdv, "dateEditFromAdv");
      this.dateEditFromAdv.Name = "dateEditFromAdv";
      EditorButtonCollection buttons5 = this.dateEditFromAdv.Properties.Buttons;
      EditorButton[] buttons6 = new EditorButton[1];
      int index72 = 0;
      EditorButton editorButton3 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("dateEditFromAdv.Properties.Buttons"));
      buttons6[index72] = editorButton3;
      buttons5.AddRange(buttons6);
      EditorButtonCollection buttons7 = this.dateEditFromAdv.Properties.CalendarTimeProperties.Buttons;
      EditorButton[] buttons8 = new EditorButton[1];
      int index73 = 0;
      EditorButton editorButton4 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("dateEditFromAdv.Properties.CalendarTimeProperties.Buttons"));
      buttons8[index73] = editorButton4;
      buttons7.AddRange(buttons8);
      this.dateEditFromAdv.EditValueChanged += new EventHandler(this.dateEditFromAdv_EditValueChanged);
      componentResourceManager.ApplyResources((object) this.dateEditToAdv, "dateEditToAdv");
      this.dateEditToAdv.Name = "dateEditToAdv";
      EditorButtonCollection buttons9 = this.dateEditToAdv.Properties.Buttons;
      EditorButton[] buttons10 = new EditorButton[1];
      int index74 = 0;
      EditorButton editorButton5 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("dateEditToAdv.Properties.Buttons"));
      buttons10[index74] = editorButton5;
      buttons9.AddRange(buttons10);
      EditorButtonCollection buttons11 = this.dateEditToAdv.Properties.CalendarTimeProperties.Buttons;
      EditorButton[] buttons12 = new EditorButton[1];
      int index75 = 0;
      EditorButton editorButton6 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("dateEditToAdv.Properties.CalendarTimeProperties.Buttons"));
      buttons12[index75] = editorButton6;
      buttons11.AddRange(buttons12);
      this.dateEditToAdv.EditValueChanged += new EventHandler(this.dateEditToAdv_EditValueChanged);
      this.ucDepartmentSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucDepartmentSelector1.Appearance.BackColor");
      this.ucDepartmentSelector1.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucDepartmentSelector1, "ucDepartmentSelector1");
      this.ucDepartmentSelector1.Name = "ucDepartmentSelector1";
      this.ucLocationSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucLocationSelector1.Appearance.BackColor");
      this.ucLocationSelector1.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucLocationSelector1, "ucLocationSelector1");
      this.ucLocationSelector1.LocationType = UCLocationType.ItemLocation;
      this.ucLocationSelector1.Name = "ucLocationSelector1";
      this.ucItemGroupSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucItemGroupSelector1.Appearance.BackColor");
      this.ucItemGroupSelector1.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucItemGroupSelector1, "ucItemGroupSelector1");
      this.ucItemGroupSelector1.Name = "ucItemGroupSelector1";
      this.ucItemTypeSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucItemTypeSelector1.Appearance.BackColor");
      this.ucItemTypeSelector1.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucItemTypeSelector1, "ucItemTypeSelector1");
      this.ucItemTypeSelector1.Name = "ucItemTypeSelector1";
      this.ucStockAssemblyOrderSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucStockAssemblyOrderSelector1.Appearance.BackColor");
      this.ucStockAssemblyOrderSelector1.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucStockAssemblyOrderSelector1, "ucStockAssemblyOrderSelector1");
      this.ucStockAssemblyOrderSelector1.Name = "ucStockAssemblyOrderSelector1";
      componentResourceManager.ApplyResources((object) this.label9, "label9");
      this.label9.Name = "label9";
      componentResourceManager.ApplyResources((object) this.label8, "label8");
      this.label8.Name = "label8";
      componentResourceManager.ApplyResources((object) this.label7, "label7");
      this.label7.Name = "label7";
      componentResourceManager.ApplyResources((object) this.label6, "label6");
      this.label6.Name = "label6";
      componentResourceManager.ApplyResources((object) this.label5, "label5");
      this.label5.Name = "label5";
      componentResourceManager.ApplyResources((object) this.label3, "label3");
      this.label3.Name = "label3";
      componentResourceManager.ApplyResources((object) this.label2, "label2");
      this.label2.Name = "label2";
      componentResourceManager.ApplyResources((object) this.label1, "label1");
      this.label1.Name = "label1";
      this.ucProjectSelector1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucProjectSelector1.Appearance.BackColor");
      this.ucProjectSelector1.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucProjectSelector1, "ucProjectSelector1");
      this.ucProjectSelector1.Name = "ucProjectSelector1";
      componentResourceManager.ApplyResources((object) this.label16, "label16");
      this.label16.Name = "label16";
      this.xtraTabPage2.Controls.Add((Control) this.ucItemSelector1);
      this.xtraTabPage2.Controls.Add((Control) this.ucDepartmentSelector2);
      this.xtraTabPage2.Controls.Add((Control) this.ucProjectSelector2);
      this.xtraTabPage2.Controls.Add((Control) this.ucLocationSelector2);
      this.xtraTabPage2.Controls.Add((Control) this.ucItemTypeSelector2);
      this.xtraTabPage2.Controls.Add((Control) this.ucItemGroupSelector2);
      this.xtraTabPage2.Controls.Add((Control) this.label10);
      this.xtraTabPage2.Controls.Add((Control) this.label11);
      this.xtraTabPage2.Controls.Add((Control) this.label12);
      this.xtraTabPage2.Controls.Add((Control) this.label13);
      this.xtraTabPage2.Controls.Add((Control) this.label14);
      this.xtraTabPage2.Controls.Add((Control) this.label15);
      this.xtraTabPage2.Name = "xtraTabPage2";
      componentResourceManager.ApplyResources((object) this.xtraTabPage2, "xtraTabPage2");
      componentResourceManager.ApplyResources((object) this.ucItemSelector1, "ucItemSelector1");
      this.ucItemSelector1.Name = "ucItemSelector1";
      this.ucDepartmentSelector2.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucDepartmentSelector2.Appearance.BackColor");
      this.ucDepartmentSelector2.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucDepartmentSelector2, "ucDepartmentSelector2");
      this.ucDepartmentSelector2.Name = "ucDepartmentSelector2";
      this.ucProjectSelector2.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucProjectSelector2.Appearance.BackColor");
      this.ucProjectSelector2.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucProjectSelector2, "ucProjectSelector2");
      this.ucProjectSelector2.Name = "ucProjectSelector2";
      this.ucLocationSelector2.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucLocationSelector2.Appearance.BackColor");
      this.ucLocationSelector2.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucLocationSelector2, "ucLocationSelector2");
      this.ucLocationSelector2.LocationType = UCLocationType.ItemLocation;
      this.ucLocationSelector2.Name = "ucLocationSelector2";
      this.ucItemTypeSelector2.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucItemTypeSelector2.Appearance.BackColor");
      this.ucItemTypeSelector2.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucItemTypeSelector2, "ucItemTypeSelector2");
      this.ucItemTypeSelector2.Name = "ucItemTypeSelector2";
      this.ucItemGroupSelector2.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucItemGroupSelector2.Appearance.BackColor");
      this.ucItemGroupSelector2.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucItemGroupSelector2, "ucItemGroupSelector2");
      this.ucItemGroupSelector2.Name = "ucItemGroupSelector2";
      componentResourceManager.ApplyResources((object) this.label10, "label10");
      this.label10.Name = "label10";
      componentResourceManager.ApplyResources((object) this.label11, "label11");
      this.label11.Name = "label11";
      componentResourceManager.ApplyResources((object) this.label12, "label12");
      this.label12.Name = "label12";
      componentResourceManager.ApplyResources((object) this.label13, "label13");
      this.label13.Name = "label13";
      componentResourceManager.ApplyResources((object) this.label14, "label14");
      this.label14.Name = "label14";
      componentResourceManager.ApplyResources((object) this.label15, "label15");
      this.label15.Name = "label15";
      this.panelBasic.BorderStyle = BorderStyles.NoBorder;
      this.panelBasic.Controls.Add((Control) this.groupControl3);
      this.panelBasic.Controls.Add((Control) this.groupControl2);
      this.panelBasic.Controls.Add((Control) this.groupControl1);
      componentResourceManager.ApplyResources((object) this.panelBasic, "panelBasic");
      this.panelBasic.Name = "panelBasic";
      this.groupControl3.Controls.Add((Control) this.radioGroup2);
      componentResourceManager.ApplyResources((object) this.groupControl3, "groupControl3");
      this.groupControl3.Name = "groupControl3";
      componentResourceManager.ApplyResources((object) this.radioGroup2, "radioGroup2");
      this.radioGroup2.Name = "radioGroup2";
      RadioGroupItemCollection items8 = this.radioGroup2.Properties.Items;
      RadioGroupItem[] items9 = new RadioGroupItem[2];
      int index76 = 0;
      RadioGroupItem radioGroupItem3 = new RadioGroupItem(componentResourceManager.GetObject("radioGroup2.Properties.Items"), componentResourceManager.GetString("radioGroup2.Properties.Items1"));
      items9[index76] = radioGroupItem3;
      int index77 = 1;
      RadioGroupItem radioGroupItem4 = new RadioGroupItem(componentResourceManager.GetObject("radioGroup2.Properties.Items2"), componentResourceManager.GetString("radioGroup2.Properties.Items3"));
      items9[index77] = radioGroupItem4;
      items8.AddRange(items9);
      this.radioGroup2.SelectedIndexChanged += new EventHandler(this.radioGroup2_SelectedIndexChanged);
      this.groupControl2.Controls.Add((Control) this.chkEditShowCriteriaBasic);
      this.groupControl2.Controls.Add((Control) this.cbSortByBasic);
      this.groupControl2.Controls.Add((Control) this.cbGroupByBasic);
      this.groupControl2.Controls.Add((Control) this.label23);
      this.groupControl2.Controls.Add((Control) this.label24);
      componentResourceManager.ApplyResources((object) this.groupControl2, "groupControl2");
      this.groupControl2.Name = "groupControl2";
      componentResourceManager.ApplyResources((object) this.chkEditShowCriteriaBasic, "chkEditShowCriteriaBasic");
      this.chkEditShowCriteriaBasic.Name = "chkEditShowCriteriaBasic";
      this.chkEditShowCriteriaBasic.Properties.Caption = componentResourceManager.GetString("chkEditShowCriteriaBasic.Properties.Caption");
      componentResourceManager.ApplyResources((object) this.cbSortByBasic, "cbSortByBasic");
      this.cbSortByBasic.Name = "cbSortByBasic";
      EditorButtonCollection buttons13 = this.cbSortByBasic.Properties.Buttons;
      EditorButton[] buttons14 = new EditorButton[1];
      int index78 = 0;
      EditorButton editorButton7 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("cbSortByBasic.Properties.Buttons"));
      buttons14[index78] = editorButton7;
      buttons13.AddRange(buttons14);
      ComboBoxItemCollection items10 = this.cbSortByBasic.Properties.Items;
      object[] items11 = new object[2];
      int index79 = 0;
      string string13 = componentResourceManager.GetString("cbSortByBasic.Properties.Items");
      items11[index79] = (object) string13;
      int index80 = 1;
      string string14 = componentResourceManager.GetString("cbSortByBasic.Properties.Items1");
      items11[index80] = (object) string14;
      items10.AddRange(items11);
      this.cbSortByBasic.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
      this.cbSortByBasic.SelectedIndexChanged += new EventHandler(this.cbSortByBasic_SelectedIndexChanged);
      componentResourceManager.ApplyResources((object) this.cbGroupByBasic, "cbGroupByBasic");
      this.cbGroupByBasic.Name = "cbGroupByBasic";
      EditorButtonCollection buttons15 = this.cbGroupByBasic.Properties.Buttons;
      EditorButton[] buttons16 = new EditorButton[1];
      int index81 = 0;
      EditorButton editorButton8 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("cbGroupByBasic.Properties.Buttons"));
      buttons16[index81] = editorButton8;
      buttons15.AddRange(buttons16);
      this.cbGroupByBasic.Properties.DropDownRows = 10;
      ComboBoxItemCollection items12 = this.cbGroupByBasic.Properties.Items;
      object[] items13 = new object[10];
      int index82 = 0;
      string string15 = componentResourceManager.GetString("cbGroupByBasic.Properties.Items");
      items13[index82] = (object) string15;
      int index83 = 1;
      string string16 = componentResourceManager.GetString("cbGroupByBasic.Properties.Items1");
      items13[index83] = (object) string16;
      int index84 = 2;
      string string17 = componentResourceManager.GetString("cbGroupByBasic.Properties.Items2");
      items13[index84] = (object) string17;
      int index85 = 3;
      string string18 = componentResourceManager.GetString("cbGroupByBasic.Properties.Items3");
      items13[index85] = (object) string18;
      int index86 = 4;
      string string19 = componentResourceManager.GetString("cbGroupByBasic.Properties.Items4");
      items13[index86] = (object) string19;
      int index87 = 5;
      string string20 = componentResourceManager.GetString("cbGroupByBasic.Properties.Items5");
      items13[index87] = (object) string20;
      int index88 = 6;
      string string21 = componentResourceManager.GetString("cbGroupByBasic.Properties.Items6");
      items13[index88] = (object) string21;
      int index89 = 7;
      string string22 = componentResourceManager.GetString("cbGroupByBasic.Properties.Items7");
      items13[index89] = (object) string22;
      int index90 = 8;
      string string23 = componentResourceManager.GetString("cbGroupByBasic.Properties.Items8");
      items13[index90] = (object) string23;
      int index91 = 9;
      string string24 = componentResourceManager.GetString("cbGroupByBasic.Properties.Items9");
      items13[index91] = (object) string24;
      items12.AddRange(items13);
      this.cbGroupByBasic.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
      this.cbGroupByBasic.SelectedIndexChanged += new EventHandler(this.cbGroupByBasic_SelectedIndexChanged);
      componentResourceManager.ApplyResources((object) this.label23, "label23");
      this.label23.Name = "label23";
      componentResourceManager.ApplyResources((object) this.label24, "label24");
      this.label24.Name = "label24";
      this.groupControl1.Controls.Add((Control) this.ucbomItemSelector2);
      this.groupControl1.Controls.Add((Control) this.sbtnAdvOptions);
      this.groupControl1.Controls.Add((Control) this.ucStockAssemblyOrderSelector2);
      this.groupControl1.Controls.Add((Control) this.dateEditToBasic);
      this.groupControl1.Controls.Add((Control) this.dateEditFromBasic);
      this.groupControl1.Controls.Add((Control) this.label22);
      this.groupControl1.Controls.Add((Control) this.label19);
      this.groupControl1.Controls.Add((Control) this.label20);
      this.groupControl1.Controls.Add((Control) this.label21);
      componentResourceManager.ApplyResources((object) this.groupControl1, "groupControl1");
      this.groupControl1.Name = "groupControl1";
      componentResourceManager.ApplyResources((object) this.ucbomItemSelector2, "ucbomItemSelector2");
      this.ucbomItemSelector2.Name = "ucbomItemSelector2";
      componentResourceManager.ApplyResources((object) this.sbtnAdvOptions, "sbtnAdvOptions");
      this.sbtnAdvOptions.Name = "sbtnAdvOptions";
      this.sbtnAdvOptions.Click += new EventHandler(this.sbtnAdvOptions_Click);
      componentResourceManager.ApplyResources((object) this.ucStockAssemblyOrderSelector2, "ucStockAssemblyOrderSelector2");
      this.ucStockAssemblyOrderSelector2.Name = "ucStockAssemblyOrderSelector2";
      componentResourceManager.ApplyResources((object) this.dateEditToBasic, "dateEditToBasic");
      this.dateEditToBasic.Name = "dateEditToBasic";
      EditorButtonCollection buttons17 = this.dateEditToBasic.Properties.Buttons;
      EditorButton[] buttons18 = new EditorButton[1];
      int index92 = 0;
      EditorButton editorButton9 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("dateEditToBasic.Properties.Buttons"));
      buttons18[index92] = editorButton9;
      buttons17.AddRange(buttons18);
      EditorButtonCollection buttons19 = this.dateEditToBasic.Properties.CalendarTimeProperties.Buttons;
      EditorButton[] buttons20 = new EditorButton[1];
      int index93 = 0;
      EditorButton editorButton10 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("dateEditToBasic.Properties.CalendarTimeProperties.Buttons"));
      buttons20[index93] = editorButton10;
      buttons19.AddRange(buttons20);
      this.dateEditToBasic.EditValueChanged += new EventHandler(this.dateEditToBasic_EditValueChanged);
      componentResourceManager.ApplyResources((object) this.dateEditFromBasic, "dateEditFromBasic");
      this.dateEditFromBasic.Name = "dateEditFromBasic";
      EditorButtonCollection buttons21 = this.dateEditFromBasic.Properties.Buttons;
      EditorButton[] buttons22 = new EditorButton[1];
      int index94 = 0;
      EditorButton editorButton11 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("dateEditFromBasic.Properties.Buttons"));
      buttons22[index94] = editorButton11;
      buttons21.AddRange(buttons22);
      EditorButtonCollection buttons23 = this.dateEditFromBasic.Properties.CalendarTimeProperties.Buttons;
      EditorButton[] buttons24 = new EditorButton[1];
      int index95 = 0;
      EditorButton editorButton12 = new EditorButton((ButtonPredefines) componentResourceManager.GetObject("dateEditFromBasic.Properties.CalendarTimeProperties.Buttons"));
      buttons24[index95] = editorButton12;
      buttons23.AddRange(buttons24);
      this.dateEditFromBasic.EditValueChanged += new EventHandler(this.dateEditFromBasic_EditValueChanged);
      componentResourceManager.ApplyResources((object) this.label22, "label22");
      this.label22.Name = "label22";
      componentResourceManager.ApplyResources((object) this.label19, "label19");
      this.label19.Name = "label19";
      componentResourceManager.ApplyResources((object) this.label20, "label20");
      this.label20.Name = "label20";
      componentResourceManager.ApplyResources((object) this.label21, "label21");
      this.label21.Name = "label21";
      this.panelCenter.BorderStyle = BorderStyles.NoBorder;
      this.panelCenter.Controls.Add((Control) this.sbtnClose);
      this.panelCenter.Controls.Add((Control) this.sbtnToggleOptions);
      this.panelCenter.Controls.Add((Control) this.printButton1);
      this.panelCenter.Controls.Add((Control) this.previewButton1);
      this.panelCenter.Controls.Add((Control) this.sbtnInquiry);
      componentResourceManager.ApplyResources((object) this.panelCenter, "panelCenter");
      this.panelCenter.Name = "panelCenter";
      this.sbtnClose.DialogResult = DialogResult.Cancel;
      componentResourceManager.ApplyResources((object) this.sbtnClose, "sbtnClose");
      this.sbtnClose.Name = "sbtnClose";
      this.sbtnClose.Click += new EventHandler(this.sbtnClose_Click);
      componentResourceManager.ApplyResources((object) this.sbtnToggleOptions, "sbtnToggleOptions");
      this.sbtnToggleOptions.Name = "sbtnToggleOptions";
      this.sbtnToggleOptions.Click += new EventHandler(this.sbtnToggleOptions_Click);
      componentResourceManager.ApplyResources((object) this.printButton1, "printButton1");
      this.printButton1.Name = "printButton1";
      this.printButton1.ReportType = "";
      this.printButton1.Print += new PrintEventHandler(this.printButton1_Print);
      componentResourceManager.ApplyResources((object) this.previewButton1, "previewButton1");
      this.previewButton1.Name = "previewButton1";
      this.previewButton1.ReportType = "";
      this.previewButton1.Preview += new PrintEventHandler(this.previewButton1_Preview);
      componentResourceManager.ApplyResources((object) this.sbtnInquiry, "sbtnInquiry");
      this.sbtnInquiry.Name = "sbtnInquiry";
      this.sbtnInquiry.Click += new EventHandler(this.sbtnInquiry_Click);
      this.panelControl4.BorderStyle = BorderStyles.NoBorder;
      this.panelControl4.Controls.Add((Control) this.xtraTabControl2);
      componentResourceManager.ApplyResources((object) this.panelControl4, "panelControl4");
      this.panelControl4.Name = "panelControl4";
      componentResourceManager.ApplyResources((object) this.xtraTabControl2, "xtraTabControl2");
      this.xtraTabControl2.Name = "xtraTabControl2";
      this.xtraTabControl2.SelectedTabPage = this.xtraTabPage3;
      XtraTabPageCollection tabPages2 = this.xtraTabControl2.TabPages;
      XtraTabPage[] pages2 = new XtraTabPage[2];
      int index96 = 0;
      XtraTabPage xtraTabPage3 = this.xtraTabPage3;
      pages2[index96] = xtraTabPage3;
      int index97 = 1;
      XtraTabPage xtraTabPage4 = this.xtraTabPage4;
      pages2[index97] = xtraTabPage4;
      tabPages2.AddRange(pages2);
      this.xtraTabPage3.Controls.Add((Control) this.gridControl1);
      this.xtraTabPage3.Controls.Add((Control) this.ucSearchResult1);
      this.xtraTabPage3.Name = "xtraTabPage3";
      componentResourceManager.ApplyResources((object) this.xtraTabPage3, "xtraTabPage3");
      this.ucSearchResult1.Appearance.BackColor = (Color) componentResourceManager.GetObject("ucSearchResult1.Appearance.BackColor");
      this.ucSearchResult1.Appearance.BackColor2 = (Color) componentResourceManager.GetObject("ucSearchResult1.Appearance.BackColor2");
      this.ucSearchResult1.Appearance.GradientMode = (LinearGradientMode) componentResourceManager.GetObject("ucSearchResult1.Appearance.GradientMode");
      this.ucSearchResult1.Appearance.Options.UseBackColor = true;
      componentResourceManager.ApplyResources((object) this.ucSearchResult1, "ucSearchResult1");
      this.ucSearchResult1.LookAndFeel.UseDefaultLookAndFeel = false;
      this.ucSearchResult1.Name = "ucSearchResult1";
      this.xtraTabPage4.Controls.Add((Control) this.memoEdit_Criteria);
      this.xtraTabPage4.Name = "xtraTabPage4";
      componentResourceManager.ApplyResources((object) this.xtraTabPage4, "xtraTabPage4");
      componentResourceManager.ApplyResources((object) this.memoEdit_Criteria, "memoEdit_Criteria");
      this.memoEdit_Criteria.Name = "memoEdit_Criteria";
      this.memoEdit_Criteria.Properties.Appearance.Font = (Font) componentResourceManager.GetObject("memoEdit_Criteria.Properties.Appearance.Font");
      this.memoEdit_Criteria.Properties.Appearance.Options.UseFont = true;
      this.memoEdit_Criteria.Properties.ReadOnly = true;
      this.memoEdit_Criteria.UseOptimizedRendering = true;
      Bars bars1 = this.barManager1.Bars;
      Bar[] bars2 = new Bar[1];
      int index98 = 0;
      Bar bar = this.bar2;
      bars2[index98] = bar;
      bars1.AddRange(bars2);
      this.barManager1.DockControls.Add(this.barDockControlTop);
      this.barManager1.DockControls.Add(this.barDockControlBottom);
      this.barManager1.DockControls.Add(this.barDockControlLeft);
      this.barManager1.DockControls.Add(this.barDockControlRight);
      this.barManager1.Form = (Control) this;
      BarItems items14 = this.barManager1.Items;
      BarItem[] items15 = new BarItem[3];
      int index99 = 0;
      BarSubItem barSubItem = this.barSubItem1;
      items15[index99] = (BarItem) barSubItem;
      int index100 = 1;
      BarButtonItem barButtonItem1 = this.barbtnAdvancedFilter;
      items15[index100] = (BarItem) barButtonItem1;
      int index101 = 2;
      BarButtonItem barButtonItem2 = this.barBtnDesignOutstandingListingReport;
      items15[index101] = (BarItem) barButtonItem2;
      items14.AddRange(items15);
      this.barManager1.MainMenu = this.bar2;
      this.barManager1.MaxItemId = 3;
      this.bar2.BarName = "Custom 3";
      this.bar2.DockCol = 0;
      this.bar2.DockRow = 0;
      this.bar2.DockStyle = BarDockStyle.Top;
      LinksInfo linksPersistInfo1 = this.bar2.LinksPersistInfo;
      LinkPersistInfo[] links1 = new LinkPersistInfo[1];
      int index102 = 0;
      LinkPersistInfo linkPersistInfo1 = new LinkPersistInfo((BarItem) this.barSubItem1, true);
      links1[index102] = linkPersistInfo1;
      linksPersistInfo1.AddRange(links1);
      this.bar2.OptionsBar.AllowQuickCustomization = false;
      this.bar2.OptionsBar.DrawDragBorder = false;
      this.bar2.OptionsBar.MultiLine = true;
      this.bar2.OptionsBar.UseWholeRow = true;
      componentResourceManager.ApplyResources((object) this.bar2, "bar2");
      componentResourceManager.ApplyResources((object) this.barSubItem1, "barSubItem1");
      this.barSubItem1.Id = 0;
      LinksInfo linksPersistInfo2 = this.barSubItem1.LinksPersistInfo;
      LinkPersistInfo[] links2 = new LinkPersistInfo[2];
      int index103 = 0;
      LinkPersistInfo linkPersistInfo2 = new LinkPersistInfo((BarItem) this.barBtnDesignOutstandingListingReport, true);
      links2[index103] = linkPersistInfo2;
      int index104 = 1;
      LinkPersistInfo linkPersistInfo3 = new LinkPersistInfo((BarItem) this.barbtnAdvancedFilter, true);
      links2[index104] = linkPersistInfo3;
      linksPersistInfo2.AddRange(links2);
      this.barSubItem1.MergeOrder = 1;
      this.barSubItem1.Name = "barSubItem1";
      componentResourceManager.ApplyResources((object) this.barBtnDesignOutstandingListingReport, "barBtnDesignOutstandingListingReport");
      this.barBtnDesignOutstandingListingReport.Id = 2;
      this.barBtnDesignOutstandingListingReport.Name = "barBtnDesignOutstandingListingReport";
      this.barBtnDesignOutstandingListingReport.ItemClick += new ItemClickEventHandler(this.barBtnDesignOutstandingListingReport_ItemClick);
      componentResourceManager.ApplyResources((object) this.barbtnAdvancedFilter, "barbtnAdvancedFilter");
      this.barbtnAdvancedFilter.Id = 1;
      this.barbtnAdvancedFilter.Name = "barbtnAdvancedFilter";
      this.barbtnAdvancedFilter.ItemClick += new ItemClickEventHandler(this.barbtnAdvancedFilter_ItemClick);
      this.barDockControlTop.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlTop, "barDockControlTop");
      this.barDockControlBottom.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlBottom, "barDockControlBottom");
      this.barDockControlLeft.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlLeft, "barDockControlLeft");
      this.barDockControlRight.CausesValidation = false;
      componentResourceManager.ApplyResources((object) this.barDockControlRight, "barDockControlRight");
      componentResourceManager.ApplyResources((object) this.panelHeader1, "panelHeader1");
      this.panelHeader1.HelpTopicId = "Stock_Assembly_Order.htm";
      this.panelHeader1.Name = "panelHeader1";
      componentResourceManager.ApplyResources((object) this, "$this");
      this.AutoScaleMode = AutoScaleMode.Dpi;
      this.CancelButton = (IButtonControl) this.sbtnClose;
      this.Controls.Add((Control) this.panelControl4);
      this.Controls.Add((Control) this.panelCenter);
      this.Controls.Add((Control) this.panelBasic);
      this.Controls.Add((Control) this.panelAdv);
      this.Controls.Add((Control) this.panelHeader1);
      this.Controls.Add((Control) this.barDockControlLeft);
      this.Controls.Add((Control) this.barDockControlRight);
      this.Controls.Add((Control) this.barDockControlBottom);
      this.Controls.Add((Control) this.barDockControlTop);
      this.KeyPreview = true;
      this.Name = "FormStockAssemblyOrderPrintOutstandingListing";
      this.ShowInTaskbar = false;
      this.Closing += new CancelEventHandler(this.FormStockAssemblyOrderPrintOutstandingListing_Closing);
      this.Load += new EventHandler(this.FormStockAssemblyOrderPrintOutstandingListing_Load);
      this.KeyDown += new KeyEventHandler(this.FormStockAssemblyOrderPrintOutstandingListing_KeyDown);
      this.gridView2.EndInit();
      this.gridControl1.EndInit();
      this.gridView3.EndInit();
      this.gridView1.EndInit();
      this.repositoryItemCheckEdit1.EndInit();
      this.repositoryItemTextEdit1.EndInit();
      this.panelAdv.EndInit();
      this.panelAdv.ResumeLayout(false);
      this.gbPrint.EndInit();
      this.gbPrint.ResumeLayout(false);
      this.radioGroup1.Properties.EndInit();
      this.gbReport.EndInit();
      this.gbReport.ResumeLayout(false);
      this.gbReport.PerformLayout();
      this.chkEditShowCriteria.Properties.EndInit();
      this.cbSortBy.Properties.EndInit();
      this.cbGroupBy.Properties.EndInit();
      this.gbFilter.EndInit();
      this.gbFilter.ResumeLayout(false);
      this.xtraTabControl1.EndInit();
      this.xtraTabControl1.ResumeLayout(false);
      this.xtraTabPage1.ResumeLayout(false);
      this.xtraTabPage1.PerformLayout();
      this.dateEditFromAdv.Properties.CalendarTimeProperties.EndInit();
      this.dateEditFromAdv.Properties.EndInit();
      this.dateEditToAdv.Properties.CalendarTimeProperties.EndInit();
      this.dateEditToAdv.Properties.EndInit();
      this.xtraTabPage2.ResumeLayout(false);
      this.xtraTabPage2.PerformLayout();
      this.panelBasic.EndInit();
      this.panelBasic.ResumeLayout(false);
      this.groupControl3.EndInit();
      this.groupControl3.ResumeLayout(false);
      this.radioGroup2.Properties.EndInit();
      this.groupControl2.EndInit();
      this.groupControl2.ResumeLayout(false);
      this.groupControl2.PerformLayout();
      this.chkEditShowCriteriaBasic.Properties.EndInit();
      this.cbSortByBasic.Properties.EndInit();
      this.cbGroupByBasic.Properties.EndInit();
      this.groupControl1.EndInit();
      this.groupControl1.ResumeLayout(false);
      this.groupControl1.PerformLayout();
      this.dateEditToBasic.Properties.CalendarTimeProperties.EndInit();
      this.dateEditToBasic.Properties.EndInit();
      this.dateEditFromBasic.Properties.CalendarTimeProperties.EndInit();
      this.dateEditFromBasic.Properties.EndInit();
      this.panelCenter.EndInit();
      this.panelCenter.ResumeLayout(false);
      this.panelControl4.EndInit();
      this.panelControl4.ResumeLayout(false);
      this.xtraTabControl2.EndInit();
      this.xtraTabControl2.ResumeLayout(false);
      this.xtraTabPage3.ResumeLayout(false);
      this.xtraTabPage4.ResumeLayout(false);
      this.memoEdit_Criteria.Properties.EndInit();
      this.barManager1.EndInit();
      this.ResumeLayout(false);
    }
  }
}
