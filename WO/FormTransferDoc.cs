﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.FormTransferDoc
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Application;
using BCE.AutoCount;
using BCE.AutoCount.Manufacturing;
using BCE.AutoCount.XtraUtils;
using BCE.Data;
using BCE.Localization;
using BCE.XtraUtils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace RPASystem.WorkOrder
{
  public class FormTransferDoc : XtraForm
  {
    private StockWorkOrder myStockAssemblyOrder;
    private DataTable myTable;
    private IContainer components;
    private PanelControl panelControl1;
    private SimpleButton sbtnCancel;
    private SimpleButton sbtnOK;
    private GridColumn colDate;
    private GridColumn colDebtorCode;
    private GridColumn colDeptNo;
    private GridColumn colDescription;
    private GridColumn colDiscount;
    private GridColumn colDocNo;
    private GridColumn colFOCQty;
    private GridColumn colItemCode;
    private GridColumn colLocation;
    private GridColumn colNewQty;
    private GridColumn colNumbering;
    private GridColumn colProjNo;
    private GridColumn colRemainingQty;
    private GridColumn colSmallestQty;
    private GridColumn colSubTotal;
    private GridColumn colTotalQty;
    private GridColumn colUnitPrice;
    private GridControl gridctrTransDoc;
    private GridView gridView1;
    private GridView gvTransDoc;

    public FormTransferDoc(StockWorkOrder aStockAssemblyOrder)
    {
      this.InitializeComponent();
      this.myStockAssemblyOrder = aStockAssemblyOrder;
      this.myTable = this.myStockAssemblyOrder.Command.LoadPartialTransferItemFromSO();
      this.Icon = BCE.AutoCount.Application.Icon;
      this.AddColumn();
      this.InitFormControls();
      this.gridctrTransDoc.DataSource = (object) this.myTable;
    }

    private void AddColumn()
    {
      this.myTable.Columns.Add("NewQty", typeof (Decimal));
    }

    private void InitFormControls()
    {
      FormControlUtil formControlUtil = new FormControlUtil(this.myStockAssemblyOrder.Command.DBSetting, false);
      string fieldname1 = "Qty";
      string fieldtype1 = "Quantity";
      formControlUtil.AddField(fieldname1, fieldtype1);
      string fieldname2 = "SmallestQty";
      string fieldtype2 = "Quantity";
      formControlUtil.AddField(fieldname2, fieldtype2);
      string fieldname3 = "RemainingQty";
      string fieldtype3 = "Quantity";
      formControlUtil.AddField(fieldname3, fieldtype3);
      string fieldname4 = "TotalQty";
      string fieldtype4 = "Quantity";
      formControlUtil.AddField(fieldname4, fieldtype4);
      string fieldname5 = "FOCQty";
      string fieldtype5 = "Quantity";
      formControlUtil.AddField(fieldname5, fieldtype5);
      string fieldname6 = "NewQty";
      string fieldtype6 = "Quantity";
      formControlUtil.AddField(fieldname6, fieldtype6);
      string fieldname7 = "DocDate";
      string fieldtype7 = "Date";
      formControlUtil.AddField(fieldname7, fieldtype7);
      string fieldname8 = "SubTotal";
      string fieldtype8 = "Currency";
      formControlUtil.AddField(fieldname8, fieldtype8);
      string fieldname9 = "Rate";
      string fieldtype9 = "Quantity";
      formControlUtil.AddField(fieldname9, fieldtype9);
      FormTransferDoc formTransferDoc = this;
      formControlUtil.InitControls((Control) formTransferDoc);
    }

    private void sbtnCancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
    }

    private void sbtnOK_Click(object sender, EventArgs e)
    {
      if (this.gvTransDoc.FocusedRowHandle >= 0)
      {
        DataRow dataRow = this.gvTransDoc.GetDataRow(this.gvTransDoc.FocusedRowHandle);
        Decimal qty = dataRow["NewQty"] != DBNull.Value ? BCE.Data.Convert.ToDecimal(dataRow["NewQty"]) : BCE.Data.Convert.ToDecimal(dataRow["RemainingQty"]);
        if (BCE.Data.Convert.ToDecimal(dataRow["RemainingQty"]) < Decimal.Zero)
        {
          AppMessage.ShowErrorMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ErrorMessage_CannotTransferCauseRemainingQty, new object[0]));
          this.gridctrTransDoc.Focus();
          this.gvTransDoc.FocusedColumn = this.colNewQty;
          this.DialogResult = DialogResult.None;
        }
        else
        {
          this.myStockAssemblyOrder.TransferFromSO(BCE.Data.Convert.ToInt64(dataRow["DtlKey"]), dataRow["ItemCode"].ToString(), qty);
          this.DialogResult = DialogResult.OK;
        }
      }
      else
      {
        AppMessage.ShowErrorMessage((IWin32Window) this, BCE.Localization.Localizer.GetString((Enum) StockAssemblyOrderStringId.ErrorMessage_SelectRowAndSpecifyQty, new object[0]));
        this.gridctrTransDoc.Focus();
        this.DialogResult = DialogResult.None;
      }
    }

    private void gvTransDoc_DoubleClick(object sender, EventArgs e)
    {
      if (BCE.XtraUtils.GridViewUtils.GetGridHitInfo((GridView) sender).InRow)
        this.sbtnOK.PerformClick();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormTransferDoc));
      this.panelControl1 = new PanelControl();
      this.sbtnOK = new SimpleButton();
      this.sbtnCancel = new SimpleButton();
      this.gridctrTransDoc = new GridControl();
      this.gvTransDoc = new GridView();
      this.colNewQty = new GridColumn();
      this.colRemainingQty = new GridColumn();
      this.colTotalQty = new GridColumn();
      this.colSmallestQty = new GridColumn();
      this.colFOCQty = new GridColumn();
      this.colDebtorCode = new GridColumn();
      this.colDocNo = new GridColumn();
      this.colItemCode = new GridColumn();
      this.colDescription = new GridColumn();
      this.colDate = new GridColumn();
      this.colDeptNo = new GridColumn();
      this.colDiscount = new GridColumn();
      this.colLocation = new GridColumn();
      this.colNumbering = new GridColumn();
      this.colProjNo = new GridColumn();
      this.colSubTotal = new GridColumn();
      this.colUnitPrice = new GridColumn();
      this.gridView1 = new GridView();
      this.panelControl1.BeginInit();
      this.panelControl1.SuspendLayout();
      this.gridctrTransDoc.BeginInit();
      this.gvTransDoc.BeginInit();
      this.gridView1.BeginInit();
      this.SuspendLayout();
      this.panelControl1.BorderStyle = BorderStyles.NoBorder;
      this.panelControl1.Controls.Add((Control) this.sbtnOK);
      this.panelControl1.Controls.Add((Control) this.sbtnCancel);
      componentResourceManager.ApplyResources((object) this.panelControl1, "panelControl1");
      this.panelControl1.Name = "panelControl1";
      componentResourceManager.ApplyResources((object) this.sbtnOK, "sbtnOK");
      this.sbtnOK.DialogResult = DialogResult.OK;
      this.sbtnOK.Name = "sbtnOK";
      this.sbtnOK.Click += new EventHandler(this.sbtnOK_Click);
      componentResourceManager.ApplyResources((object) this.sbtnCancel, "sbtnCancel");
      this.sbtnCancel.DialogResult = DialogResult.Cancel;
      this.sbtnCancel.Name = "sbtnCancel";
      this.sbtnCancel.Click += new EventHandler(this.sbtnCancel_Click);
      componentResourceManager.ApplyResources((object) this.gridctrTransDoc, "gridctrTransDoc");
      this.gridctrTransDoc.EmbeddedNavigator.Buttons.Append.Visible = false;
      this.gridctrTransDoc.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
      this.gridctrTransDoc.EmbeddedNavigator.Buttons.Edit.Visible = false;
      this.gridctrTransDoc.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
      this.gridctrTransDoc.EmbeddedNavigator.Buttons.Remove.Visible = false;
      this.gridctrTransDoc.MainView = (BaseView) this.gvTransDoc;
      this.gridctrTransDoc.Name = "gridctrTransDoc";
      this.gridctrTransDoc.UseEmbeddedNavigator = true;
      ViewRepositoryCollection viewCollection = this.gridctrTransDoc.ViewCollection;
      BaseView[] views = new BaseView[2];
      int index1 = 0;
      GridView gridView1 = this.gvTransDoc;
      views[index1] = (BaseView) gridView1;
      int index2 = 1;
      GridView gridView2 = this.gridView1;
      views[index2] = (BaseView) gridView2;
      viewCollection.AddRange(views);
      GridColumnCollection columns1 = this.gvTransDoc.Columns;
      GridColumn[] columns2 = new GridColumn[17];
      int index3 = 0;
      GridColumn gridColumn1 = this.colNewQty;
      columns2[index3] = gridColumn1;
      int index4 = 1;
      GridColumn gridColumn2 = this.colRemainingQty;
      columns2[index4] = gridColumn2;
      int index5 = 2;
      GridColumn gridColumn3 = this.colTotalQty;
      columns2[index5] = gridColumn3;
      int index6 = 3;
      GridColumn gridColumn4 = this.colSmallestQty;
      columns2[index6] = gridColumn4;
      int index7 = 4;
      GridColumn gridColumn5 = this.colFOCQty;
      columns2[index7] = gridColumn5;
      int index8 = 5;
      GridColumn gridColumn6 = this.colDebtorCode;
      columns2[index8] = gridColumn6;
      int index9 = 6;
      GridColumn gridColumn7 = this.colDocNo;
      columns2[index9] = gridColumn7;
      int index10 = 7;
      GridColumn gridColumn8 = this.colItemCode;
      columns2[index10] = gridColumn8;
      int index11 = 8;
      GridColumn gridColumn9 = this.colDescription;
      columns2[index11] = gridColumn9;
      int index12 = 9;
      GridColumn gridColumn10 = this.colDate;
      columns2[index12] = gridColumn10;
      int index13 = 10;
      GridColumn gridColumn11 = this.colDeptNo;
      columns2[index13] = gridColumn11;
      int index14 = 11;
      GridColumn gridColumn12 = this.colDiscount;
      columns2[index14] = gridColumn12;
      int index15 = 12;
      GridColumn gridColumn13 = this.colLocation;
      columns2[index15] = gridColumn13;
      int index16 = 13;
      GridColumn gridColumn14 = this.colNumbering;
      columns2[index16] = gridColumn14;
      int index17 = 14;
      GridColumn gridColumn15 = this.colProjNo;
      columns2[index17] = gridColumn15;
      int index18 = 15;
      GridColumn gridColumn16 = this.colSubTotal;
      columns2[index18] = gridColumn16;
      int index19 = 16;
      GridColumn gridColumn17 = this.colUnitPrice;
      columns2[index19] = gridColumn17;
      columns1.AddRange(columns2);
      this.gvTransDoc.GridControl = this.gridctrTransDoc;
      this.gvTransDoc.Name = "gvTransDoc";
      this.gvTransDoc.OptionsCustomization.AllowFilter = false;
      this.gvTransDoc.OptionsView.ShowGroupPanel = false;
      this.gvTransDoc.DoubleClick += new EventHandler(this.gvTransDoc_DoubleClick);
      this.colNewQty.AppearanceCell.ForeColor = (Color) componentResourceManager.GetObject("colNewQty.AppearanceCell.ForeColor");
      this.colNewQty.AppearanceCell.Options.UseForeColor = true;
      componentResourceManager.ApplyResources((object) this.colNewQty, "colNewQty");
      this.colNewQty.FieldName = "NewQty";
      this.colNewQty.Name = "colNewQty";
      this.colRemainingQty.AppearanceCell.ForeColor = (Color) componentResourceManager.GetObject("colRemainingQty.AppearanceCell.ForeColor");
      this.colRemainingQty.AppearanceCell.Options.UseForeColor = true;
      componentResourceManager.ApplyResources((object) this.colRemainingQty, "colRemainingQty");
      this.colRemainingQty.FieldName = "RemainingQty";
      this.colRemainingQty.Name = "colRemainingQty";
      this.colRemainingQty.OptionsColumn.AllowEdit = false;
      this.colTotalQty.AppearanceCell.ForeColor = (Color) componentResourceManager.GetObject("colTotalQty.AppearanceCell.ForeColor");
      this.colTotalQty.AppearanceCell.Options.UseForeColor = true;
      componentResourceManager.ApplyResources((object) this.colTotalQty, "colTotalQty");
      this.colTotalQty.FieldName = "TotalQty";
      this.colTotalQty.Name = "colTotalQty";
      this.colTotalQty.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colSmallestQty, "colSmallestQty");
      this.colSmallestQty.FieldName = "SmallestQty";
      this.colSmallestQty.Name = "colSmallestQty";
      this.colSmallestQty.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colFOCQty, "colFOCQty");
      this.colFOCQty.FieldName = "FOCQty";
      this.colFOCQty.Name = "colFOCQty";
      this.colFOCQty.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDebtorCode, "colDebtorCode");
      this.colDebtorCode.FieldName = "DebtorCode";
      this.colDebtorCode.Name = "colDebtorCode";
      this.colDebtorCode.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDocNo, "colDocNo");
      this.colDocNo.FieldName = "DocNo";
      this.colDocNo.Name = "colDocNo";
      this.colDocNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colItemCode, "colItemCode");
      this.colItemCode.FieldName = "ItemCode";
      this.colItemCode.Name = "colItemCode";
      this.colItemCode.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDescription, "colDescription");
      this.colDescription.FieldName = "Description";
      this.colDescription.Name = "colDescription";
      this.colDescription.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDate, "colDate");
      this.colDate.FieldName = "DocDate";
      this.colDate.Name = "colDate";
      this.colDate.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDeptNo, "colDeptNo");
      this.colDeptNo.FieldName = "DeptNo";
      this.colDeptNo.Name = "colDeptNo";
      this.colDeptNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colDiscount, "colDiscount");
      this.colDiscount.FieldName = "Discount";
      this.colDiscount.Name = "colDiscount";
      this.colDiscount.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colLocation, "colLocation");
      this.colLocation.FieldName = "Location";
      this.colLocation.Name = "colLocation";
      this.colLocation.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colNumbering, "colNumbering");
      this.colNumbering.FieldName = "Numbering";
      this.colNumbering.Name = "colNumbering";
      this.colNumbering.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colProjNo, "colProjNo");
      this.colProjNo.FieldName = "ProjNo";
      this.colProjNo.Name = "colProjNo";
      this.colProjNo.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colSubTotal, "colSubTotal");
      this.colSubTotal.FieldName = "SubTotal";
      this.colSubTotal.Name = "colSubTotal";
      this.colSubTotal.OptionsColumn.AllowEdit = false;
      componentResourceManager.ApplyResources((object) this.colUnitPrice, "colUnitPrice");
      this.colUnitPrice.FieldName = "UnitPrice";
      this.colUnitPrice.Name = "colUnitPrice";
      this.colUnitPrice.OptionsColumn.AllowEdit = false;
      this.gridView1.GridControl = this.gridctrTransDoc;
      this.gridView1.Name = "gridView1";
      componentResourceManager.ApplyResources((object) this, "$this");
      this.AutoScaleMode = AutoScaleMode.Dpi;
      this.Controls.Add((Control) this.gridctrTransDoc);
      this.Controls.Add((Control) this.panelControl1);
      this.Name = "FormTransferDoc";
      this.panelControl1.EndInit();
      this.panelControl1.ResumeLayout(false);
      this.gridctrTransDoc.EndInit();
      this.gvTransDoc.EndInit();
      this.gridView1.EndInit();
      this.ResumeLayout(false);
    }
  }
}
