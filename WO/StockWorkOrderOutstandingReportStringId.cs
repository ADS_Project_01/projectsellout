﻿// Type: BCE.AutoCount.Manufacturing.StockAssemblyOrder.StockAssemblyOrderOutstandingReportStringId
// Assembly: BCE.AutoCount.Manufacturing, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\BCE.AutoCount.Manufacturing.dll

using BCE.Localization;

namespace RPASystem.WorkOrder
{
  [LocalizableString]
  internal enum StockWorkOrderOutstandingReportStringId
  {
    [DefaultString("Outstanding Stock Assembly Order Listing")] OutstandingStockAssemblyOrderListingReportTitle,
  }
}
