﻿using AutoCount;
using AutoCount.Settings;
using AutoCount.Data;
using AutoCount.Localization;
using AutoCount.XtraUtils.LookupEditBuilder;
using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Base;
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;

namespace ProjectSellOut.Export
{
    public class InvoiceLookupEditBuilder : LookupEditBuilderEx
    {
        private string myLastDocNo = "!@#!@#!!@#";
        public InvoiceLookupEditBuilder()
        {
            //this.myUseCaching = true;
            //this.myCacheFileName = "RPA_RCV.Data";
            this.myCmdString = "SELECT DocNo,DocDate, DebtorName FROM IV where Cancelled='F'";
        }

        protected override void SetupLookupEdit(RepositoryItemLookUpEdit myLookupEdit)
        {
            base.SetupLookupEdit(myLookupEdit);
            myLookupEdit.DisplayMember = "DocNo";
            myLookupEdit.ValueMember = "DocNo";
            myLookupEdit.DataSource = (object)this.myDataTable;
            myLookupEdit.Columns.Clear();
            myLookupEdit.Columns.Add(new LookUpColumnInfo("DocNo", "DocNo", 150));
            myLookupEdit.Columns.Add(new LookUpColumnInfo("DocDate", "DocDate", 100));
            //   myLookupEdit.Columns.Add(new LookUpColumnInfo("Description", AutoCount.Localization.Localizer.GetString((Enum)Budget.GeneralMaintenance.ItemBOM.ItemBOMString.Description, new object[0]), 150));
            myLookupEdit.PopupWidth = 400;
            myLookupEdit.AutoSearchColumnIndex = 1;
          //  myLookupEdit.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
            myLookupEdit.KeyPress += new KeyPressEventHandler(this.LookupEdt_KeyPress);
        }
        protected override void FillDataTable(string SQLSelect)
        {
            base.FillDataTable(SQLSelect);
            this.myLastDocNo = "!@#!@#!!@#";
        }

  
        //protected override int GetChangeCount()
        //{
        //    return this.myChangeCount.GetChangeCount("RPA_WO");
        //}

        public override void UnlinkEventHandlers(RepositoryItemLookUpEdit lookupEdit)
        {
            base.UnlinkEventHandlers(lookupEdit);
            lookupEdit.KeyPress -= new KeyPressEventHandler(this.LookupEdt_KeyPress);
        }

        private void LookupEdt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (sender is LookUpEdit)
            {
                LookUpEdit lookUpEdit = sender as LookUpEdit;
                if (!lookUpEdit.IsPopupOpen)
                {
                    BaseView baseView = (BaseView)null;
                    if ((int)e.KeyChar >= 32 && (int)e.KeyChar <= 126)
                    {
                        if (baseView != null)
                        {
                            baseView.ShowEditorByKey(new KeyEventArgs((Keys)115));
                            baseView.ShowEditorByKeyPress(new KeyPressEventArgs(e.KeyChar));
                        }
                        else
                        {
                            lookUpEdit.ShowPopup();
                            lookUpEdit.SendKey((object)new Message(), new KeyPressEventArgs(e.KeyChar));
                        }
                        e.Handled = true;
                    }
                }
            }
        }
    }

}
