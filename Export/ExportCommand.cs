﻿// Type: AutoCount.Stock.Budget.BudgetReportCommand
// Assembly: AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\AutoCount.Stock.dll
using AutoCount.Authentication;
using AutoCount;
using AutoCount.Report;
using AutoCount.SearchFilter;
using AutoCount.Data;
using AutoCount.Localization;
using System;
using System.Data;
using System.Data.SqlClient;
namespace ProjectSellOut.Export
{
    public class ExportCommand
    {
        private BasicReportOption myReportOption;
        protected DBSetting myDBSetting;
        protected UserSession myUserSession;
        protected const string MasterTableName = "Master";
        protected const string DetailTableName = "Detail";
        public const string DetailListingReportStyle = "Budget Detail Listing";

        public BasicReportOption ReportOption
        {
            get
            {
                return this.myReportOption;
            }
        }

        public DBSetting DBSetting
        {
            get
            {
                return this.myDBSetting;
            }
        }
        public UserSession UserSession
        {
            get
            {
                return this.myUserSession;
            }
        }

        public static ExportCommand Create(DBSetting dbSetting, UserSession userSession, BasicReportOption reportOption)
        {

            if (dbSetting.ServerType == DBServerType.SQL2000)
            {
                ExportCommand issueReportCommand = (ExportCommand)new ExportCommandSQL();
                issueReportCommand.myDBSetting = dbSetting;
                issueReportCommand.myUserSession = userSession;
                issueReportCommand.myReportOption = reportOption;
                return issueReportCommand;
            }
            else
                throw new ArgumentException("Server type: " + (object)dbSetting.ServerType + " not supported.");
        }



        protected string GetWhereSQL(ExportCriteria reportingCriteria, SqlCommand cmd)
        {
            string str1 = "";
            SearchCriteria searchCriteria = new SearchCriteria();
            // AutoCount.SearchFilter.Filter dateFilter = reportingCriteria.DateFilter;
            // searchCriteria.AddFilter(dateFilter);
            //AutoCount.SearchFilter.Filter documentFilter = reportingCriteria.DocumentFilter;
            //searchCriteria.AddFilter(documentFilter);
            AutoCount.SearchFilter.Filter documentFilter = reportingCriteria.DocumentFilter;
            searchCriteria.AddFilter(documentFilter);

            int num = 1;
            searchCriteria.MatchAll = num != 0;
            SqlCommand sqlCommand = cmd;
            string str2 = searchCriteria.BuildSQL((IDbCommand)sqlCommand);
            if (str2.Length > 0)
                str1 = str2;
            if (reportingCriteria.ShowInvoice == true)
                str1 = str1.Length <= 0 ? " (a.UDF_HasExport='F' or a.UDF_HasExport='T') " : str1 + " And (a.UDF_HasExport='F' or a.UDF_HasExport='T') ";
            else
            if (reportingCriteria.ShowInvoice == false)
                str1 = str1.Length <= 0 ? " IsNull(a.UDF_HasExport,'F') = 'F' " : str1 + " And IsNull(a.UDF_HasExport,'F') = 'F' ";
            //if (reportingCriteria.IsPrintCancelled == CancelledDocumentOption.Cancelled)
            str1 = str1.Length <= 0 ? String.Format(" Invoice_Date BETWEEN '{0}' and '{1}' ", (object)reportingCriteria.FromDate.ToString("yyyy/MM/dd"), (object)reportingCriteria.ToDate.ToString("yyyy/MM/dd")) : str1 + " And  " + String.Format(" Invoice_Date BETWEEN '{0}' and '{1}' ", (object)reportingCriteria.FromDate.ToString("yyyy/MM/dd"), (object)reportingCriteria.ToDate.ToString("yyyy/MM/dd"));
            return str1;
        }

        protected virtual DataSet LoadDetailListingReportData(string dtlKeys, ExportCriteria criteria)
        {
            return (DataSet)null;
        }

        protected virtual DataSet LoadDetailListingReportDesignerData()
        {
            return (DataSet)null;
        }

        public virtual void BasicSearch(ExportCriteria criteria, DataSet newDS)
        {
        }
        
    }
}
