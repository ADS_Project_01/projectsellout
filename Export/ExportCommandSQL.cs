﻿// Type: AutoCount.Stock.Budget.BudgetReportCommandSQL
// Assembly: AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\AutoCount.Stock.dll

using AutoCount;
using AutoCount.Stock;
//using AutoCount.UDF;
using AutoCount.Utils;
using AutoCount.Data;
using System;
using System.Data;
using System.Data.SqlClient;
using AutoCount.Authentication;
namespace ProjectSellOut.Export
{
  public class ExportCommandSQL : ExportCommand
  {
 
    protected override DataSet LoadDetailListingReportDesignerData()
    {
      string cmdText = string.Format("Select TOP 100 A.*, B.* {0} {1} from BGT_Budget A inner join v_BGT_BudgetDetail B on (A.DocKey = B.DocKey)", (object) ", (cast(year(A.DocDate) as varchar(4)) + case when (month(A.DocDate) < 10) then '0' + cast(month(A.DocDate) as varchar(2)) else cast(month(A.DocDate) as varchar(2)) end + case when (day(A.DocDate) < 10) then '0' + cast(day(A.DocDate) as varchar(2)) else cast(day(A.DocDate) as varchar(2))end )AS SortID ", (object) ",B.ItemCode AS GroupID, 'Item Code' AS GroupIDName, B.ItemCode AS GroupIDDisplay, B.ItemDescription AS GroupIDDescription ");
      DataSet ds = new DataSet();
      this.myDBSetting.LoadDataSet(ds, "Master", cmdText, false, new object[0]);
      return ds;
    }

 
    public override void BasicSearch(ExportCriteria criteria, DataSet newDS)
    {
      if (newDS.Relations.Contains("MasterDetailRelation") && newDS.Relations.CanRemove(newDS.Relations["MasterDetailRelation"]))
        newDS.Relations.Clear();
      DataTable DistributorTable = newDS.Tables["Distributor"];
      DataTable CustomerTable = newDS.Tables["Customer"];
            DataTable EmployeeTable = newDS.Tables["Employee"];
            DataTable ProductTable = newDS.Tables["Product"];
            DataTable InvoiceTable = newDS.Tables["Invoice"];
            DataTable StockTable = newDS.Tables["Stock"];
            DateTime DateTo=new DateTime();
            DateTime DateFrom = new DateTime();
            if (criteria.FromDate != null)
            {
                DateFrom = criteria.FromDate;// AutoCount.Converter.ToDateTime(criteria.DateFilter.To);
            }
            else
                DateFrom = myDBSetting.GetServerTime();

            if (criteria.ToDate != null)
            {
                DateTo = AutoCount.Converter.ToDateTime(criteria.ToDate.ToShortDateString());// AutoCount.Converter.ToDateTime(criteria.DateFilter.To);
            }
            else
                DateTo = AutoCount.Converter.ToDateTime(myDBSetting.GetServerTime().ToShortDateString());
                           string formatInvoice = "SELECT a.* from SO_Invoice_View a inner join IV with(NOLOCK) on IV.DocNo=a.Invoice_Number WHERE 1=1 {0} ";
            string formatProfile = string.Format("SELECT * from SO_CompanyProfile_View where (ISNULL(UDF_HasExport,'F')='F' or ISNULL(UDF_HasExport,'F')='{0}')", (object)AutoCount.Converter.BooleanToText(criteria.ShowDistributor));
            string formatCustomer = string.Format("select * from SO_Debtor_View where (ISNULL(UDF_HasExport,'F')='F' or ISNULL(UDF_HasExport,'F')='{0}')", (object)AutoCount.Converter.BooleanToText(criteria.ShowEndCustomer));
            string formatEmployee = string.Format("select * from SO_SalesAgent_View where (ISNULL(UDF_HasExport,'F')='F' or ISNULL(UDF_HasExport,'F')='{0}')", (object)AutoCount.Converter.BooleanToText(criteria.ShowEmployeeSales));
            string formatProduct = string.Format("select * from SO_Product_View where (ISNULL(UDF_HasExport,'F')='F' or ISNULL(UDF_HasExport,'F')='{0}')", (object)AutoCount.Converter.BooleanToText(criteria.ShowProduct));
            // string formatStock = "select Distributor_Code,Local_Product_Code,UOM,'{0}' as Stock_Date,case when SUM(Stock_position) <0 then 0 else SUM(Stock_position) end as Stock_position,MAX(Creation_Date) AS Creation_Date, MAX(Modification_Date) AS Modification_Date from SO_StockCard_View";

            //  string formatNonStock = "select Distributor_Code,Local_Product_Code,UOM,'{0}' as Stock_Date,0 as Stock_position,MAX(Creation_Date) AS Creation_Date, MAX(Modification_Date) AS Modification_Date from SO_StockCard_View";

            string formatStock2 = "[dbo].[SO_StockQuery_P]";


            string str20 = string.Empty;
      SqlConnection sqlConnection = new SqlConnection(this.myDBSetting.ConnectionString);
      try
      {
        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.Connection = sqlConnection;
        string whereSql = this.GetWhereSQL(criteria, sqlCommand);
        string str22 = !(whereSql == string.Empty) ? string.Format(formatInvoice, (object) " AND "+whereSql) : string.Format(formatInvoice, (object)" AND " + " (1=1)");
        string str23 = SQLHelper.BuildFilteredByUserSQL(this.myUserSession, "IV");

                //str22= str22+ string.Format(" AND (a.UDF_HasExport='F' or a.UDF_HasExport='{0}')", (object)criteria.CheckIncludeExported);
                if (str23.Length > 0)
                    str22 = str22 + " AND " + str23;


                // formatCustomer = formatCustomer; + " WHERE End_Customer_Code in (" + str22.Replace("a.*", "DebtorCode") + ")";
                //   formatEmployee = formatEmployee + " WHERE employee_Num in (" + str22.Replace("a.*", "SalesAgent") + ")";
                //  formatProduct = formatProduct + " WHERE local_product_code in (" + str22.Replace("a.*", "local_product_code") + ")";


                // di disable Script kondisi  WHERE Stock_Date<='{1}' atas permintaan Dwi , tanggal : 19/10/2020
               // formatStock2 = "union all select * from(" + string.Format(formatStock + " WHERE Stock_Date<='{1}' GROUP By Distributor_Code,Local_Product_Code,UOM) result", (object)DateTo.Date.ToString("dd/MM/yyyy"), (object)DateTo.Date.ToString("yyyy/MM/dd"));  //local_product_code in (" + str22.Replace("a.*", "local_product_code") + ")";

              //  string formatItemFinal = string.Format(formatNonStock + " where Local_Product_Code Not in(select Local_Product_Code from SO_StockCard_View WHERE Stock_Date<='{1}')  GROUP By Distributor_Code,Local_Product_Code,UOM ", (object)DateTo.Date.ToString("dd/MM/yyyy"), (object)DateTo.Date.ToString("yyyy/MM/dd")) + formatStock2;

                //formatStock = string.Format(formatStock + " WHERE 1=1 GROUP By Distributor_Code,Local_Product_Code,UOM", (object)DateTo.Date.ToString("dd/MM/yyyy"), (object)DateTo.Date.ToString("yyyy/MM/dd"));  //local_product_code in (" + str22.Replace("a.*", "local_product_code") + ")";


                sqlCommand.CommandText = str22;
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
        long[] numArray = (long[]) null;
                sqlConnection.Open();
                
                    sqlDataAdapter.Fill(InvoiceTable);
                
                sqlCommand.CommandText = formatProfile;
                sqlDataAdapter = new SqlDataAdapter(sqlCommand);                
                    sqlDataAdapter.Fill(DistributorTable);

                sqlCommand.CommandText = formatCustomer;
                sqlDataAdapter = new SqlDataAdapter(sqlCommand);                
                    sqlDataAdapter.Fill(CustomerTable);

                sqlCommand.CommandText = formatEmployee;
                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(EmployeeTable);

                sqlCommand.CommandText = formatProduct;
                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(ProductTable);

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = formatStock2;
                sqlCommand.Parameters.Add(new SqlParameter("@ToDate", (object)DateTo.Date.ToString("yyyy/MM/dd")));
                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(StockTable);
                //if (!criteria.KeepSearchResult)
                //{
                //  dataTable2.Clear();
                //  dataTable1.Clear();
                //}
                //else 
                //if (chkEditColumnName.Length > 0 && dataTable1.Rows.Count > 0)
                //{
                //  DataRow[] dataRowArray = dataTable1.Select(string.Format("{0} = true", (object) chkEditColumnName));
                //  numArray = new long[dataRowArray.Length];
                //  for (int index23 = 0; index23 < dataRowArray.Length; ++index23)
                //    numArray[index23] = AutoCount.Converter.ToInt64(dataRowArray[index23]["DtlKey"]);
                //}

                //if (InvoiceTable.PrimaryKey.Length == 0)
                //{
                //  DataTable dataTable3 = dataTable1;
                //  DataColumn[] dataColumnArray = new DataColumn[1];
                //  int index24 = 0;
                //  DataColumn dataColumn = dataTable1.Columns["DtlKey"];
                //  dataColumnArray[index24] = dataColumn;
                //  dataTable3.PrimaryKey = dataColumnArray;
                //}
                //if (criteria.SortBy == DetailListingSortByOption.DocumentNo)
                //  dataTable1.DefaultView.Sort = "DocNo";
                //else if (criteria.SortBy == DetailListingSortByOption.Date)
                //  dataTable1.DefaultView.Sort = "DocDate";
                //else if (criteria.SortBy == DetailListingSortByOption.ActivityCode)
                //  dataTable1.DefaultView.Sort = "ActivityCode";
                //else if (criteria.SortBy == DetailListingSortByOption.ActivityGroup)
                //  dataTable1.DefaultView.Sort = "ActivityGroup";
                //else if (criteria.SortBy == DetailListingSortByOption.BudgetType)
                //  dataTable1.DefaultView.Sort = "BudgetType";
                //else if (criteria.SortBy == DetailListingSortByOption.ContractNo)
                //  dataTable1.DefaultView.Sort = "ContractNo";
                //else if (criteria.SortBy == DetailListingSortByOption.CreditorName)
                //  dataTable1.DefaultView.Sort = "CreditorName";
                //else if (criteria.SortBy == DetailListingSortByOption.ProjNo)
                //  dataTable1.DefaultView.Sort = "ProjNo";
                //if (numArray != null)
                //{
                //  foreach (long num2 in numArray)
                //  {
                //    DataRow dataRow = dataTable1.Rows.Find((object) num2);
                //    if (dataRow != null)
                //      dataRow[chkEditColumnName] = (object) true;
                //  }


                //foreach(DataRow drStock in StockTable.Rows)
                //{
                //    drStock.BeginEdit();
                //    object obj = myDBSetting.ExecuteScalar("select SUM(BalQty) from ItemBatchBalQty WHERE ItemCode=? and UOM=? GROUP By ItemCode,UOM",(object )drStock["Local_Product_Code"], (object)drStock["UOM"]);
                //   if(obj!=null && obj!=DBNull.Value)
                //    {
                //        drStock["Stock_position"] = obj;
                //    }
                //    drStock.EndEdit();
                //}
                //}
            }
      catch (Exception ex)
      {
        if (ex is SqlException)
          AutoCount.Data.DataError.HandleSqlException((SqlException) ex);
        if (!(ex is ConstraintException))
          throw;
      }
      finally
      {
        sqlConnection.Close();
        sqlConnection.Dispose();
      }
    }
  }
}
