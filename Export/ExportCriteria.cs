﻿// Type: AutoCount.Stock.Budget.BudgetDetailReportingCriteria
// Assembly: AutoCount.Stock, Version=1.8.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: C:\Program Files\AutoCount\Accounting 1.8\AutoCount.Stock.dll

using AutoCount.SearchFilter;
using AutoCount.Stock;
using AutoCount.Localization;
using System;
using System.Collections;

namespace ProjectSellOut.Export
{
  [Serializable]
  public class ExportCriteria :SearchCriteria
  {
    protected AutoCount.SearchFilter.Filter myDocumentNo = new AutoCount.SearchFilter.Filter("IV", "DocNo","DocNo",FilterControlType.Invoice);
        protected AutoCount.SearchFilter.Filter myDateFilter = new AutoCount.SearchFilter.Filter("IV", "DocDate", "DocDate", FilterControlType.Date);
        protected DetailListingSortByOption mySortBy = DetailListingSortByOption.Date;
    private bool myAdvancedOptions;
    private bool myCheckAll;
        private bool myShowInvoice;
        private bool myShowDistributor;
        private bool myShowEndCustomer;
        private bool myShowEmployeeSales;
        private bool myShowProduct;
        private bool myCheckIncludeExported;
        protected DetailListingGroupByOption myGroupBy;
        protected DateTime myFromDate;
        protected DateTime myToDate;

        



        public bool ShowInvoice
        {
            get
            {
                return this.myShowInvoice;
            }
            set
            {
                this.myShowInvoice = value;
            }
        }

        public bool ShowDistributor
        {
            get
            {
                return this.myShowDistributor;
            }
            set
            {
                this.myShowDistributor = value;
            }
        }
        public bool ShowEndCustomer
        {
            get
            {
                return this.myShowEndCustomer;
            }
            set
            {
                this.myShowEndCustomer = value;
            }
        }

        public bool ShowEmployeeSales
        {
            get
            {
                return this.myShowEmployeeSales;
            }
            set
            {
                this.myShowEmployeeSales = value;
            }
        }
        public bool ShowProduct
        {
            get
            {
                return this.myShowProduct;
            }
            set
            {
                this.myShowProduct = value;
            }
        }

        public bool CheckIncludeExported
        {
            get
            {
                return this.myCheckIncludeExported;
            }
            set
            {
                this.myCheckIncludeExported = value;
            }
        }

        public AutoCount.SearchFilter.Filter DateFilter
        {
            get
            {
                return this.myDateFilter;
            }
        }


        public DateTime FromDate
        {
            get
            {
                return this.myFromDate;
            }
            set
            {
                this.myFromDate = value;
            }
        }
        public DateTime ToDate
        {
            get
            {
                return this.myToDate;
            }
            set
            {
                this.myToDate = value;
            }
        }

        public AutoCount.SearchFilter.Filter DocumentFilter
    {
      get
      {
        return this.myDocumentNo;
      }
    }

    

    public bool AdvancedOptions
    {
      get
      {
        return this.myAdvancedOptions;
      }
      set
      {
        this.myAdvancedOptions = value;
      }
    }

    public bool CheckAll
    {
      get
      {
        return this.myCheckAll;
      }
      set
      {
        this.myCheckAll = value;
      }
    }

    public DetailListingGroupByOption GroupBy
    {
      get
      {
        return this.myGroupBy;
      }
      set
      {
        this.myGroupBy = value;
      }
    }

    public DetailListingSortByOption SortBy
    {
      get
      {
        return this.mySortBy;
      }
      set
      {
        this.mySortBy = value;
      }
    }

    //protected override ArrayList GetFilterOptionsText()
    //{
    //  ArrayList filterOptionsText = base.GetFilterOptionsText();
    //  string str1 = this.myItemCode.BuildReadableText(false);
    //  if (str1.Length > 0)
    //    filterOptionsText.Add((object) str1);
    //  string str2 = this.myItemGroup.BuildReadableText(false);
    //  if (str2.Length > 0)
    //    filterOptionsText.Add((object) str2);
    //  string str3 = this.myItemType.BuildReadableText(false);
    //  if (str3.Length > 0)
    //    filterOptionsText.Add((object) str3);
    //  string str4 = this.myLocation.BuildReadableText(false);
    //  if (str4.Length > 0)
    //    filterOptionsText.Add((object) str4);
    //  string str5 = this.myProjNo.BuildReadableText(false);
    //  if (str5.Length > 0)
    //    filterOptionsText.Add((object) str5);
    //  string str6 = this.myDeptNo.BuildReadableText(false);
    //  if (str6.Length > 0)
    //    filterOptionsText.Add((object) str6);
    //  filterOptionsText.Add((object) this.GetCancelledDocumentOptionText(this.myIsPrintCancelled));
    //  if (filterOptionsText.Count == 0)
    //    filterOptionsText.Add((object) Localizer.GetString((Enum) ReportCriteriaStringId.NoFilter, new object[0]));
    //  return filterOptionsText;
    //}

    //protected override ArrayList GetReportOptionsText()
    //{
    //  ArrayList reportOptionsText = base.GetReportOptionsText();
    //  reportOptionsText.Add((object) this.GetSortByText(Localizer.GetString((Enum) this.mySortBy, new object[0])));
    //  reportOptionsText.Add((object) this.GetGroupByText(Localizer.GetString((Enum) this.myGroupBy, new object[0])));
    //  return reportOptionsText;
    //}
  }
}
