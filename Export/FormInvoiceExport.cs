﻿using AutoCount.ActivityStream;
using AutoCount.Application;
using AutoCount.Authentication;
using AutoCount.Controller;
using AutoCount.Controls;
using AutoCount.Controls.FilterUI;
using AutoCount.Data;
using AutoCount.Document;
using AutoCount.LicenseControl;
using AutoCount.Options;
using AutoCount.Report;
using AutoCount.Scripting;
using AutoCount.SearchFilter;
using AutoCount.Serialization;
using AutoCount.Utils;
using AutoCount.XtraUtils;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using System;
using AutoCount;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using AutoCount.Invoicing;
using AutoCount.Invoicing.Sales;
using AutoCount.Invoicing.Sales.Invoice;
namespace ProjectSellOut.Export
{
    [SingleInstanceThreadForm]
    public class FormInvoicePrintListing : XtraForm
    {
        private object oldDataSource = new object();
        private ArrayList oldDocKeyList = new ArrayList();
        private IContainer components;
        private PanelHeader panelHeader1;
        private PreviewButton previewButton1;
        private PrintButton printButton1;
        private UCAreaSelector ucAreaSelector1;
        private UCCurrencySelector ucCurrencySelector1;
        private UCDateSelector ucDateSelector1;
        private UCDebtorSelector ucDebtorSelector1;
        private UCDebtorTypeSelector ucDebtorTypeSelector1;
        private UCInvoiceSelector ucInvoiceSelector1;
        private UCLocationSelector ucLocationSelector1;
        private UCSalesAgentSelector ucSalesAgentSelector1;
        private UCSearchResult ucSearchResult1;
        private InvoiceGrid invoiceGrid1;
        private Bar bar2;
        private BarButtonItem barBtnDesignDocumentStyleReport;
        private BarButtonItem barBtnDesignListingStyleReport;
        private BarDockControl barDockControlBottom;
        private BarDockControl barDockControlLeft;
        private BarDockControl barDockControlRight;
        private BarDockControl barDockControlTop;
        private BarManager barManager1;
        private BarSubItem barSubItem1;
        private CheckEdit chkEditShowCriteria;
        private ComboBoxEdit cbCancelledStatus;
        private ComboBoxEdit cbEditGroupBy;
        private ComboBoxEdit cbEditReportType;
        private ComboBoxEdit cbEditSortBy;
        private ComboBoxEdit cbUOMOptions;
        private GroupControl gbAnalysisOptions;
        private GroupControl gbReportOption;
        private GroupControl groupBox_SearchCriteria;
        private PanelControl panel_Center;
        private PanelControl panelButtons;
        private PanelControl panelCriteria;
        private PanelControl pnOptionBottom;
        private SimpleButton sbtnClose;
        private SimpleButton sbtnInquiry;
        private SimpleButton sbtnToggleOptions;
        private SimpleButton simpleButtonAdvanceSearch;
        private Label label2;
        private Label label1;
        private Label label11;
        private Label label3;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label lblArea;
        private Label lblCancelled;
        private Label lblCurrency;
        private Label lblSalesLocation;
        private SimpleButton btnCriteria;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.Utils.ImageCollection imageCollection32;
        private DevExpress.Utils.ImageCollection imageCollection40;
        private DevExpress.Utils.ImageCollection imageCollection48;
        private DevExpress.Utils.ImageCollection imageCollection56;
        private DevExpress.Utils.ImageCollection imageCollection64;
        private DBSetting myDBSetting;
        private UserSession myUserSession;
        private DataTable myDataTable;
        private InvoiceListingReport myCommand;
        private InvoiceAdvanceCriteria myCriteria;
        private bool myInSearch;
        private InvoiceReportingCriteria myReportingCriteria;
        private bool myLoadAllColumns;
        private ListingGroupByOption oldGroupBy;
        private ListingSortByOption oldSortBy;
        private int oldReportType;
        private bool oldShowCriteria;
        private bool myFilterByLocation;
        private ScriptObject myScriptObject;
        private MouseDownHelper myMouseDownHelper;
        private InvoiceAdvanceCriteria myAdvancedCriteria;
        private DevExpress.Utils.ImageCollection myCurrentImageCollection;

        protected override void Dispose(bool disposing)
        {
            this.SaveCriteria();
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = (IContainer)new Container();
          //  //componentResourceManager //componentResourceManager = new //componentResourceManager(typeof(FormInvoicePrintListing));
            this.panelCriteria = new PanelControl();
            this.gbAnalysisOptions = new GroupControl();
            this.pnOptionBottom = new PanelControl();
            this.label2 = new Label();
            this.cbUOMOptions = new ComboBoxEdit();
            this.gbReportOption = new GroupControl();
            this.label8 = new Label();
            this.label7 = new Label();
            this.cbEditGroupBy = new ComboBoxEdit();
            this.cbEditSortBy = new ComboBoxEdit();
            this.chkEditShowCriteria = new CheckEdit();
            this.cbEditReportType = new ComboBoxEdit();
            this.label9 = new Label();
            this.groupBox_SearchCriteria = new GroupControl();
            this.ucLocationSelector1 = new UCLocationSelector();
            this.lblSalesLocation = new Label();
            this.ucInvoiceSelector1 = new UCInvoiceSelector();
            this.lblArea = new Label();
            this.ucAreaSelector1 = new UCAreaSelector();
            this.lblCancelled = new Label();
            this.ucCurrencySelector1 = new UCCurrencySelector();
            this.label6 = new Label();
            this.lblCurrency = new Label();
            this.label5 = new Label();
            this.ucSalesAgentSelector1 = new UCSalesAgentSelector();
            this.label1 = new Label();
            this.ucDebtorSelector1 = new UCDebtorSelector();
            this.label11 = new Label();
            this.ucDebtorTypeSelector1 = new UCDebtorTypeSelector();
            this.label3 = new Label();
            this.ucDateSelector1 = new UCDateSelector();
            this.cbCancelledStatus = new ComboBoxEdit();
            this.simpleButtonAdvanceSearch = new SimpleButton();
            this.invoiceGrid1 = new InvoiceGrid();
            this.ucSearchResult1 = new UCSearchResult();
            this.panel_Center = new PanelControl();
            this.panelButtons = new PanelControl();
            this.btnCriteria = new SimpleButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.printButton1 = new PrintButton();
            this.previewButton1 = new PreviewButton();
            this.sbtnToggleOptions = new SimpleButton();
            this.sbtnClose = new SimpleButton();
            this.sbtnInquiry = new SimpleButton();
            this.barManager1 = new BarManager(this.components);
            this.bar2 = new Bar();
            this.barSubItem1 = new BarSubItem();
            this.barBtnDesignDocumentStyleReport = new BarButtonItem();
            this.barBtnDesignListingStyleReport = new BarButtonItem();
            this.barDockControlTop = new BarDockControl();
            this.barDockControlBottom = new BarDockControl();
            this.barDockControlLeft = new BarDockControl();
            this.barDockControlRight = new BarDockControl();
            this.panelHeader1 = new PanelHeader();
            this.imageCollection32 = new DevExpress.Utils.ImageCollection(this.components);
            this.imageCollection40 = new DevExpress.Utils.ImageCollection(this.components);
            this.imageCollection48 = new DevExpress.Utils.ImageCollection(this.components);
            this.imageCollection56 = new DevExpress.Utils.ImageCollection(this.components);
            this.imageCollection64 = new DevExpress.Utils.ImageCollection(this.components);
            this.panelCriteria.BeginInit();
            this.panelCriteria.SuspendLayout();
            this.gbAnalysisOptions.BeginInit();
            this.gbAnalysisOptions.SuspendLayout();
            this.pnOptionBottom.BeginInit();
            this.pnOptionBottom.SuspendLayout();
            this.cbUOMOptions.Properties.BeginInit();
            this.gbReportOption.BeginInit();
            this.gbReportOption.SuspendLayout();
            this.cbEditGroupBy.Properties.BeginInit();
            this.cbEditSortBy.Properties.BeginInit();
            this.chkEditShowCriteria.Properties.BeginInit();
            this.cbEditReportType.Properties.BeginInit();
            this.groupBox_SearchCriteria.BeginInit();
            this.groupBox_SearchCriteria.SuspendLayout();
            this.cbCancelledStatus.Properties.BeginInit();
            this.panel_Center.BeginInit();
            this.panel_Center.SuspendLayout();
            this.panelButtons.BeginInit();
            this.panelButtons.SuspendLayout();
            this.imageCollection1.BeginInit();
            this.barManager1.BeginInit();
            this.imageCollection32.BeginInit();
            this.imageCollection40.BeginInit();
            this.imageCollection48.BeginInit();
            this.imageCollection56.BeginInit();
            this.imageCollection64.BeginInit();
            this.SuspendLayout();
            this.panelCriteria.BorderStyle = BorderStyles.NoBorder;
            this.panelCriteria.Controls.Add((Control)this.gbAnalysisOptions);
            this.panelCriteria.Controls.Add((Control)this.gbReportOption);
            this.panelCriteria.Controls.Add((Control)this.cbEditReportType);
            this.panelCriteria.Controls.Add((Control)this.label9);
            this.panelCriteria.Controls.Add((Control)this.groupBox_SearchCriteria);
            this.panelCriteria.Controls.Add((Control)this.simpleButtonAdvanceSearch);
            //componentResourceManager.ApplyResources((object)this.panelCriteria, "panelCriteria");
            this.panelCriteria.Name = "panelCriteria";
            this.gbAnalysisOptions.Controls.Add((Control)this.pnOptionBottom);
            //componentResourceManager.ApplyResources((object)this.gbAnalysisOptions, "gbAnalysisOptions");
            this.gbAnalysisOptions.Name = "gbAnalysisOptions";
            this.pnOptionBottom.BorderStyle = BorderStyles.NoBorder;
            this.pnOptionBottom.Controls.Add((Control)this.label2);
            this.pnOptionBottom.Controls.Add((Control)this.cbUOMOptions);
            //componentResourceManager.ApplyResources((object)this.pnOptionBottom, "pnOptionBottom");
            this.pnOptionBottom.Name = "pnOptionBottom";
            //componentResourceManager.ApplyResources((object)this.label2, "label2");
            this.label2.Name = "label2";
            //componentResourceManager.ApplyResources((object)this.cbUOMOptions, "cbUOMOptions");
            this.cbUOMOptions.Name = "cbUOMOptions";
        //    this.cbUOMOptions.Properties.Buttons.AddRange(new EditorButton[1]
        //    {
        //new EditorButton((ButtonPredefines) //componentResourceManager.GetObject("cbUOMOptions.Properties.Buttons"))
        //    });
           // this.cbUOMOptions.Properties.NullText = //componentResourceManager.GetString("cbUOMOptions.Properties.NullText");
            this.cbUOMOptions.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            this.cbUOMOptions.SelectedIndexChanged += new EventHandler(this.cbUOMOptions_SelectedIndexChanged);
            this.gbReportOption.Controls.Add((Control)this.label8);
            this.gbReportOption.Controls.Add((Control)this.label7);
            this.gbReportOption.Controls.Add((Control)this.cbEditGroupBy);
            this.gbReportOption.Controls.Add((Control)this.cbEditSortBy);
            this.gbReportOption.Controls.Add((Control)this.chkEditShowCriteria);
            //componentResourceManager.ApplyResources((object)this.gbReportOption, "gbReportOption");
            this.gbReportOption.Name = "gbReportOption";
            //componentResourceManager.ApplyResources((object)this.label8, "label8");
            this.label8.BackColor = Color.Transparent;
            this.label8.Name = "label8";
            //componentResourceManager.ApplyResources((object)this.label7, "label7");
            this.label7.BackColor = Color.Transparent;
            this.label7.Name = "label7";
            //componentResourceManager.ApplyResources((object)this.cbEditGroupBy, "cbEditGroupBy");
            this.cbEditGroupBy.Name = "cbEditGroupBy";
        //    this.cbEditGroupBy.Properties.Buttons.AddRange(new EditorButton[1]
        //    {
        //new EditorButton((ButtonPredefines) //componentResourceManager.GetObject("cbEditGroupBy.Properties.Buttons"))
        //    });
            this.cbEditGroupBy.Properties.DropDownRows = 10;
            this.cbEditGroupBy.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            this.cbEditGroupBy.SelectedIndexChanged += new EventHandler(this.cbEditGroupBy_SelectedIndexChanged);
            //componentResourceManager.ApplyResources((object)this.cbEditSortBy, "cbEditSortBy");
            this.cbEditSortBy.Name = "cbEditSortBy";
        //    this.cbEditSortBy.Properties.Buttons.AddRange(new EditorButton[1]
        //    {
        //new EditorButton((ButtonPredefines) //componentResourceManager.GetObject("cbEditSortBy.Properties.Buttons"))
        //    });
            this.cbEditSortBy.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            this.cbEditSortBy.SelectedIndexChanged += new EventHandler(this.cbEditSortBy_SelectedIndexChanged);
            //componentResourceManager.ApplyResources((object)this.chkEditShowCriteria, "chkEditShowCriteria");
            this.chkEditShowCriteria.Name = "chkEditShowCriteria";
          //  this.chkEditShowCriteria.Properties.Appearance.BackColor = (Color)//componentResourceManager.GetObject("chkEditShowCriteria.Properties.Appearance.BackColor");
            this.chkEditShowCriteria.Properties.Appearance.Options.UseBackColor = true;
            this.chkEditShowCriteria.Properties.Caption = "Show Criteria"; //componentResourceManager.GetString("chkEditShowCriteria.Properties.Caption");
            this.chkEditShowCriteria.CheckedChanged += new EventHandler(this.chkEditShowCriteria_CheckedChanged);
            //componentResourceManager.ApplyResources((object)this.cbEditReportType, "cbEditReportType");
            this.cbEditReportType.Name = "cbEditReportType";
        //    this.cbEditReportType.Properties.Buttons.AddRange(new EditorButton[1]
        //    {
        //new EditorButton((ButtonPredefines) //componentResourceManager.GetObject("cbEditReportType.Properties.Buttons"))
        //    });
            this.cbEditReportType.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            this.cbEditReportType.SelectedIndexChanged += new EventHandler(this.cbEditReportType_SelectedIndexChanged);
            //componentResourceManager.ApplyResources((object)this.label9, "label9");
            this.label9.BackColor = Color.Transparent;
            this.label9.Name = "label9";
            this.groupBox_SearchCriteria.Controls.Add((Control)this.ucLocationSelector1);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.lblSalesLocation);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.ucInvoiceSelector1);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.lblArea);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.ucAreaSelector1);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.lblCancelled);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.ucCurrencySelector1);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.label6);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.lblCurrency);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.label5);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.ucSalesAgentSelector1);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.label1);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.ucDebtorSelector1);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.label11);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.ucDebtorTypeSelector1);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.label3);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.ucDateSelector1);
            this.groupBox_SearchCriteria.Controls.Add((Control)this.cbCancelledStatus);
            //componentResourceManager.ApplyResources((object)this.groupBox_SearchCriteria, "groupBox_SearchCriteria");
            this.groupBox_SearchCriteria.Name = "groupBox_SearchCriteria";
            //componentResourceManager.ApplyResources((object)this.ucLocationSelector1, "ucLocationSelector1");
            this.ucLocationSelector1.LocationType = UCLocationType.ItemLocation;
            this.ucLocationSelector1.Name = "ucLocationSelector1";
            //componentResourceManager.ApplyResources((object)this.lblSalesLocation, "lblSalesLocation");
            this.lblSalesLocation.BackColor = Color.Transparent;
            this.lblSalesLocation.Name = "lblSalesLocation";
            //componentResourceManager.ApplyResources((object)this.ucInvoiceSelector1, "ucInvoiceSelector1");
            this.ucInvoiceSelector1.Name = "ucInvoiceSelector1";
            //componentResourceManager.ApplyResources((object)this.lblArea, "lblArea");
            this.lblArea.BackColor = Color.Transparent;
            this.lblArea.Name = "lblArea";
            //componentResourceManager.ApplyResources((object)this.ucAreaSelector1, "ucAreaSelector1");
            this.ucAreaSelector1.Name = "ucAreaSelector1";
            //componentResourceManager.ApplyResources((object)this.lblCancelled, "lblCancelled");
            this.lblCancelled.BackColor = Color.Transparent;
            this.lblCancelled.Name = "lblCancelled";
            //componentResourceManager.ApplyResources((object)this.ucCurrencySelector1, "ucCurrencySelector1");
            this.ucCurrencySelector1.Name = "ucCurrencySelector1";
            //componentResourceManager.ApplyResources((object)this.label6, "label6");
            this.label6.BackColor = Color.Transparent;
            this.label6.Name = "label6";
            //componentResourceManager.ApplyResources((object)this.lblCurrency, "lblCurrency");
            this.lblCurrency.BackColor = Color.Transparent;
            this.lblCurrency.Name = "lblCurrency";
            //componentResourceManager.ApplyResources((object)this.label5, "label5");
            this.label5.BackColor = Color.Transparent;
            this.label5.Name = "label5";
            //componentResourceManager.ApplyResources((object)this.ucSalesAgentSelector1, "ucSalesAgentSelector1");
            this.ucSalesAgentSelector1.Name = "ucSalesAgentSelector1";
            //componentResourceManager.ApplyResources((object)this.label1, "label1");
            this.label1.BackColor = Color.Transparent;
            this.label1.Name = "label1";
            //componentResourceManager.ApplyResources((object)this.ucDebtorSelector1, "ucDebtorSelector1");
            this.ucDebtorSelector1.Name = "ucDebtorSelector1";
            //componentResourceManager.ApplyResources((object)this.label11, "label11");
            this.label11.BackColor = Color.Transparent;
            this.label11.Name = "label11";
            //componentResourceManager.ApplyResources((object)this.ucDebtorTypeSelector1, "ucDebtorTypeSelector1");
            this.ucDebtorTypeSelector1.Name = "ucDebtorTypeSelector1";
            //componentResourceManager.ApplyResources((object)this.label3, "label3");
            this.label3.BackColor = Color.Transparent;
            this.label3.Name = "label3";
            //componentResourceManager.ApplyResources((object)this.ucDateSelector1, "ucDateSelector1");
            this.ucDateSelector1.Name = "ucDateSelector1";
            //componentResourceManager.ApplyResources((object)this.cbCancelledStatus, "cbCancelledStatus");
            this.cbCancelledStatus.Name = "cbCancelledStatus";
        //    this.cbCancelledStatus.Properties.Buttons.AddRange(new EditorButton[1]
        //    {
        //new EditorButton((ButtonPredefines) //componentResourceManager.GetObject("cbCancelledStatus.Properties.Buttons"))
        //    });
            this.cbCancelledStatus.Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
            this.cbCancelledStatus.SelectedIndexChanged += new EventHandler(this.cbCancelledStatus_SelectedIndexChanged);
            //componentResourceManager.ApplyResources((object)this.simpleButtonAdvanceSearch, "simpleButtonAdvanceSearch");
            this.simpleButtonAdvanceSearch.Name = "simpleButtonAdvanceSearch";
            this.simpleButtonAdvanceSearch.Click += new EventHandler(this.simpleButtonAdvanceSearch_Click);
            //componentResourceManager.ApplyResources((object)this.invoiceGrid1, "invoiceGrid1");
            this.invoiceGrid1.Name = "invoiceGrid1";
            this.invoiceGrid1.ShowTickColumn = true;
            this.invoiceGrid1.ShowTransferColumnsInCustomizationForm = false;
            this.invoiceGrid1.TickColumnFieldName = "ToBeUpdate";
            this.invoiceGrid1.ReloadAllColumnsEvent += new EventHandler(this.ReloadAllColumns);
            this.invoiceGrid1.DrawGroupPanelEvent += new CustomDrawEventHandler(this.invoiceGrid1_DrawGroupPanelEvent);
            //componentResourceManager.ApplyResources((object)this.ucSearchResult1, "ucSearchResult1");
            //this.ucSearchResult1.Appearance.BackColor = (Color)//componentResourceManager.GetObject("ucSearchResult1.Appearance.BackColor");
            //this.ucSearchResult1.Appearance.BackColor2 = (Color)//componentResourceManager.GetObject("ucSearchResult1.Appearance.BackColor2");
            //this.ucSearchResult1.Appearance.GradientMode = (LinearGradientMode)//componentResourceManager.GetObject("ucSearchResult1.Appearance.GradientMode");
            this.ucSearchResult1.Appearance.Options.UseBackColor = true;
            this.ucSearchResult1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ucSearchResult1.Name = "ucSearchResult1";
            this.panel_Center.BorderStyle = BorderStyles.NoBorder;
            this.panel_Center.Controls.Add((Control)this.invoiceGrid1);
            this.panel_Center.Controls.Add((Control)this.ucSearchResult1);
            //componentResourceManager.ApplyResources((object)this.panel_Center, "panel_Center");
            this.panel_Center.Name = "panel_Center";
            this.panelButtons.BorderStyle = BorderStyles.NoBorder;
            this.panelButtons.Controls.Add((Control)this.btnCriteria);
            this.panelButtons.Controls.Add((Control)this.printButton1);
            this.panelButtons.Controls.Add((Control)this.previewButton1);
            this.panelButtons.Controls.Add((Control)this.sbtnToggleOptions);
            this.panelButtons.Controls.Add((Control)this.sbtnClose);
            this.panelButtons.Controls.Add((Control)this.sbtnInquiry);
            //componentResourceManager.ApplyResources((object)this.panelButtons, "panelButtons");
            this.panelButtons.Name = "panelButtons";
            this.btnCriteria.Appearance.Options.UseTextOptions = true;
            this.btnCriteria.Appearance.TextOptions.WordWrap = WordWrap.Wrap;
            //componentResourceManager.ApplyResources((object)this.btnCriteria, "btnCriteria");
            this.btnCriteria.ImageOptions.ImageIndex = 4;
            this.btnCriteria.ImageOptions.ImageList = (object)this.imageCollection1;
            this.btnCriteria.ImageOptions.Location = ImageLocation.MiddleLeft;
            this.btnCriteria.Name = "btnCriteria";
            this.btnCriteria.Click += new EventHandler(this.btnCriteria_Click);
            //componentResourceManager.ApplyResources((object)this.imageCollection1, "imageCollection1");
           // this.imageCollection1.ImageStream = (ImageCollectionStreamer)//componentResourceManager.GetObject("imageCollection1.ImageStream");
            this.imageCollection1.IsDpiAware = DefaultBoolean.True;
            this.imageCollection1.Images.SetKeyName(0, "inquiry.png");
            this.imageCollection1.Images.SetKeyName(1, "preview.png");
            this.imageCollection1.Images.SetKeyName(2, "print.png");
            this.imageCollection1.Images.SetKeyName(3, "hide option.png");
            this.imageCollection1.Images.SetKeyName(4, "criteria.png");
            this.imageCollection1.Images.SetKeyName(5, "close.png");
            this.printButton1.Appearance.Options.UseTextOptions = true;
            this.printButton1.Appearance.TextOptions.WordWrap = WordWrap.Wrap;
            this.printButton1.ImageIndex = 2;
            this.printButton1.ImageList = (object)this.imageCollection1;
            this.printButton1.ImageLocation = ImageLocation.MiddleLeft;
            //componentResourceManager.ApplyResources((object)this.printButton1, "printButton1");
            this.printButton1.Name = "printButton1";
            this.printButton1.ReportType = "";
            this.printButton1.Print += new PrintEventHandler(this.printButton1_Print);
            this.previewButton1.Appearance.Options.UseTextOptions = true;
            this.previewButton1.Appearance.TextOptions.WordWrap = WordWrap.Wrap;
            this.previewButton1.ImageIndex = 1;
            this.previewButton1.ImageList = (object)this.imageCollection1;
            this.previewButton1.ImageLocation = ImageLocation.MiddleLeft;
            //componentResourceManager.ApplyResources((object)this.previewButton1, "previewButton1");
            this.previewButton1.Name = "previewButton1";
            this.previewButton1.ReportType = "";
            this.previewButton1.Preview += new PrintEventHandler(this.previewButton1_Preview);
            this.sbtnToggleOptions.Appearance.Options.UseTextOptions = true;
            this.sbtnToggleOptions.Appearance.TextOptions.WordWrap = WordWrap.Wrap;
            //componentResourceManager.ApplyResources((object)this.sbtnToggleOptions, "sbtnToggleOptions");
            this.sbtnToggleOptions.ImageOptions.ImageIndex = 3;
            this.sbtnToggleOptions.ImageOptions.ImageList = (object)this.imageCollection1;
            this.sbtnToggleOptions.ImageOptions.Location = ImageLocation.MiddleLeft;
            this.sbtnToggleOptions.Name = "sbtnToggleOptions";
            this.sbtnToggleOptions.Click += new EventHandler(this.sbtnToggleOptions_Click);
            this.sbtnClose.Appearance.Options.UseTextOptions = true;
            this.sbtnClose.Appearance.TextOptions.WordWrap = WordWrap.Wrap;
            this.sbtnClose.DialogResult = DialogResult.Cancel;
            this.sbtnClose.ImageOptions.ImageIndex = 5;
            this.sbtnClose.ImageOptions.ImageList = (object)this.imageCollection1;
            this.sbtnClose.ImageOptions.Location = ImageLocation.MiddleLeft;
            //componentResourceManager.ApplyResources((object)this.sbtnClose, "sbtnClose");
            this.sbtnClose.Name = "sbtnClose";
            this.sbtnClose.Click += new EventHandler(this.sbtnClose_Click);
            this.sbtnInquiry.Appearance.Options.UseTextOptions = true;
            this.sbtnInquiry.Appearance.TextOptions.WordWrap = WordWrap.Wrap;
            this.sbtnInquiry.ImageOptions.ImageIndex = 0;
            this.sbtnInquiry.ImageOptions.ImageList = (object)this.imageCollection1;
            this.sbtnInquiry.ImageOptions.Location = ImageLocation.MiddleLeft;
            //componentResourceManager.ApplyResources((object)this.sbtnInquiry, "sbtnInquiry");
            this.sbtnInquiry.Name = "sbtnInquiry";
            this.sbtnInquiry.Click += new EventHandler(this.sbtnInquiry_Click);
            this.barManager1.Bars.AddRange(new Bar[1] { this.bar2 });
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = (Control)this;
            this.barManager1.Items.AddRange(new BarItem[3]
            {
        (BarItem) this.barSubItem1,
        (BarItem) this.barBtnDesignListingStyleReport,
        (BarItem) this.barBtnDesignDocumentStyleReport
            });
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new LinkPersistInfo[1]
            {
        new LinkPersistInfo((BarItem) this.barSubItem1, true)
            });
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            //componentResourceManager.ApplyResources((object)this.bar2, "bar2");
            //componentResourceManager.ApplyResources((object)this.barSubItem1, "barSubItem1");
            this.barSubItem1.Id = 0;
            this.barSubItem1.LinksPersistInfo.AddRange(new LinkPersistInfo[2]
            {
        new LinkPersistInfo((BarItem) this.barBtnDesignDocumentStyleReport),
        new LinkPersistInfo((BarItem) this.barBtnDesignListingStyleReport)
            });
            this.barSubItem1.MergeOrder = 1;
            this.barSubItem1.Name = "barSubItem1";
            //componentResourceManager.ApplyResources((object)this.barBtnDesignDocumentStyleReport, "barBtnDesignDocumentStyleReport");
            this.barBtnDesignDocumentStyleReport.Id = 2;
            this.barBtnDesignDocumentStyleReport.Name = "barBtnDesignDocumentStyleReport";
            this.barBtnDesignDocumentStyleReport.ItemClick += new ItemClickEventHandler(this.barBtnDesignDocumentStyleReport_ItemClick);
            //componentResourceManager.ApplyResources((object)this.barBtnDesignListingStyleReport, "barBtnDesignListingStyleReport");
            this.barBtnDesignListingStyleReport.Id = 1;
            this.barBtnDesignListingStyleReport.Name = "barBtnDesignListingStyleReport";
            this.barBtnDesignListingStyleReport.ItemClick += new ItemClickEventHandler(this.barBtnDesignListingStyleReport_ItemClick);
            this.barDockControlTop.CausesValidation = false;
            //componentResourceManager.ApplyResources((object)this.barDockControlTop, "barDockControlTop");
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlBottom.CausesValidation = false;
            //componentResourceManager.ApplyResources((object)this.barDockControlBottom, "barDockControlBottom");
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlLeft.CausesValidation = false;
            //componentResourceManager.ApplyResources((object)this.barDockControlLeft, "barDockControlLeft");
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlRight.CausesValidation = false;
            //componentResourceManager.ApplyResources((object)this.barDockControlRight, "barDockControlRight");
            this.barDockControlRight.Manager = this.barManager1;
            //componentResourceManager.ApplyResources((object)this.panelHeader1, "panelHeader1");
            this.panelHeader1.HelpNavigator = HelpNavigator.AssociateIndex;
            this.panelHeader1.HelpTopicId = "Invoice";
            this.panelHeader1.Name = "panelHeader1";
            //componentResourceManager.ApplyResources((object)this.imageCollection32, "imageCollection32");
          //  this.imageCollection32.ImageStream = (ImageCollectionStreamer)//componentResourceManager.GetObject("imageCollection32.ImageStream");
            this.imageCollection32.Images.SetKeyName(0, "(160)32inquiry.png");
            this.imageCollection32.Images.SetKeyName(1, "(160)32preview.png");
            this.imageCollection32.Images.SetKeyName(2, "(160)32print.png");
            this.imageCollection32.Images.SetKeyName(3, "(160)32hide option.png");
            this.imageCollection32.Images.SetKeyName(4, "(160)32criteria.png");
            this.imageCollection32.Images.SetKeyName(5, "(160)32close.png");
            //componentResourceManager.ApplyResources((object)this.imageCollection40, "imageCollection40");
           // this.imageCollection40.ImageStream = (ImageCollectionStreamer)//componentResourceManager.GetObject("imageCollection40.ImageStream");
            this.imageCollection40.Images.SetKeyName(0, "(160)40inquiry.png");
            this.imageCollection40.Images.SetKeyName(1, "(160)40preview.png");
            this.imageCollection40.Images.SetKeyName(2, "(160)40print.png");
            this.imageCollection40.Images.SetKeyName(3, "(160)40hide option.png");
            this.imageCollection40.Images.SetKeyName(4, "(160)40criteria.png");
            this.imageCollection40.Images.SetKeyName(5, "(160)40close.png");
            //componentResourceManager.ApplyResources((object)this.imageCollection48, "imageCollection48");
           // this.imageCollection48.ImageStream = (ImageCollectionStreamer)//componentResourceManager.GetObject("imageCollection48.ImageStream");
            this.imageCollection48.Images.SetKeyName(0, "(160)48inquiry.png");
            this.imageCollection48.Images.SetKeyName(1, "(160)48preview.png");
            this.imageCollection48.Images.SetKeyName(2, "(160)48print.png");
            this.imageCollection48.Images.SetKeyName(3, "(160)48hide option.png");
            this.imageCollection48.Images.SetKeyName(4, "(160)48criteria.png");
            this.imageCollection48.Images.SetKeyName(5, "(160)48close.png");
            //componentResourceManager.ApplyResources((object)this.imageCollection56, "imageCollection56");
           // this.imageCollection56.ImageStream = (ImageCollectionStreamer)//componentResourceManager.GetObject("imageCollection56.ImageStream");
            this.imageCollection56.Images.SetKeyName(0, "(160)56inquiry.png");
            this.imageCollection56.Images.SetKeyName(1, "(160)56preview.png");
            this.imageCollection56.Images.SetKeyName(2, "(160)56print.png");
            this.imageCollection56.Images.SetKeyName(3, "(160)56hide option.png");
            this.imageCollection56.Images.SetKeyName(4, "(160)56criteria.png");
            this.imageCollection56.Images.SetKeyName(5, "(160)56close.png");
            //componentResourceManager.ApplyResources((object)this.imageCollection64, "imageCollection64");
          //  this.imageCollection64.ImageStream = (ImageCollectionStreamer)//componentResourceManager.GetObject("imageCollection64.ImageStream");
            this.imageCollection64.Images.SetKeyName(0, "(160)56inquiry.png");
            this.imageCollection64.Images.SetKeyName(1, "(160)64preview.png");
            this.imageCollection64.Images.SetKeyName(2, "(160)64print.png");
            this.imageCollection64.Images.SetKeyName(3, "(160)56hide option.png");
            this.imageCollection64.Images.SetKeyName(4, "(160)56criteria.png");
            this.imageCollection64.Images.SetKeyName(5, "(160)64close.png");
            //componentResourceManager.ApplyResources((object)this, "$this");
           // this.AutoScaleMode = AutoScaleMode.Dpi;
          //  this.CancelButton = (IButtonControl)this.sbtnClose;
            this.Controls.Add((Control)this.panel_Center);
            this.Controls.Add((Control)this.panelButtons);
            this.Controls.Add((Control)this.panelCriteria);
            this.Controls.Add((Control)this.panelHeader1);
            this.Controls.Add((Control)this.barDockControlLeft);
            this.Controls.Add((Control)this.barDockControlRight);
            this.Controls.Add((Control)this.barDockControlBottom);
            this.Controls.Add((Control)this.barDockControlTop);
            this.KeyPreview = true;
            this.Name = nameof(FormInvoicePrintListing);
            this.ShowInTaskbar = false;
            this.SizeGripStyle = SizeGripStyle.Hide;
            this.Activated += new EventHandler(this.FormInvoicePrintListing_Activated);
            this.Closing += new CancelEventHandler(this.FormInvList_Closing);
            this.Load += new EventHandler(this.FormInvPrintSearch_Load);
            this.KeyDown += new KeyEventHandler(this.FormInvPrintSearch_KeyDown);
            this.panelCriteria.EndInit();
            this.panelCriteria.ResumeLayout(false);
            this.panelCriteria.PerformLayout();
            this.gbAnalysisOptions.EndInit();
            this.gbAnalysisOptions.ResumeLayout(false);
            this.pnOptionBottom.EndInit();
            this.pnOptionBottom.ResumeLayout(false);
            this.pnOptionBottom.PerformLayout();
            this.cbUOMOptions.Properties.EndInit();
            this.gbReportOption.EndInit();
            this.gbReportOption.ResumeLayout(false);
            this.gbReportOption.PerformLayout();
            this.cbEditGroupBy.Properties.EndInit();
            this.cbEditSortBy.Properties.EndInit();
            this.chkEditShowCriteria.Properties.EndInit();
            this.cbEditReportType.Properties.EndInit();
            this.groupBox_SearchCriteria.EndInit();
            this.groupBox_SearchCriteria.ResumeLayout(false);
            this.groupBox_SearchCriteria.PerformLayout();
            this.cbCancelledStatus.Properties.EndInit();
            this.panel_Center.EndInit();
            this.panel_Center.ResumeLayout(false);
            this.panelButtons.EndInit();
            this.panelButtons.ResumeLayout(false);
            this.imageCollection1.EndInit();
            this.barManager1.EndInit();
            this.imageCollection32.EndInit();
            this.imageCollection40.EndInit();
            this.imageCollection48.EndInit();
            this.imageCollection56.EndInit();
            this.imageCollection64.EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        public DataTable ResultTable
        {
            get
            {
                return this.myDataTable;
            }
        }

        public InvoiceReportingCriteria InvoiceReportingCriteria
        {
            get
            {
                return this.myReportingCriteria;
            }
        }

        public PanelControl PanelCriteria
        {
            get
            {
                return this.panelCriteria;
            }
        }

        public PanelControl PanelButtons
        {
            get
            {
                return this.panelButtons;
            }
        }

        public InvoiceGrid InvoiceGrid
        {
            get
            {
                return this.invoiceGrid1;
            }
        }

        public SimpleButton ButtonInquiry
        {
            get
            {
                return this.sbtnInquiry;
            }
        }

        public PreviewButton ButtonPreview
        {
            get
            {
                return this.previewButton1;
            }
        }

        public PrintButton ButtonPrint
        {
            get
            {
                return this.printButton1;
            }
        }

        public SimpleButton ButtonToggleOptions
        {
            get
            {
                return this.sbtnToggleOptions;
            }
        }

        public SimpleButton ButtonClose
        {
            get
            {
                return this.sbtnClose;
            }
        }

        public UCDateSelector DateSelector
        {
            get
            {
                return this.ucDateSelector1;
            }
        }

        public UCInvoiceSelector InvoiceSelector
        {
            get
            {
                return this.ucInvoiceSelector1;
            }
        }

        public UCDebtorSelector DebtorSelector
        {
            get
            {
                return this.ucDebtorSelector1;
            }
        }

        public UCDebtorTypeSelector DebtorTypeSelector
        {
            get
            {
                return this.ucDebtorTypeSelector1;
            }
        }

        public UCSalesAgentSelector SalesAgentSelector
        {
            get
            {
                return this.ucSalesAgentSelector1;
            }
        }

        public UCCurrencySelector CurrencySelector
        {
            get
            {
                return this.ucCurrencySelector1;
            }
        }

        public UCAreaSelector AreaSelector
        {
            get
            {
                return this.ucAreaSelector1;
            }
        }

        public UCLocationSelector LocationSelector
        {
            get
            {
                return this.ucLocationSelector1;
            }
        }

        public ComboBoxEdit CancelStatusComboBoxEdit
        {
            get
            {
                return this.cbCancelledStatus;
            }
        }

        public void Inquiry()
        {
            if (!this.sbtnInquiry.Enabled)
                return;
            this.sbtnInquiry.PerformClick();
        }

        public FormInvoicePrintListing(UserSession userSession)
        {
            this.InitializeComponent();
            this.AfterInitializeComponent();
            this.myUserSession = userSession;
            this.myDBSetting = this.myUserSession.DBSetting;
            this.myScriptObject = ScriptManager.CreateObject(this.myDBSetting, "InvoiceListing");
            this.invoiceGrid1.Initialize(this.myUserSession);
            this.cbEditGroupBy.Properties.Items.Clear();
            this.cbEditSortBy.Properties.Items.Clear();
            this.cbEditReportType.Properties.Items.Clear();
            this.cbCancelledStatus.Properties.Items.Clear();
            this.cbUOMOptions.Properties.Items.Clear();
            this.cbEditGroupBy.Properties.Items.AddRange((object[])AutoCount.Localization.Localizer.GetEnumStrings(typeof(ListingGroupByOption)));
            this.cbEditSortBy.Properties.Items.AddRange((object[])AutoCount.Localization.Localizer.GetEnumStrings(typeof(ListingSortByOption)));
            this.cbEditReportType.Properties.Items.AddRange((object[])AutoCount.Localization.Localizer.GetEnumStrings(typeof(InvoiceListingReportTypeOption)));
            this.cbCancelledStatus.Properties.Items.AddRange((object[])AutoCount.Localization.Localizer.GetEnumStrings(typeof(CancelledDocumentOption)));
            this.cbUOMOptions.Properties.Items.AddRange((object[])AutoCount.Localization.Localizer.GetEnumStrings(typeof(UOMOption)));
            this.myCommand = InvoiceListingReport.Create(this.myUserSession);
            this.myDataTable = new DataTable();
            this.invoiceGrid1.DataSource = (object)this.myDataTable;
            this.LoadCriteria();
            this.ucSearchResult1.Initialize(this.invoiceGrid1.GridView, "ToBeUpdate");
            this.Tag = (object)EnterKeyMessageFilter.NoFilter;
            this.InitUserControls();
            this.RefreshDesignReport();
            this.myMouseDownHelper = new MouseDownHelper();
            this.myMouseDownHelper.Init(this.invoiceGrid1.GridView);
            this.invoiceGrid1.GridView.DoubleClick += new EventHandler(this.GridView_DoubleClick);
            AccessRightUI.GetOrCreate(this.myUserSession).AddListener(new AccessRightListenerDelegate(this.RefreshDesignReport), (Component)this);
            AutoCount.Utils.HelpProvider.SetHelpTopic(this.panelHeader1, "Invoice");
            Logger.Log(this.myUserSession, "", 0L, 0L, AutoCount.Localization.Localizer.GetString((Enum)InvoiceString.OpenedPrintInvoiceListingWindow, (object)this.myUserSession.LoginUserID), "");
        }

        private void SaveCriteria()
        {
            if (this.myDBSetting == null)
                return;
            AutoCount.Serialization.Utils.SaveCriteriaData(this.myDBSetting, (BaseSetting)this.myReportingCriteria, "InvoiceDocumentListingReport.setting");
        }

        private void LoadCriteria()
        {
            this.myReportingCriteria = (InvoiceReportingCriteria)AutoCount.Serialization.Utils.LoadCriteriaData(this.myDBSetting, "InvoiceDocumentListingReport.setting");
            if (this.myReportingCriteria == null)
                this.myReportingCriteria = new InvoiceReportingCriteria();
            this.cbEditGroupBy.SelectedIndex = (int)this.myReportingCriteria.GroupBy;
            this.cbEditSortBy.SelectedIndex = (int)this.myReportingCriteria.SortBy;
            this.ucSearchResult1.KeepSearchResult = this.myReportingCriteria.KeepSearchResult;
            this.chkEditShowCriteria.Checked = this.myReportingCriteria.IsShowCriteria;
            this.cbCancelledStatus.SelectedIndex = (int)this.myReportingCriteria.CancelledDocumentOption;
            this.cbUOMOptions.SelectedIndex = (int)this.myReportingCriteria.UOMOption;
            this.cbEditReportType.SelectedIndex = (int)this.myReportingCriteria.ReportTypeOption;
        }

        private void InitUserControls()
        {
            this.ucCurrencySelector1.Initialize(this.myUserSession, this.myReportingCriteria.CurrencyFilter);
            this.ucDateSelector1.Initialize(this.myDBSetting, this.myReportingCriteria.DateFilter);
            this.ucDebtorSelector1.Initialize(this.myUserSession, this.myReportingCriteria.DebtorFilter);
            this.ucSalesAgentSelector1.Initialize(this.myUserSession, this.myReportingCriteria.AgentFilter);
            this.ucInvoiceSelector1.Initialize(this.myUserSession, this.myReportingCriteria.DocumentFilter);
            this.ucAreaSelector1.Initialize(this.myUserSession, this.myReportingCriteria.AreaFilter);
            this.ucDebtorTypeSelector1.Initialize(this.myUserSession, this.myReportingCriteria.DebtorTypeFilter);
            this.ucLocationSelector1.Initialize(this.myUserSession, this.myReportingCriteria.SalesLocationFilter);
            this.ucLocationSelector1.LocationType = UCLocationType.SalesLocation;
        }

        private void BasicSearch(bool isSearchAll)
        {
            this.oldDocKeyList = new ArrayList();
            if (this.myInSearch)
                return;
            this.myInSearch = true;
            this.invoiceGrid1.EndEdit();
            this.invoiceGrid1.DataSource = (object)null;
            try
            {
                this.invoiceGrid1.GridControl.MainView.UpdateCurrentRow();
                this.myReportingCriteria.KeepSearchResult = this.ucSearchResult1.KeepSearchResult;
                // this.myCommand.DocumentListingBasicSearch(this.myReportingCriteria, this.invoiceGrid1.BuildSQLColumnList(isSearchAll), this.myDataTable, "ToBeUpdate");
                if (this.myDataTable.Columns.IndexOf("ToBeUpdate") < 0)
                    this.AddNewColumn();
                InvoicingHelper.GenerateMonthYear(this.myDataTable, "DocDate");
                this.invoiceGrid1.DataSource = (object)this.myDataTable;
                this.myAdvancedCriteria = (InvoiceAdvanceCriteria)null;
            }
            catch (AppException ex)
            {
                AppMessage.ShowErrorMessage(ex.Message);
            }
            finally
            {
                this.myInSearch = false;
            }
            FormInvoicePrintListing.FormInquiryEventArgs inquiryEventArgs = new FormInvoicePrintListing.FormInquiryEventArgs(this, this.myDataTable);
            this.myScriptObject.RunMethod("OnFormInquiry", new System.Type[1]
            {
        inquiryEventArgs.GetType()
            }, (object)inquiryEventArgs);
        }



        private void simpleButtonAdvanceSearch_Click(object sender, EventArgs e)
        {
            //his.AdvancedSearch();
        }

        private void FormInvList_Closing(object sender, CancelEventArgs e)
        {
            this.SaveCriteria();
        }

        private void sbtnInquiry_Click(object sender, EventArgs e)
        {
            //if (this.cbEditReportType.Text == "")
            //{
            //    AppMessage.ShowErrorMessage((IWin32Window)this, AutoCount.Localization.Localizer.GetString((Enum)InvoiceStringId.ErrorMessage_ReportTypeDoesNotSelected));
            //}
            //else
            {
                this.BasicSearch(this.myLoadAllColumns);
                Logger.Log(this.myUserSession, "", 0L, 0L, AutoCount.Localization.Localizer.GetString((Enum)InvoiceString.InquiredPrintInvoiceListing, (object)this.myUserSession.LoginUserID), this.myReportingCriteria.ReadableText);
                if (this.cbEditSortBy.Text != "")
                {
                    if (this.cbEditSortBy.SelectedIndex == 1)
                    {
                        this.invoiceGrid1.GridView.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
                        this.invoiceGrid1.GridView.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
                    }
                    else if (this.cbEditSortBy.SelectedIndex == 0)
                    {
                        this.invoiceGrid1.GridView.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
                        this.invoiceGrid1.GridView.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
                    }
                }
                this.ucSearchResult1.CheckAll();
                this.sbtnToggleOptions.Enabled = this.invoiceGrid1.DataSource != null;
                this.btnCriteria.Enabled = this.invoiceGrid1.DataSource != null;
            }
        }

        private void btnCriteria_Click(object sender, EventArgs e)
        {
            FormReportCriteria.ShowCriteria(this.myReportingCriteria.ReadableTextArray);
        }

        private void cbEditGroupBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria == null)
                return;
            this.myReportingCriteria.GroupBy = (ListingGroupByOption)this.cbEditGroupBy.SelectedIndex;
        }

        private void cbEditSortBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria == null)
                return;
            if (this.myReportingCriteria.SortBy != (ListingSortByOption)this.cbEditSortBy.SelectedIndex)
                this.myReportingCriteria.SortBy = (ListingSortByOption)this.cbEditSortBy.SelectedIndex;
            if (!(this.cbEditSortBy.Text != ""))
                return;
            if (this.cbEditSortBy.SelectedIndex == 1)
            {
                this.invoiceGrid1.GridView.Columns["DocDate"].SortOrder = ColumnSortOrder.Ascending;
                this.invoiceGrid1.GridView.Columns["DocNo"].SortOrder = ColumnSortOrder.None;
            }
            else
            {
                if (this.cbEditSortBy.SelectedIndex != 0)
                    return;
                this.invoiceGrid1.GridView.Columns["DocNo"].SortOrder = ColumnSortOrder.Ascending;
                this.invoiceGrid1.GridView.Columns["DocDate"].SortOrder = ColumnSortOrder.None;
            }
        }

        private void cbEditReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cbEditReportType.SelectedIndex == 0)
            {
                this.myReportingCriteria.ReportTypeOption = InvoiceListingReportTypeOption.BatchPrintInvoice;
                this.printButton1.ReportType = "Invoice Document";
                this.printButton1.SetUserSession(this.myUserSession);
                this.previewButton1.ReportType = "Invoice Document";
                this.previewButton1.SetUserSession(this.myUserSession);
            }
            else
            {
                if (this.cbEditReportType.SelectedIndex != 1)
                    return;
                this.myReportingCriteria.ReportTypeOption = InvoiceListingReportTypeOption.PrintInvoiceListing;
                this.printButton1.ReportType = "Invoice Listing";
                this.printButton1.SetUserSession(this.myUserSession);
                this.previewButton1.ReportType = "Invoice Listing";
                this.previewButton1.SetUserSession(this.myUserSession);
            }
        }

        private void cbCancelledStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria == null)
                return;
            this.myReportingCriteria.CancelledDocumentOption = (CancelledDocumentOption)this.cbCancelledStatus.SelectedIndex;
        }

        private void chkEditShowCriteria_CheckedChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria == null)
                return;
            this.myReportingCriteria.IsShowCriteria = this.chkEditShowCriteria.Checked;
        }

        private void FormInvPrintSearch_Load(object sender, EventArgs e)
        {
            if ((double)this.AutoScaleDimensions.Width == 96.0)
                this.myCurrentImageCollection = this.imageCollection32;
            else if ((double)this.AutoScaleDimensions.Width == 120.0)
                this.myCurrentImageCollection = this.imageCollection40;
            else if ((double)this.AutoScaleDimensions.Width == 144.0)
                this.myCurrentImageCollection = this.imageCollection48;
            else if ((double)this.AutoScaleDimensions.Width == 168.0)
                this.myCurrentImageCollection = this.imageCollection56;
            else if ((double)this.AutoScaleDimensions.Width == 192.0)
            {
                this.myCurrentImageCollection = this.imageCollection64;
            }
            else
            {
                ImageHelper.ResizeImageCollection(this.imageCollection1, new SizeF?(this.AutoScaleDimensions));
                this.myCurrentImageCollection = this.imageCollection1;
            }
            this.sbtnInquiry.ImageOptions.ImageList = (object)this.myCurrentImageCollection;
            this.previewButton1.ImageList = (object)this.myCurrentImageCollection;
            this.printButton1.ImageList = (object)this.myCurrentImageCollection;
            this.sbtnToggleOptions.ImageOptions.ImageList = (object)this.myCurrentImageCollection;
            this.btnCriteria.ImageOptions.ImageList = (object)this.myCurrentImageCollection;
            this.sbtnClose.ImageOptions.ImageList = (object)this.myCurrentImageCollection;
            this.SetModuleFeature(ModuleControl.GetOrCreate(this.myDBSetting).ModuleController);
            ModuleControllerNotifier.DefaultNotifier.AddListener(new ModuleControllerListenerDelegate(this.SetModuleFeature), (Component)this);
            this.FormInitialize();
        }

        private void FormInitialize()
        {
            FormInvoicePrintListing.FormInitializeEventArgs initializeEventArgs = new FormInvoicePrintListing.FormInitializeEventArgs(this);
            this.myScriptObject.RunMethod("OnFormInitialize", new System.Type[1]
            {
        initializeEventArgs.GetType()
            }, (object)initializeEventArgs);
        }

        private void sbtnToggleOptions_Click(object sender, EventArgs e)
        {
            this.panelCriteria.Visible = !this.panelCriteria.Visible;
            //if (this.panelCriteria.Visible)
            //    this.sbtnToggleOptions.Text = AutoCount.Localization.Localizer.GetString((Enum)InvoiceStringId.Code_HideOptions);
            //else
            //    this.sbtnToggleOptions.Text = AutoCount.Localization.Localizer.GetString((Enum)InvoiceStringId.Code_ShowOptions);
        }

        private void sbtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormInvPrintSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F9)
            {
                if (!this.sbtnInquiry.Enabled)
                    return;
                this.sbtnInquiry.PerformClick();
            }
            else if (e.KeyCode == Keys.F8)
            {
                if (!this.previewButton1.Enabled)
                    return;
                this.previewButton1.PerformClick();
            }
            else if (e.KeyCode == Keys.F7)
            {
                if (!this.printButton1.Enabled)
                    return;
                this.printButton1.PerformClick();
            }
            else
            {
                if (e.KeyCode != Keys.F6 || !this.sbtnToggleOptions.Enabled)
                    return;
                this.sbtnToggleOptions.PerformClick();
            }
        }

        private void previewButton1_Preview(object sender, AutoCount.Controls.PrintEventArgs e)
        {
            long[] selectedDocKeys = this.GetSelectedDocKeys();
            //if (selectedDocKeys.Length == 0)
            //{
            //    AppMessage.ShowMessage((IWin32Window)this, AutoCount.Localization.Localizer.GetString((Enum)InvoiceStringId.ErrorMessage_InvoiceDoesNotSelected));
            //    this.DialogResult = DialogResult.None;
            //}
            //else 
            if (this.cbEditReportType.SelectedIndex == 0)
            {
                if (!AccessRightUI.GetOrCreate(this.myUserSession).IsAccessible("INV_SALES_IV_DOC_REPORT_PREVIEW", (IWin32Window)this, ""))
                    return;
                if (SystemOptionPolicy.GetOrCreate(this.myUserSession).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
                {
                    foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
                    {
                        if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "IV", Converter.ToInt64(dataRow["DocKey"])) > 0)
                        {
                            if (!AccessRightUI.GetOrCreate(this.myUserSession).IsAccessible("INV_SALES_IV_PRINTED_PREVIEW", (IWin32Window)this, AutoCount.Localization.Localizer.GetString((Enum)InvoicingStringId.NoPermissionToPreviewPrintedDocument, (object)dataRow["DocNo"].ToString())))
                                return;
                        }
                    }
                }
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                bool flag = false;
                DataRow[] dataRowArray = this.myDataTable.Select("ToBeUpdate = TRUE");
                if (dataRowArray.Length == this.oldDocKeyList.Count && this.oldReportType == 0)
                {
                    foreach (DataRow dataRow in dataRowArray)
                    {
                        if (!this.oldDocKeyList.Contains((object)dataRow["DocKey"].ToString()))
                        {
                            flag = true;
                            break;
                        }
                    }
                }
                else
                    flag = true;
                if (flag)
                {
                    this.oldDataSource = this.myCommand.GetReportDataSource(selectedDocKeys, this.InvoiceReportingCriteria);
                    this.oldDocKeyList = new ArrayList();
                    foreach (DataRow dataRow in dataRowArray)
                        this.oldDocKeyList.Add((object)dataRow["DocKey"].ToString());
                    this.oldReportType = 0;
                }
                ReportInfo reportInfo = new ReportInfo(AutoCount.Localization.Localizer.GetString((Enum)InvoiceString.BatchPrintInvoice), "INV_SALES_IV_DOC_REPORT_PRINT", "INV_SALES_IV_DOC_REPORT_EXPORT", this.myReportingCriteria.ReadableText);
                reportInfo.UpdatePrintCountTableName = "IV";
                reportInfo.CheckBeforePrintEvent += new CheckBeforeEventHandler(this.CheckBeforePrint);
                reportInfo.CheckBeforeExportEvent += new CheckBeforeEventHandler(this.CheckBeforeExport);
                ReportTool.PreviewReport(this.previewButton1.ReportType, this.oldDataSource, this.myUserSession, e.DefaultReport, false, this.myCommand.GetBasicReportOption(), reportInfo);
                Cursor.Current = current;
            }
            else
            {
                if (this.cbEditReportType.SelectedIndex != 1 || !AccessRightUI.GetOrCreate(this.myUserSession).IsAccessible("INV_SALES_IV_LISTING_REPORT_PREVIEW", (IWin32Window)this, ""))
                    return;
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                bool flag = false;
                DataRow[] dataRowArray = this.myDataTable.Select("ToBeUpdate = TRUE");
                if (dataRowArray.Length == this.oldDocKeyList.Count && this.oldGroupBy == this.myReportingCriteria.GroupBy && (this.oldSortBy == this.myReportingCriteria.SortBy && this.oldReportType == 1) && this.oldShowCriteria == this.myReportingCriteria.IsShowCriteria)
                {
                    foreach (DataRow dataRow in dataRowArray)
                    {
                        if (!this.oldDocKeyList.Contains((object)dataRow["DocKey"].ToString()))
                        {
                            flag = true;
                            break;
                        }
                    }
                }
                else
                    flag = true;
                if (flag)
                {
                    this.oldGroupBy = this.myReportingCriteria.GroupBy;
                    this.oldSortBy = this.myReportingCriteria.SortBy;
                    this.oldShowCriteria = this.myReportingCriteria.IsShowCriteria;
                    this.oldDataSource = this.myCommand.GetDocumentListingReportDataSource(selectedDocKeys, this.InvoiceReportingCriteria, this.myAdvancedCriteria);
                    this.oldDocKeyList = new ArrayList();
                    foreach (DataRow dataRow in dataRowArray)
                        this.oldDocKeyList.Add((object)dataRow["DocKey"].ToString());
                    this.oldReportType = 1;
                }
                ReportInfo reportInfo = new ReportInfo(AutoCount.Localization.Localizer.GetString((Enum)InvoiceString.PrintInvoiceListing), "INV_SALES_IV_LISTING_REPORT_PRINT", "INV_SALES_IV_LISTING_REPORT_EXPORT", this.myReportingCriteria.ReadableText);
                ReportTool.PreviewReport(this.previewButton1.ReportType, this.oldDataSource, this.myUserSession, e.DefaultReport, false, this.myCommand.GetBasicReportOption(), reportInfo);
                Cursor.Current = current;
            }
        }

        private void printButton1_Print(object sender, AutoCount.Controls.PrintEventArgs e)
        {
            long[] selectedDocKeys = this.GetSelectedDocKeys();
            //if (selectedDocKeys.Length == 0)
            //{
            //    AppMessage.ShowMessage(AutoCount.Localization.Localizer.GetString((Enum)InvoiceStringId.ErrorMessage_InvoiceDoesNotSelected));
            //    this.DialogResult = DialogResult.None;
            //}
            //else 
            if (this.cbEditReportType.SelectedIndex == 0)
            {
                if (!AccessRightUI.GetOrCreate(this.myUserSession).IsAccessible("INV_SALES_IV_DOC_REPORT_PRINT", (IWin32Window)this, ""))
                    return;
                if (SystemOptionPolicy.GetOrCreate(this.myUserSession).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
                {
                    foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
                    {
                        if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "IV", Converter.ToInt64(dataRow["DocKey"])) > 0)
                        {
                            if (!AccessRightUI.GetOrCreate(this.myUserSession).IsAccessible("INV_SALES_IV_PRINTED_PRINT", (IWin32Window)this, AutoCount.Localization.Localizer.GetString((Enum)InvoicingStringId.NoPermissionToPrintPrintedDocument, (object)dataRow["DocNo"].ToString())))
                                return;
                        }
                    }
                }
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                bool flag = false;
                DataRow[] dataRowArray = this.myDataTable.Select("ToBeUpdate = TRUE");
                if (dataRowArray.Length == this.oldDocKeyList.Count && this.oldReportType == 0)
                {
                    foreach (DataRow dataRow in dataRowArray)
                    {
                        if (!this.oldDocKeyList.Contains((object)dataRow["DocKey"].ToString()))
                        {
                            flag = true;
                            break;
                        }
                    }
                }
                else
                    flag = true;
                if (flag)
                {
                    this.oldDataSource = this.myCommand.GetReportDataSource(selectedDocKeys, this.InvoiceReportingCriteria);
                    this.oldDocKeyList = new ArrayList();
                    foreach (DataRow dataRow in dataRowArray)
                        this.oldDocKeyList.Add((object)dataRow["DocKey"].ToString());
                    this.oldReportType = 0;
                }
                ReportTool.PrintReport(this.printButton1.ReportType, this.oldDataSource, this.myUserSession, e.DefaultReport, this.myCommand.GetBasicReportOption(), new ReportInfo(AutoCount.Localization.Localizer.GetString((Enum)InvoiceString.BatchPrintInvoice), "INV_SALES_IV_DOC_REPORT_PRINT", "INV_SALES_IV_DOC_REPORT_EXPORT", this.myReportingCriteria.ReadableText)
                {
                    UpdatePrintCountTableName = "IV"
                });
                Cursor.Current = current;
            }
            else
            {
                if (this.cbEditReportType.SelectedIndex != 1 || !AccessRightUI.GetOrCreate(this.myUserSession).IsAccessible("INV_SALES_IV_LISTING_REPORT_PRINT", (IWin32Window)this, ""))
                    return;
                Cursor current = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                bool flag = false;
                DataRow[] dataRowArray = this.myDataTable.Select("ToBeUpdate = TRUE");
                if (dataRowArray.Length == this.oldDocKeyList.Count && this.oldGroupBy == this.myReportingCriteria.GroupBy && (this.oldSortBy == this.myReportingCriteria.SortBy && this.oldReportType == 1) && this.oldShowCriteria == this.myReportingCriteria.IsShowCriteria)
                {
                    foreach (DataRow dataRow in dataRowArray)
                    {
                        if (!this.oldDocKeyList.Contains((object)dataRow["DocKey"].ToString()))
                        {
                            flag = true;
                            break;
                        }
                    }
                }
                else
                    flag = true;
                if (flag)
                {
                    this.oldGroupBy = this.myReportingCriteria.GroupBy;
                    this.oldSortBy = this.myReportingCriteria.SortBy;
                    this.oldShowCriteria = this.myReportingCriteria.IsShowCriteria;
                    this.oldDataSource = this.myCommand.GetDocumentListingReportDataSource(selectedDocKeys, this.InvoiceReportingCriteria, this.myAdvancedCriteria);
                    this.oldDocKeyList = new ArrayList();
                    foreach (DataRow dataRow in dataRowArray)
                        this.oldDocKeyList.Add((object)dataRow["DocKey"].ToString());
                    this.oldReportType = 1;
                }
                ReportInfo reportInfo = new ReportInfo(AutoCount.Localization.Localizer.GetString((Enum)InvoiceString.PrintInvoiceListing), "INV_SALES_IV_LISTING_REPORT_PRINT", "INV_SALES_IV_LISTING_REPORT_EXPORT", this.myReportingCriteria.ReadableText);
                ReportTool.PrintReport(this.printButton1.ReportType, this.oldDataSource, this.myUserSession, e.DefaultReport, this.myCommand.GetBasicReportOption(), reportInfo);
                Cursor.Current = current;
            }
        }

        private bool CheckBeforePrint(ReportInfo reportInfo)
        {
            if (SystemOptionPolicy.GetOrCreate(this.myUserSession).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
            {
                foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
                {
                    if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "IV", Converter.ToInt64(dataRow["DocKey"])) > 0)
                    {
                        if (!AccessRightUI.GetOrCreate(this.myUserSession).IsAccessible("INV_SALES_IV_PRINTED_PRINT", (IWin32Window)this, AutoCount.Localization.Localizer.GetString((Enum)InvoicingStringId.NoPermissionToPrintPrintedDocument, (object)dataRow["DocNo"].ToString())))
                            return false;
                    }
                }
            }
            return true;
        }

        private bool CheckBeforeExport(ReportInfo reportInfo)
        {
            if (SystemOptionPolicy.GetOrCreate(this.myUserSession).GeneralSystemOptionPolicy.EnablePrintedDocumentAccessRight)
            {
                foreach (DataRow dataRow in this.myDataTable.Select("ToBeUpdate = True"))
                {
                    if (DocumentHelper.GetDocumentPrintCount(this.myDBSetting, "IV", Converter.ToInt64(dataRow["DocKey"])) > 0)
                    {
                        if (!AccessRightUI.GetOrCreate(this.myUserSession).IsAccessible("INV_SALES_IV_PRINTED_EXPORT", (IWin32Window)this, AutoCount.Localization.Localizer.GetString((Enum)InvoicingStringId.NoPermissionToExportPrintedDocument, (object)dataRow["DocNo"].ToString())))
                            return false;
                    }
                }
            }
            return true;
        }

        private void cbUOMOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.myReportingCriteria == null)
                return;
            this.myReportingCriteria.UOMOption = (UOMOption)this.cbUOMOptions.SelectedIndex;
        }

        private void barBtnDesignDocumentStyleReport_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportTool.DesignReport("Invoice Document", this.myUserSession);
        }

        private void barBtnDesignListingStyleReport_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportTool.DesignReport("Invoice Listing", this.myUserSession);
        }

        private void GridView_DoubleClick(object sender, EventArgs e)
        {
            if (!this.myMouseDownHelper.IsLeftMouseDown || !GridViewUtils.GetGridHitInfo((GridView)sender).InRow || !this.myUserSession.AccessRight.IsAccessible("SYS_BHV_DRILLDOWN"))
                return;
            this.GoToDocument();
        }

        private void invoiceGrid1_DrawGroupPanelEvent(object sender, CustomDrawEventArgs e)
        {
            AutoCount.Invoicing.GridViewHelper.DrawGroupPanelString(sender, e, true, this.myUserSession);
        }

        private void SetModuleFeature(ModuleController controller)
        {
            this.gbAnalysisOptions.Visible = false;
            if (!this.gbAnalysisOptions.Visible)
                this.simpleButtonAdvanceSearch.Location = this.gbAnalysisOptions.Location;
            this.lblCurrency.Visible = true;
            this.ucCurrencySelector1.Visible = true;
            this.lblSalesLocation.Visible = true;
            this.ucLocationSelector1.Visible = true;
            this.lblArea.Location = new Point(this.lblArea.Location.X, this.CalcNewY(173));
            this.ucAreaSelector1.Location = new Point(this.ucAreaSelector1.Location.X, this.CalcNewY(170));
            this.lblSalesLocation.Location = new Point(this.lblSalesLocation.Location.X, this.CalcNewY(197));
            this.ucLocationSelector1.Location = new Point(this.ucLocationSelector1.Location.X, this.CalcNewY(194));
            this.lblCancelled.Location = new Point(this.lblCancelled.Location.X, this.CalcNewY(221));
            this.cbCancelledStatus.Location = new Point(this.cbCancelledStatus.Location.X, this.CalcNewY(218));
            Point location;
            if (!controller.MultiCurrency.Enable)
            {
                this.lblCurrency.Visible = false;
                this.ucCurrencySelector1.Visible = false;
                this.lblArea.Location = new Point(this.lblArea.Location.X, this.lblArea.Location.Y - this.lblCurrency.Height - this.CalcNewY(11));
                this.ucAreaSelector1.Location = new Point(this.ucAreaSelector1.Location.X, this.ucAreaSelector1.Location.Y - this.ucCurrencySelector1.Height - this.CalcNewY(4));
                Label lblSalesLocation = this.lblSalesLocation;
                location = this.lblSalesLocation.Location;
                int x1 = location.X;
                location = this.lblSalesLocation.Location;
                int y1 = location.Y - this.lblCurrency.Height - this.CalcNewY(11);
                Point point1 = new Point(x1, y1);
                lblSalesLocation.Location = point1;
                UCLocationSelector locationSelector1 = this.ucLocationSelector1;
                location = this.ucLocationSelector1.Location;
                int x2 = location.X;
                location = this.ucLocationSelector1.Location;
                int y2 = location.Y - this.ucCurrencySelector1.Height - this.CalcNewY(4);
                Point point2 = new Point(x2, y2);
                locationSelector1.Location = point2;
                Label lblCancelled = this.lblCancelled;
                location = this.lblCancelled.Location;
                int x3 = location.X;
                location = this.lblCancelled.Location;
                int y3 = location.Y - this.lblCurrency.Height - this.CalcNewY(11);
                Point point3 = new Point(x3, y3);
                lblCancelled.Location = point3;
                ComboBoxEdit cbCancelledStatus = this.cbCancelledStatus;
                location = this.cbCancelledStatus.Location;
                int x4 = location.X;
                location = this.cbCancelledStatus.Location;
                int y4 = location.Y - this.ucCurrencySelector1.Height - this.CalcNewY(4);
                Point point4 = new Point(x4, y4);
                cbCancelledStatus.Location = point4;
            }
            if (!controller.MultiLocation.Enable)
            {
                this.lblSalesLocation.Visible = false;
                this.ucLocationSelector1.Visible = false;
                Label lblCancelled = this.lblCancelled;
                location = this.lblCancelled.Location;
                int x1 = location.X;
                location = this.lblCancelled.Location;
                int y1 = location.Y - this.lblSalesLocation.Height - this.CalcNewY(11);
                Point point1 = new Point(x1, y1);
                lblCancelled.Location = point1;
                ComboBoxEdit cbCancelledStatus = this.cbCancelledStatus;
                location = this.cbCancelledStatus.Location;
                int x2 = location.X;
                location = this.cbCancelledStatus.Location;
                int y2 = location.Y - this.ucLocationSelector1.Height - this.CalcNewY(4);
                Point point2 = new Point(x2, y2);
                cbCancelledStatus.Location = point2;
            }
            GroupControl boxSearchCriteria = this.groupBox_SearchCriteria;
            location = this.cbCancelledStatus.Location;
            int num1 = location.Y + this.cbCancelledStatus.Height + this.CalcNewY(6);
            boxSearchCriteria.Height = num1;
            PanelControl panelCriteria = this.panelCriteria;
            location = this.groupBox_SearchCriteria.Location;
            int num2 = location.Y + this.groupBox_SearchCriteria.Height + this.CalcNewY(6);
            panelCriteria.Height = num2;
        }

        private int CalcNewX(int x)
        {
            return Converter.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Width * (double)x / 96.0));
        }

        private int CalcNewY(int y)
        {
            return Converter.ToInt32((object)Math.Round((double)this.AutoScaleDimensions.Height * (double)y / 96.0));
        }

        private void GoToDocument()
        {
            ColumnView focusedView = (ColumnView)this.invoiceGrid1.GridControl.FocusedView;
            DataRow dataRow = focusedView.GetDataRow(focusedView.FocusedRowHandle);
            if (dataRow == null)
                return;
            DocumentDispatcher.Open(this.myUserSession, "IV", Converter.ToInt64(dataRow["DocKey"]));
        }

        private void AddNewColumn()
        {
            this.myDataTable.Columns.Add(new DataColumn()
            {
                DataType = typeof(bool),
                AllowDBNull = true,
                Caption = AutoCount.Localization.Localizer.GetString((Enum)FormInvoicePrintListingStringId.Check),
                ColumnName = "ToBeUpdate",
                DefaultValue = (object)false
            });
        }

        private void ReloadAllColumns(object sender, EventArgs e)
        {
            this.myLoadAllColumns = true;
            this.BasicSearch(true);
        }

        public long[] GetSelectedDocKeys()
        {
            GridViewUtils.UpdateData(this.invoiceGrid1.GridView);
            long[] numArray = new long[0];
            if (this.invoiceGrid1.GridView.FocusedRowHandle >= 0 && this.myDataTable.Select("ToBeUpdate = True").Length != 0)
                numArray = DataTableHelper.GetSelectedKeyArray("ToBeUpdate", "DocKey", this.myDataTable);
            return numArray;
        }

        public string[] GetSelectedDocNos()
        {
            this.invoiceGrid1.EndEdit();
            List<string> stringList = new List<string>();
            foreach (DataRow row in (InternalDataCollectionBase)this.myDataTable.Rows)
            {
                if (row.RowState != DataRowState.Deleted && !row.IsNull("ToBeUpdate") && Convert.ToBoolean(row["ToBeUpdate"]))
                    stringList.Add(row["DocNo"].ToString());
            }
            return stringList.ToArray();
        }

        public virtual void RefreshDesignReport()
        {
            bool show = this.myUserSession.AccessRight.IsAccessible("TOOLS_RPT_SHOW");
            this.barBtnDesignDocumentStyleReport.Visibility = AutoCount.XtraUtils.XtraBars.ToBarItemVisibility(show);
            this.barBtnDesignListingStyleReport.Visibility = AutoCount.XtraUtils.XtraBars.ToBarItemVisibility(show);
            if (this.ucLocationSelector1 == null)
                return;
            this.myFilterByLocation = SystemOptionPolicy.GetOrCreate(this.myUserSession).GeneralSystemOptionPolicy.EnableFilterByCurrentUserLocationAccessRight;
            if (this.myFilterByLocation)
            {
                this.ucLocationSelector1.Filter.Type = FilterType.ByRange;
                this.ucLocationSelector1.Filter.From = (object)this.myUserSession.MainLocation;
                this.ucLocationSelector1.Filter.To = (object)this.myUserSession.MainLocation;
                this.ucLocationSelector1.ApplyFilter();
                this.ucLocationSelector1.Enabled = false;
            }
            else
                this.ucLocationSelector1.Enabled = true;
        }

        private void FormInvoicePrintListing_Activated(object sender, EventArgs e)
        {
            this.ucAreaSelector1?.RefreshLookupEditBuilder();
            this.ucCurrencySelector1?.RefreshLookupEditBuilder();
            this.ucDebtorTypeSelector1?.RefreshLookupEditBuilder();
            this.ucInvoiceSelector1?.RefreshLookupEditBuilder();
            this.ucLocationSelector1?.RefreshLookupEditBuilder();
            this.ucSalesAgentSelector1?.RefreshLookupEditBuilder();
        }

        public class FormEventArgs
        {
            private FormInvoicePrintListing myForm;

            public FormEventArgs(FormInvoicePrintListing form)
            {
                this.myForm = form;
            }

            public InvoiceListingReport Command
            {
                get
                {
                    return this.myForm.myCommand;
                }
            }

            public PanelControl PanelCriteria
            {
                get
                {
                    return this.myForm.panelCriteria;
                }
            }

            public PanelControl PanelButtons
            {
                get
                {
                    return this.myForm.panelButtons;
                }
            }

            public GridControl GridControl
            {
                get
                {
                    return this.myForm.invoiceGrid1.GridControl;
                }
            }

            public FormInvoicePrintListing Form
            {
                get
                {
                    return this.myForm;
                }
            }

            public UserSession UserSession
            {
                get
                {
                    return this.myForm.myUserSession;
                }
            }

            public DBSetting DBSetting
            {
                get
                {
                    return this.myForm.myDBSetting;
                }
            }
        }

        public class FormInitializeEventArgs : FormInvoicePrintListing.FormEventArgs
        {
            public FormInitializeEventArgs(FormInvoicePrintListing form)
              : base(form)
            {
            }
        }

        public class FormInquiryEventArgs : FormInvoicePrintListing.FormEventArgs
        {
            private DataTable myResultTable;

            public FormInquiryEventArgs(FormInvoicePrintListing form, DataTable resultTable)
              : base(form)
            {
                this.myResultTable = resultTable;
            }

            public DataTable ResultTable
            {
                get
                {
                    return this.myResultTable;
                }
            }
        }
    }
}
